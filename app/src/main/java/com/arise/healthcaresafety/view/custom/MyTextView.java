package com.arise.healthcaresafety.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.arise.healthcaresafety.R;


/**
 * Created by hnam on 6/17/2015.
 */
public class MyTextView extends TextView {
    public MyTextView(Context context) {
        super(context);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String fontName = a.getString(R.styleable.MyTextView_fontName);

            Typeface tf;
            if (fontName != null) {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(tf);
            } else {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "Roboto-Regular.ttf");

            }
            setTypeface(tf);
            a.recycle();
        }
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }
}
