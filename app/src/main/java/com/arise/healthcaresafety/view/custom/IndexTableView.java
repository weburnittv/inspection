package com.arise.healthcaresafety.view.custom;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.utils.Utils;

/**
 * ********************************************************** File:
 * IndexTableView.java
 * <p/>
 * This class will draw index table in leader board screen
 * <p/>
 * Notes:<br>
 *
 * @day August 04 2015, VULT. <br>
 * ***********************************************************
 */
public class IndexTableView {
    public static final int NAME_TYPE = 0;
    public static final int NAME_TITLE_TYPE = 1;
    public static final int TABLE_TITLE_TYPE = 2;
    public static final int INDEX_TYPE = 3;
    public static final int TABLE_HEADER_TYPE = 4;
    public static final int TABLE_ITEM_TYPE = 5;
    public static final int PREVIEW_HEADER = 6;
    public static final int TABLE_CATEGORY_TYPE = 7;
    public static final int NOTE_TITLE_TYPE = 8;
    private int mWidth;
    private int mHeight;
    private String mTitle, mCamara, mAudio = "";
    private int mType;
    private Context mContext;
    private String mMediaPath;

    public IndexTableView(Context context, int width, int height, String title, int type) {
        this.mWidth = width;
        this.mHeight = height;
        this.mTitle = title;
        this.mType = type;
        this.mContext = context;
    }

    public IndexTableView(Context context, int width, int height, String camera, String audio, int type) {
        this.mWidth = width;
        this.mHeight = height;
        this.mCamara = camera;
        this.mAudio = audio;
        this.mType = type;
        this.mContext = context;
    }

    public IndexTableView(Context context, int width, int height, String path) {
        this.mWidth = width;
        this.mHeight = height;
        this.mMediaPath = path;
        this.mContext = context;
    }

    public View createTitleView(int background) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        RelativeLayout scoreView = new RelativeLayout(mContext);
        scoreView.setGravity(Gravity.CENTER);
        TextView mTvTitle = new TextView(mContext);
        mTvTitle.setText(mTitle);
        scoreView.addView(mTvTitle);
        RelativeLayout.LayoutParams left = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        left.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        left.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        left.setMargins(0, 0, 0, 0);
        mTvTitle.setLayoutParams(left);
        linearLayout.setBackgroundResource(background);
        LayoutParams params = new LayoutParams(mWidth, mHeight);
        params.topMargin = 40;
        params.bottomMargin = 20;
        linearLayout.setLayoutParams(params);
        linearLayout.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        initialViews(mTvTitle);
        linearLayout.addView(scoreView);
        return linearLayout;
    }

    public View createNoteView(int background) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        RelativeLayout scoreView = new RelativeLayout(mContext);
        scoreView.setGravity(Gravity.CENTER);

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_note_view, null);
        TextView tvCamara = (TextView) view.findViewById(R.id.item_note_view_tv_camera);
        TextView tvAudio = (TextView) view.findViewById(R.id.item_note_view_tv_audio);
        tvCamara.setText(mCamara);
        tvAudio.setText(mAudio);

        scoreView.addView(view);
        linearLayout.setBackgroundResource(background);
        LayoutParams params = new LayoutParams(mWidth, mHeight);
        linearLayout.setLayoutParams(params);
        linearLayout.setGravity(Gravity.CENTER);

        tvCamara.setTextColor(Color.BLACK);
        tvCamara.setTextAppearance(mContext, R.style.preview_table_item);

        tvAudio.setTextColor(Color.BLACK);
        tvAudio.setTextAppearance(mContext, R.style.preview_table_item);

        linearLayout.addView(scoreView);
        return linearLayout;
    }


    public View createView(int background) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        RelativeLayout scoreView = new RelativeLayout(mContext);
        scoreView.setGravity(Gravity.CENTER);
        TextView mTvTitle = new TextView(mContext);
        mTvTitle.setText(mTitle);
        scoreView.addView(mTvTitle);
        RelativeLayout.LayoutParams left = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        left.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        left.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        left.setMargins(5, 0, 0, 5);
        mTvTitle.setLayoutParams(left);
        linearLayout.setBackgroundResource(background);
        LayoutParams params = new LayoutParams(mWidth, mHeight);
        linearLayout.setLayoutParams(params);
        linearLayout.setGravity(Gravity.CENTER);
        initialViews(mTvTitle);
        linearLayout.addView(scoreView);
        return linearLayout;
    }

    public View createView(int background, boolean isPreview) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        RelativeLayout scoreView = new RelativeLayout(mContext);
        scoreView.setGravity(Gravity.CENTER);
        TextView tvTitle = new TextView(mContext);
        tvTitle.setText(mTitle);
        scoreView.addView(tvTitle);
        RelativeLayout.LayoutParams left = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        left.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        left.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        left.setMargins(10, 0, 0, 10);
        tvTitle.setLayoutParams(left);
        linearLayout.setBackgroundResource(background);
        LayoutParams params = new LayoutParams(mWidth, mHeight);
        linearLayout.setLayoutParams(params);
        linearLayout.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        initialViews(tvTitle);
        linearLayout.addView(scoreView);
        return linearLayout;
    }


    public View createNotePhotoView(int background, boolean isPhoto) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        RelativeLayout scoreView = new RelativeLayout(mContext);
        scoreView.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        if (isPhoto) {
            imageView.setImageBitmap(Utils.getBitmapFromFile(mMediaPath));
        } else {
            imageView.setBackgroundResource(R.drawable.ic_default_audio);
        }
        LayoutParams ivParams = new LayoutParams(mWidth, mHeight);
        ivParams.width = 180;
        ivParams.height = 180;
        imageView.setLayoutParams(ivParams);

        scoreView.addView(imageView);
        linearLayout.setBackgroundResource(background);
        LayoutParams params = new LayoutParams(mWidth, mHeight);
        linearLayout.setLayoutParams(params);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.addView(scoreView);
        return linearLayout;
    }

    public View createQuestionView(int background) {
        LinearLayout linearLayout = new LinearLayout(mContext);
        RelativeLayout scoreView = new RelativeLayout(mContext);
        scoreView.setGravity(Gravity.CENTER);
        TextView tvTitle = new TextView(mContext);
        tvTitle.setText(mTitle);
        scoreView.addView(tvTitle);
        RelativeLayout.LayoutParams left = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        left.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        left.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        left.setMargins(10, 0, 0, 10);
        tvTitle.setLayoutParams(left);
        tvTitle.setSingleLine(false);
        tvTitle.setEllipsize(TextUtils.TruncateAt.END);
        linearLayout.setBackgroundResource(background);
        LayoutParams params = new LayoutParams(mWidth, mHeight);
        linearLayout.setLayoutParams(params);
        linearLayout.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        initialViews(tvTitle);
        linearLayout.addView(scoreView);
        return linearLayout;
    }

    /**
     * initialViews : initial views
     */
    private void initialViews(TextView tvTitle) {
        switch (mType) {
            case NAME_TYPE:
                tvTitle.setTextColor(mContext.getResources().getColor(R.color.black_text));
                tvTitle.setTextAppearance(mContext, R.style.leaderboard_text_normal);
                break;
            case NAME_TITLE_TYPE:
                tvTitle.setTextColor(mContext.getResources().getColor(R.color.black_text));
                tvTitle.setTextSize(16);
                tvTitle.setGravity(Gravity.LEFT);
                // mTvTitle.setTextAppearance(mContext, R.style.leaderboard_text_normal);
                break;
            case TABLE_TITLE_TYPE:
                tvTitle.setTextColor(Color.WHITE);
                tvTitle.setTextAppearance(mContext, R.style.leaderboard_text_title);
                break;
            case TABLE_HEADER_TYPE:
                tvTitle.setTextColor(Color.BLACK);
                tvTitle.setTextAppearance(mContext, R.style.preview_table_header);
                break;
            case TABLE_ITEM_TYPE:
                tvTitle.setTextColor(Color.BLACK);
                tvTitle.setTextAppearance(mContext, R.style.preview_table_item);
                break;
            case TABLE_CATEGORY_TYPE:
                //tvTitle.setTextColor(Color.BLACK);
                tvTitle.setTextAppearance(mContext, R.style.preview_table_category);
                break;
            case INDEX_TYPE:
                tvTitle.setTextColor(Color.WHITE);
                break;
            case PREVIEW_HEADER:
                tvTitle.setTextColor(mContext.getResources().getColor(R.color.black_text));
                tvTitle.setTextAppearance(mContext, R.style.preview_header);
                break;
            case NOTE_TITLE_TYPE:
                tvTitle.setTextAppearance(mContext, R.style.note_title);
                break;
            default:
                tvTitle.setBackgroundColor(Color.WHITE);
                break;
        }
    }
}
