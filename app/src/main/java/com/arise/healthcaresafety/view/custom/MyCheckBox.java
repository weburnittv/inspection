package com.arise.healthcaresafety.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import com.arise.healthcaresafety.R;


/**
 * Created by hnam on 6/17/2015.
 */
public class MyCheckBox extends AppCompatCheckBox {
    public MyCheckBox(Context context) {
        super(context);
        init();
    }

    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String fontName = a.getString(R.styleable.MyTextView_fontName);

            Typeface tf;
            if (fontName != null) {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(tf);
            } else {
                tf = Typeface.createFromAsset(getContext().getAssets(),
                        "fonts/" + getResources().getString(R.string.roboto_light));

            }
            setTypeface(tf);
            setTextColor(getResources().getColor(R.color.black_text));
            //setButtonTintList(getResources().getColorStateList(R.color.radio_color));
            a.recycle();
        }
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/" + getResources().getString(R.string.roboto_light));
        setTypeface(tf);
        setTextColor(getResources().getColor(R.color.black_text));
    }


}
