package com.arise.healthcaresafety.view.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.NewsCaseDetailActivity;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.eventBus.BusProvider;
import com.arise.healthcaresafety.eventBus.model.PostNumberCamera;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.RequestCode;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.ITabScreenListener;
import com.arise.healthcaresafety.view.IUpdateResultNoteListener;
import com.arise.healthcaresafety.view.adapter.EquipmentListAdapter;
import com.arise.healthcaresafety.view.custom.MyButtonView;
import com.arise.healthcaresafety.view.custom.equipment.EquipmentView;
import com.squareup.otto.Subscribe;


public class EquipmentScreenFragment extends Fragment implements ITabScreenListener, IUpdateResultNoteListener {
    private EquipmentListAdapter equipAdapter;
    private Equipments equipments;
    private NewsCaseDetailActivity mParent;
    /* handle event of item group expand list */
    /* listener whenever group open */
    //private int mGroupPosition = -1;
    ExpandableListView.OnGroupExpandListener mExpandListener = new ExpandableListView.OnGroupExpandListener() {
        @Override
        public void onGroupExpand(int groupPosition) {
            Utils.getInstance().clearFocusIME(mParent);
            //mGroupPosition = groupPosition;
            equipAdapter.invalidateIndicatorIcon(true, groupPosition);
        }
    };
    /* listener whenever group close */
    ExpandableListView.OnGroupCollapseListener mCollapseListener = new ExpandableListView.OnGroupCollapseListener() {
        @Override
        public void onGroupCollapse(int groupPosition) {
            Utils.getInstance().clearFocusIME(mParent);
            equipAdapter.invalidateIndicatorIcon(false, groupPosition);
        }
    };
    ExpandableListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            Utils.getInstance().clearFocusIME(mParent);
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };
    View.OnClickListener onBtnSaveClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mParent.scrollTabById(2);
            mParent.saveCase();
        }
    };
    private ExpandableListView equipList;
    private int mTam;

    public EquipmentScreenFragment() {
    }

    public static EquipmentScreenFragment newInstance() {
        return new EquipmentScreenFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = (NewsCaseDetailActivity) getActivity();
        mParent.setEquipmentScreenListener(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_equipment_screen, container, false);
        MyButtonView btnSave = (MyButtonView) view.findViewById(R.id.fragEquipment_btn_save_case);
        btnSave.setOnClickListener(onBtnSaveClick);
        equipList = (ExpandableListView) view.findViewById(R.id.equipScreen_expandableListView);

        equipAdapter = new EquipmentListAdapter(getActivity(), this.mParent.loadEquipments(), mTam);
        equipAdapter.setOnEquipItemListener(this);
        equipList.setAdapter(equipAdapter);
        equipList.setOnGroupExpandListener(mExpandListener);
        equipList.setOnGroupCollapseListener(mCollapseListener);
        equipList.setOnScrollListener(mOnScrollListener);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        BusProvider.getInstance().register(this);
        EquipmentView.isCheckState = false;
        super.onResume();
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(PostNumberCamera event) {
        int count = 0;
        mTam = count + 1;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent.setEquipmentScreenListener(null);
        mParent = null;
    }

    @Override
    public void bindingNote(Note note) {
        for (Equipment equipment : this.equipments.getEquipments()) {
            if (note.getEquipment().getLiveId() == equipment.getLiveId()) {
                equipment.setNote(note);
                equipAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void updateCase(Case mCase) {
        this.equipments = mCase.getEquipments();
        equipAdapter = new EquipmentListAdapter(getActivity(), this.equipments, mTam);
        equipAdapter.setOnEquipItemListener(this);
        equipList.setAdapter(equipAdapter);
        equipAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSendEmailStatus(boolean status) {
        //TODO do Nothing
    }

    /* listener from adapter when click chose equipment */
    @Override
    public void OnUpdateEquipment(Equipment equipment) {
        if (mParent.getPresenter() != null) {
            mParent.getPresenter().updateEquipment(equipment);
        }
    }

    @Override
    public void OnUpdateNoteContent(Note note) {
        if (mParent != null) {
            mParent.onUpdateNoteContent(note);
        }
    }


    @Override
    public void OpenPicture(Note note) {
        if (mParent != null) {
            String mediaPath = FileManager.getInstance().getCaseFolder(mParent.getCase(), note);
            mParent.openPhotoScreen(note, mediaPath, InspConstant.REQUEST_CODE_EQUIP_OPEN_PHOTO);
        }
    }

    @Override
    public void OpenAudio(Note note) {
        if (mParent != null) {
            String mediaPath = FileManager.getInstance().getCaseFolder(mParent.getCase(), note);
            mParent.openAudioScreen(note, mediaPath, InspConstant.REQUEST_CODE_EQUIP_OPEN_AUDIO);
        }
    }

}
