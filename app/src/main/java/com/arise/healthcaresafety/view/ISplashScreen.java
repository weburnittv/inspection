package com.arise.healthcaresafety.view;

/**
 * Created by Nam on 6/25/2015.
 */
public interface ISplashScreen {
    public void onLoginSuccess();

    public void onLoginFailed(String msg);

}
