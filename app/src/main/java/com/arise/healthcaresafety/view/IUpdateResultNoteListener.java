package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Note;


public interface IUpdateResultNoteListener {
    void OnUpdateEquipment(Equipment equipment);

    void OnUpdateNoteContent(Note note);

    void OpenPicture(Note note);

    void OpenAudio(Note note);
}
