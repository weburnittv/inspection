package com.arise.healthcaresafety.view.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.SplashActivity;
import com.arise.healthcaresafety.handler.implementation.api.sync.VerifyClientDelegatorHandler;
import com.arise.healthcaresafety.handler.implementation.api.sync.VerifyClientTask;
import com.arise.healthcaresafety.model.entities.Client;
import com.arise.healthcaresafety.utils.SharedPreference;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.ChangeServerDialog;

import java.util.HashMap;
import java.util.Map;


public class LoginScreenFragment extends Fragment implements View.OnClickListener, VerifyClientDelegatorHandler {
    private EditText mEtUsername;
    private EditText mEtPassword;
    private SplashActivity mParent;
    private TextView.OnEditorActionListener mOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                Utils.getInstance().showOrHideKeyBoard(getActivity(), false, null);
                onClickLogin();
            }
            return false;
        }
    };


    public LoginScreenFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mEtUsername = (EditText) view.findViewById(R.id.login_et_username);
        mEtPassword = (EditText) view.findViewById(R.id.login_et_password);
        mEtUsername.requestFocus();
        view.findViewById(R.id.login_bt_signin).setOnClickListener(this);
        mEtPassword.setOnEditorActionListener(mOnEditorActionListener);

        view.findViewById(R.id.login_tv_change_server).setOnClickListener(this);
        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (SplashActivity) activity;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_bt_signin:
                onClickLogin();
                break;
            case R.id.login_tv_change_server:
                changeServer();
                break;
            default:
                break;
        }
    }

    private void changeServer() {
        ChangeServerDialog changeServerDialog = new ChangeServerDialog(getActivity(), new ChangeServerDialog.ProcessChangeServer() {
            @Override
            public void onChanged(String host, String email) {
                callVerifyClient(host, email);
            }
        });

        changeServerDialog.show();
    }

    private void callVerifyClient(String host, String email) {
        Map<String, String> data = new HashMap<>();
        data.put("host", host);
        data.put("email", email);

        VerifyClientTask verifyClientTask = new VerifyClientTask(getActivity(), this, data);
        verifyClientTask.run();
    }

    @Override
    public void onResponse(Client client) {
        SharedPreference.setClientHost(getActivity(), client.getHost());
        SharedPreference.setClientEmail(getActivity(), client.getEmail());
        SharedPreference.setClientExpire(getActivity(), client.getExpire());
        SharedPreference.setClientLogo(getActivity(), client.getLogo());
    }

    @Override
    public void onError() {

    }

    private boolean isValidLogin(String user, String password) {
        if (user.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.login_input_username_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void onClickLogin() {
        if (!Utils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.notice_network_not_available), Toast.LENGTH_SHORT).show();
            return;
        }
        String username = mEtUsername.getText().toString().trim();
        String password = mEtPassword.getText().toString().trim();
        if (isValidLogin(username, password)) {
            mParent.handleLogin(username
                    , password);
        }
    }
}
