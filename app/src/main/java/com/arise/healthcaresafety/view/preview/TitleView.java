package com.arise.healthcaresafety.view.preview;


import android.content.Context;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.view.custom.IndexTableView;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;

public class TitleView extends AbsPreviewView {

    private String mTitle;

    public TitleView(Context context, int screenWidth, String title) {
        super(context, screenWidth);
        this.mTitle = title;
    }

    @Override
    public void buildView() {
        this.linearLayout = this.getTitle(mTitle);
    }

    private LinearLayout getTitle(String title) {
        LinearLayout titleView = new LinearLayout(mContext);
        titleView.setOrientation(LinearLayout.VERTICAL);
        titleView.addView(new IndexTableView(mContext, mScreenWidth,
                Constants.HEIGHT_ITEM_TABLE, title,
                IndexTableView.PREVIEW_HEADER).createTitleView(R.drawable.bg_dot_line));

        return titleView;
    }
}
