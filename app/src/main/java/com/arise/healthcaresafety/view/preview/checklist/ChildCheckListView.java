package com.arise.healthcaresafety.view.preview.checklist;


import android.content.Context;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Attribute;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Group;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.IndexTableView;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;

public class ChildCheckListView extends AbsPreviewView {

    private int mWidthChecklistColumn1, mWidthChecklistColumn2;
    private CheckList mCheckList;

    public ChildCheckListView(Context context, int screenWidth, CheckList checkList) {
        super(context, screenWidth);
        this.mCheckList = checkList;
        this.mWidthChecklistColumn1 = (screenWidth / 10) * 6;
        this.mWidthChecklistColumn2 = (screenWidth / 10) * 4;
    }

    @Override
    public void buildView() {
        this.linearLayout = this.getChildCaseEquipment();
    }

    private LinearLayout getChildCaseEquipment() {
        LinearLayout childItem = new LinearLayout(mContext);
        childItem.setOrientation(LinearLayout.VERTICAL);
        for (Group group : mCheckList.getGroups()) {
            childItem.addView(this.getGroupTitle(group));
            childItem.addView(this.getHeaderCaseChecklist());
            childItem.addView(this.getGroupItem(group));
        }

        return childItem;
    }

    private LinearLayout getGroupTitle(Group group) {
        LinearLayout rowItem = new LinearLayout(mContext);
        rowItem.setOrientation(LinearLayout.VERTICAL);
        String name = group.getName();
        rowItem.addView(new IndexTableView(mContext, mScreenWidth,
                Constants.HEIGHT_ITEM_TABLE, name,
                IndexTableView.TABLE_CATEGORY_TYPE).createView(R.drawable.bg_preview_category, true));

        return rowItem;
    }

    private LinearLayout getHeaderCaseChecklist() {
        LinearLayout tableHeader = new LinearLayout(mContext);
        tableHeader.setOrientation(LinearLayout.HORIZONTAL);
        int headerHeight = 70;
        tableHeader.addView(new IndexTableView(mContext, this.mWidthChecklistColumn1,
                headerHeight, "Questions",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        tableHeader.addView(new IndexTableView(mContext, this.mWidthChecklistColumn2,
                headerHeight, "Answer",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        return tableHeader;
    }

    private LinearLayout getGroupItem(Group group) {
        LinearLayout childItem = new LinearLayout(mContext);
        childItem.setOrientation(LinearLayout.VERTICAL);
        for (Question question : group.getQuestions()) {
            LinearLayout columnItem = new LinearLayout(mContext);
            columnItem.setOrientation(LinearLayout.HORIZONTAL);

            String content = question.getContent();
            String answer = this.getCheckListAnswer(question);

            int contentHeight = Utils.getTextContentHeight(mContext, content, 14, mWidthChecklistColumn1, 5);
            int answerHeight = Utils.getTextContentHeight(mContext, answer, 14, mWidthChecklistColumn1, 5);

            int itemHeight = contentHeight >= answerHeight ? contentHeight : answerHeight;
            int height = itemHeight >= Constants.HEIGHT_ITEM_TABLE ? itemHeight : Constants.HEIGHT_ITEM_TABLE;

            columnItem.addView(new IndexTableView(mContext, this.mWidthChecklistColumn1,
                    height, content,
                    IndexTableView.TABLE_ITEM_TYPE).createQuestionView(R.drawable.bg_preview_item));
            columnItem.addView(new IndexTableView(mContext, this.mWidthChecklistColumn2,
                    height, answer,
                    IndexTableView.TABLE_ITEM_TYPE).createQuestionView(R.drawable.bg_preview_item));
            childItem.addView(columnItem);

            Note note = question.getNote();
            if (this.isExistingNote(note))
                childItem.addView(this.getNoteView(note));
        }

        return childItem;
    }

    private String getCheckListAnswer(Question question) {
        StringBuilder stringBuilder = new StringBuilder();
        if (question.getVariation() == null) {
            return "";
        }
        for (Attribute attribute : question.getVariation().getAttributes()) {
            stringBuilder.append(attribute.getName());
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }


    private boolean isExistingNote(Note note) {
        return note.getNotePhotoMedias().size() != 0
                || note.getNoteSoundMedias().size() != 0
                || note.getContent().length() != 0;
    }

    private LinearLayout getNoteView(Note note) {
        LinearLayout childItem = new LinearLayout(mContext);
        childItem.setOrientation(LinearLayout.VERTICAL);

        int headerHeight = 60;
        childItem.addView(new IndexTableView(mContext, mScreenWidth,
                headerHeight, "Note Results",
                IndexTableView.NOTE_TITLE_TYPE).createView(R.drawable.bg_common));

        LinearLayout headerColumn = new LinearLayout(mContext);
        headerColumn.setOrientation(LinearLayout.HORIZONTAL);

        headerColumn.addView(new IndexTableView(mContext, 200,
                45, "Media",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        headerColumn.addView(new IndexTableView(mContext, mScreenWidth - 200,
                45, "Notes Text",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        childItem.addView(headerColumn);

        this.getPhotoMediaView(childItem, note);
        this.getSoundMediaView(childItem, note);

        return childItem;
    }

    private void getPhotoMediaView(LinearLayout parentView, Note note) {
        boolean isSetFistTime = false;
        for (Media media : note.getNotePhotoMedias()) {
            LinearLayout noteColumn = new LinearLayout(mContext);
            noteColumn.setOrientation(LinearLayout.HORIZONTAL);

            noteColumn.addView(new IndexTableView(mContext, 200,
                    200, media.getLocalPath()).createNotePhotoView(R.drawable.bg_preview_item, true));

            String content = "";
            if (!isSetFistTime)
                content = note.getContent();

            noteColumn.addView(new IndexTableView(mContext, mScreenWidth - 200,
                    200, content,
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));
            isSetFistTime = true;

            parentView.addView(noteColumn);
        }
    }

    private void getSoundMediaView(LinearLayout parentView, Note note) {
        for (Media media : note.getNoteSoundMedias()) {
            LinearLayout noteColumn = new LinearLayout(mContext);
            noteColumn.setOrientation(LinearLayout.HORIZONTAL);

            noteColumn.addView(new IndexTableView(mContext, 200,
                    200, media.getLocalPath()).createNotePhotoView(R.drawable.bg_preview_item, false));
            noteColumn.addView(new IndexTableView(mContext, mScreenWidth - 200,
                    200, note.getContent(),
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));
            parentView.addView(noteColumn);
        }
    }

}
