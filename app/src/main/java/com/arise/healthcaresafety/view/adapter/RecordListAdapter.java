package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.services.AudioRecordService;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class RecordListAdapter extends RecyclerView.Adapter<RecordListAdapter.RecordView> {
    private List<Media> mItems = new ArrayList<>();
    private RecordListListener mListener = null;
    View.OnClickListener onDeleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();
            Media item = mItems.get(position);
            if (RecordListAdapter.this.mListener != null) {
                if (position < mItems.size()) {
                    mItems.remove(position);
                    notifyDataSetChanged();
                }
                RecordListAdapter.this.mListener.deleteMedia(item);
            }
        }
    };
    private Context mContext;
    private AudioRecordService service;

    View.OnClickListener onPlayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();
            Media item = mItems.get(position);
            if (RecordListAdapter.this.service != null) {
                RecordListAdapter.this.service.startStopMyPlay(item);
                RecordListAdapter.this.notifyDataSetChanged();
            }
        }
    };

    public RecordListAdapter(Context context, List<Media> list) {
        this.mContext = context;
        this.mItems = list;
    }

    public void addMedia(Media media) {
        this.mItems.add(media);
        notifyDataSetChanged();
    }

    /**
     * when device is online get item which have liveId > 0. otherwise get all item
     *
     * @return
     */
    public ArrayList<Media> getUploadedMedia() {
        if (this.mItems == null) {
            return null;
        }

        ArrayList<Media> list = new ArrayList<>();
        if (Delegation.getInstance().isOnline()) {
            for (Media m : this.mItems) {
                if (m.getLiveId() > 0) {
                    list.add(m);
                }
            }
        } else {
            for (Media m : this.mItems) {
                list.add(m);
            }
        }
        return list;
    }

    public void setAudioService(AudioRecordService service) {
        this.service = service;
    }

    @Override
    public RecordView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_record_item, parent, false);
        return new RecordView(view);
    }

    @Override
    public void onBindViewHolder(RecordView holder, int position) {
        //get item from position & set event
        Media item = this.mItems.get(position);
        File file = new File(item.getLocalPath());
        if (file.exists()) {
            holder.mTvTitle.setText(FileManager.getInstance().getFileName(item.getLocalPath()));
            holder.mTvDescription.setText(FileManager.getInstance().getLastModifier(item.getLocalPath()));
            holder.mTvDuration.setText(FileManager.getInstance().getDurationOfAudio(item.getLocalPath()));
            holder.mLlRow.setTag(position);
            holder.mBtbPlay.setTag(position);
            holder.mBtDelete.setTag(position);
            //init ui
            if (this.service.isLocalPlay(item)) {
                holder.mBtbPlay.setBackgroundDrawable(ResourcesCompat.getDrawable(mContext.getResources(),
                        R.drawable.ic_pause, null));
            } else {
                holder.mBtbPlay.setBackgroundDrawable(ResourcesCompat.getDrawable(mContext.getResources(),
                        R.drawable.ic_play, null));
            }
            holder.mBtbPlay.setOnClickListener(onPlayListener);
            holder.mBtDelete.setOnClickListener(onDeleteListener);
        } else {
            if (position < mItems.size()) {
                mItems.remove(position);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (this.mItems == null) {
            return 0;
        }
        return this.mItems.size();
    }

    public void setRecordListListener(RecordListListener listener) {
        this.mListener = listener;
    }

    public interface RecordListListener {
        void deleteMedia(Media item);
    }

    public static class RecordView extends RecyclerView.ViewHolder {
        private MyTextView mTvTitle;
        private MyTextView mTvDescription;
        private MyTextView mTvDuration;
        private Button mBtbPlay;
        private Button mBtDelete;
        private LinearLayout mLlRow;

        public RecordView(View itemView) {
            super(itemView);
            this.mLlRow = (LinearLayout) itemView.findViewById(R.id.viewRecordItem_row);
            this.mTvTitle = (MyTextView) itemView.findViewById(R.id.viewRecordItem_title);
            this.mTvDescription = (MyTextView) itemView.findViewById(R.id.viewRecordItem_description);
            this.mTvDuration = (MyTextView) itemView.findViewById(R.id.viewRecordItem_duration);
            this.mBtbPlay = (Button) itemView.findViewById(R.id.viewRecordItem_btn_play);
            this.mBtDelete = (Button) itemView.findViewById(R.id.viewRecordItem_btn_delete);
        }
    }
}
