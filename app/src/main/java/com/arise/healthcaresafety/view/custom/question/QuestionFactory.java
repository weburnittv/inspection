package com.arise.healthcaresafety.view.custom.question;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Variation;


public class QuestionFactory {
    //use getShape method to get object of type shape
    public IQuestion getQuestionType(Context context, Question question, int viewMode) {
        Variation variation = question.getVariation();
        IQuestion questionView;
        if (variation == null) {
            //if variation is null. this is text entry
            questionView = new QuestionTextEntry(context, viewMode);
            questionView.setQuestion(question);
            return questionView;
        }

        //if variation != null, this is select choices
        int type = variation.getType();
        if (type == -1) {
            return null;
        }
        if (Question.SINGLE == type) {
            //return view
            questionView = new QuestionRadioGroup(context, viewMode);
            questionView.setQuestion(question);
            return questionView;
        } else if (Question.MULTI == type) {
            //return view
            questionView = new QuestionRadioGroup(context, viewMode);
            questionView.setQuestion(question);
            return questionView;
        }

        return null;
    }
}
