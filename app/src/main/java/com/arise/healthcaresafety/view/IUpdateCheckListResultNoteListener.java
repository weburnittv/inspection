package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.Note;


public interface IUpdateCheckListResultNoteListener {
    void OnUpdateNoteContent(Note note);

    void OnOpenPicture(Note note);

    void OnOpenAudio(Note note);
}
