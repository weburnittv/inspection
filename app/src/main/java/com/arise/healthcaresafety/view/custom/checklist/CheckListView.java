package com.arise.healthcaresafety.view.custom.checklist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.view.custom.MyTextView;
import com.arise.healthcaresafety.view.custom.question.IQuestion;
import com.arise.healthcaresafety.view.custom.question.QuestionFactory;


public class CheckListView extends LinearLayout implements NoteView.OnQuestionNoteViewListener, NoteView.OnSaveNoteListener {
    private static final String TAG = CheckListView.class.getSimpleName();
    IQuestion questionView;
    NoteView noteView;
    OnChecklistViewListener listener = null;
    private TextView mTxtIndex;
    private MyTextView mTxtTitle;
    private ToggleButton mToggleEdit;
    private int viewMode;
    private int groupId;
    private int childId;
    private View.OnClickListener onToggleEditCheckList = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            ToggleButton tb = (ToggleButton) v;
            noteView.setVisibility(tb.isChecked() ? View.VISIBLE : View.GONE);
        }
    };

    public CheckListView(Context context) {
        super(context);
        initView(context);
    }

    public CheckListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_checklist_item, this, true);
        mTxtIndex = (TextView) view.findViewById(R.id.checkListItem_index);
        mTxtTitle = (MyTextView) view.findViewById(R.id.checkListItem_title);
        mToggleEdit = (ToggleButton) view.findViewById(R.id.checkListItem_toggle_openEdit);
        initEvent();
    }

    /**
     * set up Attribute view
     *
     * @param groupIndex
     * @param childIndex
     * @param question
     */
    public void setUpView(int groupIndex, int childIndex, Question question, int viewMode) {
        this.groupId = groupIndex;
        this.childId = childIndex;
        this.viewMode = viewMode;
        String index;
        if ((childIndex + 1) < 10) {
            index = "0" + String.valueOf(childIndex + 1);
        } else {
            index = String.valueOf(childIndex + 1);
        }
        mTxtIndex.setText(index);
        mTxtTitle.setText(question.getContent());
        QuestionFactory factory = new QuestionFactory();
        questionView = factory.getQuestionType(getContext(), question, viewMode);
        addView(questionView.getQuestionView());
        noteView = new NoteView(getContext(), question, this);
        noteView.updateUI(question);
        noteView.setVisibility(GONE);
        noteView.setOnQuestionNoteViewListener(this);
        addView(noteView);
    }

    @Override
    public void onSaveNoteContent(Question question) {
        this.updateNoteView(question);
    }


    public void updateNoteView(Question question) {
        if (noteView != null) {
            noteView.updateUI(question);
        }
    }

    private void initNoteView() {

    }

    /* handler event */
    private void initEvent() {
        mToggleEdit.setOnClickListener(onToggleEditCheckList);
    }

    /* note view listener */
    @Override
    public void onCameraClick() {
        if (listener != null) {
            listener.onCameraClick(groupId, childId);
        }
    }

    @Override
    public void onRecordClick() {
        if (listener != null) {
            listener.onRecordClick(groupId, childId);
        }
    }


/*    public interface OnCheckListViewListener{
        public void onCheckBoxListener();
    }*/

    @Override
    public void onSaveNote(String key) {
        if (listener != null) {
            listener.onSaveNoteContent(groupId, childId, key);
        }
    }

    /**
     * set on ChecklistView listener
     *
     * @param listener
     */
    public void setOnCheckListViewListener(OnChecklistViewListener listener) {
        this.listener = listener;
        try {
            this.questionView.setOnCheckListViewListener(this.listener);
        } catch (Exception ex) {
        }
    }

    public interface OnChecklistViewListener {
        void onUpdateQuestion(Question question);

        void onCameraClick(int groupId, int childId);

        void onRecordClick(int groupId, int childId);

        void onSaveNoteContent(int groupId, int childId, String note);
    }

}
