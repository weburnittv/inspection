package com.arise.healthcaresafety.view;

/**
 * Created by Nam on 6/26/2015.
 */
public interface IMenuActivity {
    /**
     * logout callback
     *
     * @param status: status: true - success, false - falied
     */
    public void onLogoutStatus(boolean status);
}
