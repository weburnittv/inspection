package com.arise.healthcaresafety.view.custom;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.utils.EmailValidator;
import com.arise.healthcaresafety.utils.Utils;

public class ChangeServerDialog {
    private Activity activity;
    private ProcessChangeServer mListener;

    public ChangeServerDialog(Activity activity, ProcessChangeServer listener) {
        this.activity = activity;
        this.mListener = listener;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.MyAlertDialogStyle);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_change_server, null);
        final EditText etHost = (EditText) view.findViewById(R.id.change_server_et_serverId);
        final EditText etEmail = (EditText) view.findViewById(R.id.change_server_et_email);
        builder.setView(view)
                .setTitle("Server ID:")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String serverId = etHost.getText().toString().trim();
                        String email = etEmail.getText().toString().trim();
                        if (TextUtils.isEmpty(serverId)) {
                            Utils.getInstance().showMessage(activity, "Please input Server ID!");
                            return;
                        }

                        if (TextUtils.isEmpty(email)) {
                            Utils.getInstance().showMessage(activity, "Please input Email!");
                            return;
                        }

                        EmailValidator check = new EmailValidator();
                        if (!check.validate(email)) {
                            Utils.getInstance().showMessage(activity, email + "is not email format");
                            return;
                        }

                        if (ChangeServerDialog.this.mListener != null) {
                            ChangeServerDialog.this.mListener.onChanged(serverId, email);
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public interface ProcessChangeServer {
        void onChanged(String serverId, String email);
    }
}
