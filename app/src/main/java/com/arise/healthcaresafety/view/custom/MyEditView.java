package com.arise.healthcaresafety.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.arise.healthcaresafety.R;


/**
 * Created by hnam on 6/17/2015.
 */
public class MyEditView extends EditText {
    public MyEditView(Context context) {
        super(context);
        init(null);
    }

    public MyEditView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyEditView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String fontName = a.getString(R.styleable.MyTextView_fontName);

            Typeface tf;
            if (fontName != null) {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(tf);
            } else {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "Roboto-Regular.ttf");

            }
            setTypeface(tf);
            setPadding(getResources().getDimensionPixelSize(R.dimen.element_margin_4)
                    , getResources().getDimensionPixelSize(R.dimen.element_margin_4)
                    , getResources().getDimensionPixelSize(R.dimen.element_margin_4)
                    , getResources().getDimensionPixelSize(R.dimen.element_margin_4));
            a.recycle();
        }
    }

    public void setFont(String fontName) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
        setTypeface(tf);
    }


}
