package com.arise.healthcaresafety.view.preview.checklist;


import android.content.Context;

import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;
import com.arise.healthcaresafety.view.preview.interfaces.PreviewInterface;

public class CheckListView extends AbsPreviewView {

    private CheckList mCheckList;

    public CheckListView(Context context, CheckList checkList, int screenWidth) {
        super(context, screenWidth);
        this.mCheckList = checkList;
    }


    @Override
    public void buildView() {
        this.buildLayoutView();
    }

    private void buildLayoutView() {
        this.addView(new ChildCheckListView(mContext, mScreenWidth + 10, mCheckList));

        for (PreviewInterface item : this.getViews()) {
            this.linearLayout.addView(item.getLayout());
        }
    }
}
