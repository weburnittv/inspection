package com.arise.healthcaresafety.view.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.NewsCaseDetailActivity;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Group;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.ITabScreenListener;
import com.arise.healthcaresafety.view.IUpdateCheckListResultNoteListener;
import com.arise.healthcaresafety.view.adapter.CheckListAdapter;
import com.arise.healthcaresafety.view.custom.MySendEmailDialog;

import java.util.ArrayList;
import java.util.List;

public class CheckListScreenFragment extends Fragment implements ITabScreenListener,
        IUpdateCheckListResultNoteListener, MySendEmailDialog.MySendEmailDialogListener,
        CheckListAdapter.OnCheckListAdapterListener, View.OnClickListener {
    // keep state create database
    public static boolean isTemp = true;
    private CheckListAdapter mCheckListAdapter;
    private CheckList mCheckList;
    private NewsCaseDetailActivity mParent;
    ExpandableListView.OnGroupExpandListener mExpandListener = new ExpandableListView.OnGroupExpandListener() {
        @Override
        public void onGroupExpand(int groupPosition) {
            Utils.getInstance().clearFocusIME(mParent);
            mCheckListAdapter.invalidateIndicatorIcon(true, groupPosition);
        }
    };
    ExpandableListView.OnGroupCollapseListener mCollapseListener = new ExpandableListView.OnGroupCollapseListener() {
        @Override
        public void onGroupCollapse(int groupPosition) {
            Utils.getInstance().clearFocusIME(mParent);
            mCheckListAdapter.invalidateIndicatorIcon(false, groupPosition);
        }
    };
    ExpandableListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            Utils.getInstance().clearFocusIME(mParent);
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };
    private ExpandableListView mCheckListView;
    /**
     * This is list result from checklist which is store result after user has been selected answer each of question
     */
    private List<Result> mResults = new ArrayList<>();

    public CheckListScreenFragment() {
    }

    public static CheckListScreenFragment newInstance() {
        return new CheckListScreenFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        isTemp = false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        mParent = (NewsCaseDetailActivity) getActivity();
//        mParent.setCheckListScreenListener(this);
//        this.mCheckList = this.mParent.loadCheckList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = (NewsCaseDetailActivity) getActivity();
        mParent.setCheckListScreenListener(this);
        return inflater.inflate(R.layout.fragment_check_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCheckList = mParent.loadCheckList();
        mCheckListAdapter = new CheckListAdapter(getActivity(), mCheckList,
                mParent.getCase().isSynced() ? InspConstant.MODE_READ : InspConstant.MODE_EDIT);
        mCheckListView = (ExpandableListView) view.findViewById(R.id.checklist_exp_list_view);
        mCheckListView.setOnGroupExpandListener(mExpandListener);
        mCheckListView.setOnGroupCollapseListener(mCollapseListener);
        mCheckListView.setOnScrollListener(mOnScrollListener);
        mCheckListView.setAdapter(mCheckListAdapter);
        mCheckListAdapter.notifyDataSetChanged();
        mCheckListAdapter.setOnCheckListAdapterListener(CheckListScreenFragment.this);
        mCheckListAdapter.setUpdateCheckListResultListener(CheckListScreenFragment.this);

        ((TextView) view.findViewById(R.id.checkListScreen_txt_title)).setText(mParent.getTypeName());
        view.findViewById(R.id.checklist_bt_save).setOnClickListener(this);

        this.checkShowOrHiddenSendEmail(view);
        view.findViewById(R.id.checkListScreen_btn_send).setOnClickListener(this);
        view.findViewById(R.id.checkListScreen_btn_preview).setOnClickListener(this);
    }

    private void checkShowOrHiddenSendEmail(View view) {
        if (mParent.getCase().getLiveId() != 0) {
            view.findViewById(R.id.checkListScreen_btn_send).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.checkListScreen_btn_send).setVisibility(View.GONE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent.setCheckListScreenListener(null);
    }    /* listener whenever group close */

    @Override
    public void onPause() {
        super.onPause();
        isTemp = true;
    }

    @Override
    public void bindingNote(Note note) {
        for (Group group : this.mCheckList.getGroups()) {
            for (Question question : group.getQuestions()) {
                if (question.getLiveId() == note.getQuestion().getLiveId()) {
                    question.setNote(note);
                    mCheckListAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    @Override
    public void updateCase(Case mCase) {
        this.mCheckList = mCase.getCheckList();
    }

    @Override
    public void onSendEmailStatus(boolean status) {
        Utils.getInstance().hideProgressDialog();
        if (status) {
            Utils.getInstance().showMessage(getActivity(), "Email has been sent successfully.");
        } else {
            Utils.getInstance().showMessage(getActivity(), "Email delivery failure.");
        }
    }

    @Override
    public void OnUpdateNoteContent(Note note) {
        if (mParent != null) {
            mParent.onUpdateNoteContent(note);
        }
    }

    @Override
    public void OnOpenPicture(Note note) {
        if (mParent != null) {
            String mediaPath = FileManager.getInstance().getCaseFolder(mParent.getCase(), note);
            mParent.openPhotoScreen(note, mediaPath, InspConstant.REQUEST_CODE_CHECK_OPEN_PHOTO);
        }
    }

    @Override
    public void OnOpenAudio(Note note) {
        if (mParent != null) {
            String mediaPath = FileManager.getInstance().getCaseFolder(mParent.getCase(), note);
            mParent.openAudioScreen(note, mediaPath, InspConstant.REQUEST_CODE_CHECK_OPEN_AUDIO);
        }
    }

    @Override
    public void onClickSendEmail(String email) {
        if (mParent == null) {
            return;
        }
        Utils.getInstance().showProgressDialog(getActivity(), R.string.progress_loading, true);
        mParent.getPresenter().sendEmail(email);
    }

    public void openSendMyDialog() {
        MySendEmailDialog dialog = new MySendEmailDialog(getActivity(), this);
        dialog.show();
    }

    @Override
    public void updateFinal(Question question) {
        mParent.getPresenter().updateFinal(question);
    }

    @Override
    public void updateQuestion(Question question) {
        mParent.getPresenter().updateQuestion(question);
        restoreResult(mParent.getCase());
        saveResultFromQuestion(question);
    }

    private void restoreResult(Case cse) {
        mResults.clear();
        mResults.addAll(cse.getCheckListResult());
    }

    /**
     * This method using to keep the list result from check list question
     * Results is answer from user and will be keep in Inspection Case
     *
     * @param question is question was answer from user.
     */
    private void saveResultFromQuestion(Question question) {
        if (mResults != null) {
            Result result = question.getResult();
            result.setQuestion(question);
            int sizeResult = mResults.size();
            if (sizeResult == 0) {
                mResults.add(result);
            }

            if (sizeResult != 0) {
                long ansQsId = question.getLiveId();
                int loopPosition = 0;
                boolean isExisting = false;
                for (int i = 0; i < sizeResult; i++) {
                    long storedQsId = mResults.get(i).getLiveId();
                    if (ansQsId != storedQsId) {
                        isExisting = false;
                    }

                    if (ansQsId == storedQsId) {
                        loopPosition = i;
                        isExisting = true;
                    }
                }

                if (isExisting) {
                    mResults.set(loopPosition, result);
                } else {
                    mResults.add(result);
                }
            }
        }
        for (Result result : mResults) {
            mParent.getCase().saveResult(result);
        }
    }

    /**
     * This method using to save all result to Inspection Case
     *
     * @param results is final list result
     */
    private void onSaveResultToInsCase(List<Result> results) {
        if (mParent.getCase() == null) {
            return;
        }
        mParent.getCase().setResults(results);
        mParent.saveCase();
        Toast.makeText(getActivity(), "Case is saved successfully!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checklist_bt_save:
                onSaveResultToInsCase(mResults);
                break;
            case R.id.checkListScreen_btn_send:
                openSendMyDialog();
                break;
            case R.id.checkListScreen_btn_preview:
                if (mParent != null) {
                    mParent.openPreview();
                }
                break;
            default:
                break;
        }
    }
}
