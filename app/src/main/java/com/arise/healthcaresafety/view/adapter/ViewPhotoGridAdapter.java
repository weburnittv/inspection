package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Media;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nam on 7/16/2015.
 */
public class ViewPhotoGridAdapter extends RecyclerView.Adapter<ViewPhotoGridAdapter.MyImageViewHolder> {
    private static final String TAG = ViewPhotoGridAdapter.class.getSimpleName();
    private List<Media> items = new ArrayList<>();

    private Context context;
    /*grid photo adapter interface */
    private PhotoGridListener listener = null;
    /* event on click */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Media item = (Media) v.getTag();
            if (ViewPhotoGridAdapter.this.listener != null) {
                ViewPhotoGridAdapter.this.listener.onClickPhoto(item);
            }
        }
    };

    public ViewPhotoGridAdapter(Context context, List<Media> medias) {
        this.context = context;
        this.items = medias;
    }

    @Override
    public MyImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_item, parent, false);
        return new MyImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyImageViewHolder holder, int position) {
        Media item = this.items.get(position);
        Log.e(TAG, "item.getUrlLink" + item.getUrlLink());
        Picasso.with(this.context)
                .load(item.getUrlLink())
                .noFade()
                .resize(250, 250)
                .centerCrop()
                .into(holder.image);
        //set event for image view
        holder.image.setTag(item);
        holder.image.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        if (this.items == null) {
            return 0;
        }
        return this.items.size();
    }

    public void setGridPhotoListener(PhotoGridListener listener) {
        this.listener = listener;
    }

    public interface PhotoGridListener {
        public void onClickPhoto(Media media);
    }

    public static class MyImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;

        public MyImageViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.viewPhoto_image);
        }
    }
}
