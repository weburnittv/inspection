package com.arise.healthcaresafety.view.custom.equipment;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.MyCheckBox;
import com.arise.healthcaresafety.view.custom.MyEditView;
import com.arise.healthcaresafety.view.custom.MyTextView;


public class EquipmentView extends LinearLayout {
    private static final String TAG = EquipmentView.class.getSimpleName();
    public static boolean isCheckState;
    Context context;
    private int mType;
    private String mContent;
    private MyCheckBox mChkBox;
    private boolean isActive = false;
    private ToggleButton mToggleEdit;
    private LinearLayout mEditForm;
    private ImageView mImgCamera;
    private ImageView mImgMic;
    private MyTextView mTxtNoImg;
    private MyTextView mTxtNoRecord;
    private MyEditView mEdtNote;
    private int viewMode;
    private boolean isCheckToggle = true;
    private boolean isCheckBox = false;
    private int groupPosition;
    private int childPosition;
    /**
     * set group id and item id
     *
     * @param groupId
     * @param itemId
     */
    private Equipment equipment;
    private OnEquipmentViewListener mListener = null;
    /* handle event of unit view */
    private CompoundButton.OnCheckedChangeListener onCheckBokListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            isCheckBox = isChecked;
            if (mListener != null) {
                isCheckToggle = !isChecked;
                mListener.onCheckBoxListener(groupPosition, childPosition, isChecked);
                if (!isChecked && mEditForm.getVisibility() == GONE) {
                    setToggleEdit(false);
                }
                if (!isActive) {
                    mEditForm.setVisibility(View.GONE);
                }
                if (isCheckToggle && mEditForm.getVisibility() == VISIBLE) {
                    setToggleEdit(!isCheckToggle);
                    mEditForm.setVisibility(View.GONE);
                }
            }
        }
    };

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mEdtNote.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable s) {
            mContent = s.toString();
        }
    };

    private OnFocusChangeListener focus = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && mContent.length() > 0) {
                isCheckState = true;
                Log.e("NoteContent", mContent);
                equipment.getNote().setContent(mContent);
                mListener.onSaveNoteContent(groupPosition, childPosition);
            }
        }
    };

    private View.OnClickListener onToggleEditForm = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (!isCheckBox && mEditForm.getVisibility() == GONE) {
                return;
            } else if (isCheckBox) {
                if (isCheckBox && mEditForm.getVisibility() == VISIBLE) {
                    setToggleEdit(true);
                    mChkBox.setOnCheckedChangeListener(null);
                    mChkBox.setChecked(true);
                    mChkBox.setOnCheckedChangeListener(onCheckBokListener);
                    mEditForm.setVisibility(View.GONE);
                } else if (isCheckBox && mEditForm.getVisibility() == GONE) {
                    setToggleEdit(true);
                    mChkBox.setOnCheckedChangeListener(null);
                    mChkBox.setChecked(true);
                    mChkBox.setOnCheckedChangeListener(onCheckBokListener);
                    mEditForm.setVisibility(View.VISIBLE);
                }
            }
        }
    };
    private View.OnClickListener onCameraClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                isCheckState = false;
                mListener.onCameraClick(groupPosition, childPosition);
            }
        }
    };
    private View.OnClickListener onMicClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                isCheckState = false;
                mListener.onMicClick(groupPosition, childPosition);
            }
        }
    };
    private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                isCheckState = true;
                Utils.getInstance().showOrHideKeyBoard((Activity) v.getContext(), false, v);
                mListener.onSaveNoteContent(groupPosition, childPosition);
                return true;
            }
            return false;
        }
    };

    public EquipmentView(Context context, int mode) {
        super(context);
        this.context = context;
        this.viewMode = mode;
        initView(context);
    }

    public EquipmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(context);
    }

    private void initView(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_equipment_item, this, true);
        mChkBox = (MyCheckBox) view.findViewById(R.id.equipItem_checkBox);
        mToggleEdit = (ToggleButton) view.findViewById(R.id.equipItem_toggle_openEdit);
        mEditForm = (LinearLayout) view.findViewById(R.id.equipItem_ll_editView);
        mTxtNoImg = (MyTextView) view.findViewById(R.id.equipItem_txt_noImg);
        mTxtNoRecord = (MyTextView) view.findViewById(R.id.equipItem_txt_noRecord);
        mEdtNote = (MyEditView) view.findViewById(R.id.equipItem_edt_note);
        mImgCamera = (ImageView) view.findViewById(R.id.equipItem_img_camera);
        mImgMic = (ImageView) view.findViewById(R.id.equipItem_img_mic);
        initHandleEvent();
    }

    public void setIdentity(int groupId, int itemId, Equipment equipment) {
        this.equipment = equipment;
        this.groupPosition = groupId;
        this.childPosition = itemId;
        this.mChkBox.setText(equipment.getName());
        if (equipment.isChecked())
            this.mChkBox.setChecked(true);
        if (equipment.getNote() != null) {
            this.mTxtNoImg.setText(String.valueOf(equipment.getNote().getNotePhotoMedias().size()));
            this.mTxtNoRecord.setText(String.valueOf(equipment.getNote().getNoteSoundMedias().size()));
        } else {
            this.mTxtNoImg.setText("0");
            this.mTxtNoRecord.setText("0");
        }
        initUI();
    }

    /**
     * configure state of checkbox
     *
     * @param isChecked
     */
    private void setToggleEdit(boolean isChecked) {
        this.isActive = isChecked;
        mToggleEdit.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(),
                isActive ? R.drawable.toggle_active_edit : R.drawable.toggle_inactive_edit, null));
    }

    /**
     * set text for mTextNoImg
     *
     * @param txt
     */
    public void setTextImg(String txt) {
        mTxtNoImg.setText(txt);
    }

    /**
     * set text for mTextNoRecord
     *
     * @param txt
     */
    public void setTextRecord(String txt) {
        mTxtNoRecord.setText(txt);
    }

    /**
     * get note for note view
     *
     * @return string
     */
    public String getNote() {
        return mEdtNote.getText().toString().trim();
    }

    /**
     * init event for UI control
     */
    private void initHandleEvent() {
        mChkBox.setEnabled(true);
        mChkBox.setOnCheckedChangeListener(onCheckBokListener);

        mToggleEdit.setOnClickListener(onToggleEditForm);
        mImgCamera.setOnClickListener(onCameraClick);
        mImgMic.setOnClickListener(onMicClick);


        mEdtNote.setOnEditorActionListener(onEditorActionListener);
        mEdtNote.addTextChangedListener(watcher);
        mEdtNote.setOnFocusChangeListener(focus);
        mEdtNote.setHorizontallyScrolling(false);
    }

    private void initUI() {
        if (this.equipment.getNote() != null) {
            mEdtNote.setText(this.equipment.getNote().getContent());
        }
    }

    /**
     * set onListener to listener event from checkbox, toggle button, image camera, image mic
     *
     * @param listener
     */
    public void setOnEquipmentViewListener(OnEquipmentViewListener listener) {
        this.mListener = listener;
    }

    public interface OnEquipmentViewListener {
        void onCheckBoxListener(int groupPosition, int childPosition, boolean isChecked);

        void onCameraClick(int groupPosition, int childPosition);

        void onMicClick(int groupPosition, int childPosition);

        void onSaveNoteContent(int groupPosition, int childPosition);
    }
}
