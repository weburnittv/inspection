package com.arise.healthcaresafety.view.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.view.fragment.CaseInfoScreenFragment;
import com.arise.healthcaresafety.view.fragment.CheckListScreenFragment;
import com.arise.healthcaresafety.view.fragment.EquipmentScreenFragment;

/**
 * Created by hnam on 6/22/2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    int mode;
    CaseInfoScreenFragment caseDetailTab;
    EquipmentScreenFragment equipTab;
    CheckListScreenFragment checkListTab;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, int mode) {
        super(fm);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.mode = mode;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (caseDetailTab == null)
                caseDetailTab = CaseInfoScreenFragment.newInstance();
            return caseDetailTab;
        } else if (position == 1) {
            if (equipTab == null)
                equipTab = new EquipmentScreenFragment();
            return equipTab;
        } else {
            if (checkListTab == null)
                checkListTab = new CheckListScreenFragment();
            return checkListTab;
        }
    }

    public void updateEquipmentsCase(Case mCase) {
        if (equipTab == null)
            equipTab = new EquipmentScreenFragment();
        this.equipTab.updateCase(mCase);
    }

    public void updateChecklistCase(Case mCase) {
        if (checkListTab == null)
            checkListTab = new CheckListScreenFragment();
        this.checkListTab.updateCase(mCase);
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
