package com.arise.healthcaresafety.view.adapter;

import com.arise.healthcaresafety.model.entities.Equipment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nam on 6/27/2015.
 */
public class EquipmentGroup {
    private String name;
    private boolean isExpand;
    private List<Equipment> items;

    public EquipmentGroup() {
        this.items = new ArrayList<>();
    }

    public EquipmentGroup(String name) {
        this.name = name;
        this.isExpand = false;
        items = new ArrayList<>();
    }

    public EquipmentGroup(String name, List<Equipment> items) {
        this.name = name;
        this.isExpand = false;
        this.items = items;
    }


    public String getName() {
        if (name != null)
            return name;
        return "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Equipment> getItems() {
        return items;
    }

    public void setItems(List<Equipment> items) {
        this.items = items;
    }

    public void clearItems() {
        this.items.clear();
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setIsExpand(boolean isExpand) {
        this.isExpand = isExpand;
    }


}
