package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.services.AudioRecordService;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nam on 7/18/2015.
 */
public class ViewRecordListAdapter extends RecyclerView.Adapter<ViewRecordListAdapter.RecordView> {
    private static final String TAG = ViewRecordListAdapter.class.getSimpleName();
    //public void setList
    RecordListListener listener = null;
    private Context context;
    private List<Media> items = new ArrayList<>();
    private AudioRecordService service;
    // implement event of play btn
    View.OnClickListener onPlayClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();
            Media item = items.get(position);
            if (ViewRecordListAdapter.this.service != null) {
                ViewRecordListAdapter.this.service.startStopUrlPlay(item);
                ViewRecordListAdapter.this.notifyDataSetChanged();
            }
            /*if(ViewRecordListAdapter.this.listener != null){
                ViewRecordListAdapter.this.listener.onPlayStop(item);
            }*/
        }
    };


    public ViewRecordListAdapter(Context context, List<Media> medias) {
        this.context = context;
        this.items = medias;
    }

    public void setAudioService(AudioRecordService service) {
        this.service = service;
    }

    @Override
    public RecordView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_record_item, parent, false);
        return new RecordView(view);
    }

    @Override
    public void onBindViewHolder(RecordView holder, int position) {
        //get item from position & set event
        Media item = this.items.get(position);
        holder.title.setText(item.getLocalName());
        holder.description.setText("");
        holder.duration.setText("");
        holder.row.setTag(position);
        holder.btnPlay.setTag(position);
        holder.btnDelete.setTag(position);
        if (this.service.isServerPlay(item)) {
            holder.btnPlay.setBackgroundDrawable(ResourcesCompat.getDrawable(context.getResources(),
                    R.drawable.ic_pause, null));
        } else {
            holder.btnPlay.setBackgroundDrawable(ResourcesCompat.getDrawable(context.getResources(),
                    R.drawable.ic_play, null));
        }
        holder.btnPlay.setOnClickListener(onPlayClick);
    }

    @Override
    public int getItemCount() {
        //get size of items
        if (this.items == null) {
            return 0;
        }
        return this.items.size();
    }

    public void setRecordListListener(RecordListListener listener) {
        this.listener = listener;
    }

    public interface RecordListListener {
        public void onPlayStop(Media item);
    }

    public static class RecordView extends RecyclerView.ViewHolder {
        private MyTextView title;
        private MyTextView description;
        private MyTextView duration;
        private Button btnPlay;
        private Button btnDelete;
        private LinearLayout row;

        public RecordView(View itemView) {
            super(itemView);
            this.row = (LinearLayout) itemView.findViewById(R.id.viewRecordItem_row);
            this.title = (MyTextView) itemView.findViewById(R.id.viewRecordItem_title);
            this.description = (MyTextView) itemView.findViewById(R.id.viewRecordItem_description);
            this.duration = (MyTextView) itemView.findViewById(R.id.viewRecordItem_duration);
            this.btnPlay = (Button) itemView.findViewById(R.id.viewRecordItem_btn_play);
            this.btnDelete = (Button) itemView.findViewById(R.id.viewRecordItem_btn_delete);
        }
    }
}
