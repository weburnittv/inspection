package com.arise.healthcaresafety.view.preview.interfaces;


import android.widget.LinearLayout;

public interface PreviewInterface {
    LinearLayout getLayout();
}
