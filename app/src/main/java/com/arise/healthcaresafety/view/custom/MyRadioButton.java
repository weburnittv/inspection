package com.arise.healthcaresafety.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;

import com.arise.healthcaresafety.R;

public class MyRadioButton extends AppCompatRadioButton {
    public MyRadioButton(Context context) {
        super(context);
        init();
    }

    public MyRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String fontName = a.getString(R.styleable.MyTextView_fontName);
            Typeface tf;
            if (fontName != null) {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(tf);
            } else {
                tf = Typeface.createFromAsset(getContext().getAssets(),
                        "fonts/" + getResources().getString(R.string.roboto_light));

            }
            setTypeface(tf);
            setTextColor(getResources().getColor(R.color.black_text));
            a.recycle();
        }
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/" + getResources().getString(R.string.roboto_light));
        setTypeface(tf);
        setTextColor(getResources().getColor(R.color.black_text));
    }
}
