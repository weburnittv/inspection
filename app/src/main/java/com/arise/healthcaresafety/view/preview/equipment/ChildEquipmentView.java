package com.arise.healthcaresafety.view.preview.equipment;


import android.content.Context;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.view.adapter.EquipmentGroup;
import com.arise.healthcaresafety.view.custom.IndexTableView;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;

public class ChildEquipmentView extends AbsPreviewView {

    private int mWidthEquipColumn1, mWidthEquipColumn2;
    private Equipments mEquipments;

    public ChildEquipmentView(Context context, int screenWidth, Equipments equipments) {
        super(context, screenWidth);
        this.mEquipments = equipments;
        this.mWidthEquipColumn1 = (screenWidth / 3) * 2;
        this.mWidthEquipColumn2 = (screenWidth / 3);
    }

    @Override
    public void buildView() {
        this.linearLayout = this.getChildCaseEquipment();
    }

    private LinearLayout getChildCaseEquipment() {
        LinearLayout childItem = new LinearLayout(mContext);
        childItem.setOrientation(LinearLayout.VERTICAL);
        for (EquipmentGroup group : mEquipments.getEquipmentGroup()) {
            LinearLayout rowItem = new LinearLayout(mContext);
            rowItem.setOrientation(LinearLayout.VERTICAL);

            rowItem.addView(this.getGroupTitle(group));
            rowItem.addView(this.getHeaderCaseEquipment());
            rowItem.addView(this.getGroupItem(group));

            childItem.addView(rowItem);
        }

        return childItem;
    }


    private LinearLayout getGroupTitle(EquipmentGroup equipmentGroup) {
        LinearLayout rowItem = new LinearLayout(mContext);
        rowItem.setOrientation(LinearLayout.VERTICAL);
        String name = equipmentGroup.getName();
        rowItem.addView(new IndexTableView(mContext, mScreenWidth,
                Constants.HEIGHT_ITEM_TABLE, name,
                IndexTableView.TABLE_CATEGORY_TYPE).createView(R.drawable.bg_preview_category, true));

        return rowItem;
    }

    private LinearLayout getHeaderCaseEquipment() {
        LinearLayout tableHeader = new LinearLayout(mContext);
        tableHeader.setOrientation(LinearLayout.HORIZONTAL);
        int headerHeight = 70;
        tableHeader.addView(new IndexTableView(mContext, this.mWidthEquipColumn1,
                headerHeight, "Name",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        tableHeader.addView(new IndexTableView(mContext, this.mWidthEquipColumn2,
                headerHeight, "Availability",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        return tableHeader;
    }

    private LinearLayout getGroupItem(EquipmentGroup equipmentGroup) {
        LinearLayout childItem = new LinearLayout(mContext);
        childItem.setOrientation(LinearLayout.VERTICAL);
        for (Equipment equipment : equipmentGroup.getItems()) {
            LinearLayout columnItem = new LinearLayout(mContext);
            columnItem.setOrientation(LinearLayout.HORIZONTAL);

            String name = equipment.getName();
            String isSelected = equipment.isChecked() ? "Yes" : "No";

            columnItem.addView(new IndexTableView(mContext, this.mWidthEquipColumn1,
                    Constants.HEIGHT_ITEM_TABLE, name,
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));

            columnItem.addView(new IndexTableView(mContext, this.mWidthEquipColumn2,
                    Constants.HEIGHT_ITEM_TABLE, isSelected,
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item));

            childItem.addView(columnItem);
            if (isExistingNote(equipment))
                childItem.addView(this.getNoteView(equipment));
        }
        return childItem;
    }


    private boolean isExistingNote(Equipment equipment) {
        Note note = equipment.getNote();
        return note.getNotePhotoMedias().size() != 0
                || note.getNoteSoundMedias().size() != 0
                || note.getContent().length() != 0;
    }

    private LinearLayout getNoteView(Equipment equipment) {
        LinearLayout childItem = new LinearLayout(mContext);
        childItem.setOrientation(LinearLayout.VERTICAL);

        int headerHeight = 60;
        childItem.addView(new IndexTableView(mContext, mScreenWidth,
                headerHeight, "Note Results",
                IndexTableView.NOTE_TITLE_TYPE).createView(R.drawable.bg_common));

        LinearLayout headerColumn = new LinearLayout(mContext);
        headerColumn.setOrientation(LinearLayout.HORIZONTAL);

        headerColumn.addView(new IndexTableView(mContext, 200,
                45, "Media",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        headerColumn.addView(new IndexTableView(mContext, mScreenWidth - 200,
                45, "Notes Text",
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        childItem.addView(headerColumn);

        Note note = equipment.getNote();
        this.getPhotoMediaView(childItem, note);
        this.getSoundMediaView(childItem, note);

        return childItem;
    }

    private void getPhotoMediaView(LinearLayout parentView, Note note) {
        boolean isSetFistTime = false;
        for (Media media : note.getNotePhotoMedias()) {
            LinearLayout noteColumn = new LinearLayout(mContext);
            noteColumn.setOrientation(LinearLayout.HORIZONTAL);

            noteColumn.addView(new IndexTableView(mContext, 200,
                    200, media.getLocalPath()).createNotePhotoView(R.drawable.bg_preview_item, true));

            String content = "";
            if (!isSetFistTime)
                content = note.getContent();

            noteColumn.addView(new IndexTableView(mContext, mScreenWidth - 200,
                    200, content,
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));

            isSetFistTime = true;
            parentView.addView(noteColumn);
        }
    }

    private void getSoundMediaView(LinearLayout parentView, Note note) {
        for (Media media : note.getNoteSoundMedias()) {
            LinearLayout noteColumn = new LinearLayout(mContext);
            noteColumn.setOrientation(LinearLayout.HORIZONTAL);

            noteColumn.addView(new IndexTableView(mContext, 200,
                    200, media.getLocalPath()).createNotePhotoView(R.drawable.bg_preview_item, false));
            noteColumn.addView(new IndexTableView(mContext, mScreenWidth - 200,
                    200, media.getNote().getContent(),
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));
            parentView.addView(noteColumn);
        }
    }
}
