package com.arise.healthcaresafety.view.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.NewsCaseDetailActivity;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Category;
import com.arise.healthcaresafety.model.entities.Type;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.IndexTableView;
import com.arise.healthcaresafety.view.custom.MyCheckBoxDialog;
import com.arise.healthcaresafety.view.custom.MyDateTimeDialog;
import com.arise.healthcaresafety.view.custom.MyTextView;

public class CaseInfoScreenFragment extends Fragment implements View.OnClickListener,
        MyCheckBoxDialog.OnMyCheckBoxDialog, MyDateTimeDialog.OnMyTimeDateDialog {
    public static EditText mEtRefNo;
    private final int OVERVIEW_SECTION = 0;
    private final int BRANCH_SECTION = 1;
    private final int HISTORY_SECTION = 2;
    private ToggleButton mToggleOverview;
    private ToggleButton mToggleBranch;
    private ToggleButton mToggleHistory;
    private LinearLayout mOverViewLayout;
    private LinearLayout mBranchLayout;
    // private LinearLayout mTbHistory;
    private TextView mOverViewTitle;
    private TextView mBranchTitle;
    private TextView mHistoryTitle;
    private MyTextView mTxtBranchLocation;
    private MyTextView mTxtTel;
    private TextView mTvDivision;
    private TextView mTvBranch;
    private TextView mTvCategory;
    private TextView mTvType;
    private TextView mTvArrDate;
    private TextView mTvArrTime;
    private NewsCaseDetailActivity mParent;
    private android.support.v7.app.AlertDialog mDialogDivision, mDialogBranch, mDialogCategory, mDialogType;
    private LinearLayout mLlTableHistory;
    private int mNameWidth;
    private int mConductedWidth;
    private int mFailedWidth;
    private Case mCase;

    public CaseInfoScreenFragment() {
    }

    public static CaseInfoScreenFragment newInstance() {
        return new CaseInfoScreenFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (NewsCaseDetailActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mEtRefNo != null) {
            outState.putString("refNo", mEtRefNo.getText().toString().trim());
            outState.putBoolean("no1", mToggleOverview.isChecked());
            outState.putBoolean("no2", mToggleBranch.isChecked());
            outState.putBoolean("no3", mToggleHistory.isChecked());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_case_info, container, false);
        initView(view);

        //init mode edit
        initListenerEvent();
        //restore
        if (savedInstanceState != null) {
            mEtRefNo.setText(savedInstanceState.getString("refNo"));
            mToggleOverview.setChecked(savedInstanceState.getBoolean("no1", false));
            mToggleBranch.setChecked(savedInstanceState.getBoolean("no2", false));
            mToggleHistory.setChecked(savedInstanceState.getBoolean("no3", false));

            showAndHide(savedInstanceState.getBoolean("no1", false), OVERVIEW_SECTION);
            showAndHide(savedInstanceState.getBoolean("no2", false), BRANCH_SECTION);
            showAndHide(savedInstanceState.getBoolean("no3", false), HISTORY_SECTION);
        }
        return view;
    }

    private void initView(View view) {
        ((NewsCaseDetailActivity) getActivity()).showSaveButton(true);
        view.findViewById(R.id.case_info_ll_division).setOnClickListener(this);
        view.findViewById(R.id.case_info_ll_branch).setOnClickListener(this);
        view.findViewById(R.id.case_info_ll_category).setOnClickListener(this);
        view.findViewById(R.id.case_info_ll_type).setOnClickListener(this);
        view.findViewById(R.id.case_info_ll_arrival_date).setOnClickListener(this);
        view.findViewById(R.id.case_info_ll_arrival_time).setOnClickListener(this);

        mTvDivision = (TextView) view.findViewById(R.id.case_info_tv_division);
        mTvBranch = (TextView) view.findViewById(R.id.case_info_tv_branch);
        mTvCategory = (TextView) view.findViewById(R.id.case_info_tv_category);
        mTvType = (TextView) view.findViewById(R.id.case_info_tv_type);
        mTvArrDate = (TextView) view.findViewById(R.id.case_info_tv_arrival_date);
        mTvArrTime = (TextView) view.findViewById(R.id.case_info_tv_arrival_time);
        clearViews();

        mToggleOverview = (ToggleButton) view.findViewById(R.id.fragCaseInfo_toggle_overview);
        mToggleBranch = (ToggleButton) view.findViewById(R.id.fragCaseInfo_toggle_branch);
        mToggleHistory = (ToggleButton) view.findViewById(R.id.fragCaseInfo_toggle_history);
        mOverViewLayout = (LinearLayout) view.findViewById(R.id.fragCaseInfo_inspectionOverview);
        mBranchLayout = (LinearLayout) view.findViewById(R.id.fragCaseInfo_branch_info);
        // mTbHistory = (TableLayout) view.findViewById(R.id.case_info_tb_history);
        mLlTableHistory = (LinearLayout) view.findViewById(R.id.case_info_ll_table);
        mOverViewTitle = (TextView) view.findViewById(R.id.fragCaseInfo_txt_overview);
        mBranchTitle = (TextView) view.findViewById(R.id.fragCaseInfo_txt_branch);
        mHistoryTitle = (TextView) view.findViewById(R.id.fragCaseInfo_txt_history);

        //branch info view
        mTxtBranchLocation = (MyTextView) view.findViewById(R.id.fragCaseInfo_txt_branchLocation);
        mTxtTel = (MyTextView) view.findViewById(R.id.fragCaseInfo_txt_personInCharge);

        //  mTxtCreateCase = (MyButtonView) view.findViewById(R.id.fragCaseinfo_txt_save_case);
        mEtRefNo = (EditText) view.findViewById(R.id.case_info_et_ref_no);


        if (((NewsCaseDetailActivity) getActivity()).getCase().getBranch() != null) {
            mCase = ((NewsCaseDetailActivity) getActivity()).getCase();
            mTvDivision.setText(mCase.getBranch().getDivision().getName());
            mTvBranch.setText(mCase.getBranch().getName());
            mTvCategory.setText(mCase.getType().getCategory().getName());
            mTvType.setText(mCase.getType().getName());
            mTxtBranchLocation.setText(mCase.getBranch().getAddress());
            mTvArrDate.setText(mCase.getArrivalDate());
            mTvArrTime.setText(mCase.getMyArrivalTime());
            mEtRefNo.setText(mCase.getReferenceNo());
            mTxtTel.setText(mCase.getBranch().getTel());
        } else {
            mTvDivision.setText(mParent.getSelectedDivision() == null ?
                    "" : mParent.getSelectedDivision().getName());
            mTvBranch.setText(mParent.getSelectedBranch() == null ?
                    "" : mParent.getSelectedBranch().getName());
            mTvCategory.setText(mParent.getSelectedCategory() == null ?
                    "" : mParent.getSelectedCategory().getName());
            mTvType.setText(mParent.getSelectedType() == null ?
                    "" : mParent.getSelectedType().getName());
            mTxtBranchLocation.setText(mParent.getSelectedBranch() == null ?
                    "" : mParent.getSelectedBranch().getAddress());
            mTxtTel.setText(mParent.getSelectedBranch() == null ?
                    "" : mParent.getSelectedBranch().getTel());
        }

        mTvArrDate.setText(mParent.getDateTimeArrival(Utils.GET_DATE));
        mTvArrTime.setText(mParent.getDateTimeArrival(Utils.GET_TIME));
        initWidthSize();
        initHistoryView();
    }

    private void initWidthSize() {
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int screenWidth = display.getWidth() - 16;
        mNameWidth = (screenWidth / 2) - 30;
        mFailedWidth = mNameWidth / 2;
        mConductedWidth = screenWidth - (mNameWidth + mFailedWidth) - 30;
    }

    private void initListenerEvent() {
        mToggleOverview.setOnClickListener(this);
        mToggleBranch.setOnClickListener(this);
        mToggleHistory.setOnClickListener(this);

        //open view
        mToggleOverview.setChecked(true);
        showAndHide(true, OVERVIEW_SECTION);
        mToggleBranch.setChecked(true);
        showAndHide(true, BRANCH_SECTION);
        mToggleHistory.setChecked(true);
        showAndHide(true, HISTORY_SECTION);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragCaseInfo_toggle_overview:
                showAndHide(((ToggleButton) v).isChecked(), OVERVIEW_SECTION);
                break;
            case R.id.fragCaseInfo_toggle_branch:
                showAndHide(((ToggleButton) v).isChecked(), BRANCH_SECTION);
                break;
            case R.id.fragCaseInfo_toggle_history:
                showAndHide(((ToggleButton) v).isChecked(), HISTORY_SECTION);
                break;
            case R.id.case_info_ll_division:
                onShowDgDivision();
                break;
            case R.id.case_info_ll_branch:
                onShowDgBranch();
                break;
            case R.id.case_info_ll_category:
                onShowDgCategory();
                break;
            case R.id.case_info_ll_type:
                onShowDgType();
                break;
            case R.id.case_info_ll_arrival_date:
                showDateTimeDialog(MyDateTimeDialog.DIALOG_DATE);
                break;
            case R.id.case_info_ll_arrival_time:
                showDateTimeDialog(MyDateTimeDialog.DIALOG_TIME);
                break;
            default:
                break;

        }
    }

    private void showAndHide(boolean status, int sectionIndex) {
        switch (sectionIndex) {
            case OVERVIEW_SECTION: {
                mOverViewTitle.setTextColor(status ? getResources().getColor(R.color.colorPrimary)
                        : getResources().getColor(R.color.black_text));
                mOverViewLayout.setVisibility(status ? View.VISIBLE : View.GONE);
                break;
            }
            case BRANCH_SECTION: {
                mBranchTitle.setTextColor(status ? getResources().getColor(R.color.colorPrimary)
                        : getResources().getColor(R.color.black_text));
                mBranchLayout.setVisibility(status ? View.VISIBLE : View.GONE);
                break;
            }
            case HISTORY_SECTION: {
                mHistoryTitle.setTextColor(status ? getResources().getColor(R.color.colorPrimary)
                        : getResources().getColor(R.color.black_text));
                mLlTableHistory.setVisibility(status ? View.VISIBLE : View.GONE);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onSelectCheckBoxDialog(int type, final int id, String value) {
        switch (type) {
            case InspConstant.DIALOG_SELECT_DIVISION:
                mParent.setDivisionById(id);
                mTvBranch.setText("");
                mTxtBranchLocation.setText("");
                mTxtTel.setText("");
                mTvDivision.setText(value);
                break;
            case InspConstant.DIALOG_SELECT_BRANCH:
                mParent.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mParent.setBranchById(id);
                    }
                });
                mTvBranch.setText(value);
                mTxtBranchLocation.setText(mParent.getSelectedBranch().getAddress());
                mTxtTel.setText(mParent.getSelectedBranch().getTel());
                break;
            case InspConstant.DIALOG_SELECT_INSP_CATETORY:
                mParent.setCategoryById(id);
                mTvType.setText("");
                mTvCategory.setText(value);
                break;
            case InspConstant.DIALOG_SELECT_INSP_TYPE:
                mParent.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mParent.setTypeById(id);
                    }
                });
                mTvType.setText(value);
                resetResults(mParent.getCase(), id);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDateTime(int type, String value) {
        String dateTime = mTvArrDate.getText().toString().trim() +
                mTvArrTime.getText().toString().trim();

        if (type == MyDateTimeDialog.DIALOG_DATE) {
            mTvArrDate.setText(value);
            dateTime = value + " " + mTvArrTime.getText().toString().trim();
        }

        if (type == MyDateTimeDialog.DIALOG_TIME) {
            mTvArrTime.setText(value);
            dateTime = mTvArrDate.getText().toString().trim() + " " + value;
        }

        mParent.saveDateTimeArrival(dateTime);
    }

    private void onShowDgDivision() {
        if (mDialogDivision != null && mDialogDivision.isShowing()) {
            return;
        }
        mDialogDivision = new MyCheckBoxDialog(getActivity(), this)
                .openDialog(InspConstant.DIALOG_SELECT_DIVISION,
                        getString(R.string.dialog_select_division_title),
                        mParent.loadDivisionsNameList(), getDefaultValue(mTvDivision.getText().toString()));
        if (mDialogDivision == null)
            return;
        mDialogDivision.show();
    }

    private void onShowDgBranch() {
        if (mDialogBranch != null && mDialogBranch.isShowing()) {
            return;
        }
        mDialogBranch = new MyCheckBoxDialog(getActivity(), this)
                .openDialog(InspConstant.DIALOG_SELECT_BRANCH,
                        getString(R.string.dialog_select_branch_title), mParent.loadBranchNameList(),
                        getDefaultValue(mTvBranch.getText().toString()));
        if (mDialogBranch == null)
            return;
        mDialogBranch.show();
    }

    private void onShowDgCategory() {
        if (mDialogCategory != null && mDialogCategory.isShowing()) {
            return;
        }
        mDialogCategory = new MyCheckBoxDialog(getActivity(), this)
                .openDialog(InspConstant.DIALOG_SELECT_INSP_CATETORY,
                        getString(R.string.dialog_select_category_title), mParent.loadCategoryNameList(),
                        getDefaultValue(mTvCategory.getText().toString()));
        if (mDialogCategory == null)
            return;
        mDialogCategory.show();
    }

    private void onShowDgType() {
        if (mDialogType != null && mDialogType.isShowing()) {
            return;
        }
        mDialogType = new MyCheckBoxDialog(getActivity(), this)
                .openDialog(InspConstant.DIALOG_SELECT_INSP_TYPE,
                        getString(R.string.dialog_select_type_title), mParent.loadTypeNameList(),
                        getDefaultValue(mTvType.getText().toString()));
        if (mDialogType == null)
            return;
        mDialogType.show();
    }


    public void showDateTimeDialog(int type) {
        MyDateTimeDialog dialog = new MyDateTimeDialog(getActivity(), this);
        if (type == MyDateTimeDialog.DIALOG_DATE) {
            dialog.openDateDialog();
        }
        if (type == MyDateTimeDialog.DIALOG_TIME) {
            dialog.openTimeDialog();
        }
    }


    private void resetResults(Case cse, int pos) {
        if (cse.getResults() != null) {
            Type cseType = cse.getType();
            Type selectType = mParent.getTypeById(pos);
            if (cseType.getLiveId() != selectType.getLiveId()) {
                cse.getResults().clear();
            }
        }
    }

    private String getDefaultValue(String currentValue) {
        if (currentValue == null) {
            return "";
        }
        return currentValue.trim();
    }

    private void clearViews() {
        mTvDivision.setText("");
        mTvBranch.setText("");
        mTvCategory.setText("");
        mTvType.setText("");
    }

    private void initHeaderTable() {
        LinearLayout row = new LinearLayout(getActivity());
        row.setOrientation(LinearLayout.VERTICAL);
        LinearLayout headerView = new LinearLayout(getActivity());
        headerView.setOrientation(LinearLayout.HORIZONTAL);
        int headerHeight = 90;
        headerView.addView(new IndexTableView(getActivity(), mNameWidth,
                headerHeight, "Category Name",
                IndexTableView.TABLE_TITLE_TYPE).createView(R.drawable.bg_index_blue_light));
        headerView.addView(new IndexTableView(getActivity(), mConductedWidth,
                headerHeight, "Conducted",
                IndexTableView.TABLE_TITLE_TYPE).createView(R.drawable.bg_index_blue_light));
        headerView.addView(new IndexTableView(getActivity(), mFailedWidth,
                headerHeight, "Failed",
                IndexTableView.TABLE_TITLE_TYPE).createView(R.drawable.bg_index_blue_light));
        row.addView(headerView);
        mLlTableHistory.addView(row);
    }

    private void initHistoryView() {
        initHeaderTable();
        for (int i = 0; i < mParent.getCategories().size(); i++) {
            Category category = mParent.getCategories().get(i);
            if (category.getStatistic() == null) {
                break;
            }
            initChildView(category, mLlTableHistory, i);
        }
    }


    private void initChildView(Category category, LinearLayout llRow, int position) {
        String name = category.getStatistic().getName();
        String conducted = category.getStatistic().getConducted() + "";
        String failed = category.getStatistic().getFailed() + "";
        LinearLayout row = new LinearLayout(getActivity());
        row.setOrientation(LinearLayout.VERTICAL);
        LinearLayout tableItem = new LinearLayout(getActivity());
        tableItem.setOrientation(LinearLayout.HORIZONTAL);

        int backgroundItem = R.drawable.bg_index_even;
        if (position % 2 == 0) {
            backgroundItem = R.drawable.bg_index_odd;
        }
        int rowHeight = 70;
        tableItem.addView(new IndexTableView(getActivity(), mNameWidth,
                rowHeight, name,
                IndexTableView.NAME_TITLE_TYPE).createView(backgroundItem));
        tableItem.addView(new IndexTableView(getActivity(), mConductedWidth,
                rowHeight, conducted,
                IndexTableView.NAME_TYPE).createView(backgroundItem));
        tableItem.addView(new IndexTableView(getActivity(), mFailedWidth,
                rowHeight, failed,
                IndexTableView.NAME_TYPE).createView(backgroundItem));
        row.addView(tableItem);
        llRow.addView(row);
    }
}
