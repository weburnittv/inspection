package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.view.IUpdateCheckListResultNoteListener;
import com.arise.healthcaresafety.view.custom.checklist.CheckListView;
import com.arise.healthcaresafety.view.custom.equipment.EquipmentView;

import java.util.ArrayList;
import java.util.List;


public class CheckListAdapter extends BaseExpandableListAdapter implements
        CheckListView.OnChecklistViewListener {
    private static final String TAG = CheckListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<CheckListGroup> checkListGroups;
    private CheckList checkList;
    private int viewMode;
    private IUpdateCheckListResultNoteListener listener = null;
    private OnCheckListAdapterListener adapterListener = null;

    public CheckListAdapter(Context context, CheckList checkList) {
        EquipmentView.isCheckState = false;
        this.context = context;
        this.checkList = checkList;
        if (this.checkList != null) {
            this.checkListGroups = checkList.getCheckListGroup();
        }
    }

    public CheckListAdapter(Context context, CheckList checkList, int viewMode) {
        EquipmentView.isCheckState = false;
        this.context = context;
        this.checkList = checkList;
        this.viewMode = viewMode;
        if (this.checkList != null) {
            this.checkListGroups = checkList.getCheckListGroup();
        }
    }

    public void setCheckList(CheckList checkList) {
        this.checkList = checkList;
        if (this.checkList != null) {
            this.checkListGroups = checkList.getCheckListGroup();
        }
    }

    @Override
    public int getGroupCount() {
        return checkListGroups != null ? checkListGroups.size() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.checkList == null
                && this.checkListGroups == null
                || this.checkListGroups.get(groupPosition) == null
                ? 0 : checkListGroups.get(groupPosition).getGroup().getQuestions().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return checkListGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<Question> item = checkListGroups.get(groupPosition).getGroup().getQuestions();
        return item.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        CheckListGroup checkListGroup = (CheckListGroup) getGroup(groupPosition);
        GroupViewHolder gViewHolder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.view_checklist_group, parent, false);
        gViewHolder = new GroupViewHolder();
        gViewHolder.img = (ImageView) rowView.findViewById(R.id.checkListGroup_img);
        gViewHolder.img_indicator = (ImageView) rowView.findViewById(R.id.checkListGroup_indicator);
        gViewHolder.tv_title = (TextView) rowView.findViewById(R.id.checkListGroup_title);
        gViewHolder.imgLine = (View) rowView.findViewById(R.id.viewLines);
        rowView.setTag(gViewHolder);
        if (checkListGroup.getGroup().getName().equals("Finding Scales")) {
            rowView.setVisibility(View.GONE);
            return rowView;
        }
        gViewHolder.tv_title.setText(checkListGroup.getGroup().getName());
        gViewHolder.tv_title.setTextColor(isExpanded ?
                context.getResources().getColor(R.color.colorPrimary) :
                context.getResources().getColor(R.color.black_text));
        gViewHolder.img.setImageResource(isExpanded ?
                R.drawable.ic_folder_select : R.drawable.ic_folder_unselect);
        gViewHolder.img_indicator.setImageResource(isExpanded ?
                R.drawable.ic_indicator_close : R.drawable.ic_indicator_open);
        return rowView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Question question = (Question) getChild(groupPosition, childPosition);
        if (question.getChildView() != null) {
            View tampView = question.getChildView();
            CheckListView checkListView = (CheckListView) tampView.findViewById(R.id.rowChildCheckList_chkLstView);
            checkListView.updateNoteView(question);
            return question.getChildView();
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tampView = inflater.inflate(R.layout.row_child_checklist, parent, false);
        CheckListView checkListView = (CheckListView) tampView.findViewById(R.id.rowChildCheckList_chkLstView);
        checkListView.setUpView(groupPosition, childPosition, question, viewMode);
        checkListView.setOnCheckListViewListener(this);

        question.setChildView(tampView);
        return tampView;
    }

    public void invalidateIndicatorIcon(boolean isExpand, int groupId) {
        CheckListGroup item = (CheckListGroup) getGroup(groupId);
        item.setIsExpand(isExpand);
        notifyDataSetChanged();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /* CALLBACK FROM CHECKLIST VIEW*/
    @Override
    public void onUpdateQuestion(Question question) {
        if (adapterListener != null) {
            if (question.getLiveId() == 0) {
                adapterListener.updateFinal(question);
            } else {
                adapterListener.updateQuestion(question);
            }
        }
    }

    @Override
    public void onCameraClick(int groupId, int childId) {
        Question question = (Question) getChild(groupId, childId);
        Note note = question.getNote();
        if (this.listener != null) {
            this.listener.OnOpenPicture(note);
        }
    }

    @Override
    public void onRecordClick(int groupId, int childId) {
        Question question = (Question) getChild(groupId, childId);
        Note note = question.getNote();
        if (this.listener != null) {
            this.listener.OnOpenAudio(note);
        }
    }

    @Override
    public void onSaveNoteContent(int groupId, int childId, String content) {
        Question question = (Question) getChild(groupId, childId);

        Note note = question.getNote();
        //set content for note
        question.getNote().setContent(content);

        if (this.listener != null) {
            this.listener.OnUpdateNoteContent(question.getNote());
        }
    }

    public void setUpdateCheckListResultListener(IUpdateCheckListResultNoteListener listener) {
        this.listener = listener;
    }

    public void setOnCheckListAdapterListener(OnCheckListAdapterListener listener) {
        this.adapterListener = listener;
    }


    public interface OnCheckListAdapterListener {
        void updateFinal(Question question);

        void updateQuestion(Question question);
    }

    static class GroupViewHolder {
        TextView tv_title;
        ImageView img_indicator;
        ImageView img;
        View imgLine;
    }
}
