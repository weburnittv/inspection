package com.arise.healthcaresafety.view.preview.caseinfo;


import android.content.Context;
import android.widget.LinearLayout;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.view.custom.IndexTableView;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;
import java.util.ArrayList;
import java.util.List;

public class ChildCaseInfoView extends AbsPreviewView {

    private int mWidthItemCase;
    private List<String> mInfoTitles, mInfoValues;
    private Case mCase;

    public ChildCaseInfoView(Context context, Case cse, int screenWidth) {
        super(context, screenWidth);
        this.mCase = cse;
        this.mWidthItemCase = mScreenWidth / 2;
    }

    @Override
    public void buildView() {
        this.linearLayout = this.getChildCaseInfo();
    }

    private LinearLayout getChildCaseInfo() {
        this.getInfoTitle();
        this.getInfoValues();
        LinearLayout childItemRow = new LinearLayout(mContext);
        childItemRow.setOrientation(LinearLayout.VERTICAL);
        int TOTAL_INFO_OF_CASE = 8;
        for (int i = 0; i < TOTAL_INFO_OF_CASE; i++) {
            LinearLayout tableHeader = new LinearLayout(mContext);
            tableHeader.setOrientation(LinearLayout.HORIZONTAL);

            String title = mInfoTitles.get(i);
            String value = mInfoValues.get(i);

            tableHeader.addView(new IndexTableView(mContext, this.mWidthItemCase,
                    Constants.HEIGHT_ITEM_TABLE, title,
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));
            tableHeader.addView(new IndexTableView(mContext, this.mWidthItemCase,
                    Constants.HEIGHT_ITEM_TABLE, value,
                    IndexTableView.TABLE_ITEM_TYPE).createView(R.drawable.bg_preview_item, true));
            childItemRow.addView(tableHeader);
        }

        return childItemRow;
    }

    private void getInfoTitle() {
        this.mInfoTitles = new ArrayList<>();
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_ref_no));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_branch));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_division));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_arrival_date));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_category));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_type));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_finding_result));
        this.mInfoTitles.add(mContext.getString(R.string.preview_title_officer));
    }

    private void getInfoValues() {
        this.mInfoValues = new ArrayList<>();

        String refNo = this.mCase.getReferenceNo();
        String branchName = this.mCase.getBranch() != null
                ? this.mCase.getBranch().getName() : null;
        String divisionName = this.mCase.getBranch() != null
                && this.mCase.getBranch().getDivision() != null
                ? this.mCase.getBranch().getDivision().getName() : null;
        String arrivalDate = this.mCase.getArrivalDate();
        String categoryName = this.mCase.getType() != null
                && this.mCase.getType().getCategory() != null
                ? this.mCase.getType().getCategory().getName() : null;
        String typeName = this.mCase.getType() != null
                ? this.mCase.getType().getName() : null;
        String findingResult = "";
        String officer = InspectionApp.getInstance().getUsername();

        this.mInfoValues.add(refNo != null ? refNo : "");
        this.mInfoValues.add(branchName != null ? branchName : "");
        this.mInfoValues.add(divisionName != null ? divisionName : "");
        this.mInfoValues.add(arrivalDate != null ? arrivalDate : "");
        this.mInfoValues.add(categoryName != null ? categoryName : "");
        this.mInfoValues.add(typeName != null ? typeName : "");
        this.mInfoValues.add(findingResult);
        this.mInfoValues.add(officer != null ? officer : "");
    }
}
