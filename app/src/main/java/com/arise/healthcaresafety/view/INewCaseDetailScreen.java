package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Result;

import java.util.List;

/**
 * Created by hnam on 7/2/2015.
 */
public interface INewCaseDetailScreen {
    void onSetUp(Case c);

    void onLoadDivisions(Divisions divisions);

    void onLoadCategories(Categories categories);

    void onSaveCase(boolean status);

    void onLoadResults(List<Result> results);

    void onLoadResult(Result results);


    void onError(int errorCode);

    void onSendEmailStatus(boolean status);

    //add new
    void onLoadStatus(boolean status);

    void onSaveNote(boolean status, Note note);
}
