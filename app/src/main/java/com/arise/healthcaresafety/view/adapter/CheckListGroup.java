package com.arise.healthcaresafety.view.adapter;

import android.view.View;

import com.arise.healthcaresafety.model.entities.Group;

/**
 * Created by Nam on 6/27/2015.
 */
public class CheckListGroup {

    private Group group;
    private View groupView;
    private boolean isExpand;


    public CheckListGroup() {
        this.groupView = null;
    }

    public CheckListGroup(Group group) {
        this.group = group;
        this.isExpand = false;
        this.groupView = null;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public View getGroupView() {
        return groupView;
    }

    public void setGroupView(View groupView) {
        this.groupView = groupView;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setIsExpand(boolean isExpand) {
        this.isExpand = isExpand;
    }
}
