package com.arise.healthcaresafety.view.custom.question;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.MyEditView;
import com.arise.healthcaresafety.view.custom.checklist.CheckListView;

public class QuestionTextEntry extends LinearLayout implements IQuestion {
    private static final String TAG = QuestionTextEntry.class.getSimpleName();
    private MyEditView edtNote;
    private Question question;
    private int viewMode;
    private CheckListView.OnChecklistViewListener listener = null;
    /*ime action for edtNote*/
    private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utils.getInstance().showOrHideKeyBoard((Activity) v.getContext(), false, v);
                String content = edtNote.getText().toString().trim();
                question.getResult().setTextEntry(content);
                if (listener != null) {
                    listener.onUpdateQuestion(question);
                }
                return true;
            }
            return false;
        }
    };

    public QuestionTextEntry(Context context, int viewMode) {
        super(context);
        this.viewMode = viewMode;
        initView(context);
    }

    public QuestionTextEntry(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_text_entry, this, true);
        edtNote = (MyEditView) view.findViewById(R.id.viewTextEntry_edt_note);
        edtNote.setEnabled(viewMode == InspConstant.MODE_EDIT);
        edtNote.setOnEditorActionListener(onEditorActionListener);
    }

    @Override
    public void setQuestion(Question question) {
        this.question = question;
        String content = question.getResult().getTextEntry();
        edtNote.setText(TextUtils.isEmpty(content) ? "" : content);
    }

    @Override
    public void setShowOrHide(int visibility) {
    }

    @Override
    public View getQuestionView() {
        return QuestionTextEntry.this;
    }

    @Override
    public void setOnCheckListViewListener(CheckListView.OnChecklistViewListener listener) {
        this.listener = listener;
    }
}
