package com.arise.healthcaresafety.view;

/**
 * Created by hnam on 7/19/2015.
 */
public interface IListPhotoScreen {
    public void onCopySuccess();

    public void onDeleteFile(boolean isSuccess);
}
