package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.Media;

/**
 * Created by Nam on 7/26/2015.
 */
public interface IListRecordScreen {
    void onDeleteOnServer(boolean isSuccess);

    void onDeleteOnLocal(boolean isSuccess);

    void onUploadItem(boolean isSuccess, Media item);

    void onSaveItem(boolean isSuccess, Media item);

    void onDeleteOffline(boolean isSuccess);
}
