package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Equipments;

/**
 * Created by Nam on 7/9/2015.
 */
public interface IPastCaseDetailScreen {
    public void onLoadPastCaseDetailScreen(Case c);

    public void onLoadEquipments(Equipments equipments);

    public void onLoadCheckList(CheckList checkList);

    void onSendEmailStatus(boolean status);
}
