package com.arise.healthcaresafety.view.slidingtab;

public interface SlidingTabListener {
    public void onSelectedTab(int position);

    public void onErrorCase();
}
