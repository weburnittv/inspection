package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.PastCases;

/**
 * Created by Nam on 7/9/2015.
 */
public interface IListPastCasesScreen {
    public void onLoadPastCasesScreen(PastCases pastCases);

}
