package com.arise.healthcaresafety.view.custom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.SplashActivity;
import com.arise.healthcaresafety.utils.Utils;


public class MyLogoutDialog {
    private static MyLogoutDialog mInstance = new MyLogoutDialog();

    public static MyLogoutDialog getInstance() {
        return mInstance;
    }

    /* show dialog */
    public void showLogoutDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle(R.string.title_logout);
        builder.setMessage(R.string.content_logout);
        builder.setCancelable(false);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(activity, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("ID", 1);
                Utils.resetUser(activity);
                activity.startActivity(intent);
                activity.finish(); // call this to finish the current activity
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}
