package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.arise.healthcaresafety.R;

/**
 * Created by Nam on 7/16/2015.
 */
public class PhotoGridMarginDecoration extends RecyclerView.ItemDecoration {
    private int margin;

    public PhotoGridMarginDecoration(Context context) {
        margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //super.getItemOffsets(outRect, view, parent, state);
        outRect.set(margin, margin, margin, margin);
    }
}
