package com.arise.healthcaresafety.view.adapter;

import com.arise.healthcaresafety.model.entities.Case;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nam on 6/27/2015.
 */
public class CaseGroup {
    private String name;
    private boolean isExpand;
    private List<Case> items;

    public CaseGroup() {
        this.items = new ArrayList<>();
    }

    public CaseGroup(String name) {
        this.name = name;
        this.isExpand = false;
        items = new ArrayList<>();
    }

    public CaseGroup(String name, List<Case> items) {
        this.name = name;
        this.isExpand = false;
        this.items = items;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Case> getItems() {
        return items;
    }

    public void setItems(List<Case> items) {
        this.items = items;
    }

    public void clearItems() {
        this.items.clear();
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setIsExpand(boolean isExpand) {
        this.isExpand = isExpand;
    }


}
