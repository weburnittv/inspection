package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.model.entities.Media;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class PhotoGridAdapter extends RecyclerView.Adapter<PhotoGridAdapter.MyImageViewHolder> {
    private final int OPTION_PAINT = 0;
    private final int OPTION_DELETE = 1;
    public Media media = null;
    private List<Media> mItems;
    private Context mContext;
    private PhotoGridListener mListener = null;

    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            Media item = (Media) v.getTag();
            media = item;
            showFunctionDialog(item);
            return false;
        }
    };

    public PhotoGridAdapter(Context context, List<Media> list) {
        this.mContext = context;
        this.mItems = list;
    }

    public void addMedia(Media media) {
        boolean isCheck = false;
        for (Media media1 : mItems) {
            if (media1.getLiveId() == media.getLiveId()) {
                isCheck = true;
                break;
            }
        }
        if (!isCheck) {
            this.mItems.add(media);
            notifyDataSetChanged();
        }
    }

    public void deleteMedia(Media media) {
        this.mItems.remove(media);
        notifyDataSetChanged();
    }


    public Media getLastRecord() {
        if (this.mItems == null) {
            return null;
        }

        if (this.mItems.size() == 0) {
            return null;
        }
        //return last record
        return this.mItems.get(mItems.size() - 1);
    }

    /* get record */
    public Media getMedia(String localPath) {
        for (Media item : mItems) {
            if (item.getLocalPath().equalsIgnoreCase(localPath)) {
                return item;
            }
        }
        return null;
    }

    /**
     * get upload media
     *
     * @return
     */
    public ArrayList<Media> getUploadedMedia() {
        if (this.mItems == null) {
            return null;
        }
        ArrayList<Media> list = new ArrayList<>();
        if (Delegation.getInstance().isOnline()) {
            for (Media m : this.mItems) {
                if (m.getLiveId() > 0) {
                    list.add(m);
                }
            }
        } else {
            for (Media m : this.mItems) {
                list.add(m);
            }
        }
        return list;
    }

    @Override
    public MyImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_item, parent, false);
        return new MyImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyImageViewHolder holder, int position) {
        Media item = this.mItems.get(position);
        if (item != null && item.getLocalPath() != null) {
            holder.image.setImageBitmap(decodeAndResizeFile(new File(item.getLocalPath())));
            holder.image.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.image.setTag(item);
            holder.image.setOnLongClickListener(onLongClickListener);
        }
    }

    public Bitmap decodeAndResizeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inPreferredConfig = Bitmap.Config.ARGB_8888;

            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 1000;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            // o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public void showFunctionDialog(final Media item) {
        CharSequence[] items;
        items = this.mContext.getResources().getStringArray(R.array.online_functions);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
        builder.setTitle(item.getLocalName());
        builder.setCancelable(true);
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                if (index == OPTION_PAINT) {
                    //open paint
                    PhotoGridAdapter.this.mListener.paint(item);
                } else if (index == 2) {
                    deleteMedia(item);
                    PhotoGridAdapter.this.mListener.delete(item);
                }
                dialog.dismiss();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void setGridPhotoListener(PhotoGridListener listener) {
        this.mListener = listener;
    }

    public interface PhotoGridListener {
        void upload(Media item);

        void paint(Media item);

        void delete(Media item);
    }

    public static class MyImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;

        public MyImageViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.viewPhoto_image);
        }
    }
}
