package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.PastCases;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nam on 4/13/2015.
 */
public class CaseListAdapter extends BaseExpandableListAdapter {
    private static final String TAG = CaseListAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private Context context;
    private PastCases pastCases;
    private ArrayList<CaseGroup> caseGroups = null;
    private OnClickChildItemListener listener = null;

    public CaseListAdapter(Context context, PastCases pastCases) {
        this.context = context;
        this.pastCases = pastCases;
        inflater = LayoutInflater.from(context);
        if (this.pastCases != null) {
            this.caseGroups = this.pastCases.getPastCaseGroup();
        }
    }

    public void setArrData(PastCases pastCases) {
        this.pastCases = pastCases;
        if (this.pastCases != null) {
            this.caseGroups = this.pastCases.getPastCaseGroup();
            notifyDataSetChanged();
        }
    }

    @Override
    public int getGroupCount() {
        if (this.caseGroups == null) {
            return 0;
        }
        return this.caseGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (this.caseGroups == null) {
            return 0;
        }
        return this.caseGroups.get(groupPosition).getItems().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return caseGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<Case> item = caseGroups.get(groupPosition).getItems();
        return item.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        CaseGroup caseGroup = (CaseGroup) getGroup(groupPosition);
        View rowView = convertView;
        GroupViewHolder gViewHolder;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.view_equipment_group, parent, false);
            gViewHolder = new GroupViewHolder();
            gViewHolder.img_indicator = (ImageView) rowView.findViewById(R.id.equipGroup_indicator);
            gViewHolder.tv_title = (TextView) rowView.findViewById(R.id.equipGroup_title);
            rowView.setTag(gViewHolder);
        } else {
            gViewHolder = (GroupViewHolder) rowView.getTag();
        }
        String title = caseGroup.getName() +
                " (" + String.valueOf(caseGroup.getItems().size()) + ")";
        gViewHolder.tv_title.setText(title);
        gViewHolder.tv_title.setTextColor(caseGroup.isExpand() ?
                context.getResources().getColor(R.color.colorPrimary) :
                context.getResources().getColor(R.color.black_text));

        gViewHolder.img_indicator.setImageResource(caseGroup.isExpand() ?
                R.drawable.ic_indicator_close : R.drawable.ic_indicator_open);
        return rowView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final Case item = (Case) getChild(groupPosition, childPosition);

        View rowView = convertView;
        ChildViewHolder cViewHolder;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.view_case_item, parent, false);
            cViewHolder = new ChildViewHolder();
            cViewHolder.txtIndex = (MyTextView) rowView.findViewById(R.id.viewCaseItem_txt_index);
            cViewHolder.txtDivision = (MyTextView) rowView.findViewById(R.id.viewCaseItem_txt_divisions);
            cViewHolder.txtBranch = (MyTextView) rowView.findViewById(R.id.viewCaseItem_txt_branch);
            cViewHolder.txtCreated = (MyTextView) rowView.findViewById(R.id.viewCaseItem_txt_createdDate);
            cViewHolder.tvRefNo = (MyTextView) rowView.findViewById(R.id.viewCaseItem_txt_ref_no);
            rowView.setTag(cViewHolder);
        } else {
            cViewHolder = (ChildViewHolder) rowView.getTag();
        }
        cViewHolder.txtIndex.setText(String.valueOf(childPosition + 1));
        cViewHolder.txtIndex.setTag(item.getLiveId());
        cViewHolder.txtDivision.setText(item.getBranch().getDivision().getName());
        cViewHolder.txtBranch.setText(item.getBranch().getName());
        cViewHolder.txtCreated.setText(item.getCreatedDate());
        cViewHolder.tvRefNo.setText(item.getReferenceNo());
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickChildItem(item.getId());
                }
            }
        });

        return rowView;
    }

    public void invalidateIndicatorIcon(boolean isExpand, int groupId) {
        CaseGroup item = (CaseGroup) getGroup(groupId);
        item.setIsExpand(isExpand);
        notifyDataSetChanged();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setOnClickChildItemListener(OnClickChildItemListener listener) {
        this.listener = listener;
    }

    public interface OnClickChildItemListener {
        void onClickChildItem(String caseId);
    }

    static class GroupViewHolder {
        TextView tv_title;
        ImageView img_indicator;
    }

    static class ChildViewHolder {
        MyTextView txtIndex;
        MyTextView txtDivision;
        MyTextView txtBranch;
        MyTextView txtCreated;
        MyTextView tvRefNo;
    }
}
