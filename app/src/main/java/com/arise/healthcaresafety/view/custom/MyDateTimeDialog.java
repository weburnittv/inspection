package com.arise.healthcaresafety.view.custom;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by hnam on 7/5/2015.
 */
public class MyDateTimeDialog {
    public static final int DIALOG_TIME = 0;
    public static final int DIALOG_DATE = 1;
    private Activity activity;
    private OnMyTimeDateDialog listener;


    public MyDateTimeDialog(Activity activity, OnMyTimeDateDialog listener) {
        this.activity = activity;
        this.listener = listener;
    }

    /**
     * open Date dialog to chose date
     */
    public void openDateDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int month = monthOfYear + 1;
                        String date = year +
                                "-" +
                                (month < 10 ? ("0" + month) : (month)) +
                                "-" +
                                (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth));
                        listener.onDateTime(DIALOG_DATE, date);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    /**
     * open Time dialog to chose time
     */
    public void openTimeDialog() {
        // Process to get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog tpd = new TimePickerDialog(activity,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String time = (hourOfDay < 10 ? ("0" + hourOfDay) : (hourOfDay)) +
                                ":" +
                                (minute < 10 ? ("0" + minute) : (minute)) +
                                ":00";
                        listener.onDateTime(DIALOG_TIME, time);
                    }
                }, mHour, mMinute, false);
        tpd.show();
    }

    public interface OnMyTimeDateDialog {
        public void onDateTime(int type, String value);
    }
}
