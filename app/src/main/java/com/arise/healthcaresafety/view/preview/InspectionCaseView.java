package com.arise.healthcaresafety.view.preview;


import android.content.Context;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;
import com.arise.healthcaresafety.view.preview.caseinfo.CaseInfoView;
import com.arise.healthcaresafety.view.preview.checklist.CheckListView;
import com.arise.healthcaresafety.view.preview.equipment.EquipmentView;
import com.arise.healthcaresafety.view.preview.interfaces.PreviewInterface;

public class InspectionCaseView extends AbsPreviewView {

    private Case mCase;

    public InspectionCaseView(Context context, Case cse, int screenWidth) {
        super(context, screenWidth);
        this.mCase = cse;
    }

    @Override
    public void buildView() {
        this.addView(new TitleView(mContext, mScreenWidth, mContext.getString(R.string.preview_title_case_info)));
        this.addView(new CaseInfoView(mContext, this.mCase, this.mScreenWidth));

        this.addView(new TitleView(mContext, mScreenWidth, mContext.getString(R.string.preview_title_equipment)));
        this.addView(new EquipmentView(mContext, this.mCase.getEquipments(), this.mScreenWidth));

        this.addView(new TitleView(mContext, mScreenWidth, mContext.getString(R.string.preview_title_checklist)));
        this.addView(new CheckListView(mContext, this.mCase.getCheckList(), this.mScreenWidth));

        for (PreviewInterface view : this.getViews()) {
            this.linearLayout.addView(view.getLayout());
        }
    }
}
