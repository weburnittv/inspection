package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.view.IUpdateResultNoteListener;
import com.arise.healthcaresafety.view.custom.equipment.EquipmentView;

import java.util.ArrayList;
import java.util.List;


public class EquipmentListAdapter extends BaseExpandableListAdapter implements EquipmentView.OnEquipmentViewListener {
    private Context context;
    private Equipments equipments;
    private ArrayList<EquipmentGroup> equipGroups;
    private int mode = InspConstant.MODE_EDIT;
    private IUpdateResultNoteListener listener;

    public EquipmentListAdapter(Context context, Equipments equipments) {
        this.context = context;
        this.equipments = equipments;
        if (this.equipments != null) {
            this.equipGroups = this.equipments.getEquipmentGroup();
        }
    }

    public EquipmentListAdapter(Context context, Equipments equipments, int mode) {
        this.context = context;
        this.equipments = equipments;
        this.mode = mode;
        if (this.equipments != null) {
            this.equipGroups = this.equipments.getEquipmentGroup();
        }
    }

    public void setArrData(Equipments equipments) {
        this.equipments = equipments;
        if (this.equipments != null) {
            this.equipGroups = this.equipments.getEquipmentGroup();
        }
    }

    @Override
    public int getGroupCount() {
        if (this.equipGroups == null) {
            return 0;
        }
        return this.equipGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (this.equipGroups == null) {
            return 0;
        }
        if (this.equipGroups.get(groupPosition) == null) {
            return 0;
        }
        return this.equipGroups.get(groupPosition).getItems().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.equipGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<Equipment> item = this.equipGroups.get(groupPosition).getItems();
        return item.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        EquipmentGroup equipGroup = (EquipmentGroup) getGroup(groupPosition);
        View rowView = convertView;
        GroupViewHolder gViewHolder;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.view_equipment_group, parent, false);
            gViewHolder = new GroupViewHolder();
            gViewHolder.img_indicator = (ImageView) rowView.findViewById(R.id.equipGroup_indicator);
            gViewHolder.tv_title = (TextView) rowView.findViewById(R.id.equipGroup_title);
            rowView.setTag(gViewHolder);
        } else {
            gViewHolder = (GroupViewHolder) rowView.getTag();
        }
        gViewHolder.tv_title.setText(equipGroup.getName());
        gViewHolder.tv_title.setTextColor(isExpanded ?
                context.getResources().getColor(R.color.colorPrimary) :
                context.getResources().getColor(R.color.black_text));

        gViewHolder.img_indicator.setImageResource(isExpanded ?
                R.drawable.ic_indicator_close : R.drawable.ic_indicator_open);
        return rowView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Equipment item = (Equipment) getChild(groupPosition, childPosition);

        View rowView = convertView;
        ChildViewHolder cViewHolder;
        if (rowView == null) {
            EquipmentView equipView = new EquipmentView(context, mode);
            equipView.setOnEquipmentViewListener(this);
            rowView = equipView;
            cViewHolder = new ChildViewHolder();
            cViewHolder.equipView = equipView;
            rowView.setTag(cViewHolder);
        } else {
            cViewHolder = (ChildViewHolder) rowView.getTag();
        }
        cViewHolder.equipView.setIdentity(groupPosition, childPosition, item);
        return rowView;
    }

    public void invalidateIndicatorIcon(boolean isExpand, int groupId) {
        EquipmentGroup item = (EquipmentGroup) getGroup(groupId);
        item.setIsExpand(isExpand);
        notifyDataSetChanged();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setOnEquipItemListener(IUpdateResultNoteListener listener) {
        this.listener = listener;
    }

    /* callback equipment view */
    @Override
    public void onCheckBoxListener(int groupPosition, int childPosition, boolean isChecked) {
        //update result of checkList
        Equipment item = (Equipment) getChild(groupPosition, childPosition);

        item.setChecked(isChecked);
        if (this.listener != null) {
            this.listener.OnUpdateEquipment(item);
        }
    }

    @Override
    public void onCameraClick(int groupPosition, int childPosition) {
        Equipment item = (Equipment) getChild(groupPosition, childPosition);

        if (this.listener != null) {
            this.listener.OpenPicture(item.getNote());
        }
    }

    @Override
    public void onMicClick(int groupPosition, int childPosition) {
        Equipment item = (Equipment) getChild(groupPosition, childPosition);
        if (this.listener != null) {
            this.listener.OpenAudio(item.getNote());
        }
    }

    @Override
    public void onSaveNoteContent(int groupPosition, int childPosition) {
        Equipment item = (Equipment) getChild(groupPosition, childPosition);

        if (this.listener != null) {
            this.listener.OnUpdateNoteContent(item.getNote());
        }
    }

    static class GroupViewHolder {
        TextView tv_title;
        ImageView img_indicator;
    }

    static class ChildViewHolder {
        EquipmentView equipView;
    }
}
