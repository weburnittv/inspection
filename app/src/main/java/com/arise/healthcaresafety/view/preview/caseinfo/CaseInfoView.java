package com.arise.healthcaresafety.view.preview.caseinfo;


import android.content.Context;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;
import com.arise.healthcaresafety.view.preview.interfaces.PreviewInterface;

public class CaseInfoView extends AbsPreviewView {

    private Case mCase;

    public CaseInfoView(Context context, Case cse, int screenWidth) {
        super(context, screenWidth);
        this.mCase = cse;
    }

    @Override
    public void buildView() {
        this.buildChildView();
        for (PreviewInterface item : this.getViews()) {
            this.linearLayout.addView(item.getLayout());
        }
    }

    private void buildChildView() {
        this.addView(new HeaderCaseInfoView(mContext, mScreenWidth + 10));
        this.addView(new ChildCaseInfoView(mContext, mCase, mScreenWidth + 10));
    }
}
