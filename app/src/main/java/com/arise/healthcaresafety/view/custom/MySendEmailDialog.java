package com.arise.healthcaresafety.view.custom;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.utils.EmailValidator;
import com.arise.healthcaresafety.utils.Utils;

public class MySendEmailDialog {
    private Activity activity;
    private MySendEmailDialogListener listener;

    public MySendEmailDialog(Activity activity, MySendEmailDialogListener listener) {
        this.activity = activity;
        this.listener = listener;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.MyAlertDialogStyle);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_send_email, null);
        final EditText etEmail = (EditText) view.findViewById(R.id.send_email_et_email);
        builder.setView(view)
                .setTitle("Send to")
                .setCancelable(false)
                .setPositiveButton("SEND", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String email = etEmail.getText().toString().trim();
                        //check edt is null
                        if (TextUtils.isEmpty(email)) {
                            Utils.getInstance().showMessage(activity, "Please input Email !");
                            return;
                        }

                        //check edt is format email
                        EmailValidator check = new EmailValidator();
                        if (!check.validate(email)) {
                            Utils.getInstance().showMessage(activity, email + "is not email format");
                            return;
                        }

                        if (MySendEmailDialog.this.listener != null) {
                            MySendEmailDialog.this.listener.onClickSendEmail(email);
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        //create dialog
        AlertDialog dialog = builder.create();
        //show dialog
        dialog.show();
    }


    public interface MySendEmailDialogListener {
        void onClickSendEmail(String email);
    }
}
