package com.arise.healthcaresafety.view.preview.caseinfo;


import android.content.Context;
import android.widget.LinearLayout;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.view.custom.IndexTableView;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;

public class HeaderCaseInfoView extends AbsPreviewView {

    private int mWidthItemCase;

    public HeaderCaseInfoView(Context context, int screenWidth) {
        super(context, screenWidth);
        this.mWidthItemCase = mScreenWidth / 2;
    }


    private LinearLayout getHeaderCaseInfo() {
        LinearLayout headerRow = new LinearLayout(mContext);
        headerRow.setOrientation(LinearLayout.VERTICAL);
        LinearLayout headerColumn = new LinearLayout(mContext);
        headerColumn.setOrientation(LinearLayout.HORIZONTAL);
        int headerHeight = 70;
        headerColumn.addView(new IndexTableView(mContext, this.mWidthItemCase,
                headerHeight, mContext.getString(R.string.preview_title_fields),
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        headerColumn.addView(new IndexTableView(mContext, this.mWidthItemCase,
                headerHeight, mContext.getString(R.string.preview_title_info),
                IndexTableView.TABLE_HEADER_TYPE).createView(R.drawable.bg_preview_item_header));
        headerRow.addView(headerColumn);
        return headerRow;
    }

    @Override
    public void buildView() {
        this.linearLayout = this.getHeaderCaseInfo();
    }
}
