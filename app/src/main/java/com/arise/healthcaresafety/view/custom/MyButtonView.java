package com.arise.healthcaresafety.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import com.arise.healthcaresafety.R;


/**
 * Created by hnam on 6/17/2015.
 */
public class MyButtonView extends Button {
    public MyButtonView(Context context) {
        super(context);
    }

    public MyButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);

            Typeface tf;
            tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "Roboto-Regular.ttf");
            setTypeface(tf);
            setGravity(Gravity.CENTER);
            setPadding(getResources().getDimensionPixelSize(R.dimen.element_margin_8)
                    , getResources().getDimensionPixelSize(R.dimen.element_margin_8)
                    , getResources().getDimensionPixelSize(R.dimen.element_margin_8)
                    , getResources().getDimensionPixelSize(R.dimen.element_margin_8));
            a.recycle();
        }
    }


}
