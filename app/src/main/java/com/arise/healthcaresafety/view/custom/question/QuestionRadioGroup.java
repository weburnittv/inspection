package com.arise.healthcaresafety.view.custom.question;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Attribute;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Variation;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.MyCheckBox;
import com.arise.healthcaresafety.view.custom.MyRadioButton;
import com.arise.healthcaresafety.view.custom.checklist.CheckListView;


public class QuestionRadioGroup extends LinearLayout implements IQuestion {
    private static final String TAG = QuestionRadioGroup.class.getSimpleName();
    //private boolean isMultiple = false; // false: single -- true: multiple
    //private MyRadioButton[] mRadioButtons;
    private CompoundButton[] mCompoundButtons;
    private LinearLayout ll;


    private int mType;
    private String mContent;
    //private GridView mGridView;
    private int viewMode;
    private int type;
    private Variation variation = null;
    private Question question;
    private CheckListView.OnChecklistViewListener listener = null;
    private boolean isSetupQuestion = false;
    /* listener for compound button*/
    private CompoundButton mSelectedComBtn = null;
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d("click", "bbb");
            if (type == Question.SINGLE) {
                // select checkbox on single Attribute
                if (mSelectedComBtn != null) {
                    mSelectedComBtn.setOnCheckedChangeListener(null);
                    mSelectedComBtn.setChecked(false);
                    mSelectedComBtn.setOnCheckedChangeListener(this);
                }

                mSelectedComBtn = buttonView;
                //add attribute to result
                Attribute attr = question.getVariation().getAttributes().get(mSelectedComBtn.getId());
                question.addAttribute(attr);
                if (listener != null) {
                    listener.onUpdateQuestion(question);
                }
            } else if (type == Question.MULTI) {
                mSelectedComBtn = buttonView;
                //add attribute to result
                Attribute attr = question.getVariation().getAttributes().get(mSelectedComBtn.getId());
                if (isChecked) question.addAttribute(attr);
                else question.removeAttribute(attr);

                if (listener != null) {
                    listener.onUpdateQuestion(question);
                }
            }
        }
    };

    public QuestionRadioGroup(Context context, int mode) {
        super(context);
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.viewMode = mode;
    }

    @Override
    public void setQuestion(Question question) {
        this.question = question;
        this.variation = question.getVariation();
        this.type = variation.getType();
        addViewBasedOnType(this.type);
    }

    @Override
    public void setShowOrHide(int visibility) {
        setVisibility(visibility);
    }

    @Override
    public View getQuestionView() {
        return QuestionRadioGroup.this;
    }

    @Override
    public void setOnCheckListViewListener(CheckListView.OnChecklistViewListener listener) {
        this.listener = listener;
    }

    private void addViewBasedOnType(int type) {
        ll = new LinearLayout(getContext());
        ll.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ll.setOrientation(VERTICAL);
        addView(ll);
        if (type == Question.SINGLE) {
            addSingleSelectedView();
        } else if (type == Question.MULTI) {
            addMultiSelectedView();
        }
    }

    private void addMultiSelectedView() {
        mCompoundButtons = new CompoundButton[variation.getAttributes().size()];
        addCheckBoxBtn(0);
    }

    private void addSingleSelectedView() {
        mCompoundButtons = new CompoundButton[variation.getAttributes().size()];
        addRadioBtn(0);
    }

    private boolean isSelected(Attribute attr) {
        for (Attribute selected : this.question.getResult().getAttributes()) {
            if (attr.getLiveId() == selected.getLiveId())
                return true;
        }
        return false;
    }

    private void addRadioBtn(int offset) {
        int tamp = offset;
        LinearLayout row = new LinearLayout(getContext());
        row.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        row.setOrientation(HORIZONTAL);
        ll.addView(row);
        int length = 0;

        boolean isNewLine;
        do {
            mCompoundButtons[tamp] = new MyRadioButton(getContext());
            mCompoundButtons[tamp].setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mCompoundButtons[tamp].setText(this.variation.getAttributes().get(tamp).getName());
            mCompoundButtons[tamp].setTextColor(getResources().getColor(R.color.black_text));
            mCompoundButtons[tamp].measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            boolean isSelected = this.isSelected(this.variation.getAttributes().get(tamp));
            mCompoundButtons[tamp].setChecked(isSelected);
            if (isSelected)
                mSelectedComBtn = mCompoundButtons[tamp];
            mCompoundButtons[tamp].setId(tamp);
            mCompoundButtons[tamp].setOnCheckedChangeListener(onCheckedChangeListener);//set event

            int tLength = length + mCompoundButtons[tamp].getMeasuredWidth();
            if (tLength <= Utils.getInstance().getScreenWidth(getContext())) {
                //continue add radio button to this row
                isNewLine = false;
                row.addView(mCompoundButtons[tamp]);
                length += mCompoundButtons[tamp].getMeasuredWidth();
                tamp++;
                if (tamp >= variation.getAttributes().size()) {
                    break;
                }
            } else {
                isNewLine = true;
            }
        } while (!isNewLine);


        if (isNewLine) {
            //add new line
            //Log.e(TAG,"add new line");
            addRadioBtn(tamp);
        } else {
            //add successfully
            return;
        }
    }

    private void addCheckBoxBtn(int offset) {
        int tamp = offset;
        LinearLayout row = new LinearLayout(getContext());
        row.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        row.setOrientation(HORIZONTAL);
        ll.addView(row);
        //int parentWidth = getMeasuredWidth();
        int length = 0;
        boolean isNewLine;
        do {
            mCompoundButtons[tamp] = new MyCheckBox(getContext());
            mCompoundButtons[tamp].setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mCompoundButtons[tamp].setText(variation.getAttributes().get(tamp).getName());
            boolean isSelected = this.question.getResult().isSelected(this.variation.getAttributes().get(tamp));
            mCompoundButtons[tamp].setChecked(isSelected);
            mCompoundButtons[tamp].setTextColor(getResources().getColor(R.color.black_text));
            mCompoundButtons[tamp].measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mCompoundButtons[tamp].setId(tamp);
            mCompoundButtons[tamp].setOnCheckedChangeListener(onCheckedChangeListener);//set event
            int tLength = length + mCompoundButtons[tamp].getMeasuredWidth();
            //Log.e(TAG,"CheckBox tamp:" + tLength + "CheckBox screenWitdh:" + Utils.getInstance().getScreenWidth(getContext()));
            if (tLength <= Utils.getInstance().getScreenWidth(getContext())) {
                //continue add radio button to this row
                isNewLine = false;
                row.addView(mCompoundButtons[tamp]);
                length += mCompoundButtons[tamp].getMeasuredWidth();
                tamp++;
                if (tamp >= variation.getAttributes().size()) {
                    break;
                }
            } else {
                isNewLine = true;
            }
        } while (!isNewLine);

        if (isNewLine) {
            //add new line
            addCheckBoxBtn(tamp);
        } else {
            //add successfully
            return;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }
}
