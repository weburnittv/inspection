package com.arise.healthcaresafety.view.preview.equipment;


import android.content.Context;

import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.view.preview.abstracts.AbsPreviewView;
import com.arise.healthcaresafety.view.preview.interfaces.PreviewInterface;

public class EquipmentView extends AbsPreviewView {

    private Equipments mEquipments;

    public EquipmentView(Context context, Equipments equipments, int screenWidth) {
        super(context, screenWidth);
        this.mEquipments = equipments;
    }

    @Override
    public void buildView() {
        this.buildLayoutView();
    }

    private void buildLayoutView() {
        this.addView(new ChildEquipmentView(mContext, mScreenWidth + 10, mEquipments));

        for (PreviewInterface item : this.getViews()) {
            this.linearLayout.addView(item.getLayout());
        }
    }
}
