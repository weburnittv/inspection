package com.arise.healthcaresafety.view;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Note;


public interface ITabScreenListener {
    void onSendEmailStatus(boolean status);

    void bindingNote(Note note);

    void updateCase(Case mCase);
}
