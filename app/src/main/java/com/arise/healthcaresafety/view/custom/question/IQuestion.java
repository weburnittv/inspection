package com.arise.healthcaresafety.view.custom.question;

import android.view.View;

import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.view.custom.checklist.CheckListView;

/**
 * Created by hnam on 6/23/2015.
 */
public interface IQuestion {
    public void setQuestion(Question question);


    /**
     * set visibility
     *
     * @param visibility
     */
    public void setShowOrHide(int visibility);

    /**
     * return view
     *
     * @return view
     */
    public View getQuestionView();

    public void setOnCheckListViewListener(CheckListView.OnChecklistViewListener listener);
}
