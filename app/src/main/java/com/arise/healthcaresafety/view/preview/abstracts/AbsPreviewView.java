package com.arise.healthcaresafety.view.preview.abstracts;


import android.content.Context;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.view.preview.interfaces.PreviewInterface;

import java.util.ArrayList;
import java.util.List;

public abstract class AbsPreviewView implements PreviewInterface {
    protected Context mContext;
    protected LinearLayout linearLayout;
    protected int mScreenWidth;

    private List<PreviewInterface> views = new ArrayList<>();

    public AbsPreviewView(Context context, int screenWidth) {
        this.mContext = context;
        this.mScreenWidth = screenWidth - 10;
        this.linearLayout = new LinearLayout(mContext);
        this.linearLayout.setOrientation(LinearLayout.VERTICAL);
        this.views = new ArrayList<PreviewInterface>();
    }

    public final void addView(AbsPreviewView view) {
        view.buildView();
        views.add(view);
    }

    public final LinearLayout getLayout() {
        return linearLayout;
    }

    public List<PreviewInterface> getViews() {
        return this.views;
    }

    public abstract void buildView();
}
