package com.arise.healthcaresafety.view.custom.checklist;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.custom.MyEditView;
import com.arise.healthcaresafety.view.custom.MyTextView;


public class NoteView extends LinearLayout {
    private static final String TAG = NoteView.class.getSimpleName();
    Context context;
    private ImageView mImgCamera;
    private ImageView mImgMic;
    private MyTextView mTxtNoImg;
    private MyTextView mTxtNoRecord;
    private MyEditView mEdtNote;
    private String mContent;
    private Question mQuestion;
    private OnSaveNoteListener mSaveNoteListener;
    /*set listener for note view*/
    private OnQuestionNoteViewListener listener = null;
    private View.OnClickListener onCameraClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (listener != null) {
                Log.d(TAG, "onCameraClick");
                listener.onCameraClick();
            }
        }
    };
    private View.OnClickListener onMicClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onRecordClick();
            }
        }
    };
    private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utils.getInstance().showOrHideKeyBoard((Activity) v.getContext(), false, v);
                listener.onSaveNote(mContent);
                return true;
            }
            return false;
        }
    };

    public NoteView(Context context, Question question, OnSaveNoteListener listener) {
        super(context);
        this.context = context;
        this.mSaveNoteListener = listener;
        this.mQuestion = question;
        initView(context);
    }

    public NoteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(context);
    }

    private void initView(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_note, this, true);
        mImgCamera = (ImageView) view.findViewById(R.id.viewNote_img_camera);
        mImgMic = (ImageView) view.findViewById(R.id.viewNote_img_mic);
        mTxtNoImg = (MyTextView) view.findViewById(R.id.viewNote_txt_noImg);
        mTxtNoRecord = (MyTextView) view.findViewById(R.id.viewNote_txt_noRecord);
        mEdtNote = (MyEditView) view.findViewById(R.id.viewNote_edt_note);
        initHandleEvent();
    }

    private void initHandleEvent() {
        mImgCamera.setOnClickListener(onCameraClick);
        mImgMic.setOnClickListener(onMicClick);
        mEdtNote.setOnEditorActionListener(onEditorActionListener);
        mEdtNote.setOnFocusChangeListener(focus);
        mEdtNote.addTextChangedListener(watcher);
        mEdtNote.setHorizontallyScrolling(false);
    }


    /**
     * update UI due to question changed
     *
     * @param question
     */
    public void updateUI(Question question) {
        if (question.getNote() != null) {
            mTxtNoImg.setText(String.valueOf(question.getNote().getNotePhotoMedias().size()));
            mTxtNoRecord.setText(String.valueOf(question.getNote().getNoteSoundMedias().size()));
            mEdtNote.setText(question.getNote().getContent());
        } else {
            mTxtNoImg.setText("0");
            mTxtNoRecord.setText("0");
            mEdtNote.setText("");
        }
    }

    public void setOnQuestionNoteViewListener(OnQuestionNoteViewListener listener) {
        this.listener = listener;
    }

    public interface OnQuestionNoteViewListener {
        void onCameraClick();

        void onRecordClick();

        void onSaveNote(String key);
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mEdtNote.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable s) {
            mContent = s.toString();
        }
    };

    private OnFocusChangeListener focus = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && mContent.length() > 0) {
                Log.e("NoteContent", mContent);
                mQuestion.getNote().setContent(mContent);
                mSaveNoteListener.onSaveNoteContent(mQuestion);
            }
        }
    };

    public interface OnSaveNoteListener {
        void onSaveNoteContent(Question question);
    }
}
