package com.arise.healthcaresafety.view.custom;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.utils.Utils;

import java.util.List;


public class MyCheckBoxDialog {
    private Activity activity;
    private OnMyCheckBoxDialog listener;
    private int selectedIndex = -1;


    public MyCheckBoxDialog(Activity activity, OnMyCheckBoxDialog listener) {
        this.activity = activity;
        this.listener = listener;
    }

    public AlertDialog openDialog(int type, String title, final List<String> list, String defaultValue) {
        if (list == null) {
            showNotifyListNull(type);
            return null;
        }

        if (list.size() == 0) {
            showNotifyListEmpty(type);
            return null;
        }
        final int mType = type;
        final CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        int selectedItemPosition = -1;
        for (int i = 0; i < items.length; i++) {
            if (defaultValue == items[i]) {
                selectedItemPosition = i;
                break;
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setSingleChoiceItems(items, selectedItemPosition, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                selectedIndex = index;
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (selectedIndex == -1) {
                    dialog.dismiss();
                    return;
                }
                listener.onSelectCheckBoxDialog(mType, selectedIndex, items[selectedIndex].toString());
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    public void showNotifyListNull(int type) {
        if (type == InspConstant.DIALOG_SELECT_BRANCH) {
            Utils.getInstance().showMessage(activity, "Please select Divisions");
        }

        if (type == InspConstant.DIALOG_SELECT_INSP_TYPE) {
            Utils.getInstance().showMessage(activity, "Please select Inspection Category");
        }
    }

    public void showNotifyListEmpty(int type) {
        if (type == InspConstant.DIALOG_SELECT_BRANCH) {
            Utils.getInstance().showMessage(activity, "no Branch in this division");
        }

        if (type == InspConstant.DIALOG_SELECT_INSP_TYPE) {
            Utils.getInstance().showMessage(activity, "no Type in this category");
        }
    }


    public interface OnMyCheckBoxDialog {
        public void onSelectCheckBoxDialog(int type, int id, String value);
    }
}
