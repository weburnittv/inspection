package com.arise.healthcaresafety.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.InspReference;
import com.arise.healthcaresafety.model.entities.InspReferenceFactory;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.util.List;

/**
 * Created by Nam on 8/6/2015.
 */
public class ViewReferenceGridAdapter extends RecyclerView.Adapter<ViewReferenceGridAdapter.MyViewHolder> {
    ReferenceGridAdapterListener listener = null;
    private List<InspReference> list = null;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();
            InspReference item = list.get(position);
            if (listener != null) {
                listener.onItemClick(item);
            }
        }
    };

    public ViewReferenceGridAdapter(Context context, List<InspReference> list) {
        Context c = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_references,
                parent,
                false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        InspReference item = this.list.get(position);
        holder.itemLayout.setTag(position);
        if (item.getType() == InspReferenceFactory.IMAGE_REF) {
            holder.image.setImageResource(R.drawable.ic_image);
        } else {
            holder.image.setImageResource(R.drawable.ic_pdf);
        }
        holder.itemLayout.setOnClickListener(onClickListener);
        holder.txtTitle.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public void setReferenceGridAdapterListener(ReferenceGridAdapterListener listener) {
        this.listener = listener;
    }

    public interface ReferenceGridAdapterListener {
        void onItemClick(InspReference item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout itemLayout;
        private ImageView image;
        private MyTextView txtTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.itemReference_image);
            txtTitle = (MyTextView) itemView.findViewById(R.id.itemReference_title);
            itemLayout = (LinearLayout) itemView.findViewById(R.id.itemReferences);
        }
    }

}
