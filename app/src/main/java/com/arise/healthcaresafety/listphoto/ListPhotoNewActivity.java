package com.arise.healthcaresafety.listphoto;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.presenter.IListPhotoPresenter;
import com.arise.healthcaresafety.presenter.impl.ListPhotoPresenter;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.IListPhotoScreen;
import com.arise.healthcaresafety.view.adapter.PhotoGridAdapter;
import com.arise.healthcaresafety.view.adapter.PhotoGridMarginDecoration;
import com.arise.healthcaresafety.view.custom.MyTextView;

import org.catrobat.paintroid.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class ListPhotoNewActivity extends AppCompatActivity implements IListPhotoScreen, PhotoGridAdapter.PhotoGridListener {
    public static final int REQUEST_TAKE_PHOTO = 1;
    public static final int REQUEST_IMAGE_SELECTOR = 2;
    public static final int REQUEST_IMAGE_EDIT = 3;
    View.OnClickListener onAddPhotoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onPickPhoto();
        }
    };
    private Button mBtTakePhoto;
    private Button mBtAddPhoto;
    private String mStrCasePath;
    private ArrayList<Media> mMedias;
    View.OnClickListener onBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    private RecyclerView mRecyclerView;
    private PhotoGridAdapter mPhotoAdapter;
    //open camera screen
    private File mPhotoFile = null;
    View.OnClickListener onTakePhotoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onTakePicture();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_photo_new);
        Bundle data = getIntent().getExtras();
        if (data == null) {
            return;
        }
        this.mStrCasePath = data.getString(Constants.KEY_SAVE_PATH);

        Intent intent = this.getIntent();
        this.mMedias = intent.getParcelableArrayListExtra(Constants.KEY_MEDIAS);

        initView();
        refreshAdapter();
        setEvents();
        setUpToolBars();
        setEventForUIControl();
    }

    private void initView() {
        mBtTakePhoto = (Button) findViewById(R.id.listPhotoAct_btn_takePhoto);
        mBtAddPhoto = (Button) findViewById(R.id.listPhotoAct_btn_addPhoto);
        mRecyclerView = (RecyclerView) findViewById(R.id.list_photo_rv);
        mRecyclerView.addItemDecoration(new PhotoGridMarginDecoration(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void refreshAdapter() {
        mPhotoAdapter = new PhotoGridAdapter(this, mMedias);
        mRecyclerView.setAdapter(mPhotoAdapter);
        mPhotoAdapter.setGridPhotoListener(this);
        mPhotoAdapter.notifyDataSetChanged();
    }


    private void setEvents() {
        IListPhotoPresenter mIListPhotoPresenter = new ListPhotoPresenter(this, this);
    }

    @Override
    public void onBackPressed() {
        //onSaveNote(mMedias);
        super.onBackPressed();
    }

    private void onSaveNote(ArrayList<Media> medias) {
        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_MEDIAS, medias);
        setResult(RESULT_OK, intent);
    }

    private void setUpToolBars() {
        MyTextView title = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        ImageView btnBack = (ImageView) findViewById(R.id.toolBar_detailView_img_home);
        ImageView btnUpload = (ImageView) findViewById(R.id.toolBar_detailView_img_logout);
        title.setText(getString(R.string.image_title));
        btnBack.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                R.drawable.ic_arrow_left_white_24dp,
                null));
        btnUpload.setVisibility(View.INVISIBLE);
        btnBack.setOnClickListener(onBackListener);

        TextView tvSaveNote = (TextView) findViewById(R.id.toolBar_detailView_tv_save);
        tvSaveNote.setVisibility(View.VISIBLE);
        tvSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveNote(mMedias);
                finish();
            }
        });
    }

    /*event of button */
    private void setEventForUIControl() {
        this.mBtTakePhoto.setOnClickListener(onTakePhotoListener);
        this.mBtAddPhoto.setOnClickListener(onAddPhotoListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                Media media = new Media(this.mPhotoFile.getAbsolutePath(), Media.MEDIA_PHOTO);
                mMedias.add(media);
                refreshAdapter();
            }

        } else if (requestCode == REQUEST_IMAGE_SELECTOR && resultCode == RESULT_OK) { //return from selected photo
            if (data != null && data.getData() != null) {
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = ListPhotoNewActivity.this.getContentResolver().query(data.getData(), filePathColumn, null, null, null);
                if (cursor == null || cursor.getCount() < 1) {
                    return;
                }

                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                if (columnIndex < 0) { // no column index
                    return;
                }

                if (cursor.getString(columnIndex) != null) {
                    File selectFile = new File(cursor.getString(columnIndex));
                    Media media = new Media(selectFile.getAbsolutePath(), Media.MEDIA_PHOTO);
                    mMedias.add(media);
                    refreshAdapter();
                    cursor.close();
                    //  this.mIListPhotoPresenter.copyFile(selectFile, this.mPhotoFile);//save photo to mNote folder
                }
            }
        } else if (requestCode == REQUEST_IMAGE_EDIT) {
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                String path = bundle.getString(getString(org.catrobat.paintroid.R.string.extra_picture_path_catroid));
                Media item = mPhotoAdapter.getMedia(path);
                refreshAdapter();
            }
        }
    }


    private void onTakePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                this.mPhotoFile = FileManager.getInstance().createImageFile(this.mStrCasePath);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (this.mPhotoFile == null) {
            }
            // Continue only if the File was successfully created
            //  if (this.mPhotoFile != null) {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(this.mPhotoFile));
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            //   }
        }
    }

    //open select photo
    private void onPickPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_SELECTOR);
    }

    /*call back from mIListPhotoPresenter */
    @Override
    public void onCopySuccess() {
//        mIListPhotoPresenter.upload(ListPhotoNewActivity.this, this.mNote,
//                new Media(this.mPhotoFile.getAbsolutePath(), Media.MEDIA_PHOTO));
    }


    @Override
    public void onDeleteFile(boolean isSuccess) {
        Utils.getInstance().hideProgressDialog();
        if (isSuccess) {
            Utils.getInstance().showMessage(this, R.string.delete_success);
        } else {
            Utils.getInstance().showMessage(this, R.string.delete_failed);
        }
    }

    public void save(String filename, Bitmap bitmap) {
        try {
            OutputStream fOut;
            File file = new File("", filename);
            fOut = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG, 50, fOut);
            fOut.flush();
            fOut.close();

            MediaStore.Images.Media.insertImage(getContentResolver()
                    , file.getAbsolutePath(), file.getName(), file.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void upload(Media item) {

    }

    @Override
    public void paint(Media item) {
        Intent i = new Intent(ListPhotoNewActivity.this, MainActivity.class);
        i.putExtra(getString(org.catrobat.paintroid.R.string.extra_picture_path_catroid),
                item.getLocalPath());
        startActivityForResult(i, REQUEST_IMAGE_EDIT);
    }

    @Override
    public void delete(Media item) {

    }
}

