package com.arise.healthcaresafety;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.presenter.IListPhotoPresenter;
import com.arise.healthcaresafety.view.adapter.PhotoGridMarginDecoration;
import com.arise.healthcaresafety.view.adapter.ViewPhotoGridAdapter;
import com.arise.healthcaresafety.view.custom.MyTextView;

/**
 * Created by Nam on 7/28/2015.
 */
public class ListPhotoPastCaseActivity extends AppCompatActivity implements ViewPhotoGridAdapter.PhotoGridListener {
    private static final String TAG = ListPhotoPastCaseActivity.class.getSimpleName();

    //private RecyclerView recyclerView;
    //private ViewPhotoGridAdapter adapter;
    View.OnClickListener onBackClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    //private Note note;
    private int mode;
    private IListPhotoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_list_photo);
        Bundle data = getIntent().getExtras();

        Note note = data.getParcelable(Note.NOTE_PATH);


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.pastListPhotoAct_recyclerView);
        recyclerView.addItemDecoration(new PhotoGridMarginDecoration(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        ViewPhotoGridAdapter adapter = new ViewPhotoGridAdapter(this, note.getNotePhotoMedias());
        adapter.setGridPhotoListener(this);
        recyclerView.setAdapter(adapter);//set adapter to recycler view

        setUpToolBars(); // setup toolbar with back arrow
    }

    /* option menu */
    private void setUpToolBars() {
        MyTextView title = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        ImageView btnBack = (ImageView) findViewById(R.id.toolBar_detailView_img_home);
        ImageView btnUpload = (ImageView) findViewById(R.id.toolBar_detailView_img_logout);

        //init value for toolbar
        title.setText(getString(R.string.image_title));
        btnBack.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                R.drawable.ic_arrow_left_white_24dp,
                null));
        btnUpload.setVisibility(View.INVISIBLE);
        btnBack.setOnClickListener(onBackClick);
    }

    @Override
    public void onClickPhoto(Media media) {
        //todo open photo view
        Log.e(TAG, "open view photo activity");
        Intent i = new Intent(ListPhotoPastCaseActivity.this, PhotoViewActivity.class);
        i.putExtra("PATH", media.getUrlLink());
        startActivity(i);
    }
}
