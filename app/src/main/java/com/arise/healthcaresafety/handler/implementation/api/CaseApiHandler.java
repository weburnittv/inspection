package com.arise.healthcaresafety.handler.implementation.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;

public class CaseApiHandler {
    private static final String TAG = CaseApiHandler.class.getSimpleName();
    private ProgressDialog progress;
    private int items;

    public void saveEntity(Context context, BaseEntity entity) {
        if (!(entity instanceof Case)) {
            return;
        }
    }

    public void syncCases(Context context, Case[] entities) {
        SynchronizeLive synchronize = new SynchronizeLive(context);

        if (entities.length != 0) {
            synchronize.syncCases(entities);
        } else {
            Toast.makeText(context, "There is Case to Synchronize.", Toast.LENGTH_SHORT).show();
        }
    }

}
