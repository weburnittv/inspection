package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Variations;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.VariationApi;
import com.arise.healthcaresafety.utils.Utils;

import retrofit.Call;

/**
 * Created by user on 1/10/16.
 */
public class VariationTask extends AbstractSyncTask {

    public VariationTask(Context context, SyncDelegatorHandler handler) {
        super(context, handler);
    }

    @Override
    protected void process(BaseEntity entity) {
        VariationApi api = APIBuilder.createService(VariationApi.class, Utils.getServerUrl(this.mContext));
        Call<Variations> call = api.getVariations();

        try {
            Variations variations = call.execute().body();

            if (variations != null) {
                variations.save();
                this.handler.onFinishSync(null, SyncDelegatorHandler.SYNCED);
            }
        } catch (Exception e) {
            Log.e("Sync Error", e.getMessage());
            this.handler.onErrorSync(null);
        }
    }

    @Override
    public int getTotalItems() {
        return 1;
    }
}
