package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.MediaApi;
import com.arise.healthcaresafety.utils.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;

/**
 * Created by user on 1/1/16.
 */
public class MediaTask extends AbstractSyncTask {
    private Case caseBelong;
    private Note note;

    public MediaTask(Context context, SyncDelegatorHandler handler, Media data) {
        super(context, handler, data);
    }

    @Override
    protected void process(final BaseEntity entity) {
        MediaApi api = APIBuilder.createService(MediaApi.class, Utils.getServerUrl(this.mContext));

        note = ((Media) entity).getNote();
        if (note == null || note.getLiveId() == 0)
            handler.onErrorSync(entity);

        caseBelong = note.getInspectionCase();
        if (caseBelong == null || caseBelong.getLiveId() == 0)
            handler.onErrorSync(entity);

        Call<Media> call = null;
        Map<String, RequestBody> picMap = new HashMap<>();
        if (((Media) entity).isSynced())
            this.handler.onFinishSync(entity, SyncDelegatorHandler.SYNCED);
        else if (((Media) entity).isDeleted())
            api.deleteMedia(entity.getLiveId());
        else {
            long liveId = entity.getLiveId();
            RequestBody requestBody2 = null;
            try {
                RequestBody rb1 = RequestBody.create(MediaType.parse("image/*"), ((Media) entity).getUploadFile());
                picMap.put("media[media_file]\"; filename=\"" + ((Media) entity).getLocalName(), rb1);
            } catch (Exception e) {

            }
            if (liveId > 0) {
                call = api.updateMedia(liveId, picMap);
            } else {
                call = api.addNewMedia(((Media) entity).getNoteId(), entity.getKind(), picMap);
            }
        }
        Media savedMedia = null;
        try {
            Log.e("Sync", "Calling media api:" + entity.getKind());
            savedMedia = call.execute().body();
        } catch (Exception e) {
            handler.onErrorSync(entity);
        }
        if (savedMedia != null && !((Media) entity).isDeleted()) {
            saveMedia(savedMedia.getLiveId());
            handler.onFinishSync(entity, savedMedia.getLiveId());
        } else if (((Media) entity).isDeleted()) {
            handler.onFinishSync(entity, SyncDelegatorHandler.DELETED);
            this.deleteMedia();
        } else handler.onErrorSync(entity);
    }

    @Override
    public int getTotalItems() {
        return 1;
    }

    public void saveMedia(long liveId) {
        for (Note note : caseBelong.getNotes()) {
            if (note.getLiveId() == ((Media) this.data).getNoteId()) {
                for (Media media : note.getMedias()) {
                    if (media.getId().equals(this.data.getId())) {
                        media.setLiveId(liveId);
                        break;
                    }
                }
            }
        }
    }

    private void deleteMedia() {
        for (Note note : caseBelong.getNotes()) {
            if (note.getLiveId() == ((Media) this.data).getNoteId()) {
                for (Media media : note.getMedias()) {
                    if (media.getId().equals(this.data.getId())) {
                        List<Media> list = note.getMedias();
                        list.remove(media);
                        note.setMedias(list);
                        caseBelong.save();
                        break;
                    }
                }
            }
        }
    }
}
