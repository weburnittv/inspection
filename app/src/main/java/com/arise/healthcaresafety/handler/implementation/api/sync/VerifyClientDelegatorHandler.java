package com.arise.healthcaresafety.handler.implementation.api.sync;

import com.arise.healthcaresafety.model.entities.Client;

public interface VerifyClientDelegatorHandler {
    void onResponse(Client client);

    void onError();
}
