package com.arise.healthcaresafety.handler.implementation.api;

import android.content.Context;

import com.arise.healthcaresafety.handler.implementation.api.sync.CategoryTask;
import com.arise.healthcaresafety.handler.implementation.api.sync.DivisionTask;
import com.arise.healthcaresafety.handler.implementation.api.sync.VariationTask;

/**
 * Created by user on 1/10/16.
 */
public class SychronizeOrganization extends SynchronizeLive {

    private Context mContext;

    public SychronizeOrganization(Context context) {
        super(context);
        this.mContext = context;
    }

    public void startSync() {
        if (syncing)
            return;
        syncing = true;
        this.progress.setProgress(0);
        CategoryTask cTask = new CategoryTask(this.mContext, this);
        this.pool.execute(cTask);

        DivisionTask dTask = new DivisionTask(this.mContext, this);
        this.pool.execute(dTask);

        VariationTask vTask = new VariationTask(this.mContext, this);
        this.pool.execute(vTask);
        this.progress.setMax(3);
        this.progress.setSecondaryProgress(3);
    }
}
