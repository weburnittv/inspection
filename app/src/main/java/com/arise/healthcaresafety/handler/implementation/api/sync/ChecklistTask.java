package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Type;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.ChecklistApi;
import com.arise.healthcaresafety.utils.Utils;

import retrofit.Call;

/**
 * Created by user on 1/10/16.
 */
public class ChecklistTask extends AbstractSyncTask {

    private Context mContext;

    public ChecklistTask(Context context, SyncDelegatorHandler handler, Type type) {
        super(context, handler, type);
        this.mContext = context;
    }

    @Override
    protected void process(BaseEntity entity) {
        ChecklistApi checklistApi = APIBuilder.createService(ChecklistApi.class, Utils.getServerUrl(mContext));
        Call<CheckList> call = checklistApi.getChecklist(entity.getLiveId());

        try {
            CheckList checkList = call.execute().body();

            if (checkList != null) {
                checkList.setType((Type) entity);
                checkList.save();
                this.handler.onFinishSync(checkList, checkList.getLiveId());
            }
        } catch (Exception e) {
            Log.e("API", e.getMessage());
            this.handler.onErrorSync(entity);
        }
    }

    @Override
    public int getTotalItems() {
        return 0;
    }
}
