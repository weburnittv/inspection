package com.arise.healthcaresafety.handler.implementation.api;

import android.app.ProgressDialog;
import android.content.Context;

import com.arise.healthcaresafety.handler.implementation.api.sync.AbstractSyncTask;
import com.arise.healthcaresafety.handler.implementation.api.sync.CaseTask;
import com.arise.healthcaresafety.handler.implementation.api.sync.SyncDelegatorHandler;
import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Note;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Paul Aan on 1/1/16.
 */
public class SynchronizeLive implements SyncDelegatorHandler {
    protected ProgressDialog progress;
    protected ExecutorService pool;
    private int totalItems = 0;
    private AbstractSyncTask task;
    private Case[] cases;
    public static boolean syncing = false;
    private Context mContext;

    public SynchronizeLive(Context context) {
        this.mContext = context;
        pool = Executors.newSingleThreadExecutor();
        ProgressDialog progressBar = new ProgressDialog(context);
        progressBar.setMessage("Synchronizing ...");
        progressBar.setIndeterminate(false);
        progressBar.setCancelable(false);
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        this.progress = progressBar;
        this.progress.show();
        this.progress.setProgress(0);
    }

    public void syncCases(Case[] cases) {
        this.progress.setMax(cases.length);
        this.progress.setSecondaryProgress(cases.length);
        if (syncing)
            return;
        this.cases = cases;
        for (Case item : this.cases) {
            CaseTask caseTask = new CaseTask(this.mContext, this, item);
            this.totalItems += item.getResults().size();
            this.totalItems += item.getNotes().size();
            for (Note note : item.getNotes()) {
                this.totalItems += note.getMedias().size();
            }
            task = caseTask;
            pool.execute(task);
        }
        this.progress.setSecondaryProgress(this.totalItems);
        this.progress.setProgress(this.cases.length);
    }

    @Override
    public synchronized void onFinishSync(BaseEntity entity, long liveId) {
        if (liveId == SyncDelegatorHandler.DELETED || liveId == SyncDelegatorHandler.SYNCED) {
            if (entity instanceof Note) {
                this.increaseProcess(1 + ((Note) entity).getMedias().size());
            } else if (entity instanceof Case) {
                this.increaseProcess(1);
                this.increaseProcess(((Case) entity).getNotes().size());
                this.increaseProcess(((Case) entity).getResults().size());
            } else {
                this.increaseProcess(1);
            }
            return;
        }

        this.increaseProcess(1);
    }

    @Override
    public void onErrorSync(BaseEntity entity) {
        if (entity instanceof Note) {
            this.increaseProcess(1 + ((Note) entity).getMedias().size());
        } else {
            this.increaseProcess(1);
        }
    }

    public synchronized void increaseProcess(int processed) {
        this.progress.incrementProgressBy(processed);
        this.progress.incrementSecondaryProgressBy(processed);
        if (this.progress.getSecondaryProgress() == this.progress.getMax()) {
            syncing = false;
            this.finish();
        }
    }

    public void finish() {
        this.progress.setTitle("Wait for few seconds!");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.progress.dismiss();
    }

    public void executeTask(Runnable task) {
        this.progress.setMax(this.progress.getMax() + 1);
        this.pool.execute(task);
    }
}
