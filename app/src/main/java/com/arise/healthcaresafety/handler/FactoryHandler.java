package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.handler.implementation.api.CommonApiHandler;
import com.arise.healthcaresafety.handler.implementation.local.CaseLocalHandler;
import com.arise.healthcaresafety.handler.implementation.local.CommonLocalHandler;
import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;

/**
 * Created by hnam on 8/27/2015.
 */
public class FactoryHandler {

    public final static int LOCAL_HANDLER = 0;
    public final static int API_HANDLER = 1;

    public LocalStorageHandler getLocalHandler(BaseEntity entity) {
        if (entity instanceof Case) {
            return new CaseLocalHandler();
        }
        return null;
    }

    public CommonLocalHandler getLocalHandler() {
        return new CommonLocalHandler();
    }

    public CommonApiHandler getAPIHandler() {
        return CommonApiHandler.getInstance();
    }

}
