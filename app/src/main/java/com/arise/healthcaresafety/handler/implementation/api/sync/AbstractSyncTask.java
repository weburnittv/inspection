package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.BaseEntity;

/**
 * Created by Paul Aan on 1/1/16.
 */
public abstract class AbstractSyncTask extends Thread implements SyncTaskInterface {
    protected BaseEntity data;
    protected SyncDelegatorHandler handler;
    protected Context mContext;

    public AbstractSyncTask(Context context, SyncDelegatorHandler handler, BaseEntity data) {
        this.handler = handler;
        this.data = data;
        this.mContext = context;
    }

    public AbstractSyncTask(Context context, SyncDelegatorHandler handler) {
        this.handler = handler;
        this.mContext = context;
    }

    @Override
    public void run() {
        this.process(this.data);
    }

    protected abstract void process(final BaseEntity entity);

}
