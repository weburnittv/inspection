package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.EquipmentApi;
import com.arise.healthcaresafety.utils.Utils;

import retrofit.Call;

/**
 * Created by user on 1/11/16.
 */
public class EquipmentTask extends AbstractSyncTask {

    public EquipmentTask(Context context, SyncDelegatorHandler handler, Branch data) {
        super(context, handler, data);
    }

    @Override
    protected void process(BaseEntity entity) {
        EquipmentApi api = APIBuilder.createService(EquipmentApi.class, Utils.getServerUrl(this.mContext));

        Call<Equipments> call = api.getEquipments(entity.getLiveId());

        try {
            Equipments equipments = call.execute().body();
            if (equipments != null) {
                equipments.setBranch((Branch) entity);
                equipments.save();
                this.handler.onFinishSync(null, SyncDelegatorHandler.SYNCED);
            }
        } catch (Exception e) {
            Log.e("API", e.getMessage());
            this.handler.onErrorSync(null);
        }
    }

    @Override
    public int getTotalItems() {
        return 1;
    }
}
