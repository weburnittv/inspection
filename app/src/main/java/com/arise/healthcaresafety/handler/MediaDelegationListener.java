package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.model.entities.Media;

/**
 * Created by hnam on 8/29/2015.
 */
public interface MediaDelegationListener {
    void onUploadItem(boolean isSuccess, Media item);

    void onDeleteItem(boolean isSuccess);
}
