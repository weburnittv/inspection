package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.CaseApi;
import com.arise.healthcaresafety.utils.Utils;

import java.io.IOException;

import retrofit.Call;

/**
 * Created by user on 1/1/16.
 */
public class CaseTask extends AbstractSyncTask {
    private int totalItems;

    public CaseTask(Context context, SyncDelegatorHandler handler, Case data) {
        super(context, handler, data);
        this.totalItems = 1;
        for (Note note : data.getNotes()) {
            this.totalItems++;
            this.totalItems += note.getMedias().size();
        }
        this.totalItems += data.getResults().size();
    }

    @Override
    protected void process(final BaseEntity entity) {
        if (((Case) entity).isSynced())
            this.handler.onFinishSync(entity, SyncDelegatorHandler.SYNCED);

        CaseApi caseApi = APIBuilder.createService(CaseApi.class, Utils.getServerUrl(mContext));

        Call<Case> call = null;

        if (entity.getLiveId() > 0) {
            call = caseApi.updateCase(entity.getLiveId(),
                    ((Case) entity).getRetrofitDataPost());
        } else {
            call = caseApi.createCase(((Case) entity).getRetrofitDataPost());
        }

        Case inspectionCase = null;
        try {
            Log.e("Sync", "Calling media api:" + entity.getKind());
            inspectionCase = call.execute().body();
        } catch (IOException e) {
            handler.onErrorSync(entity);
            return;
        }

        Log.e("Sync", "Finish Case");
        if (inspectionCase != null) {
            for (Note note : ((Case) data).getNotes()) {
                note.setCaseId(inspectionCase.getLiveId());
                note.setInspectionCase((Case) entity);
                NoteTask noteTask = new NoteTask(this.mContext, handler, note);
                this.handler.executeTask(noteTask);
            }
            for (Result result : ((Case) data).getResults()) {
                result.setCaseId(inspectionCase.getLiveId());
                result.setInspectionCase((Case) entity);
                ResultTask resultTask = new ResultTask(this.mContext, handler, result);
                this.handler.executeTask(resultTask);
            }
            handler.onFinishSync(entity, inspectionCase.getLiveId());
            inspectionCase.setSynced(true);
            saveCase(inspectionCase.getLiveId());
        } else handler.onErrorSync(entity);
    }

    @Override
    public int getTotalItems() {
        return this.totalItems;
    }

    private void saveCase(long liveId) {
        this.data.setLiveId(liveId);
        this.data.save();
    }
}
