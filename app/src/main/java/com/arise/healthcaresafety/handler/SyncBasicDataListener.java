package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.Variations;

/**
 * Created by user on 12/13/15.
 */
public interface SyncBasicDataListener {

    void onDLoadDivisions(Divisions d);

    void onDLoadEquipments(Equipments equipments);

    void onDLoadCategories(Categories categories);

    void onDLoadCheckList(CheckList checklist);

    void onDLoadVariations(Variations variations);
}
