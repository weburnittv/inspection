package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Status;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.ResultApi;
import com.arise.healthcaresafety.utils.Utils;

import java.io.IOException;
import java.util.List;

import retrofit.Call;

/**
 * Created by Paul Aan on 1/1/16.
 */
public class ResultTask extends AbstractSyncTask {
    private int totalItems = 0;
    private Case caseBelong;

    public ResultTask(Context context, SyncDelegatorHandler handler, Result data) {
        super(context, handler, data);
        this.totalItems = 1;
    }

    @Override
    protected void process(final BaseEntity entity) {
        caseBelong = ((Result) entity).getInspectionCase();

        if (caseBelong.getLiveId() == 0)
            handler.onErrorSync(entity);

        if (((Result) entity).isSynced()) {
            this.handler.onFinishSync(entity, SyncDelegatorHandler.SYNCED);
        }

        ResultApi api = APIBuilder.createService(ResultApi.class, Utils.getServerUrl(this.mContext));

        Call<Result> call = null;
        Result savedResult = null;
        if (entity.getLiveId() > 0 && ((Result) entity).isDeleted()) {
            Call<Status> deleteResult = api.deleteResult(entity.getLiveId());

            try {
                Status deleted = deleteResult.execute().body();
                handler.onFinishSync(entity, SyncDelegatorHandler.DELETED);
            } catch (Exception e) {

            } finally {
                deleteResult((Result) entity);
                return;
            }
        } else if (entity.getLiveId() > 0)
            call = api.updateResult(entity.getLiveId(), ((Result) entity).getRetrofitDataPost());
        else
            call = api.createResult(((Result) entity).getCaseId(), ((Result) entity).getRetrofitDataPost());

        try {
            Log.e("Sync", "Calling result api:" + entity.getKind());
            savedResult = call.execute().body();
        } catch (IOException e) {

            handler.onErrorSync(entity);
        }
        Log.e("Sync", "Finish Result");
        if (savedResult != null) {
            handler.onFinishSync(entity, savedResult.getLiveId());
            saveResult(savedResult.getLiveId());
        } else handler.onErrorSync(entity);
    }

    @Override
    public int getTotalItems() {
        return this.totalItems;
    }

    private void saveResult(long liveId) {
        for (Result result : caseBelong.getResults()) {
            if (result.getId().equals(this.data.getId())) {
                result.setLiveId(liveId);
                break;
            }
        }
        caseBelong.save();
    }

    private void deleteResult(Result entity) {
        List<Result> results = caseBelong.getResults();
        results.remove(entity);
        caseBelong.setResults(results);
        caseBelong.save();
    }
}
