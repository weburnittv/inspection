package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.Status;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.SendMailApi;
import com.arise.healthcaresafety.utils.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;

public class SendMailTask extends Thread {

    private Map<String, String> mRequestData;
    protected MailDelegatorHandler mHandler;
    private Context mContext;

    public SendMailTask(Context context, MailDelegatorHandler handler, Map<String, String> data) {
        this.mHandler = handler;
        this.mRequestData = data;
        this.mContext = context;
    }

    @Override
    public void run() {
        SendMailApi api = APIBuilder.createService(SendMailApi.class, Utils.getServerUrl(this.mContext));

        Call<Status> call;
        Status response;
        try {
            Map<String, RequestBody> emailMap = new HashMap<>();
            RequestBody rb1 = RequestBody.create(MediaType.parse("form[email]"), mRequestData.get("email"));
            RequestBody rb2 = RequestBody.create(MediaType.parse("form[case]"), mRequestData.get("caseId"));
            emailMap.put("form[email]", rb1);
            emailMap.put("form[email]", rb2);

            call = api.sendEmail(emailMap);
            response = call.execute().body();

            mHandler.onResponse(response.isStatus());
        } catch (Exception e) {
            mHandler.onResponse(false);
        }
    }
}
