package com.arise.healthcaresafety.handler;

import android.content.Context;

import com.arise.healthcaresafety.handler.implementation.api.CaseApiHandler;
import com.arise.healthcaresafety.handler.implementation.api.CommonApiHandler;
import com.arise.healthcaresafety.handler.implementation.local.CommonLocalHandler;
import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;

import java.util.List;

import co.uk.rushorm.core.RushSearch;

public class Delegation {

    public static final String TAG = Delegation.class.getSimpleName();
    public static Delegation mInstance = new Delegation();
    private CommonLocalHandler localHandler;
    private CommonApiHandler apiHandler;
    private FactoryHandler factory = new FactoryHandler();
    private boolean isOnline;
    private Delegation() {
        this.isOnline = false;
        this.apiHandler = this.factory.getAPIHandler();
        this.localHandler = this.factory.getLocalHandler();
    }

    //singleton pattern
    public static Delegation getInstance() {
        return mInstance;
    }

    public CommonLocalHandler getLocalHandler() {
        return this.localHandler;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setStatus(boolean online) {
        this.isOnline = online;
    }

    public void syncOrganizeData(Context context) {
        CommonApiHandler handler = CommonApiHandler.getInstance();
        handler.syncOrganization(context);
    }

    public int syncCasesData(Context context) {
        List<Case> list = new RushSearch().find(Case.class);
        CaseApiHandler handler = new CaseApiHandler();
        Case[] cases = new Case[list.size()];

        handler.syncCases(context, list.toArray(cases));
        return list.size();
    }

    public void saveEntity(Context context, BaseEntity entity, AbstractHandlerListener listener) {
        LocalStorageHandler localStorage = factory.getLocalHandler(entity);
        BaseEntity result = localStorage.saveEntity(entity);
        listener.setEntity(result);
        LocalStorageHandler handler = factory.getLocalHandler(entity);
        handler.onSaveEntity(result, listener);
    }

    public void saveLiveId(Context context, BaseEntity entity) {
        LocalStorageHandler localHandler = this.factory.getLocalHandler(entity);
        localHandler.saveLiveId(context, entity);
    }

    public void sendEmail(Context context, Case c, String email, ChecklistListener callback) {
        this.apiHandler.sendEmail(context, c.getLiveId(), email, callback);
    }

    public void loadPastCases(PastCaseDelegationListener listener) {
        this.localHandler.loadPastCases(listener);
    }


    public void searchPastCases(Context context, String key, PastCaseDelegationListener listener) {

    }

}
