package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Division;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.DivisionApi;
import com.arise.healthcaresafety.utils.Utils;

import retrofit.Call;

/**
 * Created by user on 1/10/16.
 */
public class DivisionTask extends AbstractSyncTask {

    public DivisionTask(Context context, SyncDelegatorHandler handler) {
        super(context, handler);
    }

    @Override
    protected void process(BaseEntity entity) {
        DivisionApi api = APIBuilder.createService(DivisionApi.class, Utils.getServerUrl(this.mContext));

        Call<Divisions> call = api.getDivisions();

        try {
            Divisions divisions = call.execute().body();
            if (divisions != null) {
                divisions.save();
                for (Division division : divisions.getDivisions()) {
                    for (Branch branch : division.getBranches()) {
                        EquipmentTask task = new EquipmentTask(this.mContext, this.handler, branch);
                        this.handler.executeTask(task);
                    }
                }
                this.handler.onFinishSync(null, SyncDelegatorHandler.SYNCED);
            }
        } catch (Exception e) {
            Log.e("API", e.getMessage());
            this.handler.onErrorSync(null);
        }
    }

    @Override
    public int getTotalItems() {
        return 1;
    }
}
