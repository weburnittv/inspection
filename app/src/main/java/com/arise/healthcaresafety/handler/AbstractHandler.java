package com.arise.healthcaresafety.handler;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.BaseEntity;

/**
 * Created by hnam on 8/27/2015.
 */
public abstract class AbstractHandler {


    public abstract BaseEntity saveEntity(Context context, BaseEntity entity, AbstractHandlerListener callback);

/*    public abstract void loadDivision(Context context, DelegationListener callback);

    public abstract void loadEquipments(Context context, Branch branch, DelegationListener callback);

    public abstract void loadCategories(Context context, DelegationListener callback);

    public abstract void loadCheckLists(Context context, Type type, DelegationListener callback);

    public abstract void saveCase(Context context, Case c, DelegationListener callback);


    public abstract void updateEquipResult(Context context, Case c, Result result,
                                           DelegationListener callback);

    public abstract void updateChecklistResult(Context context, Case c, Result result,
                                               DelegationListener callback);


    public abstract void updateNote(Context context, Case c, Note note,
                                    DelegationListener callback);

    public abstract void updateFinalQuestion(Context context, Case c, DelegationListener callback);


    public abstract void saveMedia(Context c, Media media, File file, MediaDelegationListener callback);

    public abstract void updateMedia(Context c, Media media, MediaDelegationListener callback);

    public abstract void deleteMedia(Context c, Media media, MediaDelegationListener callback);*/
}
