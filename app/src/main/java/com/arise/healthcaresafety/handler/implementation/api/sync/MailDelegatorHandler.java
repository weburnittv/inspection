package com.arise.healthcaresafety.handler.implementation.api.sync;

public interface MailDelegatorHandler {
    void onResponse(boolean status);
}
