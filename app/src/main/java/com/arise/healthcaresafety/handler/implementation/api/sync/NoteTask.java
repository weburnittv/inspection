package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.NoteApi;
import com.arise.healthcaresafety.utils.Utils;

import java.io.IOException;

import retrofit.Call;

/**
 * Created by Paul Aan on 1/1/16.
 */
public class NoteTask extends AbstractSyncTask {
    private int totalItems = 0;
    private Case caseBelong;

    public NoteTask(Context context, SyncDelegatorHandler handler, Note data) {
        super(context, handler, data);
        this.totalItems = 1;
        this.totalItems += data.getMedias().size();
    }

    @Override
    protected void process(final BaseEntity entity) {
        caseBelong = ((Note) entity).getInspectionCase();
        NoteApi api = APIBuilder.createService(NoteApi.class, Utils.getServerUrl(this.mContext));

        if (((Note) entity).isSynced()) {
            this.handler.onFinishSync(entity, SyncDelegatorHandler.SYNCED);
        }

        Call<Note> call = null;
        if (entity.getLiveId() > 0 && !((Note) entity).isSynced())
            call = api.updateNote(entity.getLiveId(), ((Note) entity).getRetrofitDataPost());
        else if (entity.getLiveId() == 0)
            call = api.createNote(((Note) entity).getRetrofitDataPost());

        Note savedNote = null;
        try {
            Log.e("Sync", "Calling note api:" + entity.getKind());
            if (call != null)
                savedNote = call.execute().body();
        } catch (IOException e) {
            handler.onErrorSync(entity);
            return;
        }

        if (savedNote != null) {
            for (Media media : ((Note) entity).getMedias()) {
                media.setNoteId(savedNote.getLiveId());
                media.setNote((Note) entity);
                MediaTask mediaTask = new MediaTask(mContext, handler, media);
                this.handler.executeTask(mediaTask);
            }
            handler.onFinishSync(entity, savedNote.getLiveId());
            saveNote(savedNote.getLiveId());
        } else handler.onErrorSync(entity);

    }

    @Override
    public int getTotalItems() {
        return this.totalItems;
    }

    public void saveNote(long liveId) {
        for (Note note : caseBelong.getNotes()) {
            if (note.getId().equals(this.data.getId())) {
                note.setLiveId(liveId);
                break;
            }
        }
        caseBelong.save();
    }
}
