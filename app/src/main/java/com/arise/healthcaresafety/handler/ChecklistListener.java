package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Status;

/**
 * Created by user on 12/12/15.
 */
public interface ChecklistListener {

    void onDLoadError(String message);

    void onDSaveCase(Case c);

    void onDSaveResult(Result result);

    void onDDeleteResult(Result result, Status status);

    void onDUpdateNote(Note note);

    void onDSendEmailStatus(boolean status);

}
