package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.Category;
import com.arise.healthcaresafety.model.entities.Type;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.CategoryApi;
import com.arise.healthcaresafety.utils.Utils;

import retrofit.Call;

/**
 * Created by user on 1/10/16.
 */
public class CategoryTask extends AbstractSyncTask {
    public CategoryTask(Context context, SyncDelegatorHandler handler) {
        super(context, handler);
    }

    @Override
    protected void process(BaseEntity entity) {
        CategoryApi api = APIBuilder.createService(CategoryApi.class, Utils.getServerUrl(this.mContext));

        Call<Categories> call = api.getCategories();

        try {
            Categories categories = call.execute().body();
            categories.save();
            for (Category cat : categories.getCategories()) {
                for (Type type : cat.getTypes()) {
                    ChecklistTask task = new ChecklistTask(this.mContext, this.handler, type);
                    this.handler.executeTask(task);
                }

            }
            this.handler.onFinishSync(null, SyncDelegatorHandler.SYNCED);
        } catch (Exception e) {
            Log.e("API", e.getMessage());
            this.handler.onErrorSync(null);
        }
    }

    @Override
    public int getTotalItems() {
        return 1;
    }
}
