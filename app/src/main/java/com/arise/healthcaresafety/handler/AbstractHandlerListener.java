package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.model.entities.BaseEntity;

/**
 * Created by hnam on 8/31/2015.
 */
public abstract class AbstractHandlerListener {
    private boolean isOnline;
    private BaseEntity entity;

    public void setIsOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public BaseEntity getEntity() {
        return entity;
    }

    public void setEntity(BaseEntity entity) {
        this.entity = entity;
    }

    public void setLiveId(long liveId) {
        if (this.entity == null) {
            return;
        }
        this.entity.setLiveId(liveId);
    }
}
