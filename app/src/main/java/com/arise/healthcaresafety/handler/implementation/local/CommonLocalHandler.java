package com.arise.healthcaresafety.handler.implementation.local;

import android.content.Context;

import com.arise.healthcaresafety.handler.AbstractCommonHandler;
import com.arise.healthcaresafety.handler.DelegationListener;
import com.arise.healthcaresafety.handler.PastCaseDelegationListener;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.Category;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Division;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.PastCases;
import com.arise.healthcaresafety.model.entities.Type;

/**
 * Created by hnam on 9/1/2015.
 */
public class CommonLocalHandler extends AbstractCommonHandler {

    public void loadDivision(DelegationListener callback) {
        Divisions list = new Divisions();
        list.loadDivisions();
        //callback return data to presenter
        callback.onDLoadDivisions(list);
    }

    public void loadCategories(DelegationListener callback) {
        Categories list = new Categories();
        list.loadCategories();
        //callback to screen
        callback.onDLoadCategories(list);
    }

    public void loadPastCases(PastCaseDelegationListener listener) {
        PastCases list = new PastCases();
        list.loadPastCases();
        listener.onListPastCases(list);
    }
}
