package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.model.entities.PastCases;

/**
 * Created by hnam on 9/16/2015.
 */
public interface PastCaseDelegationListener {
    void onListPastCases(PastCases pastCases);

    void onSearchPastCases(PastCases pastCases);
}
