package com.arise.healthcaresafety.handler.implementation.api.sync;

/**
 * Created by user on 1/1/16.
 */
public interface SyncTaskInterface {
    public int getTotalItems();
}
