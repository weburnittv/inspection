package com.arise.healthcaresafety.handler;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.BaseEntity;

/**
 * Created by hnam on 8/27/2015.
 */
public abstract class LocalStorageHandler {
    public abstract BaseEntity saveEntity(BaseEntity entity);

    public abstract void saveLiveId(Context context, BaseEntity entity);

    public abstract void onSaveEntity(BaseEntity entity, AbstractHandlerListener callBack);
}
