package com.arise.healthcaresafety.handler.implementation.local;

import android.content.Context;

import com.arise.healthcaresafety.handler.AbstractHandlerListener;
import com.arise.healthcaresafety.handler.DelegationListener;
import com.arise.healthcaresafety.handler.LocalStorageHandler;
import com.arise.healthcaresafety.model.entities.BaseEntity;
import com.arise.healthcaresafety.model.entities.Case;

/**
 * Created by hnam on 8/31/2015.
 */
public class CaseLocalHandler extends LocalStorageHandler {

    private static final String TAG = CaseLocalHandler.class.getSimpleName();

    @Override
    public BaseEntity saveEntity(BaseEntity entity) {
        Case inspectionCase = (Case) entity;
        inspectionCase.save();
        return inspectionCase;
    }

    @Override
    public void saveLiveId(Context context, BaseEntity entity) {
    }

    @Override
    public void onSaveEntity(BaseEntity entity, AbstractHandlerListener listener) {
        ((DelegationListener) listener).onDSaveCase((Case) entity);
    }
}
