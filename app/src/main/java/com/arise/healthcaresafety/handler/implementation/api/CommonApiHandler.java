package com.arise.healthcaresafety.handler.implementation.api;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.handler.AbstractCommonHandler;
import com.arise.healthcaresafety.handler.ChecklistListener;
import com.arise.healthcaresafety.model.entities.SendStatus;
import com.arise.healthcaresafety.model.network.api.SendEmailApi;

/**
 * Created by hnam on 9/1/2015.
 */
public final class CommonApiHandler extends AbstractCommonHandler {
    private static final String TAG = CommonApiHandler.class.getSimpleName();

    private static CommonApiHandler instance = new CommonApiHandler();

    private CommonApiHandler() {
    }

    public static CommonApiHandler getInstance() {
        return instance;
    }

    public void syncOrganization(Context context) {
        SychronizeOrganization sync = new SychronizeOrganization(context);

        sync.startSync();
    }

    public void sendEmail(Context context, long caseId, String email, final ChecklistListener callback) {
        //weburnittv@gmail.com
        SendEmailApi api1 = new SendEmailApi(context);
        api1.sendEmail(email, caseId, new Response.Listener<SendStatus>() {
            @Override
            public void onResponse(SendStatus status) {
                callback.onDSendEmailStatus(status.isStatus());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                callback.onDSendEmailStatus(false);
            }
        });
    }

}
