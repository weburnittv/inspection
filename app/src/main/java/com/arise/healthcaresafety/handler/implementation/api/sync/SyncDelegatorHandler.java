package com.arise.healthcaresafety.handler.implementation.api.sync;

import com.arise.healthcaresafety.model.entities.BaseEntity;

/**
 * Created by Paul Aan on 1/1/16.
 */
public interface SyncDelegatorHandler {
    public static final int DELETED = -1;
    public static final int SYNCED = 0;

    public void onFinishSync(BaseEntity entity, long liveId);

    public void onErrorSync(BaseEntity entity);

    public void executeTask(Runnable task);
}
