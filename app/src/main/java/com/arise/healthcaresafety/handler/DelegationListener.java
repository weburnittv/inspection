package com.arise.healthcaresafety.handler;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Status;

/**
 * Created by Nam on 8/28/2015.
 */
public interface DelegationListener {
    //public AbstractHandlerListener setIsOnline(boolean isOnline);
    void onDLoadDivisions(Divisions d);

    void onDLoadCategories(Categories categories);

    void onDLoadCheckList(CheckList checklist);

    void onDLoadError(String message);

    void onDSaveCase(Case c);

    void onDSaveResult(Result result);

    void onDDeleteResult(Result result, Status status);

    void onDUpdateNote(Note note);

    void onDSendEmailStatus(boolean status);


}
