package com.arise.healthcaresafety.handler.implementation.api.sync;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.ClientResult;
import com.arise.healthcaresafety.model.network.api.implement.APIBuilder;
import com.arise.healthcaresafety.model.network.api.implement.VerifyClientApi;
import com.arise.healthcaresafety.utils.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;

public class VerifyClientTask extends Thread {

    private Map<String, String> mRequestData;
    protected VerifyClientDelegatorHandler mHandler;
    private Context mContext;

    public VerifyClientTask(Context context, VerifyClientDelegatorHandler handler, Map<String, String> data) {
        this.mHandler = handler;
        this.mRequestData = data;
        this.mContext = context;
    }

    @Override
    public void run() {
        VerifyClientApi api = APIBuilder.createService(VerifyClientApi.class, Utils.getServerUrl(mContext));

        final Call<ClientResult> call;
        final ClientResult[] response = {null};
        Map<String, RequestBody> requestObject = new HashMap<>();
        RequestBody rb1 = RequestBody.create(MediaType.parse("form[host]"), mRequestData.get("host"));
        RequestBody rb2 = RequestBody.create(MediaType.parse("form[email]"), mRequestData.get("email"));
        requestObject.put("form[host]", rb1);
        requestObject.put("form[email]", rb2);

        call = api.verifyClient(requestObject);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response[0] = call.execute().body();
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.onError();
                }
            }
        });

        thread.start();

        if (response[0] != null) {
            mHandler.onResponse(response[0].getClient());
        } else {
            mHandler.onError();
        }
    }
}
