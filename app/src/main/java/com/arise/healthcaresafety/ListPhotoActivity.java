package com.arise.healthcaresafety;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.presenter.IListPhotoPresenter;
import com.arise.healthcaresafety.presenter.impl.ListPhotoPresenter;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.IListPhotoScreen;
import com.arise.healthcaresafety.view.adapter.PhotoGridAdapter;
import com.arise.healthcaresafety.view.adapter.PhotoGridMarginDecoration;
import com.arise.healthcaresafety.view.custom.MyTextView;

import org.catrobat.paintroid.MainActivity;

import java.io.File;
import java.io.IOException;


public class ListPhotoActivity extends AppCompatActivity implements IListPhotoScreen,
        PhotoGridAdapter.PhotoGridListener {
    public static final int REQUEST_TAKE_PHOTO = 1;
    public static final int REQUEST_IMAGE_SELECTOR = 2;
    public static final int REQUEST_IMAGE_EDIT = 3;
    private static final String TAG = ListPhotoActivity.class.getSimpleName();
    public static Note note;
    View.OnClickListener onAddPhotoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dispatchPhotoSelectionIntent();
        }
    };
    View.OnClickListener onUploadClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //upload all picture
            Toast.makeText(ListPhotoActivity.this, "onUploadClick", Toast.LENGTH_LONG).show();
        }
    };
    private Button btnTakePhoto;
    private Button btnAddPhoto;
    private RecyclerView recyclerView;
    private PhotoGridAdapter adapter;
    View.OnClickListener onBackClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    private String caseFolder;
    private IListPhotoPresenter mIListPhotoPresenter;
    //open camera screen
    private File photoFile = null;
    View.OnClickListener onTakePhotoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dispatchTakePictureIntent();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_photo);
        Bundle data = getIntent().getExtras();
        this.caseFolder = data.getString("PATH");
        this.note = data.getParcelable(Note.NOTE_PATH);

        Log.d(TAG, "Note List Photo Activity " + this.note);
        btnTakePhoto = (Button) findViewById(R.id.listPhotoAct_btn_takePhoto);
        btnAddPhoto = (Button) findViewById(R.id.listPhotoAct_btn_addPhoto);
        recyclerView = (RecyclerView) findViewById(R.id.listPhotoAct_recyclerView);
        recyclerView.addItemDecoration(new PhotoGridMarginDecoration(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        adapter = new PhotoGridAdapter(this, this.note.getMedias());
        adapter.setGridPhotoListener(this);
        recyclerView.setAdapter(adapter);//set adapter to recycler view

        setUpToolBars(); // setup toolbar with back arrow
        setEventForUIControl();//event of ui control
        mIListPhotoPresenter = new ListPhotoPresenter(this, this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("NUM", this.adapter.getUploadedMedia());
        setResult(RESULT_OK, intent);
        finish();
    }

    /* option menu */
    private void setUpToolBars() {
        MyTextView title = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        ImageView btnBack = (ImageView) findViewById(R.id.toolBar_detailView_img_home);
        ImageView btnUpload = (ImageView) findViewById(R.id.toolBar_detailView_img_logout);
        //init value for toolbar
        title.setText(getString(R.string.image_title));
        btnBack.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                R.drawable.ic_arrow_left_white_24dp,
                null));
        btnUpload.setVisibility(View.INVISIBLE);
        btnBack.setOnClickListener(onBackClick);
        btnUpload.setOnClickListener(onUploadClick);
    }

    /*event of button */
    private void setEventForUIControl() {
        this.btnTakePhoto.setOnClickListener(onTakePhotoClick);
        this.btnAddPhoto.setOnClickListener(onAddPhotoClick);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                //     Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
                Log.e(TAG, "" + note.toString());
                Log.e(TAG, "" + this.photoFile);
                //     Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
                Log.e(TAG, "ttt1" + note.toString());
                Log.e(TAG, "ttt2" + this.photoFile);
                mIListPhotoPresenter.upload(ListPhotoActivity.this, this.note,
                        new Media(this.photoFile.getAbsolutePath(), Media.MEDIA_PHOTO));
            } else {
                if (this.photoFile != null) {
                    Log.e(TAG + "ttt", "delete file");
                    this.photoFile.delete();
                }
            }
        } else if (requestCode == REQUEST_IMAGE_SELECTOR && resultCode == RESULT_OK) { //return from selected photo
            if (data != null && data.getData() != null) {
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = ListPhotoActivity.this.getContentResolver().query(data.getData(), filePathColumn, null, null, null);
                if (cursor == null || cursor.getCount() < 1) {
                    return;
                }
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                if (columnIndex < 0) { // no column index
                    return;
                }
                this.photoFile = null;
                File selectFile = null;
                try {
                    photoFile = FileManager.getInstance().createImageFile(this.caseFolder);
                } catch (IOException ex) {
                    Log.e(TAG, ex.toString());
                }

                selectFile = new File(cursor.getString(columnIndex));
                Log.e(TAG, selectFile.toString());
                if (cursor != null) {
                    cursor.close();
                }
                Log.e(TAG, "ttt3" + this.photoFile);
                this.mIListPhotoPresenter.copyFile(selectFile, photoFile);//save photo to note folder
            }
        } else if (requestCode == REQUEST_IMAGE_EDIT) { //return from edit photo
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                String path = bundle.getString(getString(org.catrobat.paintroid.R.string.extra_picture_path_catroid));
                Media item = adapter.getMedia(path);
                Log.e(TAG, "ttt4" + item);
                if (item != null) {
                    adapter.notifyDataSetChanged();
                    if (Utils.isNetworkConnected(this)) {
                        //if it's online, so upload new photo after change
                        Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
                        Log.e(TAG, "tag: " + item.toString());
                        mIListPhotoPresenter.upload(ListPhotoActivity.this, this.note, item);
                    }
                }
            } else {
                //do nothing
            }
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                this.photoFile = FileManager.getInstance().createImageFile(this.caseFolder);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, ex.toString());
            }
            if (this.photoFile == null) {
                Log.e(TAG, "photo file is null");
            }
            // Continue only if the File was successfully created
            if (this.photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(this.photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    //open select photo
    private void dispatchPhotoSelectionIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        this.startActivityForResult(galleryIntent, REQUEST_IMAGE_SELECTOR);
    }

    /*call back from mIListPhotoPresenter */
    @Override
    public void onCopySuccess() {
        Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
        mIListPhotoPresenter.upload(ListPhotoActivity.this, this.note,
                new Media(this.photoFile.getAbsolutePath(), Media.MEDIA_PHOTO));
    }

    @Override
    public void onDeleteFile(boolean isSuccess) {
        Utils.getInstance().hideProgressDialog();
        if (isSuccess) {
            Utils.getInstance().showMessage(this, R.string.delete_success);
            this.adapter.notifyDataSetChanged();
        } else {
            Utils.getInstance().showMessage(this, R.string.delete_failed);
        }
    }


    /* listener form image */
    @Override
    public void upload(Media item) {
        Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
        mIListPhotoPresenter.upload(ListPhotoActivity.this, this.note, item);
    }

    //String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Inspection/c90/n28/JPEG_20150721_003219_-1022170518.jpg";
    @Override
    public void paint(Media item) {
        Log.d(TAG + "ttt", "paint + item");
        Intent i = new Intent(ListPhotoActivity.this, MainActivity.class);
        i.putExtra(getString(org.catrobat.paintroid.R.string.extra_picture_path_catroid),
                item.getLocalPath());
        startActivityForResult(i, REQUEST_IMAGE_EDIT);
    }

    @Override
    public void delete(Media item) {
        this.mIListPhotoPresenter.deleteLocal(item);
    }

}
