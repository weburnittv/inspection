package com.arise.healthcaresafety;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.view.preview.InspectionCaseView;

import co.uk.rushorm.core.RushSearch;

public class PreviewDetailActivity extends Activity {
    private LinearLayout mLlCase;
    private Case mCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_detail);
        this.getBundleData();
        this.loadInspectionCaseView();
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        String caseId = bundle.getString(Constants.KEY_CASE_ID);
        this.mCase = new RushSearch().whereId(caseId).findSingle(Case.class);
    }

    private int getScreenWidth() {
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        return display.getWidth() - 20;
    }

    private void initView() {
        this.mLlCase = (LinearLayout) findViewById(R.id.preview_detail_ll_case);
    }

    private void loadInspectionCaseView() {
        initView();

        InspectionCaseView caseView = new InspectionCaseView(this, mCase, getScreenWidth());
        caseView.buildView();
        this.mLlCase.addView(caseView.getLayout());
    }
}
