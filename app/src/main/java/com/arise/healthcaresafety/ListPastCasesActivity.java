package com.arise.healthcaresafety;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import com.arise.healthcaresafety.model.entities.PastCases;
import com.arise.healthcaresafety.presenter.IListPastCasesPresenter;
import com.arise.healthcaresafety.presenter.impl.ListPastCasesPresenter;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.IListPastCasesScreen;
import com.arise.healthcaresafety.view.adapter.CaseListAdapter;
import com.arise.healthcaresafety.view.custom.MyLogoutDialog;
import com.arise.healthcaresafety.view.custom.MyTextView;


public class ListPastCasesActivity extends AppCompatActivity implements IListPastCasesScreen {
    private static final String TAG = ListPastCasesActivity.class.getSimpleName();
    private CaseListAdapter adapter;
    /* handle event of item group expand list */
    /* listener whenever group open */
    ExpandableListView.OnGroupExpandListener mExpandListener = new ExpandableListView.OnGroupExpandListener() {
        @Override
        public void onGroupExpand(int groupPosition) {
            adapter.invalidateIndicatorIcon(true, groupPosition);
        }
    };
    /* listener whenever group close */
    ExpandableListView.OnGroupCollapseListener mCollapseListener = new ExpandableListView.OnGroupCollapseListener() {
        @Override
        public void onGroupCollapse(int groupPosition) {
            adapter.invalidateIndicatorIcon(false, groupPosition);
        }
    };
    private IListPastCasesPresenter listPastCasesPresenter;
    /* logout on toolbar is clicked */
    private View.OnClickListener onLogoutClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyLogoutDialog.getInstance().showLogoutDialog(ListPastCasesActivity.this);
        }
    };
    private View.OnClickListener onHomeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    /* callback from server */
    private CaseListAdapter.OnClickChildItemListener onClickChildItem = new CaseListAdapter.OnClickChildItemListener() {
        @Override
        public void onClickChildItem(String caseId) {
            Intent i = new Intent(ListPastCasesActivity.this, NewsCaseDetailActivity.class);
            i.putExtra(Constants.KEY_CASE_ID, caseId);
            startActivity(i);
        }
    };
    //*** setOnQueryTextListener ***
    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            Utils.getInstance().showProgressDialog(ListPastCasesActivity.this, R.string.progress_wait, true);
            listPastCasesPresenter.searchPastCase(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_past_cases);
        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.listPastCaseAct_expandableListView);
        PastCases list = new PastCases();
        adapter = new CaseListAdapter(this, list);
        adapter.setOnClickChildItemListener(onClickChildItem);
        expandableListView.setAdapter(adapter);

        expandableListView.setOnGroupExpandListener(mExpandListener);
        expandableListView.setOnGroupCollapseListener(mCollapseListener);

        SearchView searchView = (SearchView) findViewById(R.id.listPastCaseAct_searchView);
        searchView.setOnQueryTextListener(onQueryTextListener);
        initToolBar();

        //load data from server
        Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
        listPastCasesPresenter = new ListPastCasesPresenter(this, this);
        listPastCasesPresenter.loadPastCases();
    }

    public void onClickAddNew(View view) {
        Intent i = new Intent(ListPastCasesActivity.this, NewsCaseDetailActivity.class);
        startActivity(i);
    }

    @Override
    public void onLoadPastCasesScreen(PastCases pastCases) {
        Utils.getInstance().hideProgressDialog();
        if (pastCases == null || pastCases.getPastCases().size() == 0) {
            return;
        }
        adapter.setArrData(pastCases);
        adapter.notifyDataSetChanged();
    }

    /* init toolbar */
    private void initToolBar() {
        ImageButton btnHome = (ImageButton) findViewById(R.id.toolBar_detailView_img_home);
        ImageButton btnLogout = (ImageButton) findViewById(R.id.toolBar_detailView_img_logout);
        MyTextView mTxtTitle = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        mTxtTitle.setText(getString(R.string.pastCase_title));
        btnLogout.setOnClickListener(onLogoutClick);
        btnHome.setOnClickListener(onHomeClick);
    }

}
