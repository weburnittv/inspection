package com.arise.healthcaresafety.presenter.impl;

import android.content.Context;

import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.ISplashModel;
import com.arise.healthcaresafety.model.entities.LoginResult;
import com.arise.healthcaresafety.model.impl.SplashModel;
import com.arise.healthcaresafety.model.listener.OnSplashListener;
import com.arise.healthcaresafety.presenter.ISplashPresenter;
import com.arise.healthcaresafety.utils.MySharedPreferences;
import com.arise.healthcaresafety.view.ISplashScreen;

/**
 * Created by Nam on 6/25/2015.
 */
public class SplashPresenter implements ISplashPresenter, OnSplashListener {
    private static final String TAG = SplashPresenter.class.getSimpleName();
    private ISplashScreen splashScr;
    private ISplashModel splashModel;
    private Context context;
    private MySharedPreferences mySharedPreferences;

    public SplashPresenter(Context context, ISplashScreen splashScr) {
        this.splashScr = splashScr;
        this.context = context;
        this.splashModel = new SplashModel(context, this);
        mySharedPreferences = new MySharedPreferences(context);
    }

    @Override
    public void login(String username, String password) {
        splashModel.login(username, password);
    }

    @Override
    public void onLoginResult(LoginResult result) {
        if (result.getStatus() == 200) {
            InspectionApp.getInstance().setUser(result.user);
            mySharedPreferences.putToken(result.getUser().getToken());
            splashScr.onLoginSuccess();
        }
    }

    @Override
    public void onLoginError(String msg) {
        splashScr.onLoginFailed(msg);
    }
}
