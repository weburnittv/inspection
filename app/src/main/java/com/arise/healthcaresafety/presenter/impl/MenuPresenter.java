package com.arise.healthcaresafety.presenter.impl;

import com.arise.healthcaresafety.presenter.IMenuPresenter;
import com.arise.healthcaresafety.view.IMenuActivity;

/**
 * Created by Nam on 6/26/2015.
 */
public class MenuPresenter implements IMenuPresenter {
    private IMenuActivity menuActivity;

    public MenuPresenter(IMenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    @Override
    public void logout() {
        menuActivity.onLogoutStatus(true);
    }
}
