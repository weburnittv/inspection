package com.arise.healthcaresafety.presenter;

import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;

/**
 * Created by hnam on 7/2/2015.
 */
public interface INewCaseDetailPresenter {

    void setUp();

    /**
     * load divisions and inspection category
     */
    void loadDivisions();

    void loadCategories();

    /* save inspection case */
    void saveCase();

    void updateQuestion(Question question);

    void updateEquipment(Equipment equipment);

    /**
     * update or create equipment note
     *
     * @param note
     */
    void updateEquipmentNote(Note note);

    /**
     * update or create checklist note of question
     *
     * @param note
     */
    void updateCheckListNote(Note note);

    /**
     * send out document
     *
     * @param email
     */
    void sendEmail(String email);

    /**
     * update final question
     *
     * @param question
     */
    void updateFinal(Question question);
}
