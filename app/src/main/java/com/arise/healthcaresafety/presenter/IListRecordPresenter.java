package com.arise.healthcaresafety.presenter;

import com.arise.healthcaresafety.model.entities.Media;

/**
 * Created by Nam on 7/26/2015.
 */
public interface IListRecordPresenter {
    public void deleteLocal(Media item);
}
