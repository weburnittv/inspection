package com.arise.healthcaresafety.presenter;

import android.content.Context;

import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;

import java.io.File;

/**
 * Created by hnam on 7/19/2015.
 */
public interface IListPhotoPresenter {
    public void copyFile(File src, File dest);

    public void upload(Context mActivity, Note note, Media item);

    public void delete(Media item);

    public void deleteLocal(Media item);
}
