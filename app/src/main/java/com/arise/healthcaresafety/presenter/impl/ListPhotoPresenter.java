package com.arise.healthcaresafety.presenter.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.arise.healthcaresafety.handler.AbstractHandlerListener;
import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.handler.MediaDelegationListener;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.presenter.IListPhotoPresenter;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.ImageUtils;
import com.arise.healthcaresafety.view.IListPhotoScreen;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hnam on 7/19/2015.
 */
public class ListPhotoPresenter extends AbstractHandlerListener implements IListPhotoPresenter,
        MediaDelegationListener {
    private static final String TAG = ListPhotoPresenter.class.getSimpleName();
    private IListPhotoScreen mlistPhotoScreen;
    private Context context;
    private ProgressDialog mDialog;
    private long noteID;
    private int mCount = 0;
    private List<Media> mediaList = new ArrayList<>();
    private AbstractHandlerListener callback;

    public ListPhotoPresenter(Context context, IListPhotoScreen listPhotoScreen) {
        this.context = context;
        this.mlistPhotoScreen = listPhotoScreen;
        mDialog = new ProgressDialog(context);
        mDialog.setMessage("Please wait loading ...");
        mDialog.setTitle("Uploading ...");
    }

    @Override
    public void copyFile(File src, File dest) {
        Log.d(TAG, "ttt" + " coppy");
        new CopyFileProgress().execute(src, dest);
    }

    @Override
    public void upload(Context activity, Note note, Media item) {
        Log.d(TAG, "" + item.toString());
        Log.d(TAG, "" + note.toString());
//        item.setNoteId(note.getmId());
        Log.e(TAG, ">>>>> localId" + note.getLiveId());
        Log.e(TAG, ">>>>> localIdkakaka" + note.getLiveId());
        noteID = note.getLiveId();
        item.setNoteId(note.getLiveId());
        Log.e(TAG, ">>>>> ttt" + noteID);
        UploadFileProgress progress = new UploadFileProgress(activity, noteID, item);
        progress.execute(item);
    }


    @Override
    public void delete(Media item) {
    }

    @Override
    public void deleteLocal(Media item) {
        File file = new File(item.getLocalPath());
        if (!file.exists()) {
            this.mlistPhotoScreen.onDeleteFile(false);
            return;
        }
        this.mlistPhotoScreen.onDeleteFile(file.delete());
    }

    @Override
    public void onUploadItem(boolean isSuccess, Media item) {
        //save live id for local record
        super.setLiveId(item.getLiveId());
        Delegation.getInstance().saveLiveId(context, getEntity());
    }

    @Override
    public void onDeleteItem(boolean isSuccess) {
    }


    /* async task for copy file to file*/
    private class CopyFileProgress extends AsyncTask<File, Integer, String> {
        @Override
        protected String doInBackground(File... params) {
            FileManager.getInstance().copyFile(params[0], params[1]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (ListPhotoPresenter.this.mlistPhotoScreen != null) {
                ListPhotoPresenter.this.mlistPhotoScreen.onCopySuccess();
            }
        }
    }

    /*async task for upload image file: 1-compress 2-upload */
    private class UploadFileProgress extends AsyncTask<Media, Void, String> {
        private Media media;
        private Context mActivity;
        private long noteid;

        public UploadFileProgress(Context mActivity, long noteid, Media m) {
            this.media = m;
            this.mActivity = mActivity;
            this.noteid = noteid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Media... params) {
            ImageUtils imageUtils = new ImageUtils();
            return imageUtils.decodeFile(params[0].getLocalPath(), ImageUtils.MAX_IMAGE_SIZE);
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            Log.d(TAG, "onPostExecute" + path);
            File file = new File(path);
            Log.d("ttt", "" + file.getAbsolutePath());
            if (file != null && file.length() > 0) {
                this.media.setUploadFile(file);
                this.media.setNoteId(noteid);
                Delegation.getInstance().saveEntity(context, this.media, ListPhotoPresenter.this);
            }
        }
    }
}
