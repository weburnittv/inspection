package com.arise.healthcaresafety.presenter;

/**
 * Created by Nam on 7/9/2015.
 */
public interface IListPastCasesPresenter {
    public void loadPastCases();

    public void searchPastCase(String key);
}
