package com.arise.healthcaresafety.presenter.impl;

import android.content.Context;

import com.arise.healthcaresafety.handler.AbstractHandlerListener;
import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.handler.MediaDelegationListener;
import com.arise.healthcaresafety.model.IListRecordModel;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.listener.OnListRecordListener;
import com.arise.healthcaresafety.presenter.IListRecordPresenter;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.view.IListRecordScreen;

/**
 * Created by Nam on 7/26/2015.
 */
public class ListRecordPresenter extends AbstractHandlerListener implements IListRecordPresenter,
        OnListRecordListener, MediaDelegationListener {
    private static final String TAG = ListRecordPresenter.class.getSimpleName();
    private Context context;
    private IListRecordScreen screen;
    private IListRecordModel model;

    public ListRecordPresenter(Context context, IListRecordScreen screen) {
        this.context = context;
        this.screen = screen;
    }

    @Override
    public void deleteLocal(Media item) {
        if (item == null) {
            this.screen.onDeleteOnLocal(false);
            return;
        }
        boolean status = FileManager.getInstance().deleteFile(item.getLocalPath());
        this.screen.onDeleteOnLocal(status);
    }

    @Override
    public void onUploadItem(boolean isSuccess, Media item) {
        if (isOnline() && item != null) {
            //store live id for result
            item.storeLiveId();
            //save live id for local record
            super.setLiveId(item.getLiveId());
            Delegation.getInstance().saveLiveId(context, getEntity());
        }
        ListRecordPresenter.this.screen.onUploadItem(isSuccess, (Media) super.getEntity());
    }

    @Override
    public void onDeleteItem(boolean isSuccess) {
        //update callback to notify delete on server or delete on local
        screen.onDeleteOnServer(isSuccess);
    }
}
