package com.arise.healthcaresafety.presenter.impl;

import android.content.Context;

import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.handler.PastCaseDelegationListener;
import com.arise.healthcaresafety.model.entities.PastCases;
import com.arise.healthcaresafety.presenter.IListPastCasesPresenter;
import com.arise.healthcaresafety.view.IListPastCasesScreen;

/**
 * Created by Nam on 7/9/2015.
 */
public class ListPastCasesPresenter implements IListPastCasesPresenter,
        PastCaseDelegationListener {
    private static final String TAG = ListPastCasesPresenter.class.getSimpleName();
    private IListPastCasesScreen listPastCasesScreen;
    private Context context;

    public ListPastCasesPresenter(Context context, IListPastCasesScreen listPastCasesScreen) {
        this.context = context;
        this.listPastCasesScreen = listPastCasesScreen;

    }

    @Override
    public void loadPastCases() {
        Delegation.getInstance().loadPastCases(this);
    }

    @Override
    public void searchPastCase(String key) {
        Delegation.getInstance().searchPastCases(this.context, key, this);
    }


    @Override
    public void onListPastCases(PastCases pastCases) {
        this.listPastCasesScreen.onLoadPastCasesScreen(pastCases);
    }

    @Override
    public void onSearchPastCases(PastCases pastCases) {
        this.listPastCasesScreen.onLoadPastCasesScreen(pastCases);
    }
}
