package com.arise.healthcaresafety.presenter;

/**
 * Created by Nam on 6/25/2015.
 */
public interface ISplashPresenter {
    /**
     * process login
     *
     * @param username:
     * @param password
     */
    public void login(String username, String password);
}
