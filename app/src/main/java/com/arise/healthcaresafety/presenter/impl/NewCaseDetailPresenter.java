package com.arise.healthcaresafety.presenter.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.arise.healthcaresafety.InspConstant;
import com.arise.healthcaresafety.NewsCaseDetailActivity;
import com.arise.healthcaresafety.handler.AbstractHandlerListener;
import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.handler.DelegationListener;
import com.arise.healthcaresafety.handler.implementation.api.sync.MailDelegatorHandler;
import com.arise.healthcaresafety.handler.implementation.api.sync.SendMailTask;
import com.arise.healthcaresafety.model.entities.Attribute;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Status;
import com.arise.healthcaresafety.model.helper.CaseHelper;
import com.arise.healthcaresafety.presenter.INewCaseDetailPresenter;
import com.arise.healthcaresafety.utils.MultipartUtility;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.INewCaseDetailScreen;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NewCaseDetailPresenter extends AbstractHandlerListener implements
        INewCaseDetailPresenter, DelegationListener, MailDelegatorHandler {
    private static final String TAG = NewCaseDetailPresenter.class.getSimpleName();
    private INewCaseDetailScreen screen;
    private Context context;

    /* case info screen */
    private Case mCase;
    private ProgressDialog mDialog;
    private long liveId;


    public NewCaseDetailPresenter(Context context, INewCaseDetailScreen screen) {
        this.screen = screen;
        this.context = context;
        mDialog = new ProgressDialog(context);
        mDialog.setMessage("Please wait loading ...");
        mDialog.setTitle("Uploading ...");
        mCase = ((NewsCaseDetailActivity) context).getCase();
    }

    /* REQUEST FROM ACTIVITY */
    @Override
    public void setUp() {
        this.screen.onSetUp(this.mCase);
    }

    @Override
    public void loadDivisions() {
        Delegation.getInstance().getLocalHandler().loadDivision(this);
    }

    @Override
    public void loadCategories() {
        Delegation.getInstance().getLocalHandler().loadCategories(this);
    }

    @Override
    public void saveCase() {
        if (this.mCase.getBranch() == null) {
            //please select branch;
            this.screen.onSaveCase(false);
            return;
        }

        if (this.mCase.getType() == null) {
            //please select type
            this.screen.onSaveCase(false);
            return;
        }

        if (TextUtils.isEmpty(this.mCase.getReferenceNo())) {
            //please enter referenceNo
            this.screen.onSaveCase(false);
            return;
        }

        if (TextUtils.isEmpty(this.mCase.getArrivalDate())) {
            this.screen.onSaveCase(false);
            return;
        }
        Delegation.getInstance().saveEntity(this.context, mCase, this);
    }

    @Override
    public void updateEquipment(Equipment equipment) {
        if (mCase == null) {
            screen.onError(InspConstant.ERROR_CASE_NOT_CREATED);
            return;
        }

        if (this.mCase.getLiveId() > 0)
            equipment.getResult().setCaseId(this.mCase.getLiveId());

        mCase.saveResult(equipment.getResult());
        mCase.saveNote(equipment.getNote());
//        Note exist = mCase.getNoteByEquipment(equipment.getLiveId());
//        if(equipment.isChecked())
//            mCase.saveNote(equipment.getNote());
//        if (exist != null) {
//            exist.setEquipment(equipment);
//            mCase.updateNote(exist);
//        } else mCase.saveNote(new Note(equipment));
    }

    @Override
    public void updateEquipmentNote(Note note) {
        if (mCase == null) {
            screen.onError(InspConstant.ERROR_CASE_NOT_CREATED);
            return;
        }

        if (this.mCase.getLiveId() > 0)
            note.setCaseId(this.mCase.getLiveId());
        mCase.saveNote(note);
    }

    @Override
    public void updateQuestion(Question question) {
        if (mCase == null) {
            screen.onError(InspConstant.ERROR_CASE_NOT_CREATED);
            return;
        }

        if (this.mCase.getLiveId() > 0)
            question.getResult().setCaseId(this.mCase.getLiveId());
    }


    @Override
    public void updateCheckListNote(Note note) {
        if (mCase == null) {
            screen.onError(InspConstant.ERROR_CASE_NOT_CREATED);
            return;
        }

        if (this.mCase.getLiveId() > 0)
            note.setCaseId(this.mCase.getLiveId());
        this.mCase.saveNote(note);
    }

    @Override
    public void sendEmail(String email) {
        Map<String, String> requestOjb = new HashMap<>();
        requestOjb.put("email", email);
        requestOjb.put("caseId", this.mCase.getLiveId() + "");

        SendMailTask mailTask = new SendMailTask(this.context, this, requestOjb);
        mailTask.run();
    }

    @Override
    public void updateFinal(Question question) {
        if (question == null) {
            return;
        }
        Attribute attr = question.getResult().getAttributes().get(0);
        mCase.saveCaseFinal(attr.getLiveId());
        Delegation.getInstance().saveEntity(this.context, this.mCase, this);
    }

    /* DELEGATION CALLBACK */
    @Override
    public void onDLoadDivisions(Divisions d) {
        this.screen.onLoadDivisions(d);
    }

    @Override
    public void onDLoadCategories(Categories categories) {
        this.screen.onLoadCategories(categories);
    }

    @Override
    public void onDLoadCheckList(CheckList checkList) {
        this.mCase.setCheckList(checkList);
        screen.onLoadStatus(true);
    }

    @Override
    public void onDLoadError(String message) {

    }

    @Override
    public void onDSaveCase(Case c) {
        this.mCase.updateCase(c);
        this.screen.onSaveCase(true);
    }

    @Override
    public void onDSaveResult(Result result) {
        if (this.isOnline()) {
            //store store local & live id for result
            result.storeId(this.getEntity());
            //save live id for local record
            super.setLiveId(result.getLiveId());
            Delegation.getInstance().saveLiveId(this.context, this.getEntity());
        }
        this.mCase.saveResult(result);
        screen.onLoadResult(result);
    }

    @Override
    public void onDDeleteResult(Result result, Status status) {
        if (status.isStatus()) {
            this.mCase.deleteResult(result);
        }
    }

    @Override
    public void onDUpdateNote(Note note) {
        this.mCase.updateNote(note);
        CaseHelper.getInstance().handleNote(this.mCase, note);
        screen.onSaveNote(true, note);
    }

    @Override
    public void onDSendEmailStatus(boolean status) {
        screen.onSendEmailStatus(status);
    }

    @Override
    public void onResponse(boolean status) {
        Log.e("onResponse", status + "");
    }

    private class UploadEmail extends AsyncTask<Void, Void, String> {

        private Context mActivity;
        private String result = null;
        private long liveID;
        private MultipartUtility utility;
        private String mEmail;
        private String url = "http://sotgapp.aevitasgroup.com/api/apps/inspection/sendmail.json";

        public UploadEmail(Context activity, long liveID, String email) {
            this.mActivity = activity;
            this.liveID = liveID;
            this.mEmail = email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                Log.d(TAG, "qqq" + url);
                Log.d(TAG, "qqq" + liveID);
                Log.d(TAG, "qqq" + mEmail);
                utility = new MultipartUtility(mActivity, url);
                utility.addFormField("form[case]", String.valueOf(liveID));
                utility.addFormField("form[email]", String.valueOf(mEmail));
                result = utility.finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (utility.getStatusCode() == 200) {
                Log.d(TAG, result);
                mDialog.dismiss();
            } else {
                mDialog.dismiss();
                return null;
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result == null) {
                mDialog.dismiss();
                Utils.getInstance().hideProgressDialog();
                Toast.makeText(mActivity, "Sent Email Not Success", Toast.LENGTH_LONG).show();
            } else {
                mDialog.dismiss();
                Utils.getInstance().hideProgressDialog();
                Toast.makeText(mActivity, "Sent Email Success", Toast.LENGTH_LONG).show();
            }
        }
    }
}
