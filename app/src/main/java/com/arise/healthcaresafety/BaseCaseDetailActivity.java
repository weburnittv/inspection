package com.arise.healthcaresafety;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.arise.healthcaresafety.view.adapter.ViewPagerAdapter;
import com.arise.healthcaresafety.view.custom.MyLogoutDialog;
import com.arise.healthcaresafety.view.custom.MyTextView;
import com.arise.healthcaresafety.view.slidingtab.SlidingTabLayout;
import com.arise.healthcaresafety.view.slidingtab.SlidingTabListener;

/**
 * Created by Nam on 8/10/2015.
 */
public abstract class BaseCaseDetailActivity extends AppCompatActivity {
    //private static final String TAG = BaseCaseDetailActivity.class.getSimpleName();
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    MyTextView mTxtTitle;
    /* logout on toolbar is clicked */
    private View.OnClickListener onLogoutClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyLogoutDialog.getInstance().showLogoutDialog(BaseCaseDetailActivity.this);
        }
    };
    private View.OnClickListener onHomeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    /**
     * listener event when tab changed
     */
    private ViewPager.OnPageChangeListener mSlidingTabListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            changeTitle(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private SlidingTabListener onTabListener = new SlidingTabListener() {
        @Override
        public void onSelectedTab(int position) {

        }

        @Override
        public void onErrorCase() {
            getOnTabChangedListener().onTabIsLocked();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_detail);
        initToolBar(); //init toolbar
        initTab(viewMode()); //Creating Tab
    }

    protected abstract int viewMode();

    protected abstract MyBaseTabChangedListener getOnTabChangedListener();

    /* LISTENER EVENT */

    /* init toolbar */
    private void initToolBar() {
        ImageButton btnHome = (ImageButton) findViewById(R.id.toolBar_detailView_img_home);
        ImageButton btnLogout = (ImageButton) findViewById(R.id.toolBar_detailView_img_logout);
        mTxtTitle = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        btnLogout.setOnClickListener(onLogoutClick);
        btnHome.setOnClickListener(onHomeClick);
        changeTitle(0);
    }

    private void initTab(int mode) {
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        String[] titles = getResources().getStringArray(R.array.newCases_tab_name);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),
                titles,
                titles.length,
                mode);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.caseItemAct_pager);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.caseItemAct_tabs);// Assiging the Sliding Tab Layout View
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setCustomTabView(R.layout.tab_section, R.id.tabSection_txt_title);

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimary);
            }
        });
        Integer[] iconResourceArray = {R.drawable.ic_case_info, R.drawable.ic_equipment, R.drawable.ic_checklist};
        tabs.setIconResourceArray(iconResourceArray);
        tabs.setOnPageChangeListener(mSlidingTabListener);//set listener for tab
        tabs.setViewPager(pager); // Setting the ViewPager For the SlidingTabsLayout
        tabs.setSlidingTabListener(onTabListener);
        if (mode == InspConstant.MODE_READ) {
            tabs.setInspectionCase(true);
        } else {
            tabs.setInspectionCase(false);
        }
    }

    /* UPDATE UI */
    private void changeTitle(int position) {
        if (position == 0) {
            mTxtTitle.setText(getResources().getStringArray(R.array.newCases_tab_name)[0]);
        } else if (position == 1) {
            mTxtTitle.setText(getResources().getStringArray(R.array.newCases_tab_name)[1]);
            //todo event when changed tab
            getOnTabChangedListener().onTabChanged(1);
        } else if (position == 2) {
            mTxtTitle.setText(getResources().getStringArray(R.array.newCases_tab_name)[2]);
            //todo event when changed tab
            getOnTabChangedListener().onTabChanged(2);
        }
    }

    protected void unBlockScrollTab() {
        tabs.setInspectionCase(true);
    }

    public interface MyBaseTabChangedListener {
        void onTabChanged(int position);

        void onTabIsLocked();
    }
}
