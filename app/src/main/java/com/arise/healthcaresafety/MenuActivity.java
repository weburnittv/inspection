package com.arise.healthcaresafety;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.handler.SyncBasicDataListener;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.Variations;
import com.arise.healthcaresafety.presenter.IMenuPresenter;
import com.arise.healthcaresafety.presenter.impl.MenuPresenter;
import com.arise.healthcaresafety.utils.SharedPreference;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.IMenuActivity;


public class MenuActivity extends AppCompatActivity implements IMenuActivity, SyncBasicDataListener, View.OnClickListener {
    private static final String TAG = MenuActivity.class.getSimpleName();
    private IMenuPresenter menuPresenter;
    private View.OnClickListener onLogoutClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            menuPresenter.logout();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initToolbar();
        findViewById(R.id.menu_ll_sync_case).setOnClickListener(this);
        findViewById(R.id.menu_ll_sync_organize).setOnClickListener(this);
        findViewById(R.id.menu_tv_export_db).setOnClickListener(this);
        menuPresenter = new MenuPresenter(this);
        validateToSyncData();
    }

    @Override
    public void onBackPressed() {
        // moveTaskToBack(true);
        super.onBackPressed();
    }

    public void onNewCaseClick(View view) {
        Intent i = new Intent(MenuActivity.this, NewsCaseDetailActivity.class);
        startActivity(i);
    }

    public void onPastCasesClick(View view) {
        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                Utils.showProgress(MenuActivity.this);
            }

            public void onFinish() {
                Utils.dismissProgress();
                Intent i = new Intent(MenuActivity.this, ListPastCasesActivity.class);
                startActivity(i);
            }

        }.start();
    }

    public void onReferencesClick(View view) {
        if (!Utils.isNetworkConnected(this)) {
            Toast.makeText(this, getString(R.string.notice_network_not_available), Toast.LENGTH_SHORT).show();
            return;
        }
        Intent i = new Intent(MenuActivity.this, ReferenceActivity.class);
        startActivity(i);
    }

    /* TOOLBAR */
    /* set up toolbar with custom view */
    private void initToolbar() {
        ImageButton btnHome = (ImageButton) findViewById(R.id.toolBar_detailView_img_home);
        ImageButton btnLogout = (ImageButton) findViewById(R.id.toolBar_detailView_img_logout);
        btnHome.setVisibility(View.INVISIBLE);
        btnLogout.setOnClickListener(onLogoutClick);
    }

    @Override
    public void onLogoutStatus(boolean status) {
        if (status) {
            showLogoutDialog();
        }
    }

    /* show dialog */
    private void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle(R.string.title_logout);
        builder.setMessage(getString(R.string.content_logout));
        builder.setCancelable(false);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MenuActivity.this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("ID", 1);
                Utils.resetUser(MenuActivity.this);
                startActivity(intent);
                MenuActivity.this.finish(); // call this to finish the current activity
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onDLoadDivisions(Divisions d) {
        Log.e("Sotg", "Finishing sync division:" + d.getDivisions().size());
    }

    @Override
    public void onDLoadEquipments(Equipments equipments) {
        Log.e("Sotg", "Finishing sync equipments:" + equipments.getEquipments().size());
        Utils.getInstance().hideProgressDialog();
    }

    @Override
    public void onDLoadCategories(Categories categories) {
        Log.e("Sotg", "Finishing sync cateogories:" + categories.getCategories().size());
    }

    @Override
    public void onDLoadCheckList(CheckList checklist) {
        Log.e("Sotg", "Finishing sync checklist:" + checklist.getLiveId());
    }

    @Override
    public void onDLoadVariations(Variations variations) {
        Log.e("Sotg", "Finishing sync variations:" + variations.getVariations().size());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_ll_sync_case:
                onSyncCaseData();
                break;
            case R.id.menu_ll_sync_organize:
                onSyncOrganizeData();
                break;
            default:
                break;
        }
    }

    private void onSyncCaseData() {
        if (!Utils.isNetworkConnected(this)) {
            Toast.makeText(this, getString(R.string.notice_network_not_available), Toast.LENGTH_SHORT).show();
            return;
        }
        Delegation.getInstance().syncCasesData(this);
/*        CaseApi caseApi = APIBuilder.createService(CaseApi.class);
        Map<String, String> params = new HashMap<>();
        params.put("case[branch]", "1");
        params.put("case[type]", "2");
        params.put("case[referenceNo]", "abc");
        params.put("case[arrivalDate]", "2016-01-05 10:34:22");
        params.put("case[final]", "0");
        RequestBody r1 = RequestBody.create(MediaType.parse("text/plain"),"1");
        RequestBody r2 = RequestBody.create(MediaType.parse("text/plain"),"2");
        RequestBody r3 = RequestBody.create(MediaType.parse("text/plain"),"abc");
        RequestBody r4 = RequestBody.create(MediaType.parse("text/plain"),"2016-01-05 10:34:22");
        RequestBody r5 = RequestBody.create(MediaType.parse("text/plain"), "0");
        //Call<String> call = caseApi.createCase(params);
        //final Call<String> call = caseApi.createCaseV1("1", "2", "abc", "2016-01-05 10:34:22", "0");
        //final Call<String> call = caseApi.createCaseV2(r1, r2, r3, r4, r5);
        Map<String, RequestBody> params1 = new HashMap<>();
        params1.put("case[branch]", r1);
        params1.put("case[type]", r2);
        params1.put("case[referenceNo]", r3);
        params1.put("case[arrivalDate]", r4);
        params1.put("case[final]", r5);
        final Call<String> call = caseApi.createCaseV3(params1);
*//*        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                Log.e("CaseTask", response.raw().message());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });*//*
        Case inspectionCase = null;
        new Thread(){
            @Override
            public void run() {
                super.run();
                try {
                    //inspectionCase = call.execute().body();
                    String tamp = call.execute().body().toString();
                   //Log.e("CaseTask", tamp);
                } catch (IOException e) {
                    return;
                }
            }
        }.start();*/

    }

    private void onSyncOrganizeData() {
        if (!Utils.isNetworkConnected(this)) {
            Toast.makeText(this, getString(R.string.notice_network_not_available), Toast.LENGTH_SHORT).show();
            return;
        }
        Delegation.getInstance().syncOrganizeData(this);
    }

    /**
     * This method using to check when app is open at the first time, checking network to sync
     */
    private void validateToSyncData() {
        if (SharedPreference.isFirstOpen(this)) {
            if (Utils.isNetworkConnected(this)) {
                onSyncOrganizeData();
                SharedPreference.setIsFirstOpen(this, false);
            } else {
                Toast.makeText(this, getString(R.string.notice_sync_first_time), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
