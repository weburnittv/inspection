package com.arise.healthcaresafety.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.utils.SharedPreference;

public class ManageServices extends Service {

    private static final String TAG = ManageServices.class.getSimpleName();
    public static Case mCase;
    private Context mContext;
    private boolean isRunning = false;
    private int i;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = ManageServices.this;
        if (mCase == null) {
            this.mCase = new Case();
        }
        Log.i(TAG, "Service onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service onStartCommand");
        SharedPreference.setCaseId(mContext, 1);
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {
        final int temp = SharedPreference.getInt(this, "LiveId");
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (temp == SharedPreference.getInt(ManageServices.this, "LiveId")) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        Log.i(TAG, "Service onDestroy");
    }
}