package com.arise.healthcaresafety.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.utils.FileManager;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Nam on 7/26/2015.
 */
public class AudioRecordService extends Service {
    public static final int UPDATE_RECORD_TIME = 0;
    public static final int UPDATE_LIST_RECORD = 1;
    public static final int WARNING_RECORDING = 2;
    public static final int WARNING_PLAYING = 3;
    public static final int NOTIFY_MEDIA_COMPLETION = 4;
    private static final String TAG = AudioRecordService.class.getSimpleName();
    private static String mFileName = "";
    //binder given to clients
    public final IBinder mBinder = new LocalBinder();
    /* timer task */
    Timer timer;
    TimerTask timerTask;
    int duration = 0; // in seconds
    /**
     * set root folder audio service
     *
     * @param path
     */
    boolean isRecording = false;
    boolean isPlaying = false;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;
    private String mRootFolder = null;
    private Handler handler = null;
    /**
     * start stop my playing
     *
     * @param RecordMedia
     */
    private Media mLocalMedia = null;
    /**
     * start stop url play
     *
     * @param item
     */
    private Media mServerMedia = null;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    /*start playback*/
    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                    sendMessage(null, NOTIFY_MEDIA_COMPLETION);
                }
            });
            this.isPlaying = true;
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }
    }

    /**
     * stop playing
     */
    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
        this.isPlaying = false;
        this.mFileName = null;
        mLocalMedia = null;
        mServerMedia = null;
    }

    /**
     * start recording
     */
    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
            isRecording = false;
        }
        isRecording = true;
    }

    /**
     * stop recording
     */
    private void stopRecording() {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
            isRecording = false;
            stopTimer(); // stop timer to update recording duration
            this.mFileName = null;
        }
    }

    /**
     * release audio record service
     */
    private void releaseAudioRecordService() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    /* get save path */
    private String getSavePath() {
        String path = this.mRootFolder + "/" + FileManager.getInstance().getUniqueName();
        //String extension = ".3gp";
        String extension = ".aac";
        Log.e(TAG, path + extension);
        return (path + extension);
    }

    private void startTimer() {
        timer = new Timer(); // init timer
        initializeTimerTask();//init timer task
        //schedule the timer, after the first 1000ms, the timerTask will run every 1000ms
        timer.schedule(timerTask, 1000, 1000);
    }


    /* AUDIO SERVICE API */

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            Bundle data = new Bundle();
            data.putString("PATH", this.mFileName);
            sendMessage(data, UPDATE_LIST_RECORD);
        }
    }

    private void initializeTimerTask() {
        duration = 1; // reset duration
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Bundle data = new Bundle();
                data.putString("DURATION", prepareTimer());
                sendMessage(data, UPDATE_RECORD_TIME);
                duration += 1;
            }
        };
    }

    private String prepareTimer() {
        int minutes = duration / 60;
        int seconds = duration % 60;
        String result;
        result = ((minutes < 10) ? "0" + minutes : String.valueOf(minutes)) +
                ":" +
                ((seconds < 10) ? "0" + seconds : String.valueOf(seconds));
        return result;
    }

    //send message with data
    private void sendMessage(Bundle data, int what) {
        if (this.handler == null) {
            return;
        }

        Message msg = Message.obtain(this.handler, what);
        msg.setData(data);
        msg.sendToTarget();
    }

    /**
     * set root folder
     *
     * @param path
     */
    public void setFolder(String path) {
        this.mRootFolder = path;
        File rootDirectory = new File(this.mRootFolder);
        if (!rootDirectory.exists()) {
            rootDirectory.mkdirs();
        }
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    /* start my recording */
    public void startMyRecording() {
        if (isPlaying) {
            //service is playing
            Bundle data = new Bundle();
            String msg = this.mFileName + " is playing. Please stop it before play one audio";
            data.putString("MSG", msg);
            sendMessage(data, WARNING_PLAYING);
            return;
        }
        this.mFileName = getSavePath(); //set path for save file recording
        startRecording(); // start recording
        startTimer();// start timer to update recording duration

    }

    /* stop my recording */
    public void stopMyRecording() {
        stopRecording(); // stop recording
    }

    /**
     * start my playing
     */
    private boolean startMyPlaying(String path) {
        if (isRecording) {
            //service is recording
            sendMessage(null, WARNING_RECORDING);
            return false;
        }

        if (isPlaying) {
            //service is playing
            Log.e(TAG, "other file is playing");
            Bundle data = new Bundle();
            String msg = this.mFileName + " is playing. Please stop it before play one audio";
            data.putString("MSG", msg);
            sendMessage(data, WARNING_PLAYING);
            return false;
        }

        this.mFileName = path;
        startPlaying(); // start playing
        return true;

    }

    /**
     * stop my playing
     */
    private void stopMyPlaying() {
        stopPlaying(); // stop playing
    }

    public void startStopMyPlay(Media media) {
        if (media.getLocalPath().equals(this.mFileName)) {
            stopMyPlaying();
        } else {
            boolean status = startMyPlaying(media.getLocalPath());
            if (status) {
                mLocalMedia = media;
            }
        }
    }

    public void startStopUrlPlay(Media item) {
        if (item.getUrlLink().equals(this.mFileName)) {
            stopMyPlaying();
        } else {
            boolean status = startMyPlaying(item.getUrlLink());
            if (status) {
                this.mServerMedia = item;
            }
        }
    }

    /**
     * check media
     *
     * @param item
     */
    public boolean isLocalPlay(Media item) {
        if (this.mLocalMedia == null) {
            return false;
        }
        return item.getLocalPath().equalsIgnoreCase(this.mLocalMedia.getLocalPath());
    }

    /**
     * check media
     *
     * @param item
     * @return
     */
    public boolean isServerPlay(Media item) {
        if (this.mServerMedia == null) {
            return false;
        }

        return item.getUrlLink().equalsIgnoreCase(this.mServerMedia.getUrlLink());
    }

    /**
     * class used for client binder. because we know this service always
     * runs in the same process as it's clients, we don't need to deal with IPC
     */
    public class LocalBinder extends Binder {
        public AudioRecordService getService() {
            //return this instance of localService so clients can call public methods
            return AudioRecordService.this;
        }
    }
}
