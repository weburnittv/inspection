package com.arise.healthcaresafety.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.arise.healthcaresafety.utils.InternetManager;
import com.arise.healthcaresafety.utils.Utils;


public class NetworkChangeReceiver extends BroadcastReceiver {

    public static int sIndex = 0;
    Handler handler = new Handler();

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (Utils.isNetworkConnected(context)) {
            if (sIndex == 0) {
                Toast.makeText(context, "internet connected! Case data is now being synced", Toast.LENGTH_LONG).show();
                intent = new Intent(context, ManageServices.class);
                context.startService(intent);
                sIndex = 1;
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sIndex = 0;
                }
            }, 5000);
        }
    }
}