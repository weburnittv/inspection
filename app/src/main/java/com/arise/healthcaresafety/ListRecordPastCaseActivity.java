package com.arise.healthcaresafety;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.services.AudioRecordService;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.adapter.ViewRecordListAdapter;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.lang.ref.WeakReference;

/**
 * Created by Nam on 7/28/2015.
 */
public class ListRecordPastCaseActivity extends AppCompatActivity implements ViewRecordListAdapter.RecordListListener {
    private static final String TAG = ListRecordPastCaseActivity.class.getSimpleName();
    /* HANDLER */
    private final MyHandler handler = new MyHandler(this);
    /* event of button */
    View.OnClickListener onBackClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    /* BOUND SERVICE */
    AudioRecordService mService = null;
    boolean mBound = false;
    //private RecyclerView recyclerView;
    private ViewRecordListAdapter adapter;
    private Note note;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            AudioRecordService.LocalBinder binder = (AudioRecordService.LocalBinder) service;
            mService = binder.getService();
            mService.setHandler(handler);
            Log.e(TAG, "connection to service successfully: ");
            mBound = true;
            initUI();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_list_record);
        Bundle data = getIntent().getExtras();
        this.note = data.getParcelable(Note.NOTE_PATH);

    }

    private void initUI() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.pastListRecordAct_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        adapter = new ViewRecordListAdapter(this, this.note.getNoteSoundMedias());
        adapter.setRecordListListener(this);
        adapter.setAudioService(mService);
        recyclerView.setAdapter(adapter);
        setUpToolBars();//setup toolbar
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, AudioRecordService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    /* option menu */
    private void setUpToolBars() {
        MyTextView title = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        ImageView btnBack = (ImageView) findViewById(R.id.toolBar_detailView_img_home);
        ImageView btnUpload = (ImageView) findViewById(R.id.toolBar_detailView_img_logout);

        //init value for toolbar
        title.setText(getString(R.string.audio_title));
        btnBack.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                R.drawable.ic_arrow_left_white_24dp,
                null));
        btnUpload.setVisibility(View.INVISIBLE);

        btnBack.setOnClickListener(onBackClick);
    }

    /*CallBack from adapter*/
    @Override
    public void onPlayStop(Media item) {
        Log.e(TAG, "start audio url");
        mService.startStopUrlPlay(item);
    }

    private static class MyHandler extends Handler {
        private final WeakReference<ListRecordPastCaseActivity> mActivity;

        public MyHandler(ListRecordPastCaseActivity act) {
            mActivity = new WeakReference<ListRecordPastCaseActivity>(act);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ListRecordPastCaseActivity act = mActivity.get();
            if (act != null) {
                switch (msg.what) {
                    case AudioRecordService.WARNING_PLAYING: {
                        Bundle data = msg.getData();
                        String errorMessage = data.getString("MSG");
                        Utils.getInstance().showDialog(act, errorMessage);
                        break;
                    }
                    case AudioRecordService.NOTIFY_MEDIA_COMPLETION: {
                        act.adapter.notifyDataSetChanged();
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}
