package com.arise.healthcaresafety;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.arise.healthcaresafety.listphoto.ListPhotoNewActivity;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Categories;
import com.arise.healthcaresafety.model.entities.Category;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Division;
import com.arise.healthcaresafety.model.entities.Divisions;
import com.arise.healthcaresafety.model.entities.Equipments;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Type;
import com.arise.healthcaresafety.presenter.INewCaseDetailPresenter;
import com.arise.healthcaresafety.presenter.impl.NewCaseDetailPresenter;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.RequestCode;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.INewCaseDetailScreen;
import com.arise.healthcaresafety.view.ITabScreenListener;
import com.arise.healthcaresafety.view.adapter.ViewPagerAdapter;
import com.arise.healthcaresafety.view.custom.MyTextView;
import com.arise.healthcaresafety.view.custom.equipment.EquipmentView;
import com.arise.healthcaresafety.view.fragment.CaseInfoScreenFragment;
import com.arise.healthcaresafety.view.slidingtab.SlidingTabLayout;
import com.arise.healthcaresafety.view.slidingtab.SlidingTabListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.uk.rushorm.core.RushSearch;


public class NewsCaseDetailActivity extends AppCompatActivity implements INewCaseDetailScreen, View.OnClickListener {
    private static final String TAG = NewsCaseDetailActivity.class.getSimpleName();
    public static Category category = null;
    public Case mCase;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    MyTextView mTxtTitle;
    int actionNote = InspConstant.DEFAULT_VALUE;
    private INewCaseDetailPresenter newCaseDetailPresenter;
    private View.OnClickListener onHomeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    private SlidingTabListener onTabListener = new SlidingTabListener() {
        @Override
        public void onSelectedTab(int position) {
            Log.e("Click", String.valueOf(position));
        }

        @Override
        public void onErrorCase() {
            warningCaseIsNoteCreated();
        }
    };

    private int currentIndex = 0;
    private Divisions divisions = null;
    private Categories categories = null;
    private Branch branch = null;
    private Type type = null;
    private Division division = null;
    private int selectedDivisionPosition = InspConstant.DEFAULT_VALUE;
    private int selectedCategoryPosition = InspConstant.DEFAULT_VALUE;
    private ITabScreenListener equipmentScreenListener = null;
    private ITabScreenListener checkListScreenListener = null;
    /**
     * listener event when tab changed
     */
    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            Log.e("Click", String.valueOf(position));
        }

        @Override
        public void onPageSelected(int position) {
            changeTitle(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private Note selectedNote = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_detail);
        getBundleData();
        initToolBar();
        initTab();
        newCaseDetailPresenter = new NewCaseDetailPresenter(this, this);
        newCaseDetailPresenter.setUp();
        loadDivisionAndInspectionCategories();
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String caseId = bundle.getString(Constants.KEY_CASE_ID);
            mCase = getCaseById(caseId);
        }
    }

    private Case getCaseById(String id) {
        return new RushSearch().whereId(id).findSingle(Case.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initToolBar() {
        ImageButton btnHome = (ImageButton) findViewById(R.id.toolBar_detailView_img_home);
        findViewById(R.id.toolBar_detailView_img_logout).setVisibility(View.GONE);
        findViewById(R.id.toolBar_detailView_tv_save).setVisibility(View.VISIBLE);
        findViewById(R.id.toolBar_detailView_tv_save).setOnClickListener(this);
        mTxtTitle = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        btnHome.setOnClickListener(onHomeClick);
        changeTitle(0);
    }

    private void initTab() {
        String[] titles = getResources().getStringArray(R.array.newCases_tab_name);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),
                titles,
                titles.length,
                InspConstant.MODE_EDIT);
        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.caseItemAct_pager);
        pager.setAdapter(adapter);
        tabs = (SlidingTabLayout) findViewById(R.id.caseItemAct_tabs);// Assiging the Sliding Tab Layout View
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setCustomTabView(R.layout.tab_section, R.id.tabSection_txt_title);
        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimary);
            }
        });
        Integer[] iconResourceArray = {R.drawable.ic_case_info, R.drawable.ic_equipment, R.drawable.ic_checklist};
        tabs.setIconResourceArray(iconResourceArray);
        tabs.setOnPageChangeListener(mOnPageChangeListener);//set viewpager listener
        tabs.setSlidingTabListener(onTabListener);//set tab listener
        if (mCase != null && mCase.getBranch() != null) {
            tabs.setInspectionCase(true);
        } else {
            tabs.setInspectionCase(false);
        }
        tabs.setViewPager(pager); // Setting the ViewPager For the SlidingTabsLayout
    }

    private void loadDivisionAndInspectionCategories() {
        Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
        newCaseDetailPresenter.loadDivisions();
        newCaseDetailPresenter.loadCategories();
    }

    private void changeTitle(int position) {
        currentIndex = position;
        showSaveButton(false);
        if (position == 0) {
            showSaveButton(true);
            mTxtTitle.setText(getResources().getStringArray(R.array.newCases_tab_name)[0]);
            return;
        }

        //check case is create or not when change to equipment tab or checklist tab
        if (position == 1) {
            mTxtTitle.setText(getResources().getStringArray(R.array.newCases_tab_name)[1]);
            return;
        }

        if (position == 2) {
            mTxtTitle.setText(getResources().getStringArray(R.array.newCases_tab_name)[2]);
        }

    }

    @Override
    public void onSetUp(Case c) {
        if (getIntent().getExtras() == null) {
            mCase = c;
        }
    }

    @Override
    public void onLoadDivisions(Divisions divisions) {
        this.divisions = divisions;
    }

    @Override
    public void onLoadCategories(Categories categories) {
        Utils.getInstance().hideProgressDialog();
        this.categories = categories;
    }

    @Override
    public void onSaveCase(boolean status) {
        Utils.getInstance().hideProgressDialog();
        if (status) {
            Utils.getInstance().showMessage(this, "Save case successfully.");
            tabs.setInspectionCase(true);
        } else {
            Utils.getInstance().showMessage(this, "Can't save case. Please fill empty fields.");
        }
    }

    @Override
    public void onLoadResults(List<Result> results) {
    }

    @Override
    public void onLoadResult(Result result) {
        Utils.getInstance().hideProgressDialog();
        if (result.getAttributes() == null || result.getAttributes().size() == 0) {
            Utils.getInstance().showMessage(this, R.string.save_text_entry_success);
        }
        if (currentIndex == 1) {
            if (equipmentScreenListener != null) {
                mCase.getEquipments().addEquipmentsResult(result);
            }
        } else if (currentIndex == 2) {
            if (checkListScreenListener != null) {
                mCase.getCheckList().setCheckListResult(mCase.getResults(), mCase.getNotes());
            }
        }
    }

    @Override
    public void onError(int errorCode) {
        Utils.getInstance().hideProgressDialog();
        if (errorCode == InspConstant.ERROR_CASE_NOT_CREATED) {
            Utils.getInstance().showMessage(this, "Please create case before selected Attribute");
        }
    }

    @Override
    public void onSendEmailStatus(boolean status) {
        if (currentIndex == 2) {
            if (checkListScreenListener != null) {
                checkListScreenListener.onSendEmailStatus(status);
            }
        }
    }

    @Override
    public void onLoadStatus(boolean status) {
        Utils.getInstance().hideProgressDialog();
    }

    @Override
    public void onSaveNote(boolean status, Note note) {
        String mediaPath = FileManager.getInstance().getCaseFolder(mCase, note);
        if (note != null) {
            Utils.getInstance().hideProgressDialog();
            if (this.actionNote == InspConstant.REQUEST_CODE_EQUIP_OPEN_PHOTO && !EquipmentView.isCheckState) {
                openPhotoScreen(note, mediaPath, InspConstant.REQUEST_CODE_EQUIP_OPEN_PHOTO);
            } else if (this.actionNote == InspConstant.REQUEST_CODE_EQUIP_OPEN_AUDIO && !EquipmentView.isCheckState) {
                openAudioScreen(note, mediaPath, InspConstant.REQUEST_CODE_EQUIP_OPEN_AUDIO);
            } else if (this.actionNote == InspConstant.REQUEST_CODE_CHECK_OPEN_PHOTO && !EquipmentView.isCheckState) {
                openPhotoScreen(note, mediaPath, InspConstant.REQUEST_CODE_CHECK_OPEN_PHOTO);
            } else if (this.actionNote == InspConstant.REQUEST_CODE_CHECK_OPEN_AUDIO && !EquipmentView.isCheckState) {
                openAudioScreen(note, mediaPath, InspConstant.REQUEST_CODE_CHECK_OPEN_AUDIO);
            } else {
                Utils.getInstance().showMessage(this, R.string.note_text_entry_success);
            }
        } else {
            Utils.getInstance().hideProgressDialog();
        }
    }

    /* DIVISION INFO  */
    public List<String> loadDivisionsNameList() {
        if (this.divisions == null) {
            return null;
        }
        return this.divisions.loadDivisionNameList();
    }

    public List<String> loadBranchNameList() {
        if (this.selectedDivisionPosition == InspConstant.DEFAULT_VALUE) {
            return null;
        }

        Division division = this.divisions.getDivisionByIndex(selectedDivisionPosition);
        return division.getBranchNameList();
    }

    /*get selected division*/
    public Division getSelectedDivision() {
        return this.division;
    }

    /* store selected divisions position which is selected */
    public void setDivisionById(int position) {
        this.selectedDivisionPosition = position;
        this.division = this.divisions.getDivisionByIndex(position);
    }

    public Branch getSelectedBranch() {
        return this.branch;
    }

    /**
     * store selected branch position which is selected
     *
     * @param position
     */
    public void setBranchById(int position) {
        Division division = this.divisions.getDivisionByIndex(selectedDivisionPosition);

        //create branch
        Branch branch = division.getBranches().get(position);
        Division brDivision = new Division();
        brDivision.setLiveId(division.getLiveId());
        brDivision.setName(division.getName());
        branch.setDivision(brDivision);

        this.branch = branch;
        //store selected branch into case
        mCase.setBranch(branch);
        this.adapter.updateEquipmentsCase(mCase);
    }

    /* INSPECTION CATEGORIES INFO */
    public List<String> loadCategoryNameList() {
        if (this.categories == null) {
            return null;
        }
        return this.categories.loadCategoryNameList();
    }

    public List<String> loadTypeNameList() {
        if (this.selectedCategoryPosition == InspConstant.DEFAULT_VALUE) {
            return null;
        }

        Category category = this.categories.getCategoriesByIndex(this.selectedCategoryPosition);
        return category.loadTypeNameList();
    }

    public List<Category> getCategories() {
        return this.categories.getCategories();
    }


    public Category getSelectedCategory() {
        return category;
    }

    public Type getSelectedType() {
        return this.type;
    }

    /**
     * store inspection category position
     *
     * @param position
     */
    public void setCategoryById(int position) {
        this.selectedCategoryPosition = position;
        category = this.categories.getCategoriesByIndex(position);
    }

    /**
     * store inspection type position and load type from server
     *
     * @param position
     */
    public void setTypeById(int position) {
        Category category = this.categories.getCategoriesByIndex(selectedCategoryPosition);
        Type type = category.getTypes().get(position);

        this.type = type;
        mCase.setType(type);//store type into case
        this.adapter.updateChecklistCase(mCase);
    }

    public Type getTypeById(int position) {
        Category category = this.categories.getCategoriesByIndex(selectedCategoryPosition);
        return category.getTypes().get(position);
    }

    /**
     * get equipments list
     *
     * @return ArrayList<EquipmentGroup>
     */
    public Equipments loadEquipments() {
        return mCase.getEquipments();
    }

    /**
     * get check list group
     *
     * @return ArrayList<CheckListGroup>
     */
    public CheckList loadCheckList() {
        return mCase.getCheckList();
    }

    /* CASE INFO */
    public void saveCase() {
        newCaseDetailPresenter.saveCase();
    }

    public void saveDateTimeArrival(String datetime) {
        //datetime format: Y-m-dTh:m:s
        if (mCase != null) {
            mCase.setArrivalDate(datetime);
        }
    }

    public String getDateTimeArrival(int index) {
        if (TextUtils.isEmpty(mCase.getArrivalDate())) {
            String date = Utils.getInstance().getDate(Utils.GET_DATE_TIME);
            date.replace(" ", "T");
            mCase.setArrivalDate(date);
        }

        if (index == Utils.GET_DATE) {
            return Utils.getInstance().getDate(Utils.GET_DATE);
        } else {
            return Utils.getInstance().getDate(Utils.GET_TIME);
        }
    }

    public void setEquipmentScreenListener(ITabScreenListener listener) {
        equipmentScreenListener = listener;
    }

    public void setCheckListScreenListener(ITabScreenListener listener) {
        checkListScreenListener = listener;
    }

    public INewCaseDetailPresenter getPresenter() {
        return newCaseDetailPresenter;
    }

    private void warningCaseIsNoteCreated() {
        Utils.getInstance().showDialog(this, getString(R.string.dialog_warning_save_case));
    }


    public void openPhotoScreen(Note note, String mediaPath, int requestCode) {
        this.selectedNote = note;
        Intent intent = new Intent(NewsCaseDetailActivity.this, ListPhotoNewActivity.class);
        intent.putExtra(Constants.KEY_SAVE_PATH, mediaPath);
        intent.putParcelableArrayListExtra(Constants.KEY_MEDIAS, (ArrayList<Media>) note.getNotePhotoMedias());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, requestCode);
    }

    public void openAudioScreen(Note note, String mediaPath, int requestCode) {
        this.selectedNote = note;
        Intent intent = new Intent(NewsCaseDetailActivity.this, ListRecordActivity.class);
        intent.putExtra(Constants.KEY_SAVE_PATH, mediaPath);
        intent.putParcelableArrayListExtra(Constants.KEY_MEDIAS, (ArrayList<Media>) note.getNoteSoundMedias());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, requestCode);
    }

    public void onUpdateNoteContent(Note note) {
        this.selectedNote = note;
        mCase.saveNote(selectedNote);
//        if (requestCode == RequestCode.CODE_REQUEST_EQUIPMENT
//                && equipmentScreenListener != null) {
//            equipmentScreenListener.bindingNote(selectedNote);
//        }
//
//        if (requestCode == RequestCode.CODE_REQUEST_CHECKLIST
//                && checkListScreenListener != null) {
//            checkListScreenListener.bindingNote(selectedNote);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == InspConstant.REQUEST_CODE_EQUIP_OPEN_PHOTO
                || requestCode == InspConstant.REQUEST_CODE_EQUIP_OPEN_AUDIO)
                && resultCode == RESULT_OK) {
            List<Media> medias = data.getParcelableArrayListExtra(Constants.KEY_MEDIAS);
            if (medias != null) {
                selectedNote.addMedia(medias);
                mCase.saveNote(selectedNote);
                if (equipmentScreenListener != null) {
                    equipmentScreenListener.updateCase(this.mCase);
                    equipmentScreenListener.bindingNote(selectedNote);
                }
            }
        } else if ((requestCode == InspConstant.REQUEST_CODE_CHECK_OPEN_PHOTO
                || requestCode == InspConstant.REQUEST_CODE_CHECK_OPEN_AUDIO)
                && resultCode == RESULT_OK) {
            List<Media> medias = data.getParcelableArrayListExtra(Constants.KEY_MEDIAS);
            if (medias != null) {
                selectedNote.addMedia(medias);
                mCase.saveNote(selectedNote);
                if (checkListScreenListener != null) {
                    equipmentScreenListener.updateCase(this.mCase);
                    checkListScreenListener.bindingNote(selectedNote);
                }
            }
        }
    }

    /* open preview case */
    public void openPreview() {
        Intent i = new Intent(NewsCaseDetailActivity.this, PreviewDetailActivity.class);
        i.putExtra(Constants.KEY_CASE_ID, mCase.getId());
        startActivity(i);
    }

    public void scrollTabById(int position) {
        pager.setCurrentItem(position);
        showSaveButton(false);
    }

    public String getTypeName() {
        if (this.type != null) {
            return this.type.getName();
        }
        return "";
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.toolBar_detailView_tv_save) {
            String referenceNo = CaseInfoScreenFragment.mEtRefNo.getText().toString().trim();
            mCase.setReferenceNo(referenceNo);
            mCase.setCreatedDate(Utils.getCreateDate());
            saveCase();
        }
    }

    public Case getCase() {
        if(mCase == null)
            mCase = new Case();
        return mCase;
    }

    public void setCase(Case mCase) {
        this.mCase = mCase;
    }

    public void showSaveButton(boolean isShow) {
        findViewById(R.id.toolBar_detailView_tv_save).setVisibility(View.GONE);
        if (isShow) {
            findViewById(R.id.toolBar_detailView_tv_save).setVisibility(View.VISIBLE);
        }
    }

}
