package com.arise.healthcaresafety.eventBus.model;

import com.arise.healthcaresafety.model.entities.Media;

/**
 * Created by justin on 15/10/24.
 */
public class PostMedia {
    private Media media;

    public PostMedia(Media media) {
        this.media = media;
    }


    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }
}
