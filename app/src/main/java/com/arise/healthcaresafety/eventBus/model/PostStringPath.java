package com.arise.healthcaresafety.eventBus.model;

/**
 * Created by justin on 15/10/27.
 */
public class PostStringPath {
    private String path;

    public PostStringPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
