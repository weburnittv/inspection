package com.arise.healthcaresafety.eventBus.model;

/**
 * Created by cuongnv on 31/10/15.
 */
public class PostPaint {
    public boolean isCheck;

    public PostPaint(boolean isCheck) {
        this.isCheck = isCheck;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setIsCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }
}
