package com.arise.healthcaresafety;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.presenter.IListRecordPresenter;
import com.arise.healthcaresafety.presenter.impl.ListRecordPresenter;
import com.arise.healthcaresafety.services.AudioRecordService;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.IListRecordScreen;
import com.arise.healthcaresafety.view.adapter.RecordListAdapter;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class ListRecordActivity extends AppCompatActivity implements IListRecordScreen,
        RecordListAdapter.RecordListListener {
    private static final String TAG = ListRecordActivity.class.getSimpleName();
    private final MyHandler handler = new MyHandler(this);
    AudioRecordService mService = null;
    boolean mBound = false;
    Media selectedMedia = null;
    private String mStrPath;
    private ToggleButton mBtStartStop; // false: start - true: stop
    private MyTextView mTvStatus;
    private MyTextView mTvDuration;
    CompoundButton.OnCheckedChangeListener onStartStopRecord = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                //stop record
                Log.e(TAG, "start record");
                mTvStatus.setText(getString(R.string.stop_new_recording));
                mService.startMyRecording();
            } else {
                //start record
                Log.e(TAG, "stop record");
                mTvStatus.setText(getString(R.string.start_new_recording));
                mTvDuration.setText("00:00");
                mService.stopMyRecording();
            }
        }
    };
    private RecyclerView mRecyclerView;
    private ArrayList<Media> mMedias;
    /* event of button */
    View.OnClickListener onBackClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    private IListRecordPresenter presenter;
    private RecordListAdapter mAudioAdapter;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            AudioRecordService.LocalBinder binder = (AudioRecordService.LocalBinder) service;
            mService = binder.getService();
            mService.setHandler(handler);
            mService.setFolder(mStrPath);
            mBound = true;
            initUI();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_record);
        Bundle data = getIntent().getExtras();
        this.mStrPath = data.getString(Constants.KEY_SAVE_PATH);

        Intent intent = this.getIntent();
        this.mMedias = intent.getParcelableArrayListExtra(Constants.KEY_MEDIAS);
        this.presenter = new ListRecordPresenter(this, this);//presenter
    }

    private void initUI() {
        mBtStartStop = (ToggleButton) findViewById(R.id.listRecordAct_toggle_startStop);
        mTvStatus = (MyTextView) findViewById(R.id.listRecordAct_txt_startStop);
        mTvDuration = (MyTextView) findViewById(R.id.listRecordAct_txt_duration);
        mRecyclerView = (RecyclerView) findViewById(R.id.listRecordAct_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        refreshAdapter();
        setUpToolBars();//setup toolbar
        initEvent(); //init event
    }

    private void refreshAdapter() {
        mAudioAdapter = new RecordListAdapter(this, mMedias);
        mAudioAdapter.setRecordListListener(this);
        mAudioAdapter.setAudioService(mService);
        mRecyclerView.setAdapter(mAudioAdapter);
    }

    private List<Media> deleteFileNotExit(List<Media> medias) {
        List<Media> medias1 = new ArrayList<>();
        for (int i = 0; i < medias.size(); i++) {
            File file = new File(medias.get(i).getLocalPath());
            if (file.exists()) {
                medias1.add(medias.get(i));
            }
        }
        return medias1;
    }

    @Override
    public void onBackPressed() {
       // onSaveNote(mMedias);
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, AudioRecordService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private void onSaveNote(ArrayList<Media> medias) {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(Constants.KEY_MEDIAS, medias);
        setResult(RESULT_OK, intent);
        finish();
    }

    /* option menu */
    private void setUpToolBars() {
        MyTextView title = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        ImageView btnBack = (ImageView) findViewById(R.id.toolBar_detailView_img_home);
        ImageView btnUpload = (ImageView) findViewById(R.id.toolBar_detailView_img_logout);

        //init value for toolbar
        title.setText(getString(R.string.audio_title));
        btnBack.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                R.drawable.ic_arrow_left_white_24dp,
                null));
        btnUpload.setVisibility(View.INVISIBLE);

        btnBack.setOnClickListener(onBackClick);
        TextView tvSaveNote = (TextView) findViewById(R.id.toolBar_detailView_tv_save);
        tvSaveNote.setVisibility(View.VISIBLE);
        tvSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveNote(mMedias);
                finish();
            }
        });
    }

    private void initEvent() {
        mBtStartStop.setOnCheckedChangeListener(onStartStopRecord);
        /*MediaPlayer mp = MediaPlayer.create(ListRecordActivity.this, Uri.parse("/storage/emulated/0/Inspection/c43/n9/audio_20150727_121046.3gp"));
        int duration = mp.getDuration();
        Log.e(TAG, "duration: " + duration);
        mp.release();
        mp = null;*/
    }

    /* UTILS FUNCTION */
    private void upload(String path) {
        if (path == null) {
            Utils.getInstance().showMessage(this, "file not found");
            return;
        }
        //upload item
        //  Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
        // presenter.upload(this.note, new Media(path, Media.MEDIA_SOUND));
        mMedias.add(new Media(path, Media.MEDIA_SOUND));
        refreshAdapter();
    }

    @Override
    public void deleteMedia(Media item) {
        Log.e(TAG, "delete on local");
        presenter.deleteLocal(item);
    }


    @Override
    public void onDeleteOnServer(boolean isSuccess) {
        Utils.getInstance().hideProgressDialog();
        if (isSuccess) {
            removeResult();
            presenter.deleteLocal(selectedMedia);
        } else {
            Utils.getInstance().showMessage(this, "Network error.. Please try again");
        }
    }

    @Override
    public void onDeleteOnLocal(boolean isSuccess) {
        if (isSuccess) {
            removeResult();
            Utils.getInstance().showMessage(this, "delete success");
        } else {
            Utils.getInstance().showMessage(this, R.string.warning_delete_failed);
        }
    }

    @Override
    public void onUploadItem(boolean isSuccess, Media item) {
        Utils.getInstance().hideProgressDialog();
        if (isSuccess) {
            Utils.getInstance().showMessage(this, R.string.upload_success);
            mAudioAdapter.addMedia(item);
            //mAudioAdapter.setIdForMedia(item);
            //result.add(item);
        } else {
            Utils.getInstance().showMessage(this, R.string.upload_failed);
        }
    }

    @Override
    public void onSaveItem(boolean isSuccess, Media item) {
        Utils.getInstance().hideProgressDialog();
        if (isSuccess) {
            Utils.getInstance().showMessage(this, R.string.save_success);
        } else {
            Utils.getInstance().showMessage(this, R.string.save_failed);
        }
    }

    @Override
    public void onDeleteOffline(boolean isSuccess) {
        Utils.getInstance().hideProgressDialog();
        if (isSuccess) {
            presenter.deleteLocal(selectedMedia);
        } else {
            Utils.getInstance().showMessage(this, "delete failed");
        }
    }

    public void removeResult() {
        if (this.selectedMedia == null) {
            return;
        }
        //TODO remove media
/*        for(Media media : result){
            if(media.getName().equalsIgnoreCase(this.selectedRecordMedia.getFileNameOnLocal())){
                result.remove(media);
                break;
            }
        }*/
    }

    private static class MyHandler extends Handler {
        private final WeakReference<ListRecordActivity> mActivity;

        public MyHandler(ListRecordActivity act) {
            mActivity = new WeakReference<ListRecordActivity>(act);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ListRecordActivity act = mActivity.get();
            if (act != null) {
                switch (msg.what) {
                    case AudioRecordService.UPDATE_RECORD_TIME: {
                        Bundle data = msg.getData();
                        String duration = data.getString("DURATION", "00:00");
                        act.mTvDuration.setText(duration);
                        break;
                    }
                    case AudioRecordService.UPDATE_LIST_RECORD: {
                        Bundle data = msg.getData();
                        String path = data.getString("PATH", null);
                        act.upload(path);
                        break;
                    }
                    case AudioRecordService.WARNING_RECORDING: {
                        Utils.getInstance().showDialog(act, act.getString(R.string.warning_recording));
                        break;
                    }
                    case AudioRecordService.WARNING_PLAYING: {
                        Bundle data = msg.getData();
                        String errorMessage = data.getString("MSG");
                        Utils.getInstance().showDialog(act, errorMessage);
                        break;
                    }
                    case AudioRecordService.NOTIFY_MEDIA_COMPLETION: {
                        act.mAudioAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        }
    }
}
