package com.arise.healthcaresafety;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.model.entities.InspReference;
import com.arise.healthcaresafety.model.entities.InspReferenceFactory;
import com.arise.healthcaresafety.model.network.api.ReferencesApi;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.view.adapter.PhotoGridMarginDecoration;
import com.arise.healthcaresafety.view.adapter.ViewReferenceGridAdapter;
import com.arise.healthcaresafety.view.custom.MyLogoutDialog;
import com.arise.healthcaresafety.view.custom.MyTextView;

import java.util.ArrayList;
import java.util.List;

public class ReferenceActivity extends AppCompatActivity implements
        ViewReferenceGridAdapter.ReferenceGridAdapterListener {
    private static final String TAG = ReferenceActivity.class.getSimpleName();
    View.OnClickListener onHomeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
    View.OnClickListener onLogoutClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyLogoutDialog.getInstance().showLogoutDialog(ReferenceActivity.this);
        }
    };
    //private RecyclerView recyclerView;
    private ViewReferenceGridAdapter adapter;
    private List<InspReference> list = new ArrayList<>();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reference);
        recyclerView = (RecyclerView) findViewById(R.id.refAct_recyclerView);
        recyclerView.addItemDecoration(new PhotoGridMarginDecoration(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new ViewReferenceGridAdapter(this, list);
        adapter.setReferenceGridAdapterListener(this);
        recyclerView.setAdapter(adapter);

        setUpToolBars();

        Utils.getInstance().showProgressDialog(this, R.string.progress_loading, false);
        ReferencesApi api = new ReferencesApi(this);
        api.getReferencesApi(new Response.Listener<String[]>() {
            @Override
            public void onResponse(String[] strings) {
                Log.e(TAG, "size of" + strings.length);
                for (int i = 0; i < strings.length; i++) {
                    list.add(InspReferenceFactory.create(strings[i]));
                    Log.d("qqq", "" + list.get(i).getLink());
                }
                adapter.notifyDataSetChanged();
                Utils.getInstance().hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utils.getInstance().hideProgressDialog();
            }
        });
    }

    /* option menu */
    private void setUpToolBars() {
        MyTextView title = (MyTextView) findViewById(R.id.toolBar_detailView_title);
        ImageView btnHome = (ImageView) findViewById(R.id.toolBar_detailView_img_home);
        ImageView btnLogout = (ImageView) findViewById(R.id.toolBar_detailView_img_logout);

        //init value for toolbar
        title.setText(getString(R.string.reference_title));

        btnHome.setOnClickListener(onHomeClick);
        btnLogout.setOnClickListener(onLogoutClick);
    }

    @Override
    public void onItemClick(InspReference item) {
        /*Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(item.getLink()));
        startActivity(i);*/
        if (item.getType() == InspReferenceFactory.IMAGE_REF) {
//            Intent i = new Intent(ReferenceActivity.this, PhotoViewActivity.class);
            Intent i = new Intent(ReferenceActivity.this, ReadWebViewPDFActivity.class);
            i.putExtra("PATH", item.getLink());
            startActivity(i);
        } else {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(item.getLink()));
            startActivity(i);
        }
    }
}
