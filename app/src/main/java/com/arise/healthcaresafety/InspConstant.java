package com.arise.healthcaresafety;

/**
 * Created by hnam on 7/5/2015.
 */
public class InspConstant {
    public static final int DEFAULT_VALUE = -1;
    public static final int DIALOG_SELECT_DIVISION = 0;
    public static final int DIALOG_SELECT_BRANCH = 1;
    public static final int DIALOG_SELECT_INSP_CATETORY = 2;
    public static final int DIALOG_SELECT_INSP_TYPE = 3;

    public static final int MODE_EDIT = 0;
    public static final int MODE_READ = 1;
    public static final int MODE_OFFLINE_EDIT = 2;

    public static final int ERROR_CASE_NOT_CREATED = 0;


    //public static final int ACTION_OPEN_PHOTO_SCREEN = 0;
    //public static final int ACTION_OPEN_AUDIO_SCREEN = 1;

    public static final int REQUEST_CODE_EQUIP_OPEN_PHOTO = 1;
    public static final int REQUEST_CODE_EQUIP_OPEN_AUDIO = 2;
    public static final int REQUEST_CODE_CHECK_OPEN_PHOTO = 3;
    public static final int REQUEST_CODE_CHECK_OPEN_AUDIO = 4;


}
