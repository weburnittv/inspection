package com.arise.healthcaresafety.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Cuongnv 2015-11-19
 */
public class MySharedPreferences {
    public final String PREFS_NAME = "myPrefs";
    private final String CLIENT_HOST = "CLIENT_HOST";
    private final String CLIENT_EXPIRED_AT = "CLIENT_EXPIRED_AT";
    private final String CLIENT_LOGO = "CLIENT_LOGO";
    private final String CLIENT_EMAIL = "CLIENT_EMAIL";
    SharedPreferences.Editor editor;
    SharedPreferences mSharedPreferences;

    public MySharedPreferences(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(PREFS_NAME, 0);
        editor = mSharedPreferences.edit();
    }

    // Put username
    public void putUser(String userName) {
        editor.putString("username", userName);
        editor.commit();
    }

    // Get username
    public String getUser() {
        return mSharedPreferences.getString("username", "");
    }

    // Put token
    public void putToken(String token) {
        editor.putString("token", token);
        editor.commit();
    }

    // Get token
    public String getToken() {
        return mSharedPreferences.getString("token", "");
    }

    public String getClientHost() {
        return mSharedPreferences.getString(CLIENT_HOST, "");
    }

    public void setClientHost(String host) {
        editor.putString(CLIENT_HOST, host);
        editor.commit();
    }

    public String getClientExpiredAt() {
        return mSharedPreferences.getString(CLIENT_EXPIRED_AT, "");
    }

    public void setClientExpiredAt(String ex) {
        editor.putString(CLIENT_EXPIRED_AT, ex);
        editor.commit();
    }

    public String getClientLogo() {
        return mSharedPreferences.getString(CLIENT_LOGO, "");
    }

    public void setClientLogo(String logo) {
        editor.putString(CLIENT_LOGO, logo);
        editor.commit();
    }

    public String getClientEmail() {
        return mSharedPreferences.getString(CLIENT_EMAIL, "");
    }

    public void setClientEmail(String email) {
        editor.putString(CLIENT_EMAIL, email);
        editor.commit();
    }
}