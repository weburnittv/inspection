package com.arise.healthcaresafety.utils;

public class InternetManager {

    private static InternetManager mInstance = new InternetManager();

    public InternetManager() {
        //empty constructor
    }

    public static InternetManager getInstance() {
        return mInstance;
    }
}
