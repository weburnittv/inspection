package com.arise.healthcaresafety.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arise.healthcaresafety.R;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {
    /* get data formmat "Y-m-d HH:mm:ss"*/
    public static final int GET_DATE = 0;
    public static final int GET_TIME = 1;
    public static final int GET_DATE_TIME = 2;
    public static final String TAG = "Utils";
    public static final int DEFAULT_BUFFER_SIZE = 8192;
    /**
     * A dialog showing a progress indicator and an optional text message or
     * view.
     */
    public static ProgressDialog mProgressDialog;
    private static Utils instance = new Utils();
    private static String sFormatEmail = "^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\\.[a-zA-Z]{2,4}$";

    /**
     *
     */
    protected Utils() {

    }

    public static Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

    public static void resetUser(Context context) {
        SharedPreference.setUserName(context, "");
        SharedPreference.setPassword(context, "");
    }

    /**
     * @return true if JellyBean or higher
     */
    public static boolean isJellyBeanOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    /**
     * @return true if Ice Cream or higher
     */
    public static boolean isICSOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    /**
     * @return true if HoneyComb or higher
     */
    public static boolean isHoneycombOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * @return true if GingerBreak or higher
     */
    public static boolean isGingerbreadOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    /**
     * @return true if Froyo or higher
     */
    public static boolean isFroyoOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    /**
     * Check SdCard
     *
     * @return true if External Strorage available
     */
    public static boolean isExtStorageAvailable() {
        return Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState());
    }

    /**
     * Check internet
     *
     * @param context
     * @return true if Network connected
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.isConnected();
        }
        return false;
    }

    /**
     * Check wifi
     *
     * @param context
     * @return true if Wifi connected
     */
    public static boolean isWifiConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetworkInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetworkInfo != null) {
            return wifiNetworkInfo.isConnected();
        }
        return false;
    }

    /**
     * Check on/off gps
     *
     * @return true if GPS available
     */
    public static boolean checkAvailableGps(Context context) {
        LocationManager manager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Download data url
     *
     * @param urlString
     * @return InputStream
     * @throws IOException IOException
     */
    public static InputStream downloadUrl(String urlString) throws IOException {
        HttpURLConnection conn = buildHttpUrlConnection(urlString);
        conn.connect();

        InputStream stream = conn.getInputStream();
        return stream;
    }

    /**
     * @return an {@link HttpURLConnection} using sensible default settings for
     * mobile and taking care of buggy behavior prior to Froyo.
     * @throws IOException exception
     */
    public static HttpURLConnection buildHttpUrlConnection(String urlString)
            throws IOException {
        Utils.disableConnectionReuseIfNecessary();

        URL url = new URL(urlString);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setDoInput(true);
        conn.setRequestMethod("GET");
        return conn;
    }

    /**
     * Prior to Android 2.2 (Froyo), {@link HttpURLConnection} had some
     * frustrating bugs. In particular, calling close() on a readable
     * InputStream could poison the connection pool. Work around this by
     * disabling connection pooling.
     */
    public static void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (!isFroyoOrHigher()) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    /**
     * Check an email is valid or not
     *
     * @param email the email need to check
     * @return {@code true} if valid, {@code false} if invalid
     */
    public static boolean isValidEmail(Context context, String email) {
        boolean result = false;
        Pattern pt = Pattern.compile(sFormatEmail);
        Matcher mt = pt.matcher(email);
        if (mt.matches()) {
            result = true;
        }
        return result;
    }

    public static boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    /**
     * Gets html content from the assets folder.
     */
    public static String getHtmlFromAsset(Context context, int html_file) {
        InputStream is;
        StringBuilder builder = new StringBuilder();
        String htmlString = "";
        try {
            is = context.getAssets().open(context.getString(html_file));
            if (is != null) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                htmlString = builder.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return htmlString;
    }

    public static Bitmap getRoundedCornerBitmap(Context context, Bitmap input, int pixels, int w, int h, boolean squareTL, boolean squareTR, boolean squareBL, boolean squareBR) {

        Bitmap output = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, w, h);
        final RectF rectF = new RectF(rect);

        //make sure that our rounded corner is scaled appropriately
        final float roundPx = pixels * densityMultiplier;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);


        //draw rectangles over the corners we want to be square
        if (squareTL) {
            canvas.drawRect(0, 0, w / 2, h / 2, paint);
        }
        if (squareTR) {
            canvas.drawRect(w / 2, 0, w, h / 2, paint);
        }
        if (squareBL) {
            canvas.drawRect(0, h / 2, w / 2, h, paint);
        }
        if (squareBR) {
            canvas.drawRect(w / 2, h / 2, w, h, paint);
        }

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(input, 0, 0, paint);

        return output;
    }

    /**
     * @param msgResId
     * @param keyListener
     * @param task
     */
    public static void showProgress(Context context, int msgResId,
                                    DialogInterface.OnKeyListener keyListener, AsyncTask<?, ?, ?> task) {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                return;
            }
            mProgressDialog = new ProgressDialog(context);
            // mProgressDialog.setCancelable(false);
            if (msgResId > 0) {
                mProgressDialog.setMessage(context.getResources().getString(msgResId));
            }
            if (keyListener != null) {
                mProgressDialog.setOnKeyListener(keyListener);
            }
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            if (task != null) {
                mProgressDialog.setOnCancelListener(new CancelListener(task));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * cancel progress dialog.
     */
    public static void dismissProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     *
     */
    public static void showProgress(Context context) {
        showProgress(context, R.string.mess_waitting, null, null);
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public static void saveBitmap(Context context, final View v) {
        AlertDialog.Builder editalert = new AlertDialog.Builder(context);
        editalert.setTitle("Save Letter");
        final EditText input = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT);
        lp.topMargin = 20;
        input.setLayoutParams(lp);
        input.setHint("Enter Letter name...");
        editalert.setView(input);
        editalert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String name = input.getText().toString();
                Bitmap bitmap = v.getDrawingCache();
                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/saved_images");
                myDir.mkdirs();
                String fname = "LetterToSanta-" + name + ".jpg";
                File file = new File(myDir, fname);
                if (file.exists()) file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        editalert.show();
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void saveDrawing(View v) {
        Bitmap whatTheUserDrewBitmap = v.getDrawingCache();
        // don't forget to clear it (see above) or you just get duplicates

        // almost always you will want to reduce res from the very high screen res
        whatTheUserDrewBitmap =
                ThumbnailUtils.extractThumbnail(whatTheUserDrewBitmap, 256, 256);
        // NOTE that's an incredibly useful trick for cropping/resizing squares
        // while handling all memory problems etc
        // http://stackoverflow.com/a/17733530/294884

        // you can now save the bitmap to a file, or display it in an ImageView:
//        ImageView testArea = ...
//        testArea.setImageBitmap( whatTheUserDrewBitmap );

        // these days you often need a "byte array". for example,
        // to save to parse.com or other cloud services
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        whatTheUserDrewBitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
        byte[] yourByteArray;
        yourByteArray = baos.toByteArray();
    }

    public static String getFacebookKeyHash(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.lettertosanta",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                return Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        } catch (NoSuchAlgorithmException ignored) {
            return null;
        }

        return null;
    }

    public static Bitmap getBitmapFromFile(String url) {
        if (url == null) {
            return null;
        }
        try {
            File file = new File(url);
            return BitmapFactory.decodeStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            return null;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    /*
     * getting screen width
     */
    @SuppressWarnings("deprecation")
    public int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        final Point point = new Point();
        try {
            display.getSize(point);
            point.x -= 16 * Math.round(context.getResources().getDisplayMetrics().density);
        } catch (NoSuchMethodError ignore) {
            // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }

    public int getDensity(Context context) {
        return Math.round(context.getResources().getDisplayMetrics().density);
    }

    /**
     * Check email is valid or not
     *
     * @param email
     * @return
     */
    public boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    /**
     * show message
     *
     * @param context
     * @param msg
     */
    public void showMessage(Context context, String msg) {
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * show message
     *
     * @param context
     * @param msg
     */
    public void showMessage(Context context, int msg) {
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Show dialog
     *
     * @param msg
     */
    public void showDialog(Context context, String msg) {
        Builder builder = new Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle(R.string.dialog_warning);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.dialog_close, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showOrHideKeyBoard(Activity act, boolean isShow, View view) {
        if (isShow) {
            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        } else {
            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), 0);
        }
    }

    /* read imei of phone */
    public String getPhoneImei(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public String getDeviceUniqueId(Context context) {
        String device_unique_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }

    /* get distance from 2 location coordinates */
    public Double getDistanceBetweenTwoPoints(Double latitude1, Double longitude1, Double latitude2, Double longitude2) {
        final int RADIUS_EARTH = 6371;

        double dLat = getRad(latitude2 - latitude1);
        double dLong = getRad(longitude2 - longitude1);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(getRad(latitude1)) * Math.cos(getRad(latitude2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (RADIUS_EARTH * c) * 1000;
    }

    private Double getRad(Double x) {
        return x * Math.PI / 180;
    }

    /* show progress dialog */
    //private ProgressDialog mProgressDialog = null;
    public void showProgressDialog(Context context, int stringId, boolean isCancelable) {
        mProgressDialog = new ProgressDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(isCancelable);
        mProgressDialog.setMessage(context.getResources().getString(stringId));
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /* check string have whitespace */
    public boolean isWhiteSpaces(String s) {
        return s != null && s.contains(" ");
    }

    public String getDate(int type) {
        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        if (type == GET_DATE) {
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return date.format(currentLocalTime);
        }

        if (type == GET_TIME) {
            DateFormat date = new SimpleDateFormat("HH:mm:ss", Locale.US);
            return date.format(currentLocalTime);
        }

        if (type == GET_DATE_TIME) {
            DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            return date.format(currentLocalTime);
        }

        return "";
    }

    public void clearFocusIME(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) {
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            activity.getCurrentFocus().clearFocus();
        }
    }

    /**
     * convert millliseconds to times
     *
     * @param time
     * @return
     */
    public String convertMillisecondsToTimes(long time) {
        long tamp = (time / 1000);
        long minutes = tamp / 60;
        long seconds = tamp % 60;
        String result;
        result = ((minutes < 10) ? "0" + minutes : String.valueOf(minutes)) +
                ":" +
                ((seconds < 10) ? "0" + seconds : String.valueOf(seconds));
        return result;
    }

    /**
     * get file name from local file
     *
     * @param path
     * @return
     */
    public String getFileName(String path) {
        String[] a = path.split("/");
        if (a.length > 0) {
            return a[a.length - 1];
        } else {
            return "unknown name";
        }
    }

    public static class CancelListener implements DialogInterface.OnCancelListener {

        private AsyncTask<?, ?, ?> mCancellableTask;

        /**
         * @param task
         */
        public CancelListener(AsyncTask<?, ?, ?> task) {
            mCancellableTask = task;
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            mCancellableTask.cancel(true);
        }
    }

    public static boolean isUserLoggedIn(Context context) {
        String username = SharedPreference.getUserName(context).trim();
        String password = SharedPreference.getPassword(context).trim();
        return username.length()
                != 0 && password.length() != 0;
    }

    public static String getCreateDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    public static int getTextContentHeight(Context context, CharSequence text, int textSize, int deviceWidth, int padding) {
        TextView textView = new TextView(context);
        textView.setPadding(padding, 0, padding, padding);
        textView.setTypeface(Typeface.DEFAULT);
        textView.setText(text, TextView.BufferType.SPANNABLE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public static String getServerUrl(Context context) {
        String currentHost = SharedPreference.getClientHost(context);
        currentHost = currentHost == null || currentHost.length() == 0 ? "www" : currentHost;
        return "http://" + currentHost + ".aevitasgroup.com";
    }
}