package com.arise.healthcaresafety.utils;

import android.media.MediaMetadataRetriever;
import android.os.Environment;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Nam on 7/16/2015.
 */
public class FileManager {

    private static final String TAG = FileManager.class.getSimpleName();
    private static final String root = "Inspection";
    private static final String offline = "offline";
    private static FileManager mInstance = new FileManager();

    public static FileManager getInstance() {
        return mInstance;
    }

    public List<Media> browserImageFile(String folderPath) {
        List<Media> listItem = new ArrayList<>();
        File root = new File(folderPath);

        File[] list = root.listFiles(new ImageFilter());
        if (list == null) {
            return null;
        }

        int index = 0;
        for (File file : list) {
            double size = file.length();
            listItem.add(new Media(file.getAbsolutePath(), Media.MEDIA_PHOTO));
        }
        index++;
        return listItem;
    }

    public List<Media> browserRecordFile(String folderPath) {
        List<Media> listItem = new ArrayList<>();
        File root = new File(folderPath);

        File[] list = root.listFiles(new RecordFilter());
        if (list == null) {
            return null;
        }

        int index = 0;
        for (File file : list) {
            listItem.add(new Media(file.getAbsolutePath(), Media.MEDIA_SOUND));
        }
        index++;
        return listItem;
    }

    /**
     * create application folder
     */
    public void createApplicationFolder() {
        String folderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + root;
        File rootFolder = new File(folderPath);
        if (rootFolder.exists()) {
            return;
        }
        rootFolder.mkdir();
    }

    /**
     * create note folder which contain photo and audio
     *
     * @param c
     * @param note
     * @return
     */
    public boolean createNoteFolder(Case c, Note note) {
        String folderPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" +
                root +
                "/c" +
                String.valueOf(c.getLiveId()) +
                "/n" +
                String.valueOf(note.getLiveId());
        File rootFolder = new File(folderPath);
        if (rootFolder.exists()) {
            return false;
        }
        rootFolder.mkdir();
        return true;
    }

    /**
     * create image file
     *
     * @return
     * @throws IOException
     */
    public File createImageFile(String caseFolder) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File rootFolder = new File(caseFolder);
        if (!rootFolder.exists()) {
            rootFolder.mkdirs();
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                rootFolder      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    /**
     * get path of Case Folder
     *
     * @param c
     * @param note
     * @return
     */
    public String getCaseFolder(Case c, Note note) {
        return Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" +
                root +
                "/c" +
                String.valueOf(c.getLiveId()) +
                "/n" +
                String.valueOf(note.getLiveId());
    }

    public String getOfflineCaseFolder(Case c, Note note) {
        return Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" +
                root + "/" + offline +
                "/c" +
                String.valueOf(c.getLiveId()) +
                "/n" +
                String.valueOf(note.getLiveId());
    }

    /**
     * copy file
     *
     * @param src
     * @param dest
     */
    public void copyFile(File src, File dest) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            out.flush();
            out.close();
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * get unique name. based on date
     *
     * @return
     */
    public String getUniqueName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String result = "audio_" + timeStamp;
        return result;
    }

    /**
     * get last modifier
     *
     * @param path
     * @return
     */
    public String getLastModifier(String path) {
        File file = new File(path);
        Date lastDate = new Date(file.lastModified());
        String timeStamp = new SimpleDateFormat("dd MMM yyyy, hh:mm a", Locale.US).format(lastDate);
        //Log.e(TAG,"last modified " + timeStamp);
        return timeStamp;
    }

    /**
     * get duration for audio
     *
     * @param path
     * @return
     */
    public String getDurationOfAudio(String path) {
        MediaMetadataRetriever metaRetriver;
        metaRetriver = new MediaMetadataRetriever();
        metaRetriver.setDataSource(path);
        String duration = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        Long tamp = Long.parseLong(duration);
        String result = Utils.getInstance().convertMillisecondsToTimes(tamp);
        return result;
    }

    /**
     * get file name of file
     *
     * @param path
     * @return
     */
    public String getFileName(String path) {
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        return file.getName();
    }

    /**
     * delete file
     *
     * @param path
     * @return
     */
    public boolean deleteFile(String path) {
        File file = new File(path);
        return file.exists() && file.delete();
    }

    public static class ImageFilter implements FileFilter {
        // only want to see the following audio file types
        private String[] extension = {".jpeg", ".png", ".jpg"};


        @Override
        public boolean accept(File pathname) {
            // if we are looking at a directory/file that's not hidden we want to see it so return TRUE
            if (pathname.isDirectory() && !pathname.isHidden()) {
                return true;
            }

            // loops through and determines the extension of all files in the directory
            // returns TRUE to only show the audio files defined in the String[] extension array
            for (String ext : extension) {
                if (pathname.getName().toLowerCase().endsWith(ext)) {
                    return true;
                }
            }

            return false;
        }
    }

    public static class RecordFilter implements FileFilter {
        // only want to see the following audio file types
        private String[] extension = {".3gp", ".aac"};


        @Override
        public boolean accept(File pathname) {
            // if we are looking at a directory/file that's not hidden we want to see it so return TRUE
            if (pathname.isDirectory() && !pathname.isHidden()) {
                return true;
            }

            // loops through and determines the extension of all files in the directory
            // returns TRUE to only show the audio files defined in the String[] extension array
            for (String ext : extension) {
                if (pathname.getName().toLowerCase().endsWith(ext)) {
                    return true;
                }
            }

            return false;
        }
    }
}
