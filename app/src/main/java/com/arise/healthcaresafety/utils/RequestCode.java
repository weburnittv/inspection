package com.arise.healthcaresafety.utils;


public class RequestCode {
    public static final int CODE_REQUEST_EQUIPMENT = 1;
    public static final int CODE_REQUEST_CHECKLIST = 2;


    // input request screen
    public static final int CODE_REQUEST_SCREEN_ = 99;
}
