package com.arise.healthcaresafety.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by hnam on 8/1/2015.
 */
public class ImageUtils {
    public static final int MAX_IMAGE_SIZE = 1280;
    private static final String TAG = ImageUtils.class.getSimpleName();

    public File getFileUpload(String path) {
        File file = new File(path);

        if (file.exists()) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap sBitmap = BitmapFactory.decodeFile(path, bmOptions);
            Bitmap dBitmap = scaleDown(sBitmap, MAX_IMAGE_SIZE, true);
            try {
                File tampFile = new File(getTempFile(path));
                FileOutputStream ostream = null;
                tampFile.createNewFile();
                ostream = new FileOutputStream(tampFile);
                dBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                ostream.flush();
                ostream.close();
                Log.e(TAG, "compress file");
                return tampFile;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                             boolean filter) {
        float ratio = Math.min(maxImageSize / realImage.getWidth(), maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    /* new function to compress */
    public String decodeFile(String path, int maxImageSize) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, maxImageSize, maxImageSize, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= maxImageSize && unscaledBitmap.getHeight() <= maxImageSize)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, maxImageSize, maxImageSize, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file
/*            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            String s = "tmp.png";
            File f = new File(mFolder.getAbsolutePath(), s);*/

            File tampFile = new File(getTempFile(path));
            tampFile.createNewFile();

            strMyImagePath = tampFile.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(tampFile);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }

        return strMyImagePath;

    }


    private String getTempFile(String realFile) {
        createTempFolder();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Inspection" +
                "/temp" +
                "/" + Utils.getInstance().getFileName(realFile);
        return path;
    }

    private boolean createTempFolder() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Inspection" +
                "/temp";
        File tempFolder = new File(path);
        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
            return true;
        }
        return false;
    }

}
