package com.arise.healthcaresafety.utils;


public class Constants {
    public static final String KEY_CASE = "KEY_CASE";
    public static final String KEY_CASE_ID = "CASE_ID";
    public static final String KEY_SAVE_PATH = "PATH";
    public static final String KEY_NOTE = "NOTE";
    public static final String KEY_MEDIAS = "MEDIAS";

    public static final int HEIGHT_ITEM_GROUP = 80;
    public static final int HEIGHT_ITEM_TABLE = 100;
    public static final int TOTAL_INFO_OF_CASE = 8;
    public static final String SERVER = "http://aevitasgroup.com";
}
