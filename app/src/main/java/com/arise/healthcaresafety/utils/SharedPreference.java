package com.arise.healthcaresafety.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPreference {

    public static final String SHARE_PREFERENCE_DATA = "share_preference_data";
    private static final String CASE_ID = "CASE_ID";

    private static final String KEY_USERNAME = "KEY_USERNAME";
    private static final String KEY_PASSWORD = "KEY_PASSWORD";
    private static final String KEY_FIRST_OPEN_APP = "KEY_FIRST_OPEN_APP";
    private static final String CLIENT_ID = "HOST_ID";
    private static final String CLIENT_MAIL = "HOST_MAIL";
    private static final String CLIENT_EXPIRE = "CLIENT_EXPIRE";
    private static final String CLIENT_LOGO = "CLIENT_LOGO";
    private static final String KEY_FIRST_SETUP = "KEY_FIRST_SETUP";


    public static void setIsFirstOpen(Context context, boolean isFirst) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_FIRST_OPEN_APP, isFirst);
        editor.apply();
    }

    public static boolean isFirstOpen(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_FIRST_OPEN_APP, true);
    }

    public static void setUserName(Context context, String username) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USERNAME, username);
        editor.apply();
    }

    public static String getUserName(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, "");
    }

    public static void setPassword(Context context, String password) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(KEY_PASSWORD, password);
        editor.apply();
    }

    public static String getPassword(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PASSWORD, "");
    }

    public static void setCaseId(Context context, long caseId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putLong(CASE_ID, caseId);
        editor.apply();
    }

    public static void setClientHost(Context context, String host) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(CLIENT_ID, host);
        editor.apply();
    }

    public static String getClientHost(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(CLIENT_ID, "");
    }

    public static void setClientEmail(Context context, String host) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(CLIENT_MAIL, host);
        editor.apply();
    }

    public static String setClientEmail(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(CLIENT_MAIL, "");
    }

    public static void setClientExpire(Context context, String host) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(CLIENT_EXPIRE, host);
        editor.apply();
    }

    public static String getClientExpire(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(CLIENT_EXPIRE, "");
    }

    public static void setClientLogo(Context context, String host) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(CLIENT_LOGO, host);
        editor.apply();
    }

    public static String getClientLogo(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(CLIENT_LOGO, "");
    }

    public static long getCaseId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(CASE_ID, 0);
    }

    /**
     * save String
     */
    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * get String
     */
    public static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    /**
     * save Boolean
     */
    public static void saveBoolean(Context context, String key,
                                   boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * get Boolean
     */
    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, true);
    }

    /**
     * save Integer
     */
    public static void saveInt(Context context, String key, long value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * get Integer
     */
    public static int getInt(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }

    /**
     * remove data
     */
    public static void removeFromSharedPreferences(Context mContext, String key) {
        if (mContext != null) {
            SharedPreferences mSharedPreferences = mContext.getSharedPreferences(SHARE_PREFERENCE_DATA, 0);
            if (mSharedPreferences != null)
                Log.d("aaa", "remove Key" + key);
            mSharedPreferences.edit().remove(key).commit();
        }
    }

}
