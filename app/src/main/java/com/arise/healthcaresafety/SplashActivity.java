package com.arise.healthcaresafety;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.arise.healthcaresafety.presenter.ISplashPresenter;
import com.arise.healthcaresafety.presenter.impl.SplashPresenter;
import com.arise.healthcaresafety.utils.MySharedPreferences;
import com.arise.healthcaresafety.utils.Utils;
import com.arise.healthcaresafety.utils.WeakReferenceHandler;
import com.arise.healthcaresafety.view.ISplashScreen;
import com.arise.healthcaresafety.view.fragment.LoginScreenFragment;
import com.arise.healthcaresafety.view.fragment.SplashScreen;

public class SplashActivity extends AppCompatActivity implements ISplashScreen {

    public static final int SPLASH_SCREEN = 0;
    public static final int LOGIN_SCREEN = 1;
    private final MyHandler mHandler = new MyHandler(this);
    private ISplashPresenter splashScrPresenter;
    private MySharedPreferences mySharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InspectionApp.getInstance().getGoogleAnalyticsTracker().sendCustomDimension(1, "bug");
        setContentView(R.layout.activity_splash);

        splashScrPresenter = new SplashPresenter(SplashActivity.this, this);
        Bundle data = getIntent().getExtras();
        if (data != null && data.containsKey("ID")) {
            onAutoLogin();
        } else {
            displayScreen(SPLASH_SCREEN);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onAutoLogin();
                }
            }, 3000);
        }
        mySharedPreferences = new MySharedPreferences(SplashActivity.this);
    }

    private void displayScreen(int index) {
        Fragment frag = null;
        if (LOGIN_SCREEN == index) {
            frag = new LoginScreenFragment();
        } else if (SPLASH_SCREEN == index) {
            frag = new SplashScreen();
        }

        FragmentManager fragManager = getSupportFragmentManager();
        FragmentTransaction ft = fragManager.beginTransaction();
        if (LOGIN_SCREEN == index) {
            ft.setCustomAnimations(R.anim.fade_out, R.anim.fade_in);
        }
        ft.replace(R.id.splashAct_container, frag);
        ft.commit();
    }

    public void handleLogin(String username, String password) {
        Utils.getInstance().showProgressDialog(this, R.string.progress_wait, true);
        splashScrPresenter.login(username, password);
        mySharedPreferences.putUser(username);
    }

    @Override
    public void onLoginSuccess() {
        Utils.getInstance().hideProgressDialog();
        Intent i = new Intent(SplashActivity.this, MenuActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onLoginFailed(String msg) {
        Utils.getInstance().hideProgressDialog();
        Utils.getInstance().showMessage(SplashActivity.this, msg);
    }

    /**
     * This method will call auto login when user had been logged in app at the before.
     */
    private void onAutoLogin() {
        if (Utils.isUserLoggedIn(this)) {
            Intent i = new Intent(SplashActivity.this, MenuActivity.class);
            startActivity(i);
            finish();
        } else {
            displayScreen(LOGIN_SCREEN);
        }
    }

    public static class MyHandler extends WeakReferenceHandler<SplashActivity> {
        public MyHandler(SplashActivity reference) {
            super(reference);
        }

        @Override
        protected void handleMessage(SplashActivity reference, Message msg) {
        }
    }
}
