package com.arise.healthcaresafety;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arise.healthcaresafety.model.ContainerMedia;
import com.arise.healthcaresafety.model.entities.Attribute;
import com.arise.healthcaresafety.model.entities.BaseMedia;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.utils.Constants;
import com.arise.healthcaresafety.view.adapter.CheckListGroup;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CuongNV 2015-10-24
 */
public class PreviewOfflineActivity extends Activity {
    private static final String TAG = PreviewOfflineActivity.class.getSimpleName();
    private LinearLayout mLinearLayoutCase;
    private LinearLayout mLinearLayoutEquipment;
    private LinearLayout mLinearLayoutCheckList;
    private LayoutInflater inflater;
    private ArrayList<String> mFieldCase;
    private ArrayList<String> mContentCase;
    private List<ContainerMedia> mLinearLayoutMedias;
    private Case mCase;
    private MediaPlayer mediaPlayer;
    private boolean checkStateMedia;
    private LinearLayout mLinearLayoutCurrent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }

        mCase = bundle.getParcelable(Constants.KEY_CASE);
        mediaPlayer = new MediaPlayer();

        mLinearLayoutMedias = new ArrayList<>();
        mLinearLayoutCase = (LinearLayout) findViewById(R.id.linearCase);
        mLinearLayoutEquipment = (LinearLayout) findViewById(R.id.linearEquipment);
        mLinearLayoutCheckList = (LinearLayout) findViewById(R.id.linearCheckList);
        inflater = getLayoutInflater();
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    fetValuesCase();
                    fetchDataSetCase();
                    fetchDataSetEquipments();
                    fetchDataSetCheckLists();
                }
            }).start();
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }

    private void fetValuesCase() {
        mFieldCase = new ArrayList<>();
        mFieldCase.add("Reference No");
        mFieldCase.add("Branch");
        mFieldCase.add("Division");
        mFieldCase.add("Arrival Date");
        mFieldCase.add("Inspection Category");
        mFieldCase.add("Inspection Type");
        mFieldCase.add("Finding Result");
        mFieldCase.add("Officer");

        mContentCase = new ArrayList<>();
        if (mCase == null) {
            mContentCase.add("");
            mContentCase.add("");
            mContentCase.add("");
            mContentCase.add("");
            mContentCase.add("");
            mContentCase.add("");
            mContentCase.add("");
            mContentCase.add("");
            return;
        } else {
            mContentCase.add(mCase.getReferenceNo());
        }
        if (mCase.getBranch() == null) {
            mContentCase.add("");
            mContentCase.add("");
        } else {
            mContentCase.add(mCase.getBranch().getName());
            if (mCase.getBranch().getDivision() == null) {
                mContentCase.add("");
            } else {
                mContentCase.add(mCase.getBranch().getDivision().getName());
            }
        }
        mContentCase.add(mCase.getArrivalDate());
        if (mCase.getType() == null) {
            mContentCase.add("");
            mContentCase.add("");
        } else {
            if (mCase.getType().getCategory() == null) {
                mContentCase.add(NewsCaseDetailActivity.category.getName());
                mContentCase.add(mCase.getType().getName());
            } else {
                mContentCase.add(mCase.getType().getCategory().getName());
                mContentCase.add(mCase.getType().getName());
            }
        }
        mContentCase.add("");
        mContentCase.add(InspectionApp.getInstance().getUsername());
    }

    private void fetchDataSetCase() {
        for (int i = 0; i < 8; i++) {
            final View headerSetContainer = inflater.inflate(R.layout.table_row_two, null);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLinearLayoutCase.addView(headerSetContainer);
                }
            });
            ((TextView) headerSetContainer.findViewById(R.id.firstLine)).setText(mFieldCase.get(i));
            ((TextView) headerSetContainer.findViewById(R.id.secondLine)).setText(mContentCase.get(i));
        }
    }

    // add data of equipments for preview
    private void fetchDataSetEquipments() {
        for (final Equipment equipment : mCase.getEquipments().getEquipments()) {
            final View headerSetContainer = inflater.inflate(R.layout.table_row_equipment_new, null);

            final LinearLayout linearChildren = (LinearLayout) headerSetContainer.findViewById(R.id.linearRowEquipment);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLinearLayoutEquipment.addView(headerSetContainer);
                }
            });
            ((TextView) headerSetContainer.findViewById(R.id.tvCategory)).setText(equipment.getCategory().getName());
            ((TextView) headerSetContainer.findViewById(R.id.tvNameEquipment)).setText(equipment.getName());

            if (equipment.getNote() != null) {
                ((TextView) headerSetContainer.findViewById(R.id.tvNoteEquipment)).setText(equipment.getName());
            }
            if (equipment.getResult() != null) {
                ((TextView) headerSetContainer.findViewById(R.id.tvAnswerEquipment)).setText("Yes");
                if (equipment.getResult() != null && equipment.getNote() != null && equipment.getNote().getNotePhotoMedias() != null) {
                    long start = equipment.getNote().getLiveId() - equipment.getNote().getNotePhotoMedias().size() - equipment.getNote().getNoteSoundMedias().size() + 1;
                    long end = equipment.getNote().getLiveId();
                    for (long index = start; index <= end; index++) {
                        for (Note note : mCase.getNotes()) {
                            if (mCase.getLiveId() == note.getCaseId() && note.getLiveId() == index) {
                                for (final Media media : note.getMedias()) {
                                    if (media.getNoteId() == note.getLiveId() && equipment.getNote().getNoteQuestionId() == note.getNoteQuestionId()) {
                                        final View headerSetContainerRow = inflater.inflate(R.layout.table_row_three_image, null);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                linearChildren.addView(headerSetContainerRow);
                                                final ImageView imageView = ((ImageView) headerSetContainerRow.findViewById(R.id.imgThreeLine));
                                                final LinearLayout linearLayout = (LinearLayout) headerSetContainerRow.findViewById(R.id.containerLinkMedia);
                                                fetchDataMediaOffline(media, linearLayout, imageView);
                                            }
                                        });
                                    }

                                }
                            }
                        }
                    }
                }
            } else {
                ((TextView) headerSetContainer.findViewById(R.id.tvAnswerEquipment)).setText("No");
            }
        }
    }

    // add data of checklist for preview
    private void fetchDataSetCheckLists() {
        int size = 0;
        for (final CheckListGroup checkList : mCase.getCheckList().getCheckListGroup()) {
            size++;
            final View headerSetContainer = inflater.inflate(R.layout.table_row_checklist, null);
            final LinearLayout linearChildren = (LinearLayout) headerSetContainer.findViewById(R.id.linearRowCheckList);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLinearLayoutCheckList.addView(headerSetContainer);
                }
            });
            ((TextView) headerSetContainer.findViewById(R.id.tvTitleCheckList)).setText(checkList.getGroup().getName());
            if (size < mCase.getCheckList().getCheckListGroup().size()) {
                for (Question question : checkList.getGroup().getQuestions()) {
                    final View headerSetContainerRow = inflater.inflate(R.layout.table_row_three, null);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            linearChildren.addView(headerSetContainerRow);
                        }
                    });

                    ((TextView) headerSetContainerRow.findViewById(R.id.firstLine)).setText(question.getContent());
                    if (question.getNote() != null) {
                        ((TextView) headerSetContainerRow.findViewById(R.id.threeLine)).setText(question.getContent());
                    } else {
                        ((TextView) headerSetContainerRow.findViewById(R.id.threeLine)).setText("");
                    }
                    if (question.getResult() != null && question.getResult().getCaseId() == mCase.getLiveId()) {
                        for (final Result result : mCase.getResults()) {
                            if (result.getQuestionId() >= 0 && result != null && result.getAttributes() != null && !result.getAttributes().isEmpty() && question.getResult() != null) {
                                if (result.getQuestionId() == question.getLiveId()) {
                                    if (question.getVariation().getType() == 0) {
                                        for (final Attribute attribute : result.getAttributes()) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    ((TextView) headerSetContainerRow.findViewById(R.id.secondLine)).setText(attribute.getName());
                                                }
                                            });
                                        }
                                    } else if (question.getVariation().getType() == 1) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                String answer = "";
                                                for (final Attribute attribute : result.getAttributes()) {
                                                    answer += "- " + attribute.getName() + "\n";
                                                }
                                                Log.d("key", answer);
                                                ((TextView) headerSetContainerRow.findViewById(R.id.secondLine)).setText(answer);
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        for (Note note : mCase.getNotes()) {
                            if (mCase.getLiveId() == note.getCaseId())
                                if (note.getNoteQuestionId() >= 0 && note.getNoteQuestionId() == question.getLiveId()) {
                                    for (final Media media : note.getMedias()) {
                                        if (media.getNoteId() == note.getLiveId()) {
                                            final View headerSetContainerRowList = inflater.inflate(R.layout.table_row_check_list_image, null);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    linearChildren.addView(headerSetContainerRowList);
                                                    final ImageView imageView = ((ImageView) headerSetContainerRowList.findViewById(R.id.imgThreeLine));
                                                    final LinearLayout linearLayout = (LinearLayout) headerSetContainerRowList.findViewById(R.id.containerLinkMedia);
                                                    fetchDataMediaOffline(media, linearLayout, imageView);
                                                }
                                            });
                                        }
                                    }
                                }
                        }
                    }
                }
            } else {
                ((TextView) headerSetContainer.findViewById(R.id.tvTitleCheckList)).setText(checkList.getGroup().getName());
                ((TextView) headerSetContainer.findViewById(R.id.tvAnswer)).setText("");
                ((TextView) headerSetContainer.findViewById(R.id.tvRight)).setText("Answer");
                View headerSetContainerRow = inflater.inflate(R.layout.table_row_three, null);
                linearChildren.addView(headerSetContainerRow);
                for (Question question : checkList.getGroup().getQuestions()) {
                    ((TextView) headerSetContainerRow.findViewById(R.id.firstLine)).setText(question.getContent());
                    ((TextView) headerSetContainerRow.findViewById(R.id.threeLine)).setText("");
                }
            }
        }
    }

    // get name case final by position for checklist
    private String getFindingScales(int finalId) {
        switch (finalId) {
            case 5:
                return "Very Good";
            case 4:
                return "Good";
            case 3:
                return "Satisfactory";
            case 2:
                return "Marginal";
            case 1:
                return "Unsatisfactory";
            default:
                return "";
        }
    }

    // get sound and photo media for checklist and equipments
    private void fetchDataMediaOffline(final Media media, final LinearLayout containerMedia, ImageView imgPhoto) {
        if (media.getKind().equals(BaseMedia.SOUND)) {
            imgPhoto.setVisibility(View.GONE);
            containerMedia.setVisibility(View.VISIBLE);
            final ImageView imgLinkMedia = (ImageView) containerMedia.findViewById(R.id.imgDefaultMedia);
            TextView textView = (TextView) containerMedia.findViewById(R.id.tvLinkMedia);
            textView.setText((new File(media.getLocalPath()).getName()));
            mLinearLayoutMedias.add(new ContainerMedia(containerMedia, media.getLocalPath(), imgLinkMedia));
            containerMedia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLinearLayoutCurrent == null) {
                        mLinearLayoutCurrent = containerMedia;
                    }
                    if (mLinearLayoutCurrent != containerMedia) {
                        mLinearLayoutCurrent = containerMedia;
                        mediaPlayer.stop();
                        checkStateMedia = false;
                        for (ContainerMedia linearLayoutMediaplay : mLinearLayoutMedias) {
                            linearLayoutMediaplay.getImageView().setImageResource(R.drawable.ic_media_play);
                        }
                    }
                    for (ContainerMedia linearLayoutMediaplay : mLinearLayoutMedias) {
                        if (linearLayoutMediaplay.getLinearLayout() == containerMedia && mLinearLayoutCurrent == containerMedia) {
                            playMedia(linearLayoutMediaplay.getPath(), imgLinkMedia);
                        }
                    }
                }
            });
        } else {
            imgPhoto.setVisibility(View.VISIBLE);
            containerMedia.setVisibility(View.GONE);
            Picasso.with(PreviewOfflineActivity.this)
                    .load(new File(media.getLocalPath()))
                    .skipMemoryCache()
                    .noFade()
                    .resize(70, 70)
                    .centerCrop()
                    .into(imgPhoto);
        }
    }

    // play media when click button media for view
    private void playMedia(String mediaPlay, final ImageView imgLinkMedia) {
        if (checkStateMedia == false) {
            mediaPlayer = new MediaPlayer();
            Uri myUri1 = Uri.parse(mediaPlay);
            try {
                mediaPlayer.setDataSource(PreviewOfflineActivity.this, myUri1);
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imgLinkMedia.setImageResource(R.drawable.ic_media_stop);
            mediaPlayer.start();
            checkStateMedia = true;
        } else {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
            imgLinkMedia.setImageResource(R.drawable.ic_media_play);
            checkStateMedia = false;
            mLinearLayoutCurrent = null;
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mLinearLayoutCurrent = null;
                imgLinkMedia.setImageResource(R.drawable.ic_media_play);
                checkStateMedia = false;
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            try {
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                }
            } catch (Exception ee) {
            }
            finish();
        }
        return true;
    }
}
