package com.arise.healthcaresafety;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;

import com.arise.healthcaresafety.Common.GoogleAnalyticsTracker;
import com.arise.healthcaresafety.handler.Delegation;
import com.arise.healthcaresafety.model.entities.Attribute;
import com.arise.healthcaresafety.model.entities.BaseMedia;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Category;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Division;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Group;
import com.arise.healthcaresafety.model.entities.InspReference;
import com.arise.healthcaresafety.model.entities.InspectionType;
import com.arise.healthcaresafety.model.entities.LoginResult;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.PastCase;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Statistic;
import com.arise.healthcaresafety.model.entities.Type;
import com.arise.healthcaresafety.model.entities.Variation;
import com.arise.healthcaresafety.utils.FileManager;
import com.arise.healthcaresafety.utils.MySharedPreferences;
import com.arise.healthcaresafety.utils.Utils;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.android.AndroidInitializeConfig;
import co.uk.rushorm.core.Rush;
import co.uk.rushorm.core.RushCore;
import io.fabric.sdk.android.Fabric;

public class InspectionApp extends Application {
    private static LoginResult.User user;
    private static String username;
    private static InspectionApp mInstance;
    private static Tracker tracker = null;
    MySharedPreferences mySharedPreferences;
    private GoogleAnalyticsTracker googleAnalyticsTracker;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
                Delegation.getInstance().setStatus(Utils.isNetworkConnected(getApplicationContext()));
            }
        }
    };

    public static synchronized InspectionApp getInstance() {
        return mInstance;
    }

    private static IntentFilter intentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        return intentFilter;
    }

    public GoogleAnalyticsTracker getGoogleAnalyticsTracker() {
        return googleAnalyticsTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Analytics
        googleAnalyticsTracker = new GoogleAnalyticsTracker(this, R.xml.gaconfig);
        //todo DEBUG fabric
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        FileManager.getInstance().createApplicationFolder();
        Delegation.getInstance().setStatus(Utils.isNetworkConnected(this));
        registerReceiver(receiver, intentFilter());
        mySharedPreferences = new MySharedPreferences(getApplicationContext());

        List<Class<? extends Rush>> classes = new ArrayList<>();
        // Add classes
        classes.add(Variation.class);
        classes.add(Attribute.class);
        classes.add(Question.class);
        classes.add(Group.class);
        classes.add(CheckList.class);
        classes.add(BaseMedia.class);
        classes.add(Branch.class);
        classes.add(Case.class);
        classes.add(Category.class);
        classes.add(Division.class);
        classes.add(Equipment.class);
        classes.add(InspectionType.class);
        classes.add(InspReference.class);
        classes.add(Media.class);
        classes.add(Note.class);
        classes.add(PastCase.class);
        classes.add(Result.class);
        classes.add(Type.class);
        classes.add(Statistic.class);
        AndroidInitializeConfig config = new AndroidInitializeConfig(getApplicationContext());
        config.setClasses(classes);
        RushCore.initialize(config);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterReceiver(receiver);
    }

    synchronized Tracker getTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.enableAutoActivityReports(this);
            tracker = analytics.newTracker(R.xml.app_tracker_config);
            tracker.enableAdvertisingIdCollection(true);
        }
        return tracker;
    }

    public LoginResult.User getUser() {
        return user;
    }

    public void setUser(LoginResult.User user) {
        InspectionApp.user = user;
    }

    public String getUsername() {
        return mySharedPreferences.getUser();
//        return username != null ? username : null;
    }

    public void setUsername(String username) {
        InspectionApp.username = username;
    }

    public String getToken() {
        return mySharedPreferences.getToken();
    }

    public String getServerUrl() {
        String currentHost = mySharedPreferences.getClientHost();
        currentHost = currentHost == null || currentHost.length() == 0 ? "www" : currentHost;
        return "http://" + currentHost + ".aevitasgroup.com";
    }
}
