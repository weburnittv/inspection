package com.arise.healthcaresafety.model.network.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Status;
import com.arise.healthcaresafety.model.network.api.base.BaseInspectionApi;
import com.arise.healthcaresafety.model.network.volley.MyVolley;

import java.util.Map;

/**
 * Created by Nam on 7/10/2015.
 */
public class ResultsApi extends BaseInspectionApi {
    private static final String TAG = ResultsApi.class.getSimpleName();
    /*POST: /api/apps/inspection/case/{id}/result.json;
    id => Inspection case ID*/
    private static final String CREATE_RESULT_API = BaseInspectionApi.SERVER + "/api/apps/inspection/case/";

    /*POST: /inspection/result/{id}
    id => Result ID*/
    private static final String EDIT_RESULT_API = BaseInspectionApi.SERVER + "/api/apps/inspection/result/";


    /*DELETE /api/apps/inspection/result/{id}.{_format}*/
    private static final String DELETE_RESULT_API = BaseInspectionApi.SERVER + "/api/apps/inspection/result/";


    private Context context;

    public ResultsApi(Context context) {
        this.context = context;
    }


    public void createResult(long caseId, Map<String, String> params,
                             Response.Listener<Result> listener,
                             Response.ErrorListener errorListener) {
        String url = CREATE_RESULT_API + String.valueOf(caseId) + "/result.json";
        Log.i("TAG111", url + "bi");
        Request request = super.createPostMethod(url,
                Result.class,
                listener,
                errorListener,
                params);
        MyVolley.getInstance(context).addToRequestQueue(request);
    }

    private void editResult(long resultId,
                            Response.Listener<Result> listener,
                            Response.ErrorListener errorListener, Map<String, String> params) {
        String url = EDIT_RESULT_API + String.valueOf(resultId) + ".json";
        Request<Case> request = super.createPostMethod(url,
                Result.class,
                listener,
                errorListener,
                params);
        MyVolley.getInstance(context).addToRequestQueue(request);
    }

    public void deleteEquipmentResult(long resultId,
                                      Response.Listener<Status> listener,
                                      Response.ErrorListener errorListener) {
        String url = DELETE_RESULT_API + String.valueOf(resultId) + ".json";
        Request<Case> request = super.createDeleteMethod(url,
                Status.class,
                listener,
                errorListener);
        MyVolley.getInstance(context).addToRequestQueue(request);
    }


    /**
     * save check list result
     *
     * @param case_id
     * @param result
     * @param listener
     * @param errorListener
     */
    public void saveCheckListResult(int case_id, Result result, Response.Listener<Result> listener,
                                    Response.ErrorListener errorListener) {
        if (result.getLiveId() == -1) {
            //create new result
            Log.e(TAG, ">>>>>> CREATE" + result.getPostData().toString());
            createResult(case_id, result.getPostData(), listener, errorListener);

        } else {
            Log.e(TAG, ">>>>>>> EDIT");
            //edit result
            editResult(result.getLiveId(), listener, errorListener, result.getPostData());
        }
    }

    /**
     * save equipment result
     *
     * @param result
     * @param listener
     * @param deleteListener
     * @param errorListener
     */
    public void saveEquipmentResult(int case_id, Result result,
                                    Response.Listener<Result> listener,
                                    Response.Listener<Status> deleteListener,
                                    Response.ErrorListener errorListener) {
        if (result.getLiveId() == -1) {
            //create new result
            Log.e(TAG, ">>>>>> CREATE" + result.getPostData().toString());
            createResult(case_id, result.getPostData(), listener, errorListener);
        } else {
            Log.e(TAG, ">>>>>>> REMOVE");
            deleteEquipmentResult(result.getLiveId(), deleteListener, errorListener);
        }
    }

    /**
     * save result
     *
     * @param result
     * @param listener
     * @param deleteListener
     * @param errorListener
     */
    public void saveResult(Result result,
                           Response.Listener<Result> listener,
                           Response.Listener<Status> deleteListener,
                           Response.ErrorListener errorListener) {
        if (result.getLiveId() > 0)
            editResult(result, listener, deleteListener, errorListener);
        else createResult(result.getCaseId(), result.getPostData(), listener, errorListener);
    }

    public void editResult(Result result,
                           Response.Listener<Result> listener,
                           Response.Listener<Status> deleteListener,
                           Response.ErrorListener errorListener) {
        if (result.isDeleted())
            deleteEquipmentResult(result.getLiveId(), deleteListener, errorListener);
        else editResult(result.getLiveId(), listener, errorListener, result.getPostData());
    }


}
