package com.arise.healthcaresafety.model.helper;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Note;

/**
 * Created by hnam on 8/29/2015.
 */
public class CaseHelper {
    public static CaseHelper mInstance = new CaseHelper();

    public static CaseHelper getInstance() {
        return mInstance;
    }

    public void handleNote(Case c, Note note) {
        if (note != null) {
            if (note.getQuestion() == null) {//if note below to equipment
                c.getEquipments().addEquipmentNote(note);
            } else {//if note below to checklist

                c.getCheckList().addCheckListNote(note);
            }
        }
    }
}
