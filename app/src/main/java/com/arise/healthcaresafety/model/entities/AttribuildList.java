package com.arise.healthcaresafety.model.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AttribuildList extends BaseEntity {
    @SerializedName("liveId")
    protected long liveId;
    private int idResource;
    private ArrayList<Attribute> attribute;

    public int getIdResource() {
        return idResource;
    }

    public void setIdResource(int idResource) {
        this.idResource = idResource;
    }

    public ArrayList<Attribute> getAttribute() {
        return attribute;
    }

    public void setAttribute(ArrayList<Attribute> attribute) {
        this.attribute = attribute;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }
}
