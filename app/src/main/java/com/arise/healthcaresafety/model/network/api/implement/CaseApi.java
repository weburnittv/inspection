package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Case;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Path;

/**
 * Created by Paul Aan on 1/2/16.
 */
public interface CaseApi {
    @Multipart
    @POST("/api/apps/inspection/case")
    Call<Case> createCase(@PartMap Map<String, RequestBody> attribute);

    @Multipart
    @POST("/api/apps/inspection/edit/case/{id}")
    Call<Case> updateCase(@Path(value = "id", encoded = true) long caseId,
                          @PartMap Map<String, RequestBody> attribute);
}
