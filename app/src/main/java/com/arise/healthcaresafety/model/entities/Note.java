package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushList;

public class Note extends BaseEntity implements Parcelable {
    public static final String NOTE_PATH = "NOTE";
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
    private static final String TAG = Note.class.getSimpleName();
    @SerializedName("liveId")
    protected long liveId;
    @SerializedName("medias")
    @RushList(classType = Media.class)
    List<Media> medias;
    private String content;
    private Attribute attribute;
    private Equipment equipment;
    @SerializedName("question")
    private Question question;
    @RushIgnore
    private Case inspectionCase;
    private boolean deleted = false;
    private boolean synced = false;
    /*todo 8/2/2015: offline mode*/
    private transient long caseId; //local case id
    @RushIgnore
    private int attribuitId;
    @RushIgnore
    private int noteQuestionId;

    public Note() {
    }

    public Note(Question question) {
        this.question = question;
    }

    public Note(Equipment equipment) {
        this.equipment = equipment;
    }

    //implement parcel
    // Parcelling part
    public Note(Parcel in) {
        this.content = in.readString();
        this.equipment = in.readParcelable(Equipment.class.getClassLoader());
        this.question = in.readParcelable(Question.class.getClassLoader());
        this.medias = in.createTypedArrayList(Media.CREATOR);
        this.setLiveId(in.readLong());
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.content);
        dest.writeParcelable(this.equipment, flags);
        dest.writeParcelable(this.question, flags);
        dest.writeTypedList(this.medias);
        dest.writeLong(this.getLiveId());
    }

    public int getNoteQuestionId() {
        return noteQuestionId;
    }

    public void setNoteQuestionId(int noteQuestionId) {
        this.noteQuestionId = noteQuestionId;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return "note";
    }

    @Override
    public void onSave() {

    }

    public String getContent() {
        if (content != null)
            return this.content;
        return "";
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public Question getQuestion() {
        return this.question;
    }

    public void setQuestion(Question Question) {
        this.question = Question;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public List<Media> getMedias() {
        if (this.medias != null)
            return medias;
        return new ArrayList<Media>();
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }

    public long getCaseId() {
        return caseId;
    }

    ;

    public void setCaseId(long caseId) {
        this.caseId = caseId;
    }

    public void setAttribuitId(int attribuitId) {
        this.attribuitId = attribuitId;
    }

    public int getAtribuitId() {
        return attribuitId;
    }

    /**
     * @param object: local id
     */
    public void storeId(Object object) {
        if (!(object instanceof Note)) {
            return;
        }
        Note i = (Note) object;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + this.getLiveId() +
                ", content='" + content + '\'' +
                ", attribute=" + attribute +
                ", question=" + this.question +
                ", liveId=" + this.getLiveId() +
                ", localId=" + this.getId() +
                '}';
    }

    /**
     * get post data for creating or update
     *
     * @return
     */
    public Map<String, String> getPostData(long caseId) {
        Map<String, String> result = new HashMap<>();
        String content = this.content != null ? this.content : "...";
        result.put(createKey(getKind(), "note"), content);

        result.put(createKey(getKind(), "inspection"), String.valueOf(caseId));
        if (this.getQuestion() != null) {
            result.put(createKey(getKind(), "question"), String.valueOf(this.question.getId()));
        } else if (this.getEquipment() != null) {
            result.put(createKey(getKind(), "attribute"), String.valueOf(this.equipment.getLiveId()));
        }
        return result;
    }

    public Map<String, RequestBody> getRetrofitDataPost() {
        Map<String, RequestBody> params = new HashMap<>();

        RequestBody inspectionId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.getCaseId()));

        if (this.content == null || this.content == "") {
            this.content = "...";
        }
        RequestBody content = RequestBody.create(MediaType.parse("text/plain"), this.content);
        params.put(super.createKey(getKind(), "note"), content);
        params.put(super.createKey(getKind(), "inspection"), inspectionId);

        if (this.getQuestion() != null) {
            RequestBody question = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.question.getLiveId()));
            params.put(super.createKey(getKind(), "question"), question);
        } else if (this.getEquipment() != null) {
            RequestBody attribute = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.equipment.getLiveId()));
            params.put(super.createKey(getKind(), "attribute"), attribute);
        }
        return params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * get photo medias
     *
     * @return
     */
    public List<Media> getNotePhotoMedias() {
        List<Media> result = new ArrayList<>();
        for (Media media : this.getMedias()) {
            if (Media.MEDIA_PHOTO.equalsIgnoreCase(media.getKind()) && media.getLocalPath() != null) {
                File file = new File(media.getLocalPath());
                if (file.exists() && !media.isDeleted()) {
                    result.add(media);
                }
            }
        }

        return result;
    }

    /**
     * get photo medias
     *
     * @return
     */
    public List<Media> getNoteSoundMedias() {
        List<Media> result = new ArrayList<>();
        for (Media media : this.getMedias()) {
            if (Media.MEDIA_SOUND.equalsIgnoreCase(media.getKind()) && media.getLocalPath() != null) {
                File file = new File(media.getLocalPath());
                if (file.exists() && !media.isDeleted()) {
                    result.add(media);
                }
            }
        }

        return result;
    }

    /**
     * add new media to list media
     *
     * @param items
     */
    public boolean addMedia(List<Media> items) {
        if (this.medias == null) {
            this.medias = new ArrayList<>();
        }
        //sync deleted items
        if (this.getMedias().size() == 0) {
            this.medias = items;
            return true;
        }
        for (Media media : this.getMedias()) {
            boolean exist = false;
            Media existItem = null;
            for (Media item : items) {
                item.setSynced(false);
                if (!item.getKind().equals(media.getKind()))
                    exist = true;
                else if (item.getLocalPath().equals(media.getLocalPath())) {
                    media.setDeleted(false);
                    media.setSynced(false);
                    exist = true;
                    existItem = item;
                    break;
                }
            }
            if (existItem != null) items.remove(existItem);
            if (!exist)
                media.setDeleted(true);
        }
        for (Media newItem : items) {
            this.medias.add(newItem);
        }

        this.save();
        return true;
    }

    public boolean updateNoteEquipment(Note note) {
        if (note.getEquipment() == null || this.getEquipment() == null)
            return false;
        if (note.getEquipment().getLiveId() == this.getEquipment().getLiveId()) {
            if (!note.getEquipment().isChecked())
                this.setDeleted(true);
            this.setContent(note.getContent());
            this.setMedias(note.getMedias());
            this.setSynced(false);
            return true;
        }
        return false;
    }

    public boolean updateNoteQuestion(Note note) {
        if (note.getQuestion() == null || this.getQuestion() == null)
            return false;
        if (note.getQuestion().getLiveId() == this.getQuestion().getLiveId()) {
            this.setContent(note.getContent());
            this.setMedias(note.getMedias());
            this.setSynced(false);
            return true;
        }
        return false;
    }

    public Case getInspectionCase() {
        return inspectionCase;
    }

    public void setInspectionCase(Case inspectionCase) {
        this.inspectionCase = inspectionCase;
    }
}
