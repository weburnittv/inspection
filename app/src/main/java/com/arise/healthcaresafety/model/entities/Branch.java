package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

@RushTableAnnotation
public class Branch extends BaseEntity implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Branch createFromParcel(Parcel source) {
            return new Branch(source);
        }

        @Override
        public Branch[] newArray(int size) {
            return new Branch[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    private String name;
    private String address;
    private String tel;
    private long divisionId;
    @RushIgnore
    private Division division;

    public Branch() {
    }

    public Branch(Division division, long liveId, String name, String address, String tel) {
        this.division = division;
        this.liveId = liveId;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }

    public Branch(Parcel in) {
        this.name = in.readString();
        this.address = in.readString();
        this.tel = in.readString();
        this.division = in.readParcelable(Division.class.getClassLoader());
        this.setLiveId(in.readLong());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Division getDivision() {
        if (this.division != null)
            return division;
        return new RushSearch().whereEqual("liveId", this.divisionId).findSingle(Division.class);
    }

    public void setDivision(Division division) {
        this.division = division;
        this.divisionId = division.getLiveId();
    }

    @Override
    public String toString() {
        return "Branch{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", tel='" + tel + '\'' +
                ", division=" + division +
                '}';
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return "branch";
    }

    @Override
    public void onSave() {
    }

    public Equipments getEquipments() {
        Equipments list = new Equipments(new RushSearch().whereEqual("branchId", this.getLiveId()).find(Equipment.class));
        list.setBranch(this);
        return list;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeString(this.tel);
        dest.writeParcelable(this.division, flags);
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
