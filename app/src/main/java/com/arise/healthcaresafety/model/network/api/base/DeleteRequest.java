package com.arise.healthcaresafety.model.network.api.base;

import com.android.volley.Response;

import java.util.Map;

/**
 * Created by hnam on 7/1/2015.
 */
public class DeleteRequest<T> extends BaseGsonRequest<T> {
    private static final String TAG = DeleteRequest.class.getSimpleName();

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url   URL of the request to make
     * @param clazz Relevant class object, for Gson's reflection
     */
    public DeleteRequest(String url, Class<T> clazz,
                         Response.Listener<T> listener,
                         Response.ErrorListener errorListener) {
        //constructor
        super(Method.DELETE, url, clazz, listener, errorListener);
    }

    public DeleteRequest(String url,
                         Class<T> clazz,
                         Map<String, String> headers,
                         Response.Listener<T> listener,
                         Response.ErrorListener errorListener) {
        super(Method.DELETE, url, clazz, headers, listener, errorListener);
    }

}
