package com.arise.healthcaresafety.model.entities;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.uk.rushorm.core.RushObject;


public abstract class BaseEntity extends RushObject {

    public BaseEntity() {

    }

    public abstract long getLiveId();

    public abstract void setLiveId(long liveId);


    /**
     * get kind of entity
     *
     * @return
     */
    public abstract String getKind();

    /**
     * @return
     */
    public List<String> getFields() {
        List<String> fields = new ArrayList<>();
        for (Field f : this.getClass().getDeclaredFields()) {
            fields.add(f.getName());
        }
        return fields;
    }

    public Object getValue(String field) {
        try {
            Field f = this.getClass().getDeclaredField(field);
            f.setAccessible(true);
            return f.get(this);
        } catch (NoSuchFieldException e) {

        } catch (IllegalAccessException e) {

        }
        return null;
    }

    public String createKey(String kind, String field) {
        return kind + "[" + field + "]";
    }

    public Map<String, String> parseBaseEntity(BaseEntity baseEntity) {
        Map<String, String> params = new HashMap<>();
        List<String> fields = baseEntity.getFields();
        String kind = baseEntity.getKind();
        for (String field : fields) {
            if (baseEntity.getValue(field) instanceof List) {
                List<Object> a = (List<Object>) baseEntity.getValue(field);
                int size = a.size();
                for (int i = 0; i < size; i++) {
                    params.put(createKeyArray(kind, field, i), String.valueOf(a.get(i)));
                }
            } else {
                params.put(createKey(baseEntity.getKind(), field),
                        createValue(baseEntity, field));
            }
        }
        return params;
    }


    private String createKeyArray(String kind, String field, int index) {
        return kind + "[" + field + "][" + String.valueOf(index) + "]";
    }

    private String createValue(BaseEntity baseEntity, String field) {
        return String.valueOf(baseEntity.getValue(field));
    }

    public abstract void onSave();

}
