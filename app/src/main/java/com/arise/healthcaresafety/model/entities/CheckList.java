package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.arise.healthcaresafety.view.adapter.CheckListGroup;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.core.RushCallback;
import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushList;

public class CheckList extends BaseEntity implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public CheckList createFromParcel(Parcel source) {
            return new CheckList(source);
        }

        @Override
        public CheckList[] newArray(int size) {
            return new CheckList[size];
        }
    };
    private static final String TAG = CheckList.class.getSimpleName();
    @SerializedName("liveId")
    protected long liveId;
    private boolean enable;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @RushList(classType = Group.class)
    private List<Group> groups;
    private long typeId;
    private Type type;
    @RushIgnore
    private List<Note> notes;
    @RushIgnore
    private List<Result> results;

    public CheckList() {
    }

    public CheckList(Parcel in) {
        // this.enable = in.readB();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.groups = new ArrayList<Group>();
        in.readTypedList(this.groups, Media.CREATOR);
        this.setLiveId(in.readLong());
    }

    public static CheckList getChecklistByType(long typeId) {
        return new RushSearch().startGroup().whereEqual("typeId", typeId).and().whereEqual("enable", true).endGroup()
                .findSingle(CheckList.class);
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Group> getGroups() {
        if (this.groups != null)
            return groups;
        return this.groups = new ArrayList<Group>();
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
        this.typeId = type.getLiveId();
    }

    @Override
    public String toString() {
        return "CheckList{" +
                "id=" + this.getLiveId() +
                ", enable=" + enable +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", groups=" + groups +
                '}';
    }

    /**
     * get check list group array for ui
     *
     * @return
     */
    public ArrayList<CheckListGroup> getCheckListGroup() {
        ArrayList<CheckListGroup> arrGroup = new ArrayList<>();
        List<Group> listGroup = getGroups();
        for (Group group : listGroup) {
            arrGroup.add(new CheckListGroup(group));
        }
        //todo add new final question
//        Gson gson = new Gson();
//        Group finalGroup = gson.fromJson(context.getString(R.string.final_question), Group.class);
//        arrGroup.add(new CheckListGroup(finalGroup));
        return arrGroup;
    }

    public void setCheckListResult(List<Result> results, List<Note> notes) {
        for (Group group : this.getGroups()) {
            for (Question question : group.getQuestions()) {
                if (results.size() > 0)
                    for (Result r : results) {
                        if (r.getQuestion() != null && r.getQuestion().getLiveId() == question.getLiveId()) {
                            question.setResult(r);
                            break;
                        }
                    }
                if (notes.size() > 0)
                    for (Note n : notes) {
                        if (n.getQuestion() != null && n.getQuestion().getLiveId() == question.getLiveId()) {
                            question.setNote(n);
                            break;
                        }
                    }
            }
        }
    }

    /**
     * add note to question
     *
     * @param note
     */
    public void addCheckListNote(Note note) {
        for (Group group : this.getGroups()) {
            //question != null mean that is question result
            group.addQuestionNote(note);
        }
    }

    /**
     * add medias
     *
     * @param note
     * @param medias
     */
    public void addMedias(Note note, List<Media> medias) {
        for (Group group : this.getGroups()) {
            if (note.getQuestion() != null) {
                Log.e(TAG, "add medias");
                boolean result = group.addMedias(note, medias);
                if (result) {
                    break;
                }
            }
        }
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }


//    "id=" + this.getLiveId() +
//            ", enable=" + enable +
//            ", createdAt='" + createdAt + '\'' +
//            ", updatedAt='" + updatedAt + '\'' +
//            ", groups=" + groups +

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }

    @Override
    public void save() {
        CheckList exist = new RushSearch()
                .startGroup()
                .whereNotEqual("liveId", this.getLiveId())
                .endGroup()
                .findSingle(CheckList.class);
        if (exist != null) {
            exist.setEnable(true);
            exist.setGroups(this.getGroups());
            exist.setType(this.getType());
            exist.save(new RushCallback() {
                @Override
                public void complete() {

                }
            });
        }
        if (this.type != null)
            this.typeId = type.getLiveId();
        if (this.getId() == null)
            super.save();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeTypedList(this.getGroups());
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
