package com.arise.healthcaresafety.model.entities;

import com.google.gson.annotations.SerializedName;

public class LoginResult {

    public int status;
    public User user;
    // protected long liveId;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "LoginResult{" +
                "status=" + status +
                ", user=" + user +
                '}';
    }

//    @Override
//    public String getKind() {
//        return null;
//    }
//
//    @Override
//    public void onSave() {
//
//    }

    public static class User {

        @SerializedName("last_login")
        private String lastLogin;
        private String address;
        @SerializedName("first_name")
        private String firstName;
        @SerializedName("last_name")
        private String lastName;
        private String locale;
        private String dob;
        private String hash;
        private String token;
        @SerializedName("profile_image")
        private String profileImage;
        private String gender;
        private String username;

        public String getLastLogin() {
            return lastLogin;
        }

        public void setLastLogin(String lastLogin) {
            this.lastLogin = lastLogin;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        @Override
        public String toString() {
            return "User{" +
                    "lastLogin='" + lastLogin + '\'' +
                    ", address='" + address + '\'' +
                    ", firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", locale='" + locale + '\'' +
                    ", dob='" + dob + '\'' +
                    ", hash='" + hash + '\'' +
                    ", token='" + token + '\'' +
                    ", profileImage='" + profileImage + '\'' +
                    ", gender='" + gender + '\'' +
                    ", username='" + username + '\'' +
                    '}';
        }
    }


}
