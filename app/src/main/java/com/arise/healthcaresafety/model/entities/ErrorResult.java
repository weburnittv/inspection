package com.arise.healthcaresafety.model.entities;

public class ErrorResult {
    /* {"status":false,"error":"User Not found"} */
    private boolean status;
    private String error;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ErrorResult{" +
                "status=" + status +
                ", error='" + error + '\'' +
                '}';
    }

}
