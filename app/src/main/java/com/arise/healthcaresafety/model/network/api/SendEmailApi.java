package com.arise.healthcaresafety.model.network.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.arise.healthcaresafety.model.entities.SendStatus;
import com.arise.healthcaresafety.model.network.api.base.BaseInspectionApi;
import com.arise.healthcaresafety.model.network.volley.MyVolley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hnam on 8/12/2015.
 */
public class SendEmailApi extends BaseInspectionApi {
    private static final String TAG = SendEmailApi.class.getSimpleName();
    //POST('/api/apps/inspection/sendmail', ['form[email]' => 'weburnittv@gmail.com', 'form[case]' => 5])
    private static final String SEND_EMAIL_API = BaseInspectionApi.SERVER + "/api/apps/inspection/sendmail.json";
    private Context context;

    public SendEmailApi(Context context) {
        this.context = context;
    }

    public void sendEmail(String email,
                          long caseId,
                          Response.Listener<SendStatus> listener,
                          Response.ErrorListener errorListener) {
        Map<String, String> params = new HashMap<>();
        params.put("form[email]", email);
        params.put("form[case]", String.valueOf(caseId));
        Request request = super.createPostMethod(SEND_EMAIL_API,
                SendStatus.class,
                listener,
                errorListener,
                params);
        MyVolley.getInstance(context).addToRequestQueue(request);
    }
}
