package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import co.uk.rushorm.core.annotations.RushIgnore;

public class Attribute extends BaseEntity implements Parcelable {
    @RushIgnore
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Attribute createFromParcel(Parcel source) {
            return new Attribute(source);
        }

        @Override
        public Attribute[] newArray(int size) {
            return new Attribute[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    protected String name;

    public Attribute() {
    }

    public Attribute(String name, long liveId) {
        this.name = name;
        this.liveId = liveId;
    }

    //implement parcelable
    public Attribute(Parcel in) {
        this.setLiveId(in.readLong());
        this.name = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.getLiveId());
        dest.writeString(this.name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }
}
