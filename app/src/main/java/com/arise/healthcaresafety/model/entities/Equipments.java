package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.arise.healthcaresafety.view.adapter.EquipmentGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.uk.rushorm.core.RushSearch;

public class Equipments implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Equipments createFromParcel(Parcel source) {
            return new Equipments(source);
        }

        @Override
        public Equipments[] newArray(int size) {
            return new Equipments[size];
        }
    };
    private static final String TAG = Equipments.class.getSimpleName();
    private Branch branch;
    private List<Equipment> equipments;

    public Equipments(Branch branch) {
        this.branch = branch;
    }

    public Equipments(List<Equipment> list) {
        this.equipments = list;
    }

    public Equipments() {
    }

    public Equipments(Parcel in) {
        this.equipments = new ArrayList<Equipment>();
        in.readTypedList(this.equipments, Media.CREATOR);
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<Equipment> getEquipments() {
        if (this.equipments == null)
            this.loadEquipments();
        return equipments;
    }

    public void setEquipments(List<Equipment> equipments) {
        this.equipments = equipments;
    }

    public void loadEquipments() {
        this.equipments = new RushSearch().whereEqual("branchId", this.branch.getLiveId()).find(Equipment.class);
    }

    private HashMap<String, List<Equipment>> parseEquipment() {
        HashMap<String, List<Equipment>> equipmentList = new HashMap<>();
        String previousKey = "";
        for (Equipment equipment : this.getEquipments()) {
            String categoryName = equipment.getCategory().getName();
            if (previousKey.equals(categoryName)) {
                equipmentList.get(categoryName).add(equipment);
            } else {
                //add new
                equipmentList.put(categoryName, new ArrayList<Equipment>());
                equipmentList.get(categoryName).add(equipment);
                previousKey = categoryName;
            }
        }
        return equipmentList;
    }

    public ArrayList<EquipmentGroup> getEquipmentGroup() {
        Map<String, List<Equipment>> list = parseEquipment();
        ArrayList<EquipmentGroup> arrGroup = new ArrayList<>();
        for (String name : list.keySet()) {
            arrGroup.add(new EquipmentGroup(name, list.get(name)));
        }
        return arrGroup;
    }

    /**
     * set result for each equipments.
     *
     * @param results
     */
    public Equipments setEquipmentResults(List<Result> results, List<Note> notes) {
        for (Equipment equip : this.equipments) {
            if (results.size() > 0)
                for (Result r : results) {
                    if (r.getEquipment() != null && r.getEquipment().getLiveId() == equip.getLiveId()) {
                        //TODO equipment add results
                        equip.addResult(r);
                    }
                }
            if (notes.size() > 0)
                for (Note n : notes) {
                    if (n.getEquipment() != null && n.getEquipment().getLiveId() == equip.getLiveId()) {
                        equip.setNote(n);
                    }
                }
        }
        return this;
    }

    public void addEquipmentsResult(Result result) {
        for (Equipment equip : this.equipments) {
            if (result.getQuestion() == null && result.getAttributes().get(0).getLiveId() == equip.getLiveId()) {
                //TODO  add equipment results
                equip.addResult(result);
            }
        }
    }

    /**
     * add result for pastcase screen
     *
     * @param results
     */
    public void addEquipmentResults(List<Result> results) {
        for (Equipment equip : this.equipments) {
            for (Result r : results) {
                if (r.getAttributes().size() == 0) {
                    continue;
                }
                if (r.getAttributes().get(0).getLiveId() == equip.getLiveId() && r.getQuestion() == null) {
                    //TODO equipment add results
                    equip.setResult(r);
                }
            }
        }
    }

    /**
     * detele equipement result
     *
     * @param result
     */
    public void deleteEquipmentResults(Result result) {
        for (Equipment equip : this.equipments) {
            if (equip.getResult() != null && equip.getResult().getLiveId() == result.getLiveId()) {
                equip.setResult(null);
            }
        }
    }

    /**
     * add new note to equipment
     *
     * @param note
     */
    public void addEquipmentNote(Note note) {
        for (Equipment equip : this.equipments) {
            //equip.getmId(): id of attributes
            if (equip.getLiveId() == note.getAttribute().getLiveId()) {
                if (equip.getNote() != null) {
                    //edit note
                    Log.e(TAG, "edit note");
                    equip.getNote().setContent(note.getContent());
                    break;
                } else {
                    //add new note
                    Log.e(TAG, "add new note");
                    equip.setNote(note);
                }
            }
        }
    }

    /**
     * add number of photo to equipment
     *
     * @param note
     */
    public void addMedias(Note note) {
        for (Equipment equip : this.equipments) {
            if (equip.getLiveId() == note.getEquipment().getLiveId()) {
                equip.setNote(note);
                break;
            }
        }
    }

    public void save() {
        List<Equipment> old = Equipment.listEquipmentByBranch(this.branch.getLiveId());
        HashMap<String, Equipment> equipments = new HashMap<>();
        for (Equipment oldOne : old) {
            equipments.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }
        for (Equipment equipment : this.equipments) {
            equipment.setBranch(this.branch);
            Equipment oldOne = equipments.get(String.valueOf(equipment.getLiveId()));
            if (oldOne == null)
                equipment.save();
            else {
                oldOne.setName(equipment.getName());
                oldOne.save();
                equipments.remove(String.valueOf(oldOne.getLiveId()));
            }
        }
        for (String liveId : equipments.keySet()) {
            Equipment equipment = equipments.get(liveId);
            equipment.delete();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.getEquipments());
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
