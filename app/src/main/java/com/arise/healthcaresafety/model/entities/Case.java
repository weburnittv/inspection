package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushList;

public class Case extends BaseEntity implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Case createFromParcel(Parcel source) {
            return new Case(source);
        }

        @Override
        public Case[] newArray(int size) {
            return new Case[size];
        }
    };
    private static final String TAG = Case.class.getSimpleName();
    private static final int FINAL_DEFAULT = 0;
    //Unsatisfactory
    private static final int FINAL_UNSATICFACTORY = 1;
    //Marginal
    private static final int FINAL_MARGINAL = 2;
    //Satisfactory
    private static final int FINAL_SATISFACTORY = 3;
    //Good
    private static final int FINAL_GOOD = 4;
    private static final int FINAL_VERY_GOOD = 5;
    @SerializedName("liveId")
    protected long liveId;
    @SerializedName("arrival_date")
    protected String arrivalDate;
    @SerializedName("created_at")
    protected String createdDate;
    @SerializedName("reference_no")
    protected String referenceNo;
    @RushList(classType = Result.class)
    protected List<Result> results;
    @RushList(classType = Note.class)
    protected List<Note> notes;
    protected Branch branch = null;
    protected Type type = null;
    //TODO add new final question
    @SerializedName("final")
    protected long caseFinal = FINAL_DEFAULT;
    @RushIgnore
    protected Equipments equipments;
    @RushIgnore
    protected CheckList checkList;
    private boolean synced = false;
    private boolean deleted = false;

    public Case() {
    }

    public Case(Case item) {
        this.setArrivalDate(item.getArrivalDate());
        this.setReferenceNo(item.getReferenceNo());
        this.setResults(item.getResults());
        this.setNotes(item.getNotes());
        this.setBranch(item.getBranch());
        this.setType(item.getType());
    }

    public Case(Parcel in) {
        this.arrivalDate = in.readString();
        this.createdDate = in.readString();
        this.referenceNo = in.readString();
        this.results = new ArrayList<Result>();
        in.readTypedList(this.results, Media.CREATOR);
        this.notes = new ArrayList<Note>();
        in.readTypedList(this.notes, Media.CREATOR);
        this.branch = in.readParcelable(Branch.class.getClassLoader());
        this.type = in.readParcelable(Type.class.getClassLoader());
        this.equipments = in.readParcelable(Equipment.class.getClassLoader());
        this.checkList = in.readParcelable(CheckList.class.getClassLoader());
        this.setLiveId(in.readLong());
    }

    public static List<Case> getAllCases() {
        return new RushSearch().find(Case.class);
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return "case";
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public List<Result> getResults() {
        if (this.results != null) {
            return this.results;
        }
        return new ArrayList<Result>();
    }

    public void setResults(List<Result> results) {
        this.results = results;
        this.save();
    }

    public List<Note> getNotes() {
        if (this.notes != null)
            return this.notes;
        return new ArrayList<Note>();
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public Note getNote(Note note) {
        for (Note item : this.getNotes()
                ) {
            if (note.getId().equals(item.getId())) {
                return item;
            }
        }
        return null;
    }

    public Note getNoteByLiveId(long liveId) {
        for (Note note : this.getNotes()) {
            if (note.getLiveId() == liveId)
                return note;
        }
        return null;
    }

    public Note getNoteByEquipment(long equipmentId) {
        for (Note note : this.getNotes()) {
            Equipment equipment = note.getEquipment();
            if (equipment != null && equipment.getLiveId() == equipmentId)
                return note;
        }
        return null;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
        this.equipments = new Equipments(new RushSearch().whereEqual("branchId", this.getBranch().getLiveId()).find(Equipment.class));
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
        this.checkList = null;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void saveCaseFinal(long caseFinal) {
        this.caseFinal = caseFinal;
    }

    public long getCaseFinal() {
        return this.caseFinal;
    }

    @Override
    public String toString() {
        return "Case{" +
                "id=" + this.getLiveId() +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", referenceNo='" + referenceNo + '\'' +
                ", results=" + results +
                ", notes=" + notes +
                ", branch=" + branch +
                ", type=" + type +
                ", Equipments=" + equipments +
                ", CheckList=" + checkList +
                '}';
    }

    public Map<String, String> getDataPost() {
        Map<String, String> params = new HashMap<>();
        params.put(super.createKey(getKind(), "branch"), String.valueOf(getBranch().getLiveId()));
        params.put(super.createKey(getKind(), "type"), String.valueOf(getType().getLiveId()));
        params.put(super.createKey(getKind(), "referenceNo"), getReferenceNo());
        params.put(super.createKey(getKind(), "arrivalDate"), getArrivalDate());
        params.put(super.createKey(getKind(), "final"), String.valueOf(this.caseFinal));
        return params;
    }

    public Map<String, RequestBody> getRetrofitDataPost() {
        RequestBody rBranch = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getBranch().getLiveId()));
        RequestBody rType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getType().getLiveId()));
        RequestBody rRef = RequestBody.create(MediaType.parse("text/plain"), this.referenceNo);
        RequestBody rDate = RequestBody.create(MediaType.parse("text/plain"), this.arrivalDate);
        RequestBody rCaseFinal = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.caseFinal));

        Map<String, RequestBody> params = new HashMap<>();
        params.put(super.createKey(getKind(), "branch"), rBranch);
        params.put(super.createKey(getKind(), "type"), rType);
        params.put(super.createKey(getKind(), "referenceNo"), rRef);
        params.put(super.createKey(getKind(), "arrivalDate"), rDate);
        params.put(super.createKey(getKind(), "final"), rCaseFinal);
        return params;
    }

    public Equipments getEquipments() {
        if(this.branch == null)
            return null;
        if (this.equipments == null)
            this.equipments = new Equipments(new RushSearch().whereEqual("branchId", this.getBranch().getLiveId()).find(Equipment.class));
        this.equipments.setEquipmentResults(this.getResults(), this.getNotes());
        return this.equipments;
    }

    public void setEquipments(Equipments equipments) {
        this.equipments = equipments;
    }

    public CheckList getCheckList() {
        if (this.checkList == null) {
            this.checkList = new RushSearch().whereEqual("typeId", this.getType().getLiveId()).findSingle(CheckList.class);
        }
        if (this.checkList != null)
            this.checkList.setCheckListResult(this.getResults(), this.getNotes());
        return this.checkList;
    }

    public void setCheckList(CheckList checkList) {
        this.checkList = checkList;
    }

    public String getDateTime(boolean isGettingDate) {
        if (TextUtils.isEmpty(getArrivalDate())) {
            return "";
        }
        String[] a = getArrivalDate().split(" ");
        return isGettingDate ? a[0] : a[1];
    }

    public String getMyArrivalTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.US);
        Date value = null;
        String dt = "";
        try {
            value = formatter.parse(this.arrivalDate);
            SimpleDateFormat date = new SimpleDateFormat("hh:mm:ss", Locale.US);
            dt = date.format(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dt;
    }

    public String getMyArrivalDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.US);
        Date value = null;
        String dt = "";
        try {
            value = formatter.parse(this.arrivalDate);
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            dt = date.format(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dt;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * get equipment result
     *
     * @return
     */
    public List<Result> getEquipmentResult() {
        List<Result> results = new ArrayList<>();

        for (Result r : this.getResults()) {
            if (r.getQuestion() == null) {
                results.add(r);
            }

        }
        return results;
    }

    /**
     * get checklist result
     */
    public List<Result> getCheckListResult() {
        List<Result> results = new ArrayList<>();

        for (Result r : this.getResults()) {
            if (r.getQuestion() != null) {
                results.add(r);
            }
        }
        return results;
    }

    /**
     * add note to notes lists of case
     *
     * @param note
     */
    public void updateNote(Note note) {
        this.saveNote(note);
    }

    /*update case*/
    public void updateCase(Object o) {
        if (o instanceof Case) {
            Case c = (Case) o;
            this.setLiveId(c.getLiveId());
            this.arrivalDate = c.getArrivalDate();
            this.createdDate = c.getCreatedDate();
            this.referenceNo = c.getReferenceNo();
            this.caseFinal = c.getCaseFinal();
        }
    }

    public void storeLiveId(long liveId) {
        this.setLiveId(liveId);
    }

    @Override
    public void onSave() {
        List<Case> oldCase = getAllCases();
        HashMap<String, Case> caseObj = new HashMap<>();

        for (Case oldOne : oldCase) {
            caseObj.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }

        Case oldOne = caseObj.get(String.valueOf(getLiveId()));
        if (oldOne == null)
            super.save();
        else {
            oldOne.setArrivalDate(arrivalDate);
            oldOne.setCreatedDate(createdDate);
            oldOne.setReferenceNo(referenceNo);
            oldOne.setResults(results);
            oldOne.setNotes(notes);
            oldOne.setBranch(branch);
            oldOne.setType(type);
            oldOne.setEquipments(equipments);
            oldOne.setCheckList(checkList);
            oldOne.save();
        }

        for (String liveId : caseObj.keySet()) {
            Case deletedOne = caseObj.get(liveId);
            deletedOne.delete();
        }
        super.save();
    }

    public void saveNote(Note note) {
        boolean updated = false;
        for (Note oldOne : this.getNotes()) {
            if (note.isDeleted() && oldOne.getId().equals(note.getId())) {
                this.notes.remove(oldOne);
                return;
            }
            if (oldOne.getEquipment() != null && note.getEquipment() != null) {
                updated = oldOne.updateNoteEquipment(note);
                if (updated) break;
            } else if (oldOne.getQuestion() != null && note.getQuestion() != null) {
                updated = oldOne.updateNoteQuestion(note);
                if (updated) break;
            }
        }
        if (!updated) {
            this.notes = this.getNotes();
            this.notes.add(note);
        }
    }

    public void saveResult(Result result) {
        boolean updated = false;
        for (Result oldOne : this.getResults()) {
            if (result.isDeleted() && oldOne.getId().equals(result.getId())) {
                this.results.remove(oldOne);
                return;
            }
            if (oldOne.getEquipment() != null && result.getEquipment() != null) {
                updated = oldOne.updateResultEquipment(result);
                if (updated)
                    break;
            } else if (oldOne.getQuestion() != null && result.getQuestion() != null) {
                updated = oldOne.updateResultQuestion(result);
                if (updated)
                    break;
            }
        }
        if (!updated) {
            this.results = this.getResults();
            this.results.add(result);
        }
    }

    public void deleteNote(Note note) {
        List<Note> notes = this.getNotes();
        for (Note item : notes) {
            if (note.getLiveId() == item.getLiveId() || note.getId() == item.getId()) {
                if (note.getLiveId() == 0)
                    notes.remove(item);
                else note.setDeleted(true);
                break;
            }
        }
    }

    public void deleteResult(Result result) {
        List<Result> results = this.getResults();
        for (Result item : results) {
            if (item.getLiveId() == result.getLiveId() || item.getId() == result.getId()) {
                if (item.getLiveId() == 0)
                    results.remove(item);
                else item.setDeleted(true);
                break;
            }
        }
    }

    @Override
    public void save() {
        if (this.getType() != null && this.getBranch() != null && this.getReferenceNo() != null)
            super.save();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.arrivalDate);
        dest.writeString(this.createdDate);
        dest.writeString(this.referenceNo);
        dest.writeTypedList(this.getResults());
        dest.writeTypedList(this.getNotes());
        dest.writeParcelable(this.branch, flags);
        dest.writeParcelable(this.type, flags);
        dest.writeParcelable(this.equipments, flags);
        dest.writeParcelable(this.checkList, flags);
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
