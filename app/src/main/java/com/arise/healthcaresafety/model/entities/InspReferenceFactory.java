package com.arise.healthcaresafety.model.entities;

public class InspReferenceFactory {
    public static final int DOC_REF = 0;
    public static final int IMAGE_REF = 1;

    public static InspReference create(String link) {
        if (link.endsWith("jpg")) {
            return new InspReference(IMAGE_REF, link);
        } else if (link.endsWith("png")) {
            return new InspReference(IMAGE_REF, link);
        } else {
            return new InspReference(DOC_REF, link);
        }
    }

}
