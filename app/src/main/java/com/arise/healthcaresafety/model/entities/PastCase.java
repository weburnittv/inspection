package com.arise.healthcaresafety.model.entities;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PastCase extends Case {

    public PastCase() {
        //empty constructor
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    @Override
    public String toString() {
        return "PastCase{" +
                "id=" + this.getLiveId() +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", referenceNo='" + referenceNo + '\'' +
                ", branch=" + branch +
                ", type=" + type +
                '}';
    }

    public String getDateTime(boolean isGettingDate) {
        if (TextUtils.isEmpty(this.createdDate)) {
            return "";
        }
        String[] a = getArrivalDate().split(" ");
        return isGettingDate ? a[0] : a[1];
    }

    /**
     * return format create_date
     * example: Tue, 07 Jul, 2015 09:00 PM
     *
     * @return
     */
    public String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.US);
        Date value = null;
        String dt = "";
        try {
            value = formatter.parse(this.createdDate);
            SimpleDateFormat date = new SimpleDateFormat("E, dd MMM, yyyy hh:mm a", Locale.US);
            dt = date.format(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dt;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }
}
