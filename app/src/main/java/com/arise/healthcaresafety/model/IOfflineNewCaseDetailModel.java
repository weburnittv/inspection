package com.arise.healthcaresafety.model;

import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Type;

/**
 * Created by Nam on 8/10/2015.
 */
public interface IOfflineNewCaseDetailModel {
    void loadDivisions();

    void loadCategories();

    void loadEquipments(Branch branch);

    void loadCheckLists(Type type);

    void updateEquipmentResult(Case c, Result result);

    void updateEquipmentNote(Case c, Note note);

    void updateCheckListResult(Case c, Result result);

    void updateCheckListNote(Case c, Note note);
}
