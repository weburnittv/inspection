package com.arise.healthcaresafety.model.network.api.base;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by hnam on 7/1/2015.
 */
public class BaseGsonRequest<T> extends Request<T> {
    private static final String TAG = BaseGsonRequest.class.getSimpleName();
    private final Gson gson = new Gson();
    private final Class<T> clazz;
    //private final Map<String, String> headers;
    private final Response.Listener<T> listener;
    private Context context;
    private Map<String, String> headers;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url   URL of the request to make
     * @param clazz Relevant class object, for Gson's reflection
     */
    public BaseGsonRequest(int method, String url, Class<T> clazz,
                           Response.Listener<T> listener,
                           Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.listener = listener;
    }

    public BaseGsonRequest(int method, String url,
                           Class<T> clazz,
                           Map<String, String> headers,
                           Response.Listener<T> listener,
                           Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.listener = listener;
        this.headers = headers;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            if (response.statusCode == 200) {
                String json = new String(
                        response.data,
                        HttpHeaderParser.parseCharset(response.headers));
                Log.e(TAG, new String(response.data));
                return Response.success(
                        gson.fromJson(json, clazz),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
            return Response.success(
                    gson.fromJson("", clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
