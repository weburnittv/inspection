package com.arise.healthcaresafety.model;

/**
 * Created by Nam on 7/9/2015.
 */
public interface IListPastCasesModel {
    public void onLoadPastCases();

    public void searchPastCases(String key);
}

