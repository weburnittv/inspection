package com.arise.healthcaresafety.model.entities;

import com.arise.healthcaresafety.view.adapter.CaseGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import co.uk.rushorm.core.RushSearch;


public class PastCases {
    private static final String TAG = PastCases.class.getSimpleName();
    private List<Case> pastCases;

    public List<Case> getPastCases() {
        if (pastCases == null)
            this.loadPastCases();
        return pastCases;
    }

    public void setPastCases(List<Case> pastCases) {
        this.pastCases = pastCases;
    }

    @Override
    public String toString() {
        return "PastCases{" +
                "pastCases=" + pastCases +
                '}';
    }

    public void loadPastCases() {
        List<Case> list = new RushSearch().find(Case.class);
        this.pastCases = list;
    }

    public ArrayList<CaseGroup> getPastCaseGroup() {
        HashMap<String, List<Case>> list = parsePastCases();
        ArrayList<CaseGroup> arrGroup = new ArrayList<>();
        for (String name : list.keySet()) {
            arrGroup.add(new CaseGroup(name, list.get(name)));
        }
        Collections.sort(arrGroup, new Comparator<CaseGroup>() {
            @Override
            public int compare(CaseGroup c1, CaseGroup c2) {
                return c1.getName().compareToIgnoreCase(c2.getName());
            }
        });
        return arrGroup;
    }

    private HashMap<String, List<Case>> parsePastCases() {
        HashMap<String, List<Case>> pastCasesHM = new HashMap<>();
        String previousKey = "";
        Collections.sort(this.getPastCases(), new Comparator<Case>() {
            @Override
            public int compare(Case lhs, Case rhs) {
                return lhs.getType().getCategory().getName().compareToIgnoreCase(rhs.getType().getCategory().getName());
            }
        });

        for (Case pastCase : this.getPastCases()) {
            String category = pastCase.getType().getCategory().getName();
            if (previousKey.equals(category)) {
                pastCasesHM.get(category).add(pastCase);
            } else {
                //add new
                pastCasesHM.put(category, new ArrayList<Case>());
                pastCasesHM.get(category).add(pastCase);
                previousKey = category;
            }
        }

        return pastCasesHM;
    }
}
