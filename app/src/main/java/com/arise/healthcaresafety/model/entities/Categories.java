package com.arise.healthcaresafety.model.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Categories {
    private List<Category> categories;

    private List<Statistic> statistic;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "categories=" + categories +
                '}';
    }

    public List<String> loadCategoryNameList() {
        List<String> result = new ArrayList<>();
        if (this.categories == null) {
            return result;
        }

        for (Category item : this.categories) {
            result.add(item.getName());
        }

        return result;
    }

    public Category getCategoriesByIndex(int index) {
        if (this.categories == null) {
            return null;
        }

        if (index > this.getCategories().size()) {
            return null;
        }
        return this.categories.get(index);
    }

    public void loadCategories() {
        this.categories = Category.listAll();
    }

    public void save() {
        List<Category> old = Category.listAll();
        HashMap<String, Category> categories = new HashMap<>();
        for (Category oldOne : old) {
            categories.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }
        for (Category category : this.categories) {
            Category oldOne = categories.get(String.valueOf(category.getLiveId()));
            Statistic data = this.getCategoryStatistic(category);
            category.setStatistic(data);
            if (oldOne == null)
                category.save();
            else {
                oldOne.setName(category.getName());
                oldOne.save();
                oldOne.setStatistic(data);
                oldOne.setTypes(category.getTypes());
                categories.remove(String.valueOf(oldOne.getLiveId()));
            }
        }
        for (String liveId : categories.keySet()) {
            Category category = categories.get(liveId);
            category.delete();
        }

    }

    public Statistic getCategoryStatistic(Category cat) {
        for (Statistic stat : this.statistic) {
            if (stat.getLiveId() == cat.getLiveId())
                return stat;
        }
        return null;
    }
}
