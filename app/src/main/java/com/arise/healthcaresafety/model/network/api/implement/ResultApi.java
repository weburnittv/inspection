package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Status;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Path;

/**
 * Created by Paul Aan on 1/2/16.
 */
public interface ResultApi {
    @Multipart
    @POST("/api/apps/inspection/case/{caseId}/result")
    Call<Result> createResult(@Path(value = "caseId", encoded = true) long caseId, @PartMap Map<String, RequestBody> attribute);

    @Multipart
    @POST("/api/apps/inspection/result/{id}")
    Call<Result> updateResult(@Path(value = "id", encoded = true) long resultId, @PartMap Map<String, RequestBody> attribute);


    @DELETE("/api/apps/inspection/delete/media/{id}")
    Call<Status> deleteResult(@Path(value = "id", encoded = true) long liveId);
}
