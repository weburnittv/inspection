package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Status;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;

public interface SendMailApi {

    @Multipart
    @POST("/api/apps/inspection/sendmail.json/")
    Call<Status> sendEmail(@PartMap Map<String, RequestBody> data);
}
