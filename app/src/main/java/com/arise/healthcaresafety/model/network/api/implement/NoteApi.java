package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Note;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Path;

/**
 * Created by Paul Aan on 1/2/16.
 */
public interface NoteApi {
    @Multipart
    @POST("/api/apps/inspection/note")
    Call<Note> createNote(@PartMap Map<String, RequestBody> attribute);

    @Multipart
    @POST("/api/apps/inspection/note/{id}")
    retrofit.Call<Note> updateNote(@Path(value = "id", encoded = true) long noteId, @PartMap Map<String, RequestBody> attribute);
}
