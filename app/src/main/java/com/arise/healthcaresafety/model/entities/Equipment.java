package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

@RushTableAnnotation
public class Equipment extends BaseEntity implements Parcelable {
    @RushIgnore
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Equipment createFromParcel(Parcel source) {
            return new Equipment(source);
        }

        @Override
        public Equipment[] newArray(int size) {
            return new Equipment[size];
        }
    };
    @RushIgnore
    private static final String TAG = Equipment.class.getSimpleName();
    @RushIgnore
    public transient boolean isActive;
    @RushIgnore
    public transient int numberImage = 0;
    @RushIgnore
    public transient int numberRecord = 0;
    @SerializedName("id")
    protected long liveId;
    protected String name;
    @SerializedName("quantity")
    private int quantity;
    @RushIgnore
    private boolean checked = false;
    @RushIgnore
    @SerializedName("category")
    private Category category;
    private String categoryName;
    private long branchId;
    @RushIgnore
    private Branch branch;
    @RushIgnore
    private transient Result result;
    @RushIgnore
    private transient Note note;

    public Equipment() {
    }

    public Equipment(Parcel source) {
        this.name = source.readString();
        this.setLiveId(source.readLong());
    }

    public Equipment(boolean isActive, Category category, long liveId, Branch branch, String name, int quantity) {
        this.isActive = isActive;
        this.category = category;
        this.liveId = liveId;
        this.branch = branch;
        this.name = name;
        this.quantity = quantity;
        this.branchId = branch.getLiveId();
    }

    public static List<Equipment> listAll() {
        return new RushSearch().find(Equipment.class);
    }

    public static List<Equipment> listEquipmentByBranch(long branchId) {
        return new RushSearch().whereEqual("branchId", branchId).find(Equipment.class);
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
        if (branch != null)
            this.branchId = branch.getLiveId();
    }

    public boolean isChecked() {
        if (this.result != null && !this.result.isDeleted())
            this.checked = true;
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        this.result = this.getResult();
        this.result.setDeleted(!checked);
        this.result.setSynced(false);
        this.note = this.getNote();
        this.note.setDeleted(!checked);
        this.note.setSynced(false);
    }

    public String getName() {
        if (name != null)
            return name;
        return "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Category getCategory() {
        if (this.category != null)
            return this.category;
        return new Category(this.categoryName);
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getNumberImage() {
        return numberImage;
    }

    public void setNumberImage(int numberImage) {
        this.numberImage = numberImage;
    }

    public int getNumberRecord() {
        return numberRecord;
    }

    public void setNumberRecord(int numberRecord) {
        this.numberRecord = numberRecord;
    }

    public Note getNote() {
        if (this.note == null)
            this.note = new Note(this);
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Result getResult() {
        if (this.result == null)
            this.result = new Result(this);
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", category=" + category +
                '}';
    }

    @Override
    public String getKind() {
        return "equipment";
    }

    @Override
    public void onSave() {

    }

    /**
     * get attribute of this equipment
     */
    public Attribute getAttribute() {
        return new Attribute();
    }

    /**
     * add result to equipment
     *
     * @param r
     */
    public void addResult(Result r) {
        this.result = r;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.getLiveId());
        dest.writeString(this.name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void save() {
        if (this.category != null) {
            this.categoryName = this.category.getName();
        }
        super.save();
    }

    public static class Category {
        private String name;

        public Category() {//empty contructor
        }

        public Category(String name) {
            this.name = name;
        }

        public String getName() {
            if (name != null)
                return name;
            return "";
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Category{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
