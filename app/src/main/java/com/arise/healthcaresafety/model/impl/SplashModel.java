package com.arise.healthcaresafety.model.impl;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.R;
import com.arise.healthcaresafety.model.ISplashModel;
import com.arise.healthcaresafety.model.entities.ErrorResult;
import com.arise.healthcaresafety.model.entities.LoginResult;
import com.arise.healthcaresafety.model.listener.OnSplashListener;
import com.arise.healthcaresafety.model.network.api.LoginApi;
import com.arise.healthcaresafety.utils.SharedPreference;
import com.google.gson.Gson;


public class SplashModel implements ISplashModel {
    private static final String TAG = SplashModel.class.getSimpleName();
    private final OnSplashListener onSplashListener;
    private Context mContext;
    private LoginApi loginApi;
    private String mUserName;
    private String mPassword;
    private Response.Listener<LoginResult> listener = new Response.Listener<LoginResult>() {
        @Override
        public void onResponse(LoginResult loginResult) {
            if (onSplashListener != null) {
                // loginResult.getUser().setUsername(userName);
                onSplashListener.onLoginResult(loginResult);

                // save username and password for re-open application
                SharedPreference.setUserName(mContext, mUserName);
                SharedPreference.setPassword(mContext, mPassword);
            }
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if (onSplashListener != null) {
                Gson gson = new Gson();
                ErrorResult error = null;
                if (volleyError.networkResponse != null) {
                    Log.e(TAG, new String(volleyError.networkResponse.data));
                    error = gson.fromJson(new String(volleyError.networkResponse.data),
                            ErrorResult.class);
                    SharedPreference.setUserName(mContext, "");
                    SharedPreference.setPassword(mContext, "");
                } else {
                    error = new ErrorResult();
                    error.setError(mContext.getString(R.string.warning_network_error));
                }
                onSplashListener.onLoginError(error.getError());
                InspectionApp.getInstance().setUsername(null);
            }
        }
    };

    public SplashModel(Context context, OnSplashListener onSplashListener) {
        this.mContext = context;
        this.onSplashListener = onSplashListener;
        loginApi = new LoginApi(context);
    }

    @Override
    public void login(String userName, String password) {
        loginApi.login(userName, password, listener, errorListener);
        InspectionApp.getInstance().setUsername(userName);
        this.mUserName = userName;
        this.mPassword = password;
    }
}
