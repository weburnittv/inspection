package com.arise.healthcaresafety.model;

/**
 * Created by Nam on 7/1/2015.
 */
public interface ISplashModel {
    public void login(String userName, String password);
}
