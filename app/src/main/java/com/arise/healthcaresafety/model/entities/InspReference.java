package com.arise.healthcaresafety.model.entities;


import com.google.gson.annotations.SerializedName;

public class InspReference extends BaseEntity {

    @SerializedName("liveId")
    protected long liveId;
    private int type;
    private String link;

    public InspReference(int type, String link) {
        this.type = type;
        this.link = link;
    }

    public int getType() {
        return type;
    }

    public String getLink() {
        return link;
    }

    /**
     * get name from link
     *
     * @return
     */
    public String getName() {
        String tamp = this.link.replace("\\", "");
        String[] a = tamp.split("/");
        if (a.length > 0) {
            return a[a.length - 1];
        } else {
            return "unknown name";
        }
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }
}
