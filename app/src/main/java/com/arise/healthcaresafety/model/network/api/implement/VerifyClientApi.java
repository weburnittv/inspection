package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.ClientResult;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;

public interface VerifyClientApi {

    @Multipart
    @POST("/api/apps/verify/client")
    Call<ClientResult> verifyClient(@PartMap Map<String, RequestBody> data);
}
