package com.arise.healthcaresafety.model.entities;

import co.uk.rushorm.core.RushObject;

public class Statistic extends RushObject {
    private long liveId;
    private String name;
    private int conducted;
    private int failed;

    public Statistic() {
    }

    public long getLiveId() {
        return liveId;
    }

    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getConducted() {
        return conducted;
    }

    public void setConducted(int conducted) {
        this.conducted = conducted;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }
}
