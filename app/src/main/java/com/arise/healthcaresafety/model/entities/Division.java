package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushList;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

@RushTableAnnotation
public class Division extends BaseEntity implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Division createFromParcel(Parcel source) {
            return new Division(source);
        }

        @Override
        public Division[] newArray(int size) {
            return new Division[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    @SerializedName("name")
    private String name;
    @RushList(classType = Branch.class)
    private List<Branch> branches;

    public Division() {
    }

    public Division(String name, long liveId) {
        this.name = name;
        this.liveId = liveId;
    }

    public Division(Parcel in) {
        this.name = in.readString();
        this.branches = new ArrayList<Branch>();
        in.readTypedList(this.branches, Media.CREATOR);
        this.setLiveId(in.readLong());
    }

    public static List<Division> listAll() {
        return new RushSearch().find(Division.class);
    }

    public static List<Branch> getDivisionBranch(long divisionId) {
        return new RushSearch().whereEqual("divisionId", String.valueOf(divisionId)).find(Branch.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Branch> getBranches() {
        if (this.branches != null) {
            return this.branches;
//            this.branches = new RushSearch().whereEqual("divisionId", this.getLiveId()).find(Branch.class);
        }
        return new ArrayList<Branch>();
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    @Override
    public String toString() {
        return "Division{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", branches=" + branches +
                '}';
    }

    /**
     * get Branch Name List
     *
     * @return
     */
    public List<String> getBranchNameList() {
        List<String> result = new ArrayList<>();

        if (this.branches == null) {
            return result;
        }

        for (Branch branch : this.branches) {
            result.add(branch.getName());
        }
        return result;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }

    @Override
    public void save() {
        List<Branch> old = Division.getDivisionBranch(this.liveId);
        HashMap<String, Branch> branches = new HashMap<>();

        for (Branch oldOne : old) {
            branches.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }
        if (this.branches != null) {
            for (Branch branch : this.branches) {
                branch.setDivision(this);
                Branch oldOne = branches.get(String.valueOf(branch.getLiveId()));
                if (oldOne == null)
                    branch.save();
                else {
                    oldOne.setName(branch.getName());
                    oldOne.setAddress(branch.getAddress());
                    oldOne.setTel(branch.getTel());
                    oldOne.setDivision(this);
                    oldOne.save();
                    branches.remove(String.valueOf(oldOne.getLiveId()));
                }
            }

            for (String liveId : branches.keySet()) {
                Branch deletedOne = branches.get(liveId);
                deletedOne.delete();
            }
        }
        if (this.getId() == null)
            super.save();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeTypedList(this.getBranches());
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
