package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import co.uk.rushorm.core.annotations.RushList;

public class Group extends BaseEntity implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Group createFromParcel(Parcel source) {
            return new Group(source);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };
    private static final String TAG = Group.class.getSimpleName();
    @SerializedName("liveId")
    protected long liveId;
    @SerializedName("sort_index")
    private int sortIndex;
    @SerializedName("name")
    private String name;
    @SerializedName("questions")
    @RushList(classType = Question.class)
    private List<Question> questions;

    public Group() {
    }

    public Group(Parcel in) {
        this.sortIndex = in.readInt();
        this.name = in.readString();
        this.questions = new ArrayList<Question>();
        in.readTypedList(this.questions, Media.CREATOR);
    }

    public String getName() {
        if (this.name != null)
            return name;
        return "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Question> getQuestions() {
        List<Question> list = this.loadQuestions();
        Collections.sort(list,
                new Comparator<Question>() {
                    public int compare(final Question a, final Question d) {
                        return (a.getContent().compareTo(d.getContent()));
                    }
                });
        return list;
    }

    private List<Question> loadQuestions() {
        if (this.questions != null)
            return questions;
        return new ArrayList<Question>();
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + this.getLiveId() +
                ", sortIndex=" + sortIndex +
                ", name='" + name + '\'' +
                ", questions=" + questions +
                '}';
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return "group";
    }

    @Override
    public void onSave() {

    }

    /**
     * add question result
     *
     * @param result
     */
    public boolean addQuestionResult(Result result) {
        for (Question question : this.getQuestions()) {
            if (question.getLiveId() == result.getQuestion().getLiveId()) {
                question.setResult(result);
                return true;
            }
        }
        return false;
    }

    /**
     * add question note
     *
     * @param note
     */
    public boolean addQuestionNote(Note note) {
        for (Question question : this.getQuestions()) {
            if (question.getLiveId() == note.getQuestion().getLiveId()) {
                question.setNote(note);
                return true;
            }
        }
        return false;
    }

    /**
     * add medias
     *
     * @param note
     * @param items
     * @return
     */
    public boolean addMedias(Note note, List<Media> items) {
        boolean isResult = false;
        for (Question question : this.questions) {
            if (question.getLiveId() == note.getQuestion().getLiveId()) {
                Log.e(TAG, "add medias");
                question.getNote().addMedia(items);
                isResult = true;
                break;
            }
        }
        return isResult;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sortIndex);
        dest.writeString(this.name);
        dest.writeTypedList(this.getQuestions());
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
