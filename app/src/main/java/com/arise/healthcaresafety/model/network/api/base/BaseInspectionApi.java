package com.arise.healthcaresafety.model.network.api.base;

import com.android.volley.Request;
import com.android.volley.Response;
import com.arise.healthcaresafety.model.entities.BaseEntity;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Nam on 7/1/2015.
 */
public class BaseInspectionApi {
    public static final String SERVER = "http://sotgapp.aevitasgroup.com";
    private static final String TAG = BaseInspectionApi.class.getSimpleName();

    public Request createGetMethod(String url,
                                   Class<?> clazz,
                                   Response.Listener listener,
                                   Response.ErrorListener errorListener) {
        return new GetRequest(url, clazz, wsseHeader(), listener, errorListener);
    }


    public Request createPostMethod(String url,
                                    Class<?> clazz,
                                    Response.Listener listener,
                                    Response.ErrorListener errorListener,
                                    Map<String, String> params) {
        return new CustomMultiPartRequest<>(
                url,
                clazz,
                wsseHeader(),
                listener,
                errorListener,
                params);
    }

/*    public Request createPostMethod(Context context,
                                    String url,
                                    Class<?> clazz,
                                    Response.Listener listener,
                                    Response.ErrorListener errorListener,
                                    IPostData postData) {
        return new CustomMultiPartRequest<>(
                url,
                clazz,
                wsseHeader(context),
                listener,
                errorListener,
                postData);
    }*/

    //todo add post method for upload media
    public Request createPostMethod(String url,
                                    Class<?> clazz,
                                    Response.Listener listener,
                                    Response.ErrorListener errorListener,
                                    File file,
                                    Map<String, String> params) {
        return new CustomMultiPartRequest<>(url,
                clazz, wsseHeader(), listener, errorListener, file, params);
    }

    public Request createDeleteMethod(String url,
                                      Class<?> clazz,
                                      Response.Listener listener,
                                      Response.ErrorListener errorListener) {
        return new DeleteRequest(url, clazz, wsseHeader(), listener, errorListener);
    }

    private Map<String, String> wsseHeader() {
        WebServicesSecurity wsse = new WebServicesSecurity();
        Map<String, String> headers = new HashMap<>();
        headers.put(WebServicesSecurity.HEADER_AUTHORIZATION, wsse.getAuthorizationHeader());
        headers.put(WebServicesSecurity.HEADER_WSSE, wsse.getWsseHeader());
        /*Set<Map.Entry<String, String>> set = headers.entrySet();
        for (Map.Entry<String, String> a : set) {
            Log.e(TAG, a.getKey() + a.getValue());
        }*/
        return headers;
    }

    private Map<String, String> parseBaseEntity(BaseEntity baseEntity) {
        Map<String, String> params = new HashMap<>();
        List<String> fields = baseEntity.getFields();
        String kind = baseEntity.getKind();
        for (String field : fields) {
            if (baseEntity.getValue(field) instanceof List) {
                List<Object> a = (List<Object>) baseEntity.getValue(field);
                int size = a.size();
                for (int i = 0; i < size; i++) {
                    params.put(createKeyArray(kind, field, i), String.valueOf(a.get(i)));
                }
            } else {
                params.put(createKey(baseEntity.getKind(), field),
                        createValue(baseEntity, field));
            }
        }
        return params;
    }

    private String createKey(String kind, String field) {
        return kind + "[" + field + "]";
    }

    private String createKeyArray(String kind, String field, int index) {
        return kind + "[" + field + "][" + String.valueOf(index) + "]";
    }

    private String createValue(BaseEntity baseEntity, String field) {
        return String.valueOf(baseEntity.getValue(field));
    }

}
