package com.arise.healthcaresafety.model.listener;

import com.arise.healthcaresafety.model.entities.LoginResult;

/**
 * Created by Nam on 7/1/2015.
 */
public interface OnSplashListener {

    public void onLoginResult(LoginResult result);

    public void onLoginError(String msg);
}
