package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import co.uk.rushorm.core.annotations.RushIgnore;
import co.uk.rushorm.core.annotations.RushList;

public class Result extends BaseEntity implements Parcelable {

    public static final int RESULT_CHECKLIST_CHOICE = 0;
    public static final int RESULT_CHECKLIST_TEXT = 1;
    public static final int RESULT_EQUIPMENT = 2;
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
    private static final String TAG = Result.class.getSimpleName();
    @SerializedName("liveId")
    protected long liveId;
    @SerializedName("textentry")
    private String textEntry;
    @RushList(classType = Attribute.class)
    private List<Attribute> attributes;
    private Question question;
    private Equipment equipment;
    private boolean deleted = false;
    private boolean synced = false;
    private int questionId;
    @RushIgnore
    private Case inspectionCase;
    private String attributeTextList;
    @RushIgnore
    @SerializedName("error")
    private String error;
    /*todo 8/2/2015: offline mode*/
    private transient long caseId; //local case id

    public Result() {
    }

    public Result(Question quesiton) {
        this.question = quesiton;
    }

    public Result(Equipment equipment) {
        this.equipment = equipment;
    }

    public Result(Parcel in) {
        this.textEntry = in.readString();
        this.question = in.readParcelable(Question.class.getClassLoader());
        this.attributes = new ArrayList<Attribute>();
        in.readTypedList(this.attributes, Media.CREATOR);
        this.setLiveId(in.readLong());
    }

    public String getAttributeTextList() {
        return attributeTextList;
    }

    public void setAttributeTextList(String attributeTextList) {
        this.attributeTextList = attributeTextList;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return "result";
    }

    @Override
    public void onSave() {

    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<Attribute> getAttributes() {
        if (this.attributes != null)
            return attributes;
        return new ArrayList<Attribute>();
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getTextEntry() {
        return textEntry;
    }

    public void setTextEntry(String textEntry) {
        this.textEntry = textEntry;
    }

    public long getCaseId() {
        return caseId;
    }

    public void setCaseId(long caseId) {
        this.caseId = caseId;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + this.getLiveId() +
                ", question=" + question +
                ", textEntry='" + textEntry + '\'' +
                ", attributes=" + attributes +
                ", caseId=" + caseId +
                '}';
    }

    /*get post data */
    public Map<String, String> getQuestionPostData() {
        Map<String, String> result = new HashMap<>();
        result.put(super.createKey(getKind(), "question"), String.valueOf(this.question));
        int index = 0;
        if (this.getAttributes().size() > 0)
            for (Attribute a : this.getAttributes()) {
                result.put(super.createKey(getKind(), "attributes") + "[" + index + "]", String.valueOf(a.getLiveId()));
                index++;
            }
        return result;
    }

    /**
     * get data for post equipment
     *
     * @return
     */
    private Map<String, String> getEquipmentPostData() {
        Map<String, String> result = new HashMap<>();
        result.put(super.createKey(getKind(), "attributes") + "[0]", String.valueOf(this.equipment.getLiveId()));
        return result;
    }

    /**
     * get post data for text entry
     *
     * @return
     */
    private Map<String, String> getTextEntryPostData() {
        Map<String, String> result = new HashMap<>();
        result.put(super.createKey(getKind(), "question"), String.valueOf(this.question.getId()));
        result.put(super.createKey(getKind(), "textentry"), String.valueOf(this.textEntry));
        Set<Map.Entry<String, String>> set = result.entrySet();
        for (Map.Entry<String, String> a : set) {
            Log.e(TAG, a.getKey() + a.getValue());
        }
        return result;
    }

    public Map<String, String> getPostData() {
        int type = getKindOfResult();
        if (this.getEquipment() != null) {
            return getEquipmentPostData();
        }

        if (this.getQuestion() != null) {
            return getQuestionPostData();
        }

        if (type == RESULT_CHECKLIST_TEXT) {
            return getTextEntryPostData();
        }

        return new HashMap<>();
    }

    public Map<String, RequestBody> getRetrofitDataPost() {
        Map<String, RequestBody> params = new HashMap<>();

        int type = getKindOfResult();
        if (this.getEquipment() != null) {
            RequestBody attr = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.equipment.getLiveId()));
            params.put(super.createKey(getKind(), "attributes") + "[0]", attr);
        }

        if (this.getQuestion() != null) {
            RequestBody question = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.question.getLiveId()));

            params.put(super.createKey(getKind(), "question"), question);
            int index = 0;
            if (this.getAttributes().size() > 0)
                for (Attribute a : this.getAttributes()) {
                    RequestBody attr = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(a.getLiveId()));
                    params.put(super.createKey(getKind(), "attributes") + "[" + index + "]", attr);
                    index++;
                }
        }

        if (type == RESULT_CHECKLIST_TEXT) {
            RequestBody question = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.question.getLiveId()));
            RequestBody entry = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.textEntry));

            params.put(super.createKey(getKind(), "question"), question);
            params.put(super.createKey(getKind(), "textentry"), entry);
        }
        return params;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    /**
     * add check list attribute
     *
     * @param attr
     */
    public void addAttribute(Attribute attr) {
        List<Attribute> attrs = this.getAttributes();
        if (this.question != null && this.question.getVariation().getType() == Variation.SINGLE_CHOICE) {
            this.getAttributes().clear();
        }

        attrs.add(attr);
        this.attributes = attrs;
    }

    /**
     * add equipement attribute to result
     *
     * @param attribute
     */
    public void addEquipmentAttribute(Attribute attribute) {
        List<Attribute> attrs = this.getAttributes();
        if (!attrs.contains(attribute)) {
            attrs.add(attribute);
            this.attributes = attrs;
            this.setSynced(false);
        }
    }

    public void removeAttribute(Attribute attr) {
        List<Attribute> attrs = this.getAttributes();
        attrs.remove(attr);
        this.attributes = attrs;
    }

    /**
     * check attr is selected
     *
     * @param attr
     * @return
     */
    public boolean isSelected(Attribute attr) {
        if (this.attributes == null) {
            return false;
        }
        boolean result = false;
        for (Attribute a : this.attributes) {
            if (a.getLiveId() == attr.getLiveId()) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * get kind of result
     *
     * @return
     */
    private int getKindOfResult() {
        if (this.question == null) {
            return RESULT_EQUIPMENT;
        }

        if (this.question.getVariation() == null) {
            return RESULT_CHECKLIST_TEXT;
        }

        return RESULT_CHECKLIST_CHOICE;
    }

    /**
     * get json string of attributes
     *
     * @return
     */
    public String toJsonStringAttributes() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(this.attributes);
    }

    public String toJsonStringQuestion() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(this.question);
    }

    /**
     * @param object: local object
     */
    public void storeId(Object object) {
        if (!(object instanceof Result)) {
            return;
        }
        Result i = (Result) object;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.textEntry);
        dest.writeParcelable(this.question, flags);
        dest.writeTypedList(this.getAttributes());
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean updateResultEquipment(Result result) {
        if (result.getEquipment() == null || this.getEquipment() == null)
            return false;
        if (result.getEquipment().getLiveId() == this.getEquipment().getLiveId()) {
            if (!result.getEquipment().isChecked())
                this.setDeleted(true);
            this.setEquipment(result.getEquipment());
            this.setSynced(false);
            return true;
        }
        return false;
    }

    public boolean updateResultQuestion(Result result) {
        if (result.getQuestion() == null || this.getQuestion() == null)
            return false;
        if (result.getQuestion().getLiveId() == this.getQuestion().getLiveId()) {
            if (result.getAttributes().size() == 0)
                this.setDeleted(true);
            else {
                this.setAttributes(result.getAttributes());
                this.setSynced(false);
                return true;
            }
        }
        return false;
    }

    public Case getInspectionCase() {
        return inspectionCase;
    }

    public void setInspectionCase(Case inspectionCase) {
        this.inspectionCase = inspectionCase;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
