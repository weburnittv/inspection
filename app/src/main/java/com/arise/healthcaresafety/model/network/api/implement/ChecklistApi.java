package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.CheckList;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by user on 1/10/16.
 */
public interface ChecklistApi {

    @GET("/api/apps/inspection/checklist/{typeId}")
    Call<CheckList> getChecklist(@Path(value = "typeId", encoded = true) long typeId);
}
