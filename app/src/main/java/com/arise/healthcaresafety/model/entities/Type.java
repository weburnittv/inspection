package com.arise.healthcaresafety.model.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import co.uk.rushorm.core.RushSearch;

public class Type extends BaseEntity implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Type createFromParcel(Parcel source) {
            return new Type(source);
        }

        @Override
        public Type[] newArray(int size) {
            return new Type[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    private String name;
    private long categoryId;
    private Category category;

    public Type() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        if (this.category == null)
            return this.category = new RushSearch().whereEqual("liveId", this.categoryId).findSingle(Category.class);
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
        this.categoryId = category.getLiveId();
    }

    @Override
    public String toString() {
        return "Type{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", category=" + category +
                '}';
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }

    public Type(Parcel in) {
        this.name = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.setLiveId(in.readLong());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeParcelable(this.category, flags);
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
