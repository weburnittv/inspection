package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import co.uk.rushorm.core.annotations.RushList;

public class Variation extends BaseEntity implements Parcelable {
    public static final int SINGLE_CHOICE = 0;
    public static final int MULTI_CHOICE = 1;
    public static final int NOTE_TEXT = 2;
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Variation createFromParcel(Parcel source) {
            return new Variation(source);
        }

        @Override
        public Variation[] newArray(int size) {
            return new Variation[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    private String name;
    private int type;
    @RushList(classType = Attribute.class)
    private List<Attribute> attributes;

    public Variation() {
    }

    public Variation(Parcel in) {
        this.name = in.readString();
        this.type = in.readInt();
        this.attributes = new ArrayList<Attribute>();
        in.readTypedList(this.attributes, Media.CREATOR);
        this.setLiveId(in.readLong());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Attribute> getAttributes() {
        List<Attribute> list = this.loadAttributes();
        Collections.sort(list, new Comparator<Attribute>() {
            @Override
            public int compare(Attribute lhs, Attribute rhs) {
                return (lhs.getName().compareTo(rhs.getName()));
            }
        });
        return list;
    }

    private List<Attribute> loadAttributes() {
        if (this.attributes != null)
            return this.attributes;
        return new ArrayList<Attribute>();
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "Variation{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", attributes=" + attributes +
                '}';
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.type);
        dest.writeTypedList(this.getAttributes());
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
