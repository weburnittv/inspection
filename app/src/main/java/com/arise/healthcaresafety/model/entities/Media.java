package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.arise.healthcaresafety.model.network.api.base.BaseInspectionApi;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushIgnore;

/*{
    "id":2,
        "file":"mediafile/media/JPEG_20150719_150210_-1022170518.jpg",
        or "file": "mediafile\/media\/\/JPEG_20150728_214700_1510299339.jpg"
        "created_at":"2015-07-19T20:58:05+0700",
        "kind":"image"
}*/
public class Media extends BaseEntity implements Parcelable {
    public static final String MEDIA_PHOTO = "image";
    public static final String MEDIA_SOUND = "sound";
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Media createFromParcel(Parcel source) {
            return new Media(source);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    private boolean deleted = false;
    private boolean synced = false;
    @SerializedName("file")
    private String file;
    @SerializedName("create_at")
    private String createdDate;
    @SerializedName("kind")
    private String kind;
    private long noteId;
    @RushIgnore
    private Note note;
    private String localPath;
    private transient File uploadFile;

    public Media() {
    }

    public Media(int id, String file, String createdDate, String kind) {
        this.setLiveId(new Long(Integer.toString(id)));
        this.file = file;
        this.createdDate = createdDate;
        this.kind = kind;
    }

    public Media(int noteId, File uploadFile, String kind) {
        this.noteId = noteId;
        this.uploadFile = uploadFile;
        this.kind = kind;
    }

    public Media(String localPath, String kind) {
        this.localPath = localPath;
        this.kind = kind;
    }

    public Media(String localPath, String kind, int noteId) {
        this.localPath = localPath;
        this.kind = kind;
        this.noteId = noteId;
    }

    private Media(Parcel in) {
        this.setLiveId(in.readLong());
        this.createdDate = in.readString();
        this.localPath = in.readString();
        this.kind = in.readString();
    }

    public static List<Media> listAll() {
        return new RushSearch().find(Media.class);
    }

    public static List<Media> getMediaByNoteId(long noteId) {
        return new RushSearch().whereEqual("liveId", String.valueOf(noteId)).find(Media.class);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    public String getKind() {
        if (this.getUploadFile().getAbsolutePath().endsWith(".aac")) {
            return MEDIA_SOUND;
        } else {
            return MEDIA_PHOTO;
        }
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public long getNoteId() {
        return noteId;
    }

    public void setNoteId(long noteId) {
        this.noteId = noteId;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public File getUploadFile() {
        if (this.uploadFile instanceof File)
            return uploadFile;
        else return new File(this.localPath);
    }

    public void setUploadFile(File uploadFile) {
        this.uploadFile = uploadFile != null ? uploadFile : new File(localPath);
    }

    @Override
    public String toString() {
        return "Media{" +
                "id=" + this.getLiveId() +
                ", file='" + file + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", kind='" + kind + '\'' +
                ", localPath='" + localPath + '\'' +
                ", noteId=" + noteId +
                '}';
    }

    /**
     * get full qualified name of link
     *
     * @return
     */
    public String getUrlLink() {
        return BaseInspectionApi.SERVER + "/" + getFile();
    }

    /**
     * get name from link
     *
     * @return
     */
    public String getServerName() {
        String tamp = this.file.replace("\\", "");
        String[] a = tamp.split("/");
        if (a.length > 0) {
            Log.d("qqq", "" + a[a.length - 1]);
            return a[a.length - 1];
        } else {
            return "unknown name";
        }
    }

    /**
     * get name from local path
     *
     * @return
     */
    public String getLocalName() {
        String[] a = this.localPath.split("/");
        if (a.length > 0) {
            return a[a.length - 1];
        } else {
            return "unknown name";
        }
    }

    public Map<String, String> getPostData(long liveId) {
        Map<String, String> params = new HashMap<>();
        params.put("media[note]", String.valueOf(liveId));
        return params;
    }

    public void storeLiveId() {

    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    @Override
    public void onSave() {
        List<Media> old = listAll();
        HashMap<String, Media> medias = new HashMap<>();

        for (Media oldOne : old) {
            medias.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }
        Media oldOne = medias.get(String.valueOf(getLiveId()));
        if (oldOne == null)
            save();
        else {
            oldOne.setFile(file);
            oldOne.setCreatedDate(createdDate);
            oldOne.setKind(kind);
            oldOne.setLocalPath(localPath);
            oldOne.setNoteId(noteId);
            oldOne.save();
            medias.remove(String.valueOf(oldOne.getLiveId()));
        }

        for (String liveId : medias.keySet()) {
            Media deletedOne = medias.get(liveId);
            deletedOne.delete();
        }
        super.save();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.getLiveId());
        //dest.writeString(this.file);
        dest.writeString(this.createdDate);
        dest.writeString(this.localPath);
        dest.writeString(this.kind);
    }

    public Note getNote() {
        if (this.note == null)
            return this.note = new Note();
        return this.note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
