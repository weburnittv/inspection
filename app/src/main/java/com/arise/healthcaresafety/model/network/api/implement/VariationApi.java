package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Variations;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by user on 1/10/16.
 */
public interface VariationApi {
    @GET("/api/apps/variations.json")
    Call<Variations> getVariations();
}
