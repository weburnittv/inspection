package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushList;

public class Category extends BaseEntity implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
    @SerializedName("liveId")
    protected long liveId;
    private String name;
    @RushList(classType = Type.class)
    private List<Type> types;
    private Statistic statistic;

    public Category() {
    }

    public Category(Parcel in) {
        this.name = in.readString();
        this.types = new ArrayList<Type>();
        in.readTypedList(this.types, Media.CREATOR);
        this.setLiveId(in.readLong());
    }

    public static List<Category> listAll() {
        List<Category> list = new RushSearch().find(Category.class);
        return list;
    }

    public static List<Type> getTypes(long categoryId) {
        List<Type> list = new RushSearch().whereEqual("liveId", String.valueOf(categoryId)).find(Type.class);
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Type> getTypes() {
        if (this.types != null) return types;
        return new ArrayList<Type>();
//        return new RushSearch().whereEqual("categoryId", this.getLiveId()).find(Type.class);
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", types=" + types +
                '}';
    }

    public List<String> loadTypeNameList() {
        List<String> result = new ArrayList<>();

        if (this.getTypes() == null) {
            return result;
        }

        for (Type item : this.getTypes()) {
            result.add(item.getName());
        }
        return result;
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }

    @Override
    public void save() {
        List<Type> old = Category.getTypes(this.liveId);
        HashMap<String, Type> types = new HashMap<>();

        for (Type oldOne : old) {
            types.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }
        if (this.types != null) {
            for (Type type : this.getTypes()) {
                type.setCategory(this);
                Type oldOne = types.get(String.valueOf(type.getLiveId()));
                if (oldOne == null)
                    type.save();
                else {
                    oldOne.setName(type.getName());
                    oldOne.setCategory(this);
                    oldOne.save();
                    types.remove(String.valueOf(oldOne.getLiveId()));
                }
            }

            for (String liveId : types.keySet()) {
                Type deletedOne = types.get(liveId);
                deletedOne.delete();
            }
        }
        super.save();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeTypedList(this.getTypes());
        dest.writeLong(this.getLiveId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void setStatistic(Statistic data) {
        this.statistic = data;
    }
}
