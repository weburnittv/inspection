package com.arise.healthcaresafety.model.network.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.arise.healthcaresafety.model.network.api.base.BaseInspectionApi;
import com.arise.healthcaresafety.model.network.volley.MyVolley;

/**
 * Created by hnam on 8/12/2015.
 */
public class ReferencesApi extends BaseInspectionApi {
    //GET('/api/apps/references')
    private static final String REFERENCES_API = BaseInspectionApi.SERVER + "/api/apps/references.json";
    private Context context;

    public ReferencesApi(Context context) {
        this.context = context;
    }

    public void getReferencesApi(Response.Listener<String[]> listener,
                                 Response.ErrorListener errorListener) {
        Request<String[]> request = super.createGetMethod(
                REFERENCES_API,
                String[].class,
                listener,
                errorListener);
        MyVolley.getInstance(context).addToRequestQueue(request);
    }
}
