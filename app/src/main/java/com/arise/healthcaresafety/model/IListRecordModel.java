package com.arise.healthcaresafety.model;

import com.arise.healthcaresafety.model.entities.Media;

/**
 * Created by Nam on 7/26/2015.
 */
public interface IListRecordModel {
    public void upload(Media media);
}
