package com.arise.healthcaresafety.model.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import com.google.gson.annotations.SerializedName;

import co.uk.rushorm.core.annotations.RushIgnore;

public class Question extends BaseEntity implements Parcelable {
    public static final int SINGLE = 0;
    public static final int MULTI = 1;
    public static final int NOTE = 2;
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
    @RushIgnore
    public transient int numberImage = 0;
    @RushIgnore
    public transient int numberRecord = 0;
    @SerializedName("liveId")
    protected long liveId;
    private String content;
    private Variation variation;
    @RushIgnore
    private transient View childView;
    @RushIgnore
    private transient Result result;
    @RushIgnore
    private transient Note note;

    public Question() {
    }

    public Question(Parcel in) {
        this.content = in.readString();
        this.variation = in.readParcelable(Variation.class.getClassLoader());
        this.setLiveId(in.readLong());
    }

    public String getContent() {
        if (this.content != null)
            return content;
        return "";
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Variation getVariation() {
        return variation;
    }

    public void setVariation(Variation variation) {
        this.variation = variation;
    }

    public View getChildView() {
        return childView;
    }

    public void setChildView(View childView) {
        this.childView = childView;
    }

    public Result getResult() {
        if (this.result == null)
            this.result = new Result(this);
        return this.result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public void addAttribute(Attribute attr) {
        this.getResult().addAttribute(attr);
    }

    public void removeAttribute(Attribute attr) {
        this.getResult().removeAttribute(attr);
    }

    public Note getNote() {
        if (this.note == null)
            this.note = new Note(this);
        return this.note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public int getNumberImage() {
        return numberImage;
    }

    public void setNumberImage(int numberImage) {
        this.numberImage = numberImage;
    }

    public int getNumberRecord() {
        return numberRecord;
    }

    public void setNumberRecord(int numberRecord) {
        this.numberRecord = numberRecord;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + this.getLiveId() +
                ", content='" + content + '\'' +
                ", variation=" + variation +
                '}';
    }

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return null;
    }

    @Override
    public void onSave() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.liveId);
        dest.writeParcelable(this.variation, flags);
        dest.writeString(this.content);
    }
}
