package com.arise.healthcaresafety.model;

import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by cuongnv on 25/10/15.
 * Using to manage layout for sound view in preview offline
 */
public class ContainerMedia {
    LinearLayout linearLayout;
    String path;
    ImageView imageView;

    public ContainerMedia(LinearLayout linearLayout, String path, ImageView imageView) {
        this.linearLayout = linearLayout;
        this.path = path;
        this.imageView = imageView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public LinearLayout getLinearLayout() {
        return linearLayout;
    }

    public void setLinearLayout(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
