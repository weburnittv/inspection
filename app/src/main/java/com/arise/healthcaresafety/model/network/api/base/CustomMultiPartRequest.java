package com.arise.healthcaresafety.model.network.api.base;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hnam on 7/1/2015.
 */
public class CustomMultiPartRequest<T> extends Request<T> {
    private static final String TAG = CustomMultiPartRequest.class.getSimpleName();
    private static final String FILE_PART_NAME = "media[media_file]";
    private final Response.Listener<T> mListener;
    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private final Map<String, String> params;
    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpEntity;
    private File file;
    private Map<String, String> headers;

    /**
     * @param url
     * @param clazz
     * @param listener
     * @param errorListener
     * @param file
     * @param params
     */
    public CustomMultiPartRequest(String url,
                                  Class<T> clazz,
                                  Map<String, String> headers,
                                  Response.Listener<T> listener,
                                  Response.ErrorListener errorListener,
                                  File file,
                                  Map<String, String> params) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.clazz = clazz;
        this.headers = headers;
        this.file = file;
        this.params = params;
        buildMultipartEntity();
    }

    /**
     * @param url
     * @param clazz
     * @param listener
     * @param errorListener
     * @param params
     */
    public CustomMultiPartRequest(String url,
                                  Class<T> clazz,
                                  Map<String, String> headers,
                                  Response.Listener<T> listener,
                                  Response.ErrorListener errorListener,
                                  Map<String, String> params) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.clazz = clazz;
        this.headers = headers;
        this.file = null;
        this.params = params;
        buildMultipartEntity();
    }

    public CustomMultiPartRequest(String url,
                                  Class<T> clazz,
                                  Response.Listener<T> listener,
                                  Response.ErrorListener errorListener,
                                  Map<String, String> params) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.clazz = clazz;
        this.file = null;
        this.params = params;
        buildMultipartEntity();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    //build multipart entity without file
    private void buildMultipartEntity() {
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (this.file != null) {
            entity.addPart(FILE_PART_NAME, new FileBody(file));
        }
        if (params != null) {
            for (HashMap.Entry<String, String> entry : params.entrySet()) {
                Log.e(TAG, entry.getKey() + entry.getValue());
                entity.addTextBody(entry.getKey(), entry.getValue());
            }
        }
        httpEntity = entity.build();
    }

    @Override
    public String getBodyContentType() {
        return httpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    /**
     * copied from Android StringRequest class
     */
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String json;
        try {
            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            json = new String(response.data);
        }
        Log.e(TAG, "Status Code: " + String.valueOf(response.statusCode));
        Log.e(TAG, "Data: " + new String(response.data));
        return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

}
