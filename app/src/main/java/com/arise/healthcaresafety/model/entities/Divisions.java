package com.arise.healthcaresafety.model.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Divisions {
    private List<Division> divisions;

    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
        this.save();
    }

    @Override
    public String toString() {
        return "Divisions{" +
                "divisions=" + divisions +
                '}';
    }

    /**
     * get name  of divisions
     *
     * @return
     */
    public List<String> loadDivisionNameList() {
        List<String> nameList = new ArrayList<>();
        if (divisions == null) {
            return nameList;
        }

        for (Division item : this.divisions) {
            nameList.add(item.getName());
        }
        return nameList;
    }

    public Division getDivisionByIndex(int index) {
        if (this.divisions == null) {
            return null;
        }
        if (index > this.getDivisions().size()) {
            return null;
        }

        return this.divisions.get(index);
    }

    public void save() {
        List<Division> old = Division.listAll();
        HashMap<String, Division> divisions = new HashMap<>();
        for (Division oldOne : old) {
            divisions.put(String.valueOf(oldOne.getLiveId()), oldOne);
        }
        for (Division division : this.divisions) {
            Division oldOne = divisions.get(String.valueOf(division.getLiveId()));
            if (oldOne == null)
                division.save();
            else {
                oldOne.setName(division.getName());
                oldOne.setBranches(division.getBranches());
                oldOne.save();
                divisions.remove(String.valueOf(oldOne.getLiveId()));
            }
        }
        for (String liveId : divisions.keySet()) {
            Division division = divisions.get(liveId);
            division.delete();
        }
    }

    public void loadDivisions() {
        this.divisions = Division.listAll();
    }

}
