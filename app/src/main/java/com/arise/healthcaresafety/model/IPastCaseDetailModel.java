package com.arise.healthcaresafety.model;

import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Type;

/**
 * Created by Nam on 7/9/2015.
 */
public interface IPastCaseDetailModel {
    public void loadCaseDetail(int caseId);

    public void loadEquipments(Branch branch);

    public void loadCheckList(Type type);

    public void sendEmail(int caseId, String email);
}
