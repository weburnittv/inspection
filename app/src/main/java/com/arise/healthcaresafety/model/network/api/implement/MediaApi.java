package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Media;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Path;

/**
 * Created by user on 1/6/16.
 */
public interface MediaApi {
    @Multipart
    @POST("/api/apps/inspection/upload/{note}/{type}")
    Call<Media> addNewMedia(@Path(value = "note", encoded = true) long noteId, @Path(value = "type", encoded = true) String type, @PartMap Map<String, RequestBody> file);

    @POST("/api/apps/inspection/update/media/{id}")
    Call<Media> updateMedia(@Path(value = "id", encoded = true) long liveId, @PartMap Map<String, RequestBody> file);

    @DELETE("/api/apps/inspection/delete/media/{id}")
    Call<Media> deleteMedia(@Path(value = "id", encoded = true) long liveId);
}
