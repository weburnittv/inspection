package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Equipments;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by user on 1/11/16.
 */
public interface EquipmentApi {
    @GET("/api/apps/equipments/{id}")
    Call<Equipments> getEquipments(@Path(value = "id", encoded = true) long branchId);
}
