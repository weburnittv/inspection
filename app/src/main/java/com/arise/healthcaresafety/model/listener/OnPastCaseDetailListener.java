package com.arise.healthcaresafety.model.listener;

import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.CheckList;
import com.arise.healthcaresafety.model.entities.Equipments;

/**
 * Created by Nam on 7/9/2015.
 */
public interface OnPastCaseDetailListener {
    void onLoadPastCaseDetail(Case c);

    void onLoadEquipments(Equipments equipments);

    void onLoadCheckList(CheckList checkList);

    void onSendEmailStatus(boolean status);
}
