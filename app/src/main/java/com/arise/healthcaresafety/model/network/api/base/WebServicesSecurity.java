package com.arise.healthcaresafety.model.network.api.base;

/**
 * Created by TQK666 on 4/21/2015.
 */

import android.util.Base64;
import android.util.Log;

import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.entities.LoginResult;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class WebServicesSecurity {

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_WSSE = "X-WSSE";

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);

    //private LoginResult.User user;
    private String nonce;
    private String createdAt;
    //private String userName;

    public WebServicesSecurity() {
        //we need the user object because we need his username
        //this.user = InspectionApp.getInstance().getUser();
        //this.userName = InspectionApp.getInstance().getUsername();
        this.createdAt = generateTimestamp();
        this.nonce = generateNonce();
    }

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private String generateNonce() {
        SecureRandom random = new SecureRandom();
        byte seed[] = random.generateSeed(10);
        return bytesToHex(seed);
    }

    private String generateTimestamp() {
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }

    private String generateDigest() {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            StringBuilder sb = new StringBuilder();
            sb.append(this.nonce);
            sb.append(this.createdAt);
            sb.append(InspectionApp.getInstance().getToken());
            byte sha[] = md.digest(sb.toString().getBytes());
            digest = Base64.encodeToString(sha, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return digest;
    }

    //HEADER_WSSE
    public String getWsseHeader() {
        StringBuilder header = new StringBuilder();
        header.append("UsernameToken Username=\"");
        header.append(InspectionApp.getInstance().getUsername());
        header.append("\", PasswordDigest=\"");
        header.append(this.generateDigest());
        header.append("\", Nonce=\"");
        header.append(Base64.encodeToString(this.nonce.getBytes(), Base64.NO_WRAP));
        header.append("\", Created=\"");
        header.append(this.createdAt);
        header.append("\"");
        Log.d("cuongnv", "xx: " + header.toString());
        return header.toString();
    }

    //HEADER_AUTHORIZATION
    public String getAuthorizationHeader() {
        return "WSSE profile=\"UsernameToken\"";
    }

    public LoginResult.User getUser() {
        return InspectionApp.getInstance().getUser();
    }


    public String getServerURL() {
        return InspectionApp.getInstance().getServerUrl();
    }
/*    public WebServicesSecurity setUser(LoginResult.User user) {
        //this.user = user;
        return this;
    }*/
}
