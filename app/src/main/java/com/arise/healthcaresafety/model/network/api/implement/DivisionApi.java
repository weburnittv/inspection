package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Divisions;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by user on 1/2/16.
 */
public interface DivisionApi {
    @GET("/api/apps/divisions")
    Call<Divisions> getDivisions();
}
