package com.arise.healthcaresafety.model.entities;


public class Status {
    private boolean status;

    public Status() {
    }

    public Status(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Status{" +
                "status=" + status +
                '}';
    }
}
