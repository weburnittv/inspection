package com.arise.healthcaresafety.model.entities;


public class ClientResult {
    private int mStatusCode;

    private Client mClient;

    public int getStatusCode() {
        return mStatusCode;
    }

    public void setStatusCode(int statusCode) {
        this.mStatusCode = statusCode;
    }

    public Client getClient() {
        return mClient;
    }

    public void setClient(Client client) {
        this.mClient = client;
    }

    @Override
    public String toString() {
        return "ClientResult{" +
                "status=" + mStatusCode +
                ", host=" + mClient +
                '}';
    }
}
