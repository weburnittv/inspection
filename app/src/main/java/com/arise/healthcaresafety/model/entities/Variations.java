package com.arise.healthcaresafety.model.entities;

import java.util.List;

/**
 * Created by user on 12/15/15.
 */
public class Variations {
    private List<Variation> variations;

    public List<Variation> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    public void save() {
        for (Variation variation : this.variations) {
            variation.save();
        }
    }
}
