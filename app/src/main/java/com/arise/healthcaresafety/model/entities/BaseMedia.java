package com.arise.healthcaresafety.model.entities;

import com.google.gson.annotations.SerializedName;

public abstract class BaseMedia extends BaseEntity {
    public static final String IMAGE = "image";
    public static final String SOUND = "media";


    @SerializedName("file")
    private String name;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("discr")
    private String type;

    @SerializedName("kind")
    private String kind;


    private transient String localPath;
    private transient Note note;

    public BaseMedia() {
    }

    public BaseMedia(String path) {
        this.localPath = path;
        this.note = null;
    }

    public BaseMedia(int id, String path, Note note) {
        this.setLiveId(new Long(Integer.toString(id)));
        this.localPath = path;
        this.note = note;
    }


    @Override
    public String getKind() {
        return "media";
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public String getPath() {
        return localPath;
    }

    public void setPath(String path) {
        this.localPath = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public abstract String getTypeOfMedia();

    @Override
    public String toString() {
        return "BaseMedia{" +
                "id=" + this.getLiveId() +
                ", name='" + name + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", type='" + type + '\'' +
                ", kind='" + kind + '\'' +
                ", localPath='" + localPath + '\'' +
                ", note=" + note +
                '}';
    }


    public String getFileNameOnLocal() {
        String[] tamp = getPath().split("/");
        if (tamp != null) {
            return tamp[tamp.length - 1];
        }
        return "unknown name";
    }


}
