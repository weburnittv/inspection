package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.entities.Categories;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by user on 1/2/16.
 */
public interface CategoryApi {
    @GET("/api/apps/categories")
    Call<Categories> getCategories();
}
