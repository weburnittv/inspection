package com.arise.healthcaresafety.model.network.api.implement;

import com.arise.healthcaresafety.model.network.api.base.WebServicesSecurity;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by user on 1/2/16.
 */
public class APIBuilder {
    private static OkHttpClient httpClient = new OkHttpClient();

    public static <S> S createService(Class<S> serviceClass, String server) {
        /* set your desired log level */
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(logging);  // add logging as interceptor

        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                WebServicesSecurity wsse = new WebServicesSecurity();
                Request.Builder requestBuilder = original.newBuilder()
                        .header("User-Agent", "SafetyApp")
                        .header(WebServicesSecurity.HEADER_AUTHORIZATION, wsse.getAuthorizationHeader())
                        .header(WebServicesSecurity.HEADER_WSSE, wsse.getWsseHeader());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(server)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
