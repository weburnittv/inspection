package com.arise.healthcaresafety.model.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hnam on 7/2/2015.
 */
public class InspectionType extends BaseEntity {
    @SerializedName("liveId")
    protected long liveId;

    @Override
    public long getLiveId() {
        return this.liveId;
    }

    @Override
    public void setLiveId(long liveId) {
        this.liveId = liveId;
    }

    @Override
    public String getKind() {
        return "inspectiontype";
    }

    @Override
    public void onSave() {

    }
}
