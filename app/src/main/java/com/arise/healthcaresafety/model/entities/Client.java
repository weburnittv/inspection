package com.arise.healthcaresafety.model.entities;

import com.google.gson.annotations.SerializedName;

public class Client {

    @SerializedName("host")
    private String mHost;

    @SerializedName("expire")
    private String mExpire;

    @SerializedName("logo")
    private String mLogo;

    @SerializedName("email")
    private String mEmail;

    public Client() {
    }

    public Client(String host, String expire, String logo, String email) {
        this.mHost = host;
        this.mExpire = expire;
        this.mEmail = email;
        this.mLogo = logo;
    }


    public String getHost() {
        return mHost;
    }

    public void setHost(String mHost) {
        this.mHost = mHost;
    }

    public String getExpire() {
        return mExpire;
    }

    public void setExpire(String mExpire) {
        this.mExpire = mExpire;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String mLogo) {
        this.mLogo = mLogo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    @Override
    public String toString() {
        return "Client{" +
                "host=" + mHost +
                "expire=" + mExpire +
                "logo=" + mLogo +
                "email=" + mEmail +
                '}';
    }
}
