package com.arise.healthcaresafety.model.network.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.arise.healthcaresafety.model.entities.LoginResult;
import com.arise.healthcaresafety.model.network.api.base.BaseInspectionApi;
import com.arise.healthcaresafety.model.network.api.base.CustomMultiPartRequest;
import com.arise.healthcaresafety.model.network.volley.MyVolley;

import java.util.HashMap;

;

/**
 * Created by Nam on 7/1/2015.
 */
public class LoginApi {
    private static final String LOGIN_API = BaseInspectionApi.SERVER + "/api/apps/login.json";
    private Context context;

    public LoginApi(Context context) {
        this.context = context;
    }

    public void login(String userName, String password,
                      Response.Listener<LoginResult> listener,
                      Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("_username", userName);
        params.put("_password", password);
        Request<LoginResult> loginRequest = new CustomMultiPartRequest<LoginResult>(
                LOGIN_API,
                LoginResult.class,
                listener,
                errorListener,
                params
        );
        MyVolley.getInstance(context).addToRequestQueue(loginRequest);
    }
}
