package com.arise.healthcaresafety;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

/**
 * Created by PHI LONG on 11/9/2015.
 */
public class ReadWebViewPDFActivity extends Activity {

    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view_acivity);
        Bundle data = getIntent().getExtras();
        if (data != null) {
            path = data.getString("PATH");
        }
        Log.e("qqq", "path: " + path);
        WebView webview = (WebView) findViewById(R.id.photoViewAct_webView);
        webview.getSettings().setJavaScriptEnabled(true);
        String pdf = "http://www.adobe.com/devnet/acrobat/pdfs/pdf_open_parameters.pdf";
        webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + path);
    }
}
