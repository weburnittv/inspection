package com.arise.healthcaresafety;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class PhotoViewActivity extends AppCompatActivity {
    private static final String TAG = PhotoViewActivity.class.getSimpleName();
    //private WebView webView;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getIntent().getExtras();
        if (data != null) {
            path = data.getString("PATH");
        }
        //path ="file://" + Environment.getExternalStorageDirectory().getAbsolutePath() + "/Inspection/soluong-bidao.docx";


        Log.e(TAG, "path: " + path);
        setContentView(R.layout.activity_photo_view_acivity);
        WebView webView = (WebView) findViewById(R.id.photoViewAct_webView);
        // Enable Javascript
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
/*        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);*/
        webView.loadUrl(path);
    }


}
