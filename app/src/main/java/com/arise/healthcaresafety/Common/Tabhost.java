package com.arise.healthcaresafety.Common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arise.healthcaresafety.R;

public class Tabhost extends LinearLayout implements View.OnClickListener {

    private View mView;
    private LayoutInflater mInflater;
    private TextView mTvBackgroundTabOne;
    private TextView mTvBackgroundTabTwo;
    private OnChangeEventClick mEventClick;
    private RelativeLayout mLayoutleft;
    private RelativeLayout mLayoutright;

    public Tabhost(Context context) {
        super(context);
    }

    public Tabhost(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Tabhost(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public Tabhost(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public void init(Context mContext) {
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = mInflater.inflate(R.layout.layout_tabhost, null);
        addView(mView);
        mTvBackgroundTabOne = (TextView) mView.findViewById(R.id.tvBackgroundOne);
        mTvBackgroundTabTwo = (TextView) mView.findViewById(R.id.tvBackgroundTwo);
        mLayoutleft = (RelativeLayout) mView.findViewById(R.id.rlSuccess);
        mLayoutright = (RelativeLayout) mView.findViewById(R.id.rlFail);

        setDefault();
        setValues();

    }

    private void setValues() {
        mLayoutleft.setOnClickListener(this);
        mLayoutright.setOnClickListener(this);
    }

    public void setDefault() {
        mTvBackgroundTabOne.setBackgroundColor(getResources().getColor(R.color.table_color_1));
        mTvBackgroundTabTwo.setBackgroundColor(getResources().getColor(R.color.table_color_1));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.rlSuccess) {
            clickTab(0);
        } else if (id == R.id.rlFail) {
            clickTab(1);
        }
    }

    public void clickTab(int position) {
        setDefault();
        mEventClick.onClick(position);
        if (position == 0) {
            mTvBackgroundTabOne.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            mTvBackgroundTabTwo.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    public void setEventClick(OnChangeEventClick l) {
        this.mEventClick = l;
    }


    public interface OnChangeEventClick {
        void onClick(int position);
    }
}
