package com.arise.healthcaresafety.Common;

import android.content.Context;

import com.google.android.gms.analytics.ExceptionParser;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * Created by PC on 11/1/2015.
 */
public class GoogleAnalyticsTracker {

    private static Tracker mTracker;
    private static GoogleAnalytics mGa;
    private Context mContext;

    public GoogleAnalyticsTracker(Context context, int resource) {
        mContext = context;
        mGa = GoogleAnalytics.getInstance(context);
        mTracker = getTracker(resource);

        Thread.setDefaultUncaughtExceptionHandler(new AnalyticsExceptionReporter(mTracker,
                Thread.getDefaultUncaughtExceptionHandler(), context));
    }

    synchronized Tracker getTracker(int xmlResource) {
        return mGa.newTracker(xmlResource);
    }

    public void sendScreenLabel(String screenLabel) {
        mTracker.setScreenName(screenLabel);
        mTracker.send(new HitBuilders.AppViewBuilder().build());
    }

    public void sendCustomDimension(int index, String value) {
        mTracker.send(new HitBuilders.AppViewBuilder().setCustomDimension(index, value).build());
    }

    //#region PRIVATE METHODS
    private String getCauseExceptionInfo(Throwable t) {
        String causeDescription = "";
        while (t != null && causeDescription.isEmpty()) {
            causeDescription = getExceptionInfo(t, "com.arise.healthcaresafety", false);
            t = t.getCause();
        }
        return causeDescription;
    }

    private String getExceptionInfo(Throwable t, String packageName, boolean includeExceptionName) {
        String exceptionName = "";
        String fileName = "";
        String lineNumber = "";

        for (StackTraceElement element : t.getStackTrace()) {
            String className = element.getClassName().toString().toLowerCase();
            if (packageName.isEmpty() || (!packageName.isEmpty() && className.contains(packageName))) {
                exceptionName = includeExceptionName ? t.toString() : "";
                fileName = element.getFileName();
                lineNumber = String.valueOf(element.getLineNumber());
                return exceptionName + "@" + fileName + ":" + lineNumber;
            }
        }
        return "";
    }

    private class AnalyticsExceptionReporter extends ExceptionReporter {

        public AnalyticsExceptionReporter(Tracker tracker, UncaughtExceptionHandler originalHandler, Context context) {
            super(tracker, originalHandler, context);
            setExceptionParser(new AnalyticsExceptionParser());
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            String exceptionDescription = getExceptionParser().getDescription(t.getName(), e);

            //Add code to store the exception stack trace in shared preferences

            super.uncaughtException(t, e);
        }
    }
//#endregion

    private class AnalyticsExceptionParser implements ExceptionParser {

        @Override
        public String getDescription(String arg0, Throwable arg1) {
            StringBuilder exceptionFirsLine = new StringBuilder();
            for (StackTraceElement element : arg1.getStackTrace()) {
                exceptionFirsLine.append(element.toString());
                break;
            }

            //150 Bytes is the maximum allowed by Analytics for custom dimensions values. Assumed that 1 Byte = 1 Character (UTF-8)
            String exceptionDescription = exceptionFirsLine.toString();
            if (exceptionDescription.length() > 150)
                exceptionDescription = exceptionDescription.substring(0, 149);

            return exceptionDescription;
        }
    }

    private class AnalyticsExceptionParsers implements ExceptionParser {

        @Override
        public String getDescription(String arg0, Throwable arg1) {
            String exceptionDescription = getExceptionInfo(arg1, "com.arise.healthcaresafety", true) + getCauseExceptionInfo(arg1.getCause());

            //150 Bytes is the maximum allowed by Analytics for custom dimensions values. Assumed that 1 Byte = 1 Character (UTF-8)
            if (exceptionDescription.length() > 150)
                exceptionDescription = exceptionDescription.substring(0, 150);

            return exceptionDescription;
        }
    }
}
