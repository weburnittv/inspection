package com.arise.healthcaresafety.model.network.api;

import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.entities.LoginResult;

import junit.framework.TestCase;

/**
 * Created by hnam on 8/27/2015.
 */
public class LoginApiTest extends InstrumentationTestCase {

    public void setUp() throws Exception {
        super.setUp();

    }

    public void tearDown() throws Exception {

    }

    public void testLogin() throws Exception {
        LoginApi api = new LoginApi(getInstrumentation().getTargetContext());
        api.login("admin", "admin", new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult loginResult) {
                InspectionApp.getInstance().setUser(loginResult.getUser());
                InspectionApp.getInstance().setUsername("admin");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        //getInstrumentation().waitForIdleSync();
        //wait for the server to process
        Thread.sleep(3000);
        assertEquals("admin", InspectionApp.getInstance().getUsername());

    }
}