package com.arise.healthcaresafety.handler;

import android.content.Context;

import com.arise.healthcaresafety.BuildConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by Nam on 8/28/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class DelegationTest {
    @Mock
    public Context context;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetInstance() throws Exception {
        org.junit.Assert.assertNotNull(Delegation.getInstance());
    }

    @Test
    public void testLoadDivisions() throws Exception {

    }

    @Test
    public void testLoadEquipments() throws Exception {

    }

    @Test
    public void testLoadCategories() throws Exception {

    }

    @Test
    public void testLoadChecklists() throws Exception {

    }
}