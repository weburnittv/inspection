package com.arise.healthcaresafety.sync;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.sync.entities.SyncData;
import com.arise.healthcaresafety.sync.entities.SyncMedia;
import com.arise.healthcaresafety.sync.entities.SyncNote;
import com.arise.healthcaresafety.sync.entities.SyncResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nam on 8/22/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SyncQueryTest {
    private static final String TAG = SyncQueryTest.class.getSimpleName();
    SyncQuery query;
    Context context;

    @Before
    public void setUp() throws Exception {
        context =  RuntimeEnvironment.application;
        query = new SyncQuery();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetFirstSyncData() throws Exception {
        SyncData data = query.getFirstSyncData(context);
        //test case
        Map<String, String> param1 = data.getCase().getPostData();
        for(HashMap.Entry<String, String> entry : param1.entrySet()){
            Log.e(TAG, entry.getKey() + "=="+ entry.getValue());
        }

        org.junit.Assert.assertEquals(4, data.getResults().size());
        for(SyncResult r : data.getResults()){
            Map<String, String> param = r.getPostData();
            for(HashMap.Entry<String, String> entry : param.entrySet()){
                Log.e(TAG, entry.getKey() + "==" + entry.getValue());
            }
        }

        //get post data of note
        org.junit.Assert.assertEquals(4, data.getNote().size());
        for(SyncNote n : data.getNote()){
            Map<String, String> param = n.getPostData(2);
            for(HashMap.Entry<String, String> entry : param.entrySet()){
                Log.e(TAG, entry.getKey() + "==" + entry.getValue());
            }
        }

        //set note live id
/*        org.junit.Assert.assertEquals(4, data.getSyncMediaList());
        for(SyncMedia m : data.getSyncMediaList()){
            Map<String, String> param = m.getPostData();
            for(HashMap.Entry<String, String> entry : param.entrySet()){
                Log.e(TAG, entry.getKey() + "==" + entry.getValue());
            }
        }*/
    }
}