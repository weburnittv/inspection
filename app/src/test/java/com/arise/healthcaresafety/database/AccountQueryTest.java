package com.arise.healthcaresafety.database;

import android.content.Context;

import com.arise.healthcaresafety.BuildConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

/**
 * Created by Nam on 8/19/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class AccountQueryTest {

    Context context;
    AccountQuery query;
    @Before
    public void setUp() throws Exception {
        this.context = RuntimeEnvironment.application;
        this.query = new AccountQuery(this.context);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCheckUsernameIsAvailable() throws Exception {
        //no existing
        Account x = new Account("admin", "admin");
        boolean b = query.checkUsername(x.getUsername());
        org.junit.Assert.assertFalse(b);
        //insert
        long index_1 = query.insertNewAccount("admin","admin");
        org.junit.Assert.assertEquals(1, index_1);
        //get all
        List<Account> list1 = query.getAllAccount();
        org.junit.Assert.assertEquals(1, list1.size());
        //check is existing
        boolean b2 = query.checkUsername("admin");
        org.junit.Assert.assertTrue(b2);

    }

}