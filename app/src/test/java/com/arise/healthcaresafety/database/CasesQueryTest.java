package com.arise.healthcaresafety.database;

import android.content.Context;

import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Type;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

/**
 * Created by hnam on 8/10/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
//@RunWith(MockitoJUnitRunner.class)
public class CasesQueryTest{


    CasesQuery query;
    Case myCase;
    Context context;


    @Before
    public void setUp() throws Exception {

        //RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        /*dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();*/
        //System.out.print("start \n");
        context =  RuntimeEnvironment.application;
        query = new CasesQuery(context);
        myCase = new Case();
    }

    @After
    public void tearDown() throws Exception {
        //System.out.print("end \n");
        query.close();
        query = null;
    }



    @Test
    public void testCreateCase(){
        myCase.setReferenceNo("nam1234");
        myCase.setArrivalDate("2015-08-08 16:48:00");
        Branch branch = new Branch();
        branch.setId(1);
        Type type = new Type();
        type.setId(2);
        myCase.setBranch(branch);
        myCase.setType(type);
        long id = query.createCase(myCase);
        org.junit.Assert.assertEquals(1, id);


        List<Case> list =  query.getAllCase();
        org.junit.Assert.assertEquals(1, list.size());

        List<Case> listNotSync =  query.getCaseNotSync();
        //Log.e(TAG, listNotSync.toString());
        org.junit.Assert.assertEquals(1, listNotSync.size());

        //update case
        myCase.setId(1);
        myCase.saveCaseFinal(3);
        query.updateCase(myCase);
        List<Case> list3 =  query.getAllCase();
        org.junit.Assert.assertEquals(3, list3.get(0).getCaseFinal());
        myCase.setLiveId(2);
        query.updateLiveIdByCase(myCase);
        List<Case> list2 =  query.getAllCase();
        org.junit.Assert.assertEquals(2, list2.get(0).getLiveId());
    }

/*    @Test
    public void testGetAllCase(){
        //get all case
        List<Case> list =  query.getAllCase();
        org.junit.Assert.assertEquals(1, list.size());
        query.close();
    }*/



/*    @Test
    public void testGetCaseNotSync(){
        //get all case not sync
        List<Case> listNotSync =  query.getCaseNotSync();
        org.junit.Assert.assertEquals(1,listNotSync.size());
    }*/



/*    public void testUpdateCase(){
        //update case
        myCase.setmLiveId(2);
        //Log.e(TAG, ">>> update live");
        query.updateCase(myCase);
        List<Case> list2 =  query.getAllCase();
        //Log.e(TAG, list2.toString());
        org.junit.Assert.assertEquals(2, list2.get(0).getmLiveId());
    }*/



}
