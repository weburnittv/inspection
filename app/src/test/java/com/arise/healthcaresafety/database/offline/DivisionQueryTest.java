package com.arise.healthcaresafety.database.offline;

import android.content.Context;
import android.content.SharedPreferences;
import android.test.AndroidTestCase;
import android.test.mock.MockContext;
import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

import org.mockito.Mock;

/**
 * Created by hnam on 8/11/2015.
 */
public class DivisionQueryTest extends AndroidTestCase{

    DivisionQuery query;

    public void setUp() throws Exception {
        super.setUp();
        Context c = new DelegatedMockContext(getContext());
        query = new DivisionQuery(c);
    }

    public void tearDown() throws Exception {
        query.close();
    }

    @SmallTest
    public void testGetDivisionQuery() throws Exception {
        String test = query.getDivisionQuery();
        System.out.print(test);
        assertNotNull(test);
    }

    class DelegatedMockContext extends MockContext {

        private Context mDelegatedContext;
        private static final String PREFIX = "com.arise.healthcaresafety";

        public DelegatedMockContext(Context context) {
            mDelegatedContext = context;
        }

        @Override
        public String getPackageName(){
            return PREFIX;
        }

        @Override
        public SharedPreferences getSharedPreferences(String name, int mode) {
            return mDelegatedContext.getSharedPreferences(name, mode);
        }
    }
}