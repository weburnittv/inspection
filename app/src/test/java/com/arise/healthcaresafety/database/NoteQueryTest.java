package com.arise.healthcaresafety.database;

import android.content.Context;

import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Equipment;
import com.arise.healthcaresafety.model.entities.Note;
import com.arise.healthcaresafety.model.entities.Question;
import com.arise.healthcaresafety.model.entities.Type;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

/**
 * Created by hnam on 8/10/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class NoteQueryTest {
    Case myCase;
    Context context;
    NoteQuery query;
    Equipment equipment;
    Question question;

    @Before
    public void setUp() throws  Exception{
        context = RuntimeEnvironment.application;
        query = new NoteQuery(context);
        setUpCase();
        setUpEquipment();
        setUpQuestion();
    }

    private void setUpCase(){
        myCase = new Case();
        myCase.setReferenceNo("nam1234");
        myCase.setArrivalDate("2015-08-08 16:48:00");
        Branch branch = new Branch();
        branch.setId(1);
        Type type = new Type();
        type.setId(2);
        myCase.setBranch(branch);
        myCase.setType(type);
        myCase.setId(1);
    }

    private void setUpEquipment(){
        equipment = new Equipment();
        equipment.setId(2);
    }

    private void setUpQuestion(){
        question = new Question();
        question.setId(2);
    }

    @Test
    public void testNoteTable(){
        Note note = new Note();
        note.setContent("test note");
//        long index1 = query.createNote(myCase, note);
//        org.junit.Assert.assertEquals(1, index1);
//
//        long index2 = query.createNote(myCase,note);
//        org.junit.Assert.assertEquals(2, index2);

        note.setId(1);
        note.setLiveId(3);
        query.updateNote(note);

        List<Note> notes = query.getAllNote();
        org.junit.Assert.assertEquals(2, notes.size());
        org.junit.Assert.assertEquals(3, notes.get(0).getLiveId());
    }
}
