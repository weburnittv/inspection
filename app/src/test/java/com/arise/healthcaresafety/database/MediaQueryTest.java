package com.arise.healthcaresafety.database;

import android.content.Context;

import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.model.entities.BaseMedia;
import com.arise.healthcaresafety.model.entities.Media;
import com.arise.healthcaresafety.model.entities.Note;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by hnam on 8/10/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MediaQueryTest {
    MediaQuery mediaQuery;
    Context context;

    @Before
    public void setUp() throws Exception{
        context = RuntimeEnvironment.application;
        mediaQuery = new MediaQuery(context);
    }



/*    @Test
    public void testMediaTable(){
        Note note1 = new Note();
        note1.setId(1);
        long index1 = mediaQuery.createMedia(note1, "https://abc.com", BaseMedia.IMAGE);

        long index2 = mediaQuery.createMedia(note1, "https://abc.com", BaseMedia.SOUND);
        long index3 = mediaQuery.createMedia(note1, "https://abc.com.ad", BaseMedia.SOUND);

        List<PhotoMedia> photoMedias = mediaQuery.getMediaByID(note1);
        org.junit.Assert.assertEquals(1, photoMedias.size());

        List<RecordMedia> soundMedias = mediaQuery.getRecordMediaByID(note1);
        org.junit.Assert.assertEquals(2, soundMedias.size());

    }*/

    @Test
    public void testDeleteMediaItem() throws Exception {
//        Note note1 = new Note();
//        note1.setId(1);
//        long index2 = mediaQuery.createMedia(note1, "https://abc.com", BaseMedia.SOUND);
//        Media media = new Media((int) index2,"https://abc.com", "date_time", BaseMedia.SOUND);
//        boolean b = mediaQuery.deleteMediaItem(media);
//        org.junit.Assert.assertTrue(b);
    }
}
