package com.arise.healthcaresafety.database;

import android.content.Context;
import android.util.Log;

import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.model.entities.Attribute;
import com.arise.healthcaresafety.model.entities.Branch;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.Result;
import com.arise.healthcaresafety.model.entities.Type;
import com.arise.healthcaresafety.model.entities.Variation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hnam on 8/10/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class ResultQueryTest {
     ResultQuery resultQuery;
     CasesQuery casesQuery;
     Context context;
    Case myCase;

    @Before
     public void setUp() throws Exception{
        context = RuntimeEnvironment.application;
        setUpCase();
        resultQuery = new ResultQuery(context);
     }

    private void setUpCase(){
        myCase = new Case();
        myCase.setReferenceNo("nam1234");
        myCase.setArrivalDate("2015-08-08 16:48:00");
        Branch branch = new Branch();
        branch.setId(1);
        Type type = new Type();
        type.setId(2);
        myCase.setBranch(branch);
        myCase.setType(type);
        casesQuery = new CasesQuery(context);
        casesQuery.createCase(myCase);
    }

    @Test
    public void testResultTable(){
        Result result = new Result();
        Result.MyQuestion question = new Result.MyQuestion();
        /*"variation":{
                    "id":6,
                    "name":"ISO 14001:2004",
                    "type":0,
                    "attributes":[
                    {"id":24,"name":"Yes"},
                    {"id":25,"name":"No"},
                    {"id":26,"name":"Partially complied"}]}*/
        question.setId(1);
        question.setContent("test question");
        question.setVariation(null);
        List<Attribute> attrs = new ArrayList<>();
        attrs.add(new Attribute(24, "Yes"));
        result.setQuestion(question);
        result.setAttributes(attrs);
        result.setTextEntry("Note...");
        //System.out.print(result.toJsonStringMyQuestion());

        //create query
//        long id = resultQuery.createResult(myCase, result);
//        org.junit.Assert.assertEquals(1, id);


        //get all result by case id
        List<Result> results1 = resultQuery.getAllResults();
        org.junit.Assert.assertEquals(1, results1.size());

        //update query
        result.setLiveId(1);
        resultQuery.updateResult(result);

        //get result by case id
        List<Result> results = resultQuery.getResultByCaseId(myCase);
/*        System.out.print(results.get(0).toJsonStringMyQuestion());
        System.out.print(results.get(0).toJsonStringAttributes());*/
        org.junit.Assert.assertEquals(1, results.size());
    }
}
