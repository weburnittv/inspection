package com.arise.healthcaresafety.model.network.api;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.entities.LoginResult;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

/**
 * Created by hnam on 7/8/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class ResultApiTest {
    Context context;

    @Before
    public void setUp() throws Exception {
        // setup
        context =  RuntimeEnvironment.application;
        ShadowLog.stream = System.out;
    }

    @Test
    public void TestResultApi() throws Exception{
        LoginApi api = new LoginApi(context);
        api.login("admin", "admin", new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult loginResult) {
                InspectionApp.getInstance().setUser(loginResult.getUser());
                InspectionApp.getInstance().setUsername("admin");
                //DeleteResultApi();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

/*    @Test
    public void ResultApi(){
        ResultsApi api = new ResultsApi(context);
        Case c = new Case();
        c.setId(19);
        Result result = new Result();
        result.getQuestion().setId(6);
        result.addAttribute(new Attribute(25, "No"));
        c.addResult(result);
*//*        api.saveResult(c, new Response.Listener<Result>() {
            @Override
            public void onResponse(Result result) {
                org.junit.Assert.assertNotNull(result);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });*//*

    }*/

/*    @Test
    public void DeleteResultApi(){
        ResultsApi api = new ResultsApi(context);
        api.deleteEquipmentResult(28, new Response.Listener<Result>() {
            @Override
            public void onResponse(Result result) {
                ShadowLog.e("DeleteResultApi", result.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

    }*/

}
