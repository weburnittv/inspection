package com.arise.healthcaresafety.model.network.api;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.entities.LoginResult;
import com.arise.healthcaresafety.model.entities.PastCases;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by hnam on 7/8/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class PastCasesApiTest {
    Context context;
    @Before
    public void setUp() throws Exception {
        // setup
        context =  RuntimeEnvironment.application;

    }

    @Test
    public void testPastCaseApi() throws Exception{
        LoginApi api = new LoginApi(context);
        api.login("admin", "admin", new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult loginResult) {
                InspectionApp.getInstance().setUser(loginResult.getUser());
                InspectionApp.getInstance().setUsername("admin");
                PastCasesApi();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    public void PastCasesApi(){
        PastCasesApi api = new PastCasesApi(context);
        api.getPastCasesApi(new Response.Listener<PastCases>() {
            @Override
            public void onResponse(PastCases pastCases) {
                org.junit.Assert.assertNotNull(pastCases);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }
}
