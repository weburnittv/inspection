package com.arise.healthcaresafety.model.network.api;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.LoginResult;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by hnam on 7/8/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class CasesDetailApiTest {
    Context context;
    @Before
    public void setUp() throws Exception {
        // setup
        context =  RuntimeEnvironment.application;

    }

    @Test
    public void testCaseDetailApi() throws Exception{
        LoginApi api = new LoginApi(context);
        api.login("admin", "admin", new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult loginResult) {
                InspectionApp.getInstance().setUser(loginResult.getUser());
                InspectionApp.getInstance().setUsername("admin");
                //CaseDetailApiFunction();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }


    public void CaseDetailApiFunction(){
        CaseDetailApi api = new CaseDetailApi(context);
        api.getCaseDetalApi(1, new Response.Listener<Case>() {
            @Override
            public void onResponse(Case aCase) {
                org.junit.Assert.assertEquals(1, aCase.getId());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

    }
}
