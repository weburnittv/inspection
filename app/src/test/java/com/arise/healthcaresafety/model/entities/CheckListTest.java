package com.arise.healthcaresafety.model.entities;

import com.arise.healthcaresafety.BuildConfig;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by hnam on 7/3/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
//@Config(emulateSdk = 22)
public class CheckListTest {

    @Before
    public void setUp() throws Exception {
        // setup
        //context =  Robolectric.buildActivity(HomeActivity.class).create().get();
        json = "{\"id\":1,\"enable\":true,\"created_at\":\"2015-07-02T07:56:00+0100\",\"updated_at\":\"2015-07-02T07:56:01+0100\",\"groups\":[{\"id\":3,\"sort_index\":0,\"name\":\"4.1 General requirements\",\"questions\":[{\"id\":6,\"content\":\"Has the organization established, documented, implemented, maintained and is continually improving an environmental management system in accordance with the requirements of ISO 14001:2004 and determined how it will fulfill these requirements?\",\"variation\":{\"id\":6,\"name\":\"ISO 14001:2004\",\"type\":0,\"attributes\":[{\"id\":24,\"name\":\"Yes\"},{\"id\":25,\"name\":\"No\"},{\"id\":26,\"name\":\"Partially complied\"}]},\"category\":null},{\"id\":7,\"content\":\"Has the organization defined and documented the scope of its environmental management system?\",\"variation\":{\"id\":6,\"name\":\"ISO 14001:2004\",\"type\":0,\"attributes\":[{\"id\":24,\"name\":\"Yes\"},{\"id\":25,\"name\":\"No\"},{\"id\":26,\"name\":\"Partially complied\"}]},\"category\":null}]},{\"id\":4,\"sort_index\":1,\"name\":\"4.2 Environmental policy\",\"questions\":[]},{\"id\":5,\"sort_index\":2,\"name\":\"4.3.1 Environmental aspects\",\"questions\":[]},{\"id\":6,\"sort_index\":3,\"name\":\"4.3.2 Legal and other requirements\",\"questions\":[]},{\"id\":7,\"sort_index\":4,\"name\":\"4.3.3 Objectives, targets and programme(s)\",\"questions\":[]},{\"id\":8,\"sort_index\":5,\"name\":\"4.4.1 Resources, roles, responsibility & authority\",\"questions\":[]},{\"id\":9,\"sort_index\":6,\"name\":\"4.4.2 Competence, training and awareness\",\"questions\":[]}]}";
    }

    String json;

    @Test
    public void testJSON() throws Exception{
        Gson gson = new Gson();
        CheckList checkList = gson.fromJson(json, CheckList.class);
        org.junit.Assert.assertEquals(1,checkList.getId());
    }


}
