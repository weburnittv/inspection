package com.arise.healthcaresafety.model.entities;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Nam on 7/13/2015.
 */
public class StatusTest {
    @Before
    public void setUp() throws Exception {
        // setup
        //context =  Robolectric.buildActivity(HomeActivity.class).create().get();
        json = "{\"status\":true}";
    }

    String json;

    @Test
    public void testJSON() throws Exception{
        Gson gson = new Gson();
        Status status = gson.fromJson(json, Status.class);
        org.junit.Assert.assertEquals(false,status.isStatus());
    }
}
