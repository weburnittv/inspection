package com.arise.healthcaresafety.model.entities;

import com.arise.healthcaresafety.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nam on 7/7/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class CaseTest {

    //Context context;

    /* @Before
     public void setUp() throws Exception {
         context = new MockContext();
         super.setUp();
     }

     @Test
     public void testGetWsseHeader() throws Exception {

         WebServicesSecurity wsse = new WebServicesSecurity(context);
         LoginResult.User user = new LoginResult.User();

         user.setUsername("admin");
         user.setToken("XHacTOfFH4c5on8OIDnBelJBxxA=");
         wsse.setUser(user);
         String header = wsse.getWsseHeader();
         org.junit.Assert.assertTrue(header.contains("admin"));
     }

     @Override
     public void tearDown() throws Exception {
         super.tearDown();
     }*/

    Case mCase;

    @Before
    public void setUp() throws Exception {
        // setup
        mCase = new Case();
        String datetime = "2015-07-06T11:49:01";
        mCase.setArrivalDate("2015-07-06T11:49:01");

        //add equipments result
        List<Result> results = new ArrayList<>();
        Result a = new Result();
        a.setId(12);
        a.setQuestion(null);
        a.addAttribute(new Attribute(15, "hello world"));
        results.add(a);

        //add check list question single choice
        Result b = new Result();
        b.setId(13);
        Variation bVariation = new Variation();
        bVariation.setId(100);
        bVariation.setName("Name variation 1");
        bVariation.setType(0);
        Result.MyQuestion bquestion = new Result.MyQuestion(1, "question 1", bVariation);
        b.setQuestion(bquestion);
        b.addAttribute(new Attribute(16, "hello world 16"));
        results.add(b);

        //add check list question multi choice
        Result c = new Result();
        c.setId(14);
        Variation cVariation = new Variation();
        cVariation.setId(101);
        cVariation.setName("Name variation 1");
        cVariation.setType(1);
        Result.MyQuestion cquestion = new Result.MyQuestion(2,"question 1",cVariation);
        c.setQuestion(cquestion);
        c.addAttribute(new Attribute(17, "hello world 17"));
        c.addAttribute(new Attribute(18, "hello world 18"));
        results.add(c);

        mCase.setResults(results);
    }

/*    @Test
    public void testGetDateTime() throws Exception {
        org.junit.Assert.assertEquals("2015-07-06", mCase.getDateTime(true));
        org.junit.Assert.assertEquals("11:49:01", mCase.getDateTime(false));

    }*/

/*    @Test
    public void testCheckResultIsNewOrUpdate(){
        Result a = new Result();
        a.setId(-1);
        a.setQuestion(null);
        a.addAttribute(new Attribute(20, "hello world"));
        Result result = mCase.checkResultIsNewOrUpdate(a);
        org.junit.Assert.assertNotEquals(11, result.getmId());
        org.junit.Assert.assertEquals(-1, result.getmId());
    }*/

    @Test
    public void testCheckResultIsNewOrUpdateSingleChoice(){
        Result a = new Result();
        a.setId(-1);
        Variation bVariation = new Variation();
        bVariation.setId(99);
        bVariation.setName("Name variation 1");
        bVariation.setType(0);
        Result.MyQuestion bquestion = new Result.MyQuestion(2, "question 1", bVariation);
        a.setQuestion(bquestion);
        a.addAttribute(new Attribute(16, "hello world"));

        Result result = mCase.checkResultIsNewOrUpdate(a);
        //org.junit.Assert.assertNotEquals(-1, result.getmId());
        org.junit.Assert.assertEquals(14, result.getId());
        //org.junit.Assert.assertNotEquals(13, result.getmId());
    }

}
