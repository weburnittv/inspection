package com.arise.healthcaresafety.model.network.api;

import android.app.Application;
import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.InspectionApp;
import com.arise.healthcaresafety.model.entities.Case;
import com.arise.healthcaresafety.model.entities.InspReference;
import com.arise.healthcaresafety.model.entities.LoginResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * Created by hnam on 8/12/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class ReferencesApiTest {
    Context context;
    @Before
    public void setUp() throws Exception {
        this.context = RuntimeEnvironment.application;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetReferencesApi() throws Exception {
        LoginApi api = new LoginApi(context);
        api.login("admin", "admin", new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult loginResult) {
                InspectionApp.getInstance().setUser(loginResult.getUser());
                InspectionApp.getInstance().setUsername("admin");
                getReferencesApi();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    public void getReferencesApi(){
        ReferencesApi api = new ReferencesApi(context);
        api.getReferencesApi(new Response.Listener<String[]>() {
            @Override
            public void onResponse(String[] strings) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

    }
}