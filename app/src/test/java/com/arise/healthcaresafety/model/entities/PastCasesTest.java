package com.arise.healthcaresafety.model.entities;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Nam on 7/9/2015.
 */
public class PastCasesTest {
    @Before
    public void setUp() throws Exception {
        // setup
        //context =  Robolectric.buildActivity(HomeActivity.class).create().get();
        json = "{\"pastCases\":[{\"id\":1,\"arrival_date\":\"2015-07-06T11:37:57+0100\",\"created_at\":\"2015-07-07T15:00:00+0100\",\"reference_no\":\"dgfffdd\",\"type\":{\"id\":7,\"name\":\"CONSASS\",\"category\":{\"id\":4,\"name\":\"Inspection\",\"types\":{\"0\":{\"id\":4,\"name\":\"Pre-MOM-INS\",\"category\":null},\"2\":{\"id\":8,\"name\":\"Pre-MOM\",\"category\":null}}}},\"branch\":{\"id\":2,\"name\":\"Consafe\",\"address\":\"67 Jalan pemipin\",\"tel\":\"80890809\",\"division\":{\"id\":3,\"name\":\"Consulting Division\",\"branches\":{\"0\":{\"id\":1,\"name\":\"Bizsafe\",\"address\":\"45 Jalan. Peminpin\",\"tel\":\"090909809\",\"division\":null},\"2\":{\"id\":3,\"name\":\"PolicySafe\",\"address\":\"67 Jalan Besar\",\"tel\":\"6587879879879\",\"division\":null}}}}}]";
    }

    String json;

    @Test
    public void testJSON() throws Exception{
        Gson gson = new Gson();
        CheckList checkList = gson.fromJson(json, CheckList.class);
        org.junit.Assert.assertEquals(1,checkList.getId());
    }
}
