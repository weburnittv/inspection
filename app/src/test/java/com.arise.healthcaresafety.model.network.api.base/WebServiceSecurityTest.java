package com.arise.healthcaresafety.model.network.api.base;

import com.arise.healthcaresafety.BuildConfig;
import com.arise.healthcaresafety.model.entities.LoginResult;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by hnam on 7/3/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class,sdk = 21)
public class WebServiceSecurityTest {
    //Context context;

   /* @Before
    public void setUp() throws Exception {
        context = new MockContext();
        super.setUp();
    }

    @Test
    public void testGetWsseHeader() throws Exception {

        WebServicesSecurity wsse = new WebServicesSecurity(context);
        LoginResult.User user = new LoginResult.User();

        user.setUsername("admin");
        user.setToken("XHacTOfFH4c5on8OIDnBelJBxxA=");
        wsse.setUser(user);
        String header = wsse.getWsseHeader();
        org.junit.Assert.assertTrue(header.contains("admin"));
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }*/
    LoginResult.User user;

    @Before
    public void setUp() throws Exception {
        // setup
        //context =  Robolectric.buildActivity(HomeActivity.class).create().get();

    }

    @Test
    public void testGetWsseHeader() throws Exception {
        WebServicesSecurity wsse = new WebServicesSecurity();
        LoginResult.User user = new LoginResult.User();
        user.setUsername("admin");
        user.setToken("XHacTOfFH4c5on8OIDnBelJBxxA=");
        //wsse.setUser(user);
        String header = wsse.getWsseHeader();
        org.junit.Assert.assertTrue(header.contains("admin"));
    }




}
