package org.catrobat.paintroid.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.EditText;
/**
 * Copyright © 2015 AsianTech inc.
 * Created by Binc on 27/09/2015.
 */
public class CustomEditText extends EditText {
    private Rect mRect;
    private Paint mPaint;

    // we need this constructor for LayoutInflater
    public CustomEditText(Context context) {
        super(context);

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLUE); //SET YOUR OWN COLOR HERE
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //int count = getLineCount();

        canvas.drawRect(1,1, getWidth()-1, getHeight()-1, mPaint);

        super.onDraw(canvas);
    }
}