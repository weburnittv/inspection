/**
 * Paintroid: An image manipulation application for Android.
 * Copyright (C) 2010-2015 The Catrobat Team
 * (<http://developer.catrobat.org/credits>)
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.catrobat.paintroid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.catrobat.paintroid.command.implementation.CommandManagerImplementation;
import org.catrobat.paintroid.dialog.BrushPickerDialog;
import org.catrobat.paintroid.dialog.CustomAlertDialogBuilder;
import org.catrobat.paintroid.dialog.IndeterminateProgressDialog;
import org.catrobat.paintroid.dialog.InfoDialog;
import org.catrobat.paintroid.dialog.InfoDialog.DialogType;
import org.catrobat.paintroid.dialog.TextDialog;
import org.catrobat.paintroid.dialog.ToolsDialog;
import org.catrobat.paintroid.dialog.colorpicker.ColorPickerDialog;
import org.catrobat.paintroid.listener.DrawingSurfaceListener;
import org.catrobat.paintroid.models.Texts;
import org.catrobat.paintroid.tools.Tool;
import org.catrobat.paintroid.tools.ToolFactory;
import org.catrobat.paintroid.tools.ToolType;
import org.catrobat.paintroid.tools.implementation.ImportTool;
import org.catrobat.paintroid.ui.BottomBar;
import org.catrobat.paintroid.ui.DrawingSurface;
import org.catrobat.paintroid.ui.Perspective;
import org.catrobat.paintroid.ui.TopBar;
import org.catrobat.paintroid.untils.ScreenUtil;
import org.catrobat.paintroid.views.CustomEditText;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends OptionsMenuActivity implements TextDialog.OnDialogTextUpdate {

    public static final String EXTRA_INSTANCE_FROM_CATROBAT = "EXTRA_INSTANCE_FROM_CATROBAT";
    public static final String EXTRA_ACTION_BAR_HEIGHT = "EXTRA_ACTION_BAR_HEIGHT";
    public static final int ARG_PAINT = 123;
    public static final int REQUEST_IMAGE_EDIT = 989;
    private static final int ANDROID_VERSION_ICE_CREAM_SANDWICH = 14;
    public static ViewGroup mRlRoot;
    public static int sAddSizeText = 10;
    private static String path;
    private static View mLayoutView;
    private static int mXDelta;
    private static int mYDelta;
    private static int mXMouseOld;
    private static int mYMouseOld;
    private static int mPosMove;
    private static Context mConText;
    private static List<Texts> mTexts = new ArrayList<>();
    protected DrawingSurfaceListener mDrawingSurfaceListener;
    protected TopBar mTopBar;
    protected BottomBar mBottomBar;
    protected boolean mToolbarIsVisible = true;
    private ProgressDialog mProgressDialog;
    private Menu mMenu = null;
    private ImageButton mAttributeButton1;
    private ImageButton mImgTextSize;
    private String mCatroidPictureName;

    public static void startActivity(Fragment fragment, String path, int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), MainActivity.class);
        intent.putExtra("ABC", path);
        fragment.startActivityForResult(intent, requestCode);
    }

    public static void initAddText() {
        for (int i = 0; i < mTexts.size(); i++) {
            ViewGroup viewGroup = (ViewGroup) mTexts.get(i).getView();
            TextView tvTextDesplay = new TextView(mConText);
            viewGroup.setBackgroundColor(Color.TRANSPARENT);

            try {
                TextView tvText = (TextView) viewGroup.getChildAt(0);
                Log.i("TAG11", tvText.getTextSize() + "??");
                tvTextDesplay.setText(tvText.getText());
            } catch (Exception ee) {
                CustomEditText edtText = (CustomEditText) viewGroup.getChildAt(0);
                tvTextDesplay.setText(edtText.getText());
            }
            tvTextDesplay.setTextColor(mTexts.get(i).getColor());
            tvTextDesplay.setTextSize(mTexts.get(i).getSize());
            viewGroup.removeAllViews();
            viewGroup.addView(tvTextDesplay);
        }
    }

    public static void addText(int l, int t) {
        initAddText();
        LayoutInflater layout = LayoutInflater.from(mConText);
        mLayoutView = layout.inflate(R.layout.custom_editext_paint, mRlRoot, false);
        mLayoutView.setId(mTexts.size());

        mTexts.add(new Texts(mLayoutView, 28, Color.BLUE));
        mRlRoot.addView(mLayoutView);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = l;
        layoutParams.topMargin = t;
        mLayoutView.setLayoutParams(layoutParams);

        mLayoutView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(mConText, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    for (int i = 0; i < mTexts.size(); i++) {
                        if (i != mLayoutView.getId()) {
                            ViewGroup viewGroup = (ViewGroup) mTexts.get(i).getView();
                            TextView tvTextDesplay = new TextView(mConText);

                            try {
                                TextView tvText = (TextView) viewGroup.getChildAt(0);
                                tvTextDesplay.setText(tvText.getText());
                            } catch (Exception ee) {
                                CustomEditText edtText = (CustomEditText) viewGroup.getChildAt(0);
                                tvTextDesplay.setText(edtText.getText());
                            }
                            tvTextDesplay.setTextColor(mTexts.get(i).getColor());
                            tvTextDesplay.setTextSize(mTexts.get(i).getSize());
                            viewGroup.removeAllViews();
                            viewGroup.addView(tvTextDesplay);
                        }
                    }

                    ViewGroup view = (ViewGroup) mLayoutView;
                    CustomEditText edtText = new CustomEditText(mConText);

                    edtText.setBackgroundColor(Color.TRANSPARENT);
                    float t = 10.0f / ((ScreenUtil.convertDPToPixels(mConText, 10)));

                    int left = view.getChildAt(0).getPaddingLeft();
                    int top = view.getChildAt(0).getPaddingTop();
                    int right = view.getChildAt(0).getPaddingRight();
                    int bottom = view.getChildAt(0).getPaddingBottom();
                    try {
                        TextView tvText = (TextView) view.getChildAt(0);
                        edtText.setTextSize((float) tvText.getTextSize() * t);
                        edtText.setText(tvText.getText());
                        edtText.setIncludeFontPadding(true);
                    } catch (Exception ee) {
                        CustomEditText edtText1 = (CustomEditText) view.getChildAt(0);
                        edtText.setText(edtText1.getText());
                    }
                    edtText.setPadding(left, top, right + 3, bottom);
                    edtText.setFocusableInTouchMode(true);
                    edtText.requestFocus();
                    edtText.setTextColor(mTexts.get(mPosMove).getColor());
                    view.setBackgroundColor(Color.TRANSPARENT);
                    view.removeAllViews();
                    view.addView(edtText);

                    return super.onDoubleTap(e);
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    mPosMove = mLayoutView.getId();
                    for (int i = 0; i < mTexts.size(); i++) {
                        if (i != mLayoutView.getId()) {
                            ViewGroup viewGroup = (ViewGroup) mTexts.get(i).getView();
                            TextView tvTextDesplay = new TextView(mConText);
                            viewGroup.setBackgroundColor(Color.TRANSPARENT);

                            try {
                                TextView tvText = (TextView) viewGroup.getChildAt(0);
                                Log.i("TAG11", tvText.getTextSize() + "??");
                                tvTextDesplay.setText(tvText.getText());
                            } catch (Exception ee) {
                                CustomEditText edtText = (CustomEditText) viewGroup.getChildAt(0);
                                tvTextDesplay.setText(edtText.getText());
                            }
                            tvTextDesplay.setTextColor(mTexts.get(i).getColor());
                            tvTextDesplay.setTextSize(mTexts.get(i).getSize());
                            viewGroup.removeAllViews();
                            viewGroup.addView(tvTextDesplay);
                        }
                    }
                    ViewGroup view = (ViewGroup) mLayoutView;
                    view.setBackgroundColor(Color.parseColor("#7f000000"));
                    return super.onSingleTapUp(e);
                }
            });

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                mLayoutView = view;
                gestureDetector.onTouchEvent(event);
                if (mLayoutView.getId() == mPosMove) {
                    final int X = (int) event.getRawX();
                    final int Y = (int) event.getRawY();
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view
                            .getLayoutParams();
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {

                        case MotionEvent.ACTION_DOWN:

                            mXDelta = X - lParams.leftMargin;
                            mYDelta = Y - lParams.topMargin;
                            mXMouseOld = X;
                            mYMouseOld = Y;

                            break;
                        case MotionEvent.ACTION_UP:
                      /*  int T = ScreenUtil.convertDPToPixels(ImageActivity.this, 25);
                        if (mRoteScreen == 0) {
                            heightLayout = heightLayout + ScreenUtil.getStatusBarHeight(ImageActivity.this);
                            if (mMarginLeft + mWidth - T > X && X > mMarginLeft + T && mMarginTop + mHeight - T > Y - heightLayout
                                    && Y - heightLayout > mMarginTop + T) {
                                isCheck = true;
                            }
                        } else {
                            if (mMarginLeft + mWidth - T > X - heightLayout && X - heightLayout > mMarginLeft + T &&
                                    mMarginTop + mHeight - T > Y && Y > mMarginTop + T) {
                                isCheck = true;
                            }
                        }
                        if (Math.abs(mXMouseOldT - X) <= 10
                                && Math.abs(mYMouseOldT - Y) <= 10 && isCheck) {
                            startInputBlackBoard();
                        }*/
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (X != mXMouseOld || Y != mYMouseOld) {
                                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                                        .getLayoutParams();
                                int marginleft = X - mXDelta;
                                int margintop = Y - mYDelta;
                                int height = layoutParams.height;
                                int width = layoutParams.width;

                                updateView(view, marginleft, margintop, height, width);
                                mXMouseOld = X;
                                mYMouseOld = Y;
                                mRlRoot.invalidate();
                            }
                            break;
                    }
                }
                return true;
            }
        });
    }

    private static void updateView(View view, int leftMargin, int topMargin, int height,
                                   int weight) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                .getLayoutParams();

        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;
        layoutParams.height = height;
        layoutParams.width = weight;
        view.setLayoutParams(layoutParams);
    }

    public static void notFocusView() {
        for (int i = 0; i < mTexts.size(); i++) {
            ViewGroup viewGroup = (ViewGroup) mTexts.get(i).getView();
            TextView tvTextDesplay = new TextView(mConText);
            viewGroup.setBackgroundColor(Color.TRANSPARENT);
            try {
                TextView tvText = (TextView) viewGroup.getChildAt(0);
                Log.i("TAG11", tvText.getTextSize() + "??");
                tvTextDesplay.setText(tvText.getText());
            } catch (Exception ee) {
                CustomEditText edtText = (CustomEditText) viewGroup.getChildAt(0);
                tvTextDesplay.setText(edtText.getText());
            }
            tvTextDesplay.setTextColor(mTexts.get(i).getColor());
            tvTextDesplay.setTextSize(mTexts.get(i).getSize());
            viewGroup.removeAllViews();
            viewGroup.addView(tvTextDesplay);
        }
    }

    public static void updateColor(int color) {
        try {
            ViewGroup viewGroup = (ViewGroup) mTexts.get(mPosMove).getView();
            viewGroup.setBackgroundColor(Color.TRANSPARENT);

            try {
                TextView tvText = (TextView) viewGroup.getChildAt(0);
                tvText.setTextColor(color);
            } catch (Exception ee) {
                CustomEditText edtText = (CustomEditText) viewGroup.getChildAt(0);
                edtText.setTextColor(color);
            }
            mTexts.get(mPosMove).setColor(color);
        } catch (Exception ee) {
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ColorPickerDialog.init(this);
        BrushPickerDialog.init(this);
        ToolsDialog.init(this);
        IndeterminateProgressDialog.init(this);
        mConText = this;
        mProgressDialog = new ProgressDialog(mConText);
        mProgressDialog.setMessage("Please wait loading ...");
        mProgressDialog.setTitle("Save Image");
        /**
         * EXCLUDED PREFERENCES FOR RELEASE /*SharedPreferences
         * sharedPreferences = PreferenceManager
         * .getDefaultSharedPreferences(this); String languageString =
         * sharedPreferences.getString(
         * getString(R.string.preferences_language_key), "nolang");
         *
         * if (languageString.equals("nolang")) {
         * Log.e(PaintroidApplication.TAG, "no language preference exists"); }
         * else { Log.i(PaintroidApplication.TAG, "load language: " +
         * languageString); Configuration config =
         * getBaseContext().getResources() .getConfiguration(); config.locale =
         * new Locale(languageString);
         * getBaseContext().getResources().updateConfiguration(config,
         * getBaseContext().getResources().getDisplayMetrics()); }
         */

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getSupportActionBar().getHeight();
        // setDefaultPreferences();
        initComponent();
        eventComponent();
        initActionBar();
        PaintroidApplication.catroidPicturePath = null;
        String catroidPicturePath = null;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            catroidPicturePath = extras
                    .getString(getString(R.string.extra_picture_path_catroid));
            mCatroidPictureName = extras
                    .getString(getString(R.string.extra_picture_name_catroid));
        }
        if (catroidPicturePath != null) {
            PaintroidApplication.openedFromCatroid = true;
            if (!catroidPicturePath.equals("")) {
                PaintroidApplication.catroidPicturePath = catroidPicturePath;
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
            PaintroidApplication.openedFromCatroid = false;
        }

        PaintroidApplication.drawingSurface = (DrawingSurface) findViewById(R.id.drawingSurfaceView);
        PaintroidApplication.applicationContext = MainActivity.this.getApplicationContext();
        PaintroidApplication.commandManager = new CommandManagerImplementation();
        PaintroidApplication.perspective = new Perspective(
                ((SurfaceView) PaintroidApplication.drawingSurface).getHolder());
        mDrawingSurfaceListener = new DrawingSurfaceListener();
        mTopBar = new TopBar(this, PaintroidApplication.openedFromCatroid);
        mBottomBar = new BottomBar(this);

        PaintroidApplication.drawingSurface
                .setOnTouchListener(mDrawingSurfaceListener);

        if (PaintroidApplication.openedFromCatroid
                && catroidPicturePath != null
                && catroidPicturePath.length() > 0) {
            loadBitmapFromUriAndRun(Uri.fromFile(new File(catroidPicturePath)),
                    new RunnableWithBitmap() {
                        @SuppressLint("NewApi")
                        @Override
                        public void run(Bitmap bitmap) {
                            if (!bitmap.hasAlpha()) {

                                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
                                    bitmap.setHasAlpha(true);
                                } else {
                                    bitmap = addAlphaChannel(bitmap);
                                }
                            }
                            ///////////////////////////////////////////////
                            PaintroidApplication.drawingSurface
                                    .resetBitmap(bitmap);
                        }

                        private Bitmap addAlphaChannel(Bitmap src) {
                            int width = src.getWidth();
                            int height = src.getHeight();
                            Bitmap dest = Bitmap.createBitmap(width, height,
                                    Config.ARGB_8888);

                            int[] pixels = new int[width * height];
                            src.getPixels(pixels, 0, width, 0, 0, width, height);
                            dest.setPixels(pixels, 0, width, 0, 0, width,
                                    height);

                            src.recycle();
                            return dest;
                        }
                    });

        } else {
            initialiseNewBitmap();
        }

    }

    private void eventComponent() {
        mImgTextSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTexts.size() > 0) {
                    TextDialog textDialog = new TextDialog(MainActivity.this, MainActivity.this, mTexts.get(mPosMove).getSize());
                    textDialog.show();

                }
                //TextDialog.init(MainActivity.this);

            }
        });
        mRlRoot.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    private void initComponent() {
        mRlRoot = (RelativeLayout) findViewById(R.id.main_layout);
        mImgTextSize = (ImageButton) findViewById(R.id.btn_bottom_attribute1);
       /* addText(100, 100);
        addText(200, 200);
        addText(300, 300);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkIfLoadBitmapFailed();
        sAddSizeText = convertDPToPixels(this, 3);
    }

    private int convertDPToPixels(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dp * density);
    }

    public void checkIfLoadBitmapFailed() {
        if (loadBitmapFailed) {
            loadBitmapFailed = false;
            new InfoDialog(DialogType.WARNING,
                    R.string.dialog_loading_image_failed_title,
                    R.string.dialog_loading_image_failed_text).show(
                    getSupportFragmentManager(), "loadbitmapdialogerror");
        }
    }

    private void initActionBar() {
        getSupportActionBar().setCustomView(R.layout.top_bar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        if (Build.VERSION.SDK_INT < ANDROID_VERSION_ICE_CREAM_SANDWICH) {
            Bitmap bitmapActionBarBackground = Bitmap.createBitmap(1, 1,
                    Config.ARGB_8888);
            bitmapActionBarBackground.eraseColor(getResources().getColor(
                    R.color.custom_background_color));
            Drawable drawable = new BitmapDrawable(getResources(),
                    bitmapActionBarBackground);
            getSupportActionBar().setBackgroundDrawable(drawable);
            getSupportActionBar().setSplitBackgroundDrawable(drawable);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        IndeterminateProgressDialog.getInstance().dismiss();
        showSecurityQuestionBeforeExit();
        super.onDetachedFromWindow();
    }

    @Override
    protected void onDestroy() {

        PaintroidApplication.commandManager.resetAndClear();
        PaintroidApplication.drawingSurface.recycleBitmap();
        ColorPickerDialog.getInstance().setInitialColor(
                getResources().getColor(R.color.color_chooser_black));
        PaintroidApplication.currentTool.changePaintStrokeCap(Cap.ROUND);
        PaintroidApplication.currentTool.changePaintStrokeWidth(25);
        PaintroidApplication.isPlainImage = true;
        PaintroidApplication.savedPictureUri = null;
        PaintroidApplication.saveCopy = false;

        ToolsDialog.getInstance().dismiss();
        IndeterminateProgressDialog.getInstance().dismiss();
        ColorPickerDialog.getInstance().dismiss();
        // BrushPickerDialog.getInstance().dismiss(); // TODO: how can there
        // ever be a null pointer exception?
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        mProgressDialog.show();
        if (!mToolbarIsVisible) {
            setFullScreen(false);
        } else if (PaintroidApplication.currentTool.getToolType() == ToolType.BRUSH) {
            showSecurityQuestionBeforeExit();
        } else {
            switchTool(ToolType.BRUSH);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Log.d(PaintroidApplication.TAG,
                    "onActivityResult: result not ok, most likely a dialog hast been canceled");
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_IMPORTPNG:
                Uri selectedGalleryImageUri = data.getData();
                Tool tool = ToolFactory.createTool(this, ToolType.IMPORTPNG);
                switchTool(tool);

                loadBitmapFromUriAndRun(selectedGalleryImageUri,
                        new RunnableWithBitmap() {
                            @Override
                            public void run(Bitmap bitmap) {
                                if (PaintroidApplication.currentTool instanceof ImportTool) {
                                    ((ImportTool) PaintroidApplication.currentTool)
                                            .setBitmapFromFile(bitmap);

                                } else {
                                    Log.e(PaintroidApplication.TAG,
                                            "importPngToFloatingBox: Current tool is no ImportTool as required");
                                }
                            }
                        });

                break;
            case REQUEST_CODE_FINISH:
                finish();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void importPng() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivityForResult(intent, REQUEST_CODE_IMPORTPNG);
    }

    public synchronized void switchTool(ToolType changeToToolType) {

        switch (changeToToolType) {
            case REDO:
                PaintroidApplication.commandManager.redo();
                break;
            case UNDO:
                PaintroidApplication.commandManager.undo();
                break;
            case IMPORTPNG:
                importPng();
                break;
            default:
                Tool tool = ToolFactory.createTool(this, changeToToolType);
                switchTool(tool);
                break;
        }

    }

    public synchronized void switchTool(Tool tool) {
        Paint tempPaint = new Paint(
                PaintroidApplication.currentTool.getDrawPaint());
        if (tool != null) {
            mTopBar.setTool(tool);
            mBottomBar.setTool(tool);
            PaintroidApplication.currentTool = tool;
            PaintroidApplication.currentTool.setDrawPaint(tempPaint);
        }
    }

    private void showSecurityQuestionBeforeExit() {
        if (PaintroidApplication.isSaved
                || !PaintroidApplication.commandManager.hasCommands()
                && PaintroidApplication.isPlainImage) {
            //todo return intent with data
            Intent returnIntent = new Intent();
            returnIntent.putExtra(getString(R.string.extra_picture_path_catroid),
                    PaintroidApplication.catroidPicturePath);
            setResult(REQUEST_IMAGE_EDIT, returnIntent);
            Log.d("ttt", "" + RESULT_OK);
            mProgressDialog.dismiss();
            finish();
            return;
        } else {
            AlertDialog.Builder builder = new CustomAlertDialogBuilder(this);

            if (PaintroidApplication.openedFromCatroid) {
                builder.setTitle(R.string.closing_catroid_security_question_title);
                builder.setMessage(R.string.closing_security_question);
                mProgressDialog.show();
                builder.setPositiveButton(R.string.save_button_text,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int id) {

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        exitToCatroid();
                                        dialog.dismiss();
                                    }
                                }).start();
                            }
                        });
                builder.setNegativeButton(R.string.discard_button_text,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
            } else {
                builder.setTitle(R.string.closing_security_question_title);
                builder.setMessage(R.string.closing_security_question);
                builder.setPositiveButton(R.string.save_button_text,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                saveFileBeforeExit();
                                dialog.dismiss();
                            }
                        });
                builder.setNegativeButton(R.string.discard_button_text,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
            }
            builder.setCancelable(true);
            final AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void saveFileBeforeExit() {
        mProgressDialog.show();
        saveFile();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(getString(R.string.extra_picture_path_catroid),
                PaintroidApplication.catroidPicturePath);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    public Bitmap drawTextToBitmap(Bitmap bitmap, Bitmap bitmap1) {
        Log.i("TAG11", bitmap.getHeight() + "//" + bitmap.getWidth() + "//" + bitmap1.getHeight() + "//" + bitmap1.getWidth() + "//" + ScreenUtil.getWidthScreen(this) + "//" + ScreenUtil.getHeightScreen(this));
        Config bitmapConfig = bitmap.getConfig();
        if (bitmapConfig == null) {
            bitmapConfig = Config.ARGB_8888;
        }

        bitmap = bitmap.copy(bitmapConfig, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.rgb(61, 61, 61));
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        //  Bitmap img = Bitmap.createScaledBitmap(bitmapDraw, width, height, false);
        //Bitmap img=getResizedBitmap(bitmapDraw,width,height);
        canvas.drawBitmap(bitmap1, (bitmap.getWidth() - ScreenUtil.getWidthScreen(this)) / 2.0f, (bitmap.getHeight() - ScreenUtil.getHeightScreen(this) +
                getSupportActionBar().getHeight() + ScreenUtil.getStatusBarHeight(this) + ScreenUtil.convertDPToPixels(this, 50)) / 2.0f, paint);

        return bitmap;
    }

    private void exitToCatroid() {
        //mProgressDialog.show();
        String pictureFileName = getString(R.string.temp_picture_name);
        if (PaintroidApplication.catroidPicturePath != null) {
            pictureFileName = PaintroidApplication.catroidPicturePath;
        } else {
            if (mCatroidPictureName != null
                    && mCatroidPictureName.length() > 0) {
                pictureFileName = mCatroidPictureName;
            }
            pictureFileName = FileIO.createNewEmptyPictureFile(this,
                    pictureFileName).getAbsolutePath();
        }

        Intent resultIntent = new Intent();

        if (FileIO.saveBitmap(MainActivity.this,
                PaintroidApplication.drawingSurface.getBitmapCopy(),
                pictureFileName)) {
            Bundle bundle = new Bundle();
            bundle.putString(getString(R.string.extra_picture_path_catroid),
                    pictureFileName);
            resultIntent.putExtras(bundle);
            setResult(RESULT_OK, resultIntent);
        } else {
            setResult(RESULT_CANCELED, resultIntent);
        }
        finish();
    }

    Bitmap getViewBitmap(View view) {
        //Get the dimensions of the view so we can re-layout the view at its current size
        //and create a bitmap of the same size
        int width = view.getWidth();
        int height = view.getHeight();

        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        //Cause the view to re-layout
        view.measure(measuredWidth, measuredHeight);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create a bitmap backed Canvas to draw the view into
        Bitmap b = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas c = new Canvas(b);

        //Now that the view is laid out and we have a canvas, ask the view to draw itself into the canvas
        view.draw(c);

        return b;
    }

    private void setFullScreen(boolean isFullScreen) {
        PaintroidApplication.perspective.setFullscreen(isFullScreen);
        if (isFullScreen) {
            getSupportActionBar().hide();
            LinearLayout bottomBarLayout = (LinearLayout) findViewById(R.id.main_bottom_bar);
            bottomBarLayout.setVisibility(View.GONE);
            mToolbarIsVisible = false;
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        } else {
            getSupportActionBar().show();
            LinearLayout bottomBarLayout = (LinearLayout) findViewById(R.id.main_bottom_bar);
            bottomBarLayout.setVisibility(View.VISIBLE);
            mToolbarIsVisible = true;
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @Override
    public void updateText(String text) {

    }

    @Override
    public void setFontSize(int size) {
        Log.i("TAG11", size + "//");
        ViewGroup viewGroup = (ViewGroup) mTexts.get(mPosMove).getView();
        viewGroup.setBackgroundColor(Color.TRANSPARENT);
        mTexts.get(mPosMove).setSize(size);
        try {
            TextView tvText = (TextView) viewGroup.getChildAt(0);
            tvText.setTextSize(size);
        } catch (Exception ee) {
            CustomEditText edtText = (CustomEditText) viewGroup.getChildAt(0);
            edtText.setTextSize(size);
        }
    }

	/* EXCLUDE PREFERENCES FOR RELEASE */
    // private void setDefaultPreferences() {
    // PreferenceManager
    // .setDefaultValues(this, R.xml.preferences_tools, false);
    // }

}