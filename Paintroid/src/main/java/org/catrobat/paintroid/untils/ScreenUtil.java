
package org.catrobat.paintroid.untils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * This class have some methods that support for get size of screen such
 * as {@link #getWidthScreen(Context)}, {@link #getHeightScreen(Context)}.
 * And this have also a function {@link #convertDPToPixels(Context, int)} to
 * convert db value to pixels
 * <p/>
 * Copyright © 2015 AsianTech inc.
 * Created by Binc on 27/09/2015.
 */
public class ScreenUtil {

    /**
     * This method is used to get height of screen
     *
     * @param context is current context
     * @return return height screen in pixel
     */
    public static int getHeightScreen(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    /**
     * This method is used to get width of screen
     *
     * @param context is current context
     * @return return width of screen in pixel
     */
    public static int getWidthScreen(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    /**
     * This method is used to convert dp to pixel
     *
     * @param context is current context
     * @param dp      is value you want to convert for
     * @return return value in pixel
     */
    public static float convertDPToPixels(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dp * density);
    }

    /**
     * This method is used to get height of status bar
     *
     * @param context is current context
     * @return return height of status bar
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

}