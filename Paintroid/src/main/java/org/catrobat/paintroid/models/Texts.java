package org.catrobat.paintroid.models;

import android.view.View;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by Binc on 27/09/2015.
 */
public class Texts {
    private View view;
    private int size;
    private int color;

    public Texts(View view, int size, int color) {
        this.view = view;
        this.size = size;
        this.color = color;
    }

    public View getView() {
        return view;
    }

    public int getColor() {
        return color;
    }

    public int getSize() {
        return size;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
