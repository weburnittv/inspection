package org.catrobat.paintroid.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import org.catrobat.paintroid.MainActivity;
import org.catrobat.paintroid.R;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by paulaan on 8/9/15.
 */
public class TextDialog extends BaseDialog implements
        View.OnClickListener, DialogInterface.OnClickListener {
    private static final String NOT_INITIALIZED_ERROR_MESSAGE = "TextDialog has not been initialized. Call init() first!";
    private static final String TAG = TextDialog.class.getSimpleName();

    private static TextDialog instance;
   // private EditText text;
    private NumberPicker fontSize;
    public String canvasText;
    public int canvasSize;
    private MainActivity mainActivity;
    private ArrayList<OnDialogTextUpdate> textUpdateListeners;
    private static OnDialogTextUpdate mOnDialogTextUpdate;

    public interface OnDialogTextUpdate {
        public void updateText(String text);

        public void setFontSize(int size);
    }

    public TextDialog(Context context, OnDialogTextUpdate onDialogTextUpdate,int size) {
        super(context);
        mOnDialogTextUpdate = onDialogTextUpdate;
        mainActivity = (MainActivity) context;

        textUpdateListeners = new ArrayList<OnDialogTextUpdate>();

        this.setTitle("Font Size");

        LayoutInflater inflator = mainActivity.getLayoutInflater();
        View view = inflator.inflate(R.layout.dialog_text, null);

        Button saveButton = (Button) view.findViewById(R.id.update_text);

       /* text = (EditText) view.findViewById(R.id.editText);

        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                canvasText = editable.toString();
                Log.d("Edit test", canvasText);
            }
        });*/

        fontSize = (NumberPicker) view.findViewById(R.id.fontPicker);
        setNumberPickerTextColor(fontSize, context.getResources().getColor(R.color.text_color));
        fontSize.setMinValue(20);
        fontSize.setMaxValue(40);
        fontSize.setValue(size);
        fontSize.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                canvasSize = newVal;
            }
        });
        ;

        saveButton.setOnClickListener(this);

        this.setContentView(view);
    }

    public static TextDialog getInstance() {
        if (instance == null) {
            throw new IllegalStateException(NOT_INITIALIZED_ERROR_MESSAGE);
        }
        return instance;
    }

    public static void init(Context context) {
        if (instance == null)
            instance = new TextDialog(context, mOnDialogTextUpdate,28);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (R.id.update_text == id) {
            if (canvasSize == 0) {
                canvasSize = 28;
            }
            mOnDialogTextUpdate.setFontSize(canvasSize);
            this.hide();
        }

        /*switch (view.getId()) {
            case R.id.update_text:
                updateCanvasText();
                break;
        }*/
    }

    public String getCanvasText() {
        return canvasText;
    }

    private void updateCanvasText() {
        for (OnDialogTextUpdate listener : textUpdateListeners) {
            listener.updateText(canvasText);
            listener.setFontSize(canvasSize);
        }
        this.hide();
    }

    public void addListener(OnDialogTextUpdate listener) {
        this.textUpdateListeners.add(listener);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                    Log.e(TAG, e.getMessage());
                } catch (IllegalAccessException e) {
                    Log.e(TAG, e.getMessage());
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }
        return false;
    }


}
