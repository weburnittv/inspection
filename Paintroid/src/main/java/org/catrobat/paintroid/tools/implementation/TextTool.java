package org.catrobat.paintroid.tools.implementation;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.Log;
import android.widget.ImageButton;

import org.catrobat.paintroid.MainActivity;
import org.catrobat.paintroid.R;
import org.catrobat.paintroid.dialog.TextDialog;
import org.catrobat.paintroid.tools.ToolType;
import org.catrobat.paintroid.ui.DrawingSurface;
import org.catrobat.paintroid.ui.TopBar;

/**
 * Created by paulaan on 8/9/15.
 */
public class TextTool extends GeometricFillTool {
    private String content;
    protected ImageButton mAttributeButton1;
    protected ImageButton mAttributeButton2;
    protected MainActivity mainActivity;
    protected PointF point;
    protected long downTime;

    protected String textLabel;
    protected int fontSize;
    public static Context context;

    protected TextDialog.OnDialogTextUpdate updateEvent;


    public TextTool(Activity activity, ToolType toolType) {
        super(activity, toolType);
        TextDialog.init(this.mContext);
        updateEvent = new TextDialog.OnDialogTextUpdate() {
            @Override
            public void updateText(String text) {
                textLabel = text;
                mMovedDistance.set(0, 0);
                //  createAndSetBitmap(PaintroidApplication.drawingSurface);
            }

            @Override
            public void setFontSize(int size) {
              /*  fontSize = size;
                mMovedDistance.set(0, 0);
                createAndSetBitmap(PaintroidApplication.drawingSurface);*/
            }
        };

        TextDialog.getInstance().addListener(updateEvent);
    }


    //
//    @Override
//    public void draw(Canvas canvas) {
//        if (TextDialog.getInstance().getCanvasText() != null) {
//            createAndSetBitmap(PaintroidApplication.drawingSurface);
//        }
//    }
//
    @Override
    public boolean handleDown(PointF coordinate) {
        downTime = System.currentTimeMillis();
        return super.handleDown(coordinate);
    }

    @Override
    public boolean handleUp(PointF coordinate) {

        long upTime = System.currentTimeMillis();
        Log.d("click:", String.valueOf(Math.abs(downTime - upTime)));
        if (Math.abs(downTime - upTime) < 50) {
            point = new PointF(coordinate.x, coordinate.y);
            this.showTextDialog();
            return false;
        }

        return super.handleUp(coordinate);
    }


    @Override
    public int getAttributeButtonResource(TopBar.ToolButtonIDs buttonNumber) {
        switch (buttonNumber) {
            case BUTTON_ID_PARAMETER_TOP:
                return getStrokeColorResource();
            case BUTTON_ID_PARAMETER_BOTTOM_1:
                return R.drawable.icon_menu_strokes;
            case BUTTON_ID_PARAMETER_BOTTOM_2:
                return R.drawable.icon_menu_text;
            default:
                return super.getAttributeButtonResource(buttonNumber);
        }
    }

    //
//    @Override
//    protected void onClickInBox() {
//        Point intPosition = new Point((int) mToolPosition.x,
//                (int) mToolPosition.y);
//        Command command = new StampCommand(mDrawingBitmap, intPosition,
//                mBoxWidth, mBoxHeight, mBoxRotation);
//        ((StampCommand) command).addObserver(this);
//        IndeterminateProgressDialog.getInstance().show();
//        PaintroidApplication.commandManager.commitCommand(command);
//    }
//
    @Override
    public void attributeButtonClick(TopBar.ToolButtonIDs buttonNumber) {
        switch (buttonNumber) {
            case BUTTON_ID_PARAMETER_BOTTOM_1:
                showBrushPicker();
                break;
            case BUTTON_ID_PARAMETER_BOTTOM_2:
                showTextDialog();
                break;
            default:
                break;
        }
    }

    protected void showTextDialog() {
        TextDialog.getInstance().show();
    }

    int fontSizeOld;
    int colorOld = -1000;

    protected void createAndSetBitmap(DrawingSurface drawingSurface) {

        int maxSize = 40;
        int minSize = 28;
        if (fontSize == 0 && colorOld == -1000) {
            fontSize = minSize;
        }

        if (fontSizeOld == 0 && colorOld == -1000) {
            fontSizeOld = (int) (minSize * MainActivity.sAddSizeText / 3.0f);
        }
        Log.i("TAG11", fontSize + "//aaaaaaaaa");

        try {
            if (textLabel.equals("")) {
                textLabel = "Click to edit TEXT";
            }
        } catch (Exception ee) {
            textLabel = "Click to edit TEXT";
        }
        if ((int) (fontSize * MainActivity.sAddSizeText / 3.0f) <= (int) (maxSize * MainActivity.sAddSizeText / 3.0f)) {
            fontSize = (int) (fontSize * MainActivity.sAddSizeText / 3.0f);
        }

        if (fontSize > (int) (maxSize * MainActivity.sAddSizeText / 3.0f)) {
            fontSize = (int) (minSize * MainActivity.sAddSizeText / 3.0f);
        }

        Paint rawPaint = getDrawPaint();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.rgb(61, 61, 61));
        if (fontSize == 0 && colorOld == -1000) {
            colorOld = rawPaint.getColor();
        }
        float sizeAdd = 5 * MainActivity.sAddSizeText / 3.0f;
        // text size in pixels
        paint.setTextSize((int) (fontSize) + sizeAdd);
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, rawPaint.getColor());
        mBoxHeight = heightText(fontSize + sizeAdd);
        mBoxWidth = widthText(textLabel, fontSize + sizeAdd);
        super.createAndSetBitmap(drawingSurface);
        Bitmap bitmap = Bitmap.createBitmap((int) mBoxWidth + MainActivity.sAddSizeText, (int) mBoxHeight
                + MainActivity.sAddSizeText, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        paint.setColor(rawPaint.getColor());

        if (textLabel == null) {
            textLabel = "Click to edit TEXT";
        }

        canvas.drawText(textLabel, MainActivity.sAddSizeText / 4.0f, mBoxHeight, paint);
       /* if ((int) (fontSize * 1.0f / (fontSizeOld * 1.0f) * BaseToolWithRectangleShape.widthScale) != 0) {
            mDrawingBitmap = Bitmap.createScaledBitmap(bitmap, (int) (fontSize * 1.0f / (fontSizeOld * 1.0f) * BaseToolWithRectangleShape.widthScale),
                    (int) (fontSize * 1.0f / (fontSizeOld * 1.0f) * BaseToolWithRectangleShape.heightScale), false);
        } else {

        }*/
        if ((int) (BaseToolWithRectangleShape.widthScale) != 0) {
            mBoxWidth = (fontSize * 1.0f) / (fontSizeOld * 1.0f) * BaseToolWithRectangleShape.widthScale;
            mBoxHeight = (fontSize * 1.0f) / (fontSizeOld * 1.0f) * BaseToolWithRectangleShape.heightScale;
            BaseToolWithRectangleShape.widthScale = mBoxWidth;
            BaseToolWithRectangleShape.heightScale = mBoxHeight;
        }

        mDrawingBitmap = bitmap;
        fontSizeOld = fontSize;

    }

    private float widthText(String text, float size) {
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(Color.rgb(61, 61, 61));
        Rect bounds = new Rect();
        p.setTextSize(size);
        p.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

    private float heightText(float size) {
        Paint p = new Paint();
        Rect bounds = new Rect();
        p.setTextSize(size);
        p.getTextBounds("Bag", 0, ("Bag").length(), bounds);
        return bounds.height();
    }

}
